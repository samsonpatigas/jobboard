<?php 

escape($_POST);
$job = new Job();

if (REMOTE_PORTAL == 'deactivated' && isset($location_select))
  $city_id = intval($location_select);
else
  $city_id = 1;

$apply_online = (isset($apply_online_switch)) ? 1 : 0;

$HOW_TO_APPLY = $db->getConnection()->real_escape_string($_POST['howtoapply']);

if (strpos($HOW_TO_APPLY, "http://") !== false)
  $HOW_TO_APPLY = substr($HOW_TO_APPLY, 7, strlen($HOW_TO_APPLY) - 1);
else if (strpos($HOW_TO_APPLY, "https://") !== false)
  $HOW_TO_APPLY = substr($HOW_TO_APPLY, 8, strlen($HOW_TO_APPLY) - 1);
else if (strpos($HOW_TO_APPLY, "www.") !== false)
  $HOW_TO_APPLY = substr($HOW_TO_APPLY, 4, strlen($HOW_TO_APPLY) - 1);
  
$data = array(
  "type_id" => intval($type_select),
  "job_id" => intval($job_id),
  "category_id" => intval($cat_select),
  "title" => $title,
  "salary" => $salary,
  'f9_date_posted' => $f9_date_posted,
  'f9_title' => $f9_title,
  'f9_position' => $f9_position,
  'f9_city' => $f9_city,
  'f9_state' => $f9_state,
  'f9_zip' => $f9_zip,
  'f9_salary_range' => $mSalary,
  'f9_yrs_of_experience' => $f9_yrs_of_experience,
  'f9_language' => $f9_language,
  'f9_gender' => $f9_gender,
  'f9_specialties' => $f9_specialties,
  'f9_short_description' => $_POST['description'],
  'f9_post_peroid' => $f9_post_peroid,
  'f9_service_type' => $f9_service_type,
  'f9_admin_notes' => $db->getConnection()->real_escape_string($_POST['f9_admin_notes']),
  "description" => $db->getConnection()->real_escape_string($_POST['description']), //keed the tinymce characters
  "city_id" => $city_id,
  "apply_online" => $apply_online,
  "apply_desc" => $HOW_TO_APPLY,
  "expires" => $expires
);
// var_dump($data); die();
$result = $job->Edit($data);

//substract employer account info
if ($result) {
  //show success
  $smarty->assign('msg', $translations['dashboard_recruiter']['editjob_success']);
  $smarty->assign('view', 'success.tpl');
} else {
  //failure
  $smarty->assign('msg', $translations['dashboard_recruiter']['general_err']);
  $smarty->assign('view', 'error.tpl');
}

//mail if job_type is concierge else credit card payment is prompted
if($f9_service_type == 'Concierge Service'){
  $job_type_mail = new Mailer();
  $job_type_mail->concierge('http://jobboard.f9portal.net/dashboard/edit-job/' . $job_id);  
}else{
  //check if this job is expired
  $d1 = date_create($expires);
  $d2 = date_create(date("Y/m/d"));
  $ddf = date_diff($d1, $d2);
  if ($ddf->format('%a') > 0) {
    $smarty->assign('view', 'credit_card.tpl');
  }else{
    $smarty->assign('view', 'success.tpl');
  } 
}
$template = 'dashboard/dashboard.tpl';
?>