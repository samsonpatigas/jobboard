<?php

	$cats = get_categories();
	$subscriptions = get_subscriptions($_GET['email']);

	$s = explode("," , $subscriptions['categories']);

	$number_of_categories =  count($cats);

	// foreach ($cats as $c1){
	//	if(in_array($c1['id'], $s)){
	//		$c1['ch'] = 'checked';
	//	}else{
	//	 	$c1['ch'] = '';
	//	}		
	// }

	for ($x=0; $x<=$number_of_categories; $x++){
		if(in_array($cats[$x]['id'], $s)){
			$cats[$x]['ch'] = 'checked';
		}else{
		 	$c1[$x]['ch'] = '';
		}	
	}
	

	if (!empty($_POST))
	{
		$cats_final = "";
		$numItems = count($cats);
		$i = 0;
		foreach ($cats as $c) {
			if (isset($_POST['cat' . $c['id']])) {
				if(++$i === $numItems) {
				    $cats_final .= $c['id'];
				} else 
					$cats_final .= $c['id'] . ',';
			}

		}
		//single element caused troubles
		if (substr($cats_final, -1) == ',') {
			$cats_final = rtrim($cats_final, ",");
		}

		if (!empty($cats_final)) {
			//get email
			$email = filter_var($_POST['subscribe-email'], FILTER_VALIDATE_EMAIL);

			//update user and send him confirmation
			$class = new Subscriber($email, $cats_final, true, true);
			//return success
		}
		$smarty->assign('success',true);

	}

	$smarty->assign('cats', $cats);
	if($_GET['email']==null){
		$template = 'subscription/subscribe.tpl';
		header("Location: http://jobboard.f9portal.net/profile/edit"); 
		exit; 
	}else{
		$template = 'subscription/subscribe_popup.tpl';
	}

?>
