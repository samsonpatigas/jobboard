<?php

  $smarty->assign('PLAIN_SITE_REGISTER', true);

  $captcha_enabled = ENABLE_RECAPTCHA;
  if ($captcha_enabled) {
    $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";
    $token  = ""; 
  }
  $smarty->assign('ENABLE_RECAPTCHA', $captcha_enabled);
  // echo '1'; die();

  if (isset($_POST['step'])) {
    //echo '2'; die();
    if ($_POST['step'] == "1") {
      //echo '3'; die();
        if ($captcha_enabled) {
          if (!strcmp(sha1($_POST['captcha']), $_SESSION['encryptedToken']) == 0) {
            $smarty->assign('response_msg', $translations['contact']['captcha_err']);
            $smarty->assign('captcha_err', true);
            //generate new captcha and destroy old one
            unset($_SESSION['encryptedToken']);
            for( $i = 1; $i <= 6 ; $i++ )
            {
              $token .= $pattern{rand( 0, 35 )};
            }
            $encryptedToken = sha1( $token );

            // save the $encryptedToken in the session
            $_SESSION['encryptedToken'] = $encryptedToken;
            $rawCaptchaBytes =  base64_encode(simple_php_captcha($token));

            $the_captcha = str_replace('{RAW}', $rawCaptchaBytes, recaptcha_get_html());
            $the_captcha = str_replace('{CAPTCHA_LABEL}', $translations['captcha']['captcha_label'], $the_captcha);
            $smarty->assign('captcha_html', $the_captcha);
            $template = 'auth/register-recruiters.tpl';
            return;
          }

        }

        //create employer, show second step form
        $class = new Employer();
        //check location/IP address. From one IP address we dont want multiple accounts
        $ip = get_client_ip();

        $name = $db->getConnection()->real_escape_string(trim($_POST['register_name']));
        $email = $db->getConnection()->real_escape_string(trim($_POST['register_email']));
        $password = md5(trim($_POST['register_pass1']));

        $confirmed = EMAIL_CONFIRMATION_FLAG; //by default confirmed. This option can be turned on in security settings
        
        $data = array('name' => $name, 'email' => $email, 'password' => $password,
        'confirmed' => $confirmed, 'ip' => $ip);

        //enter employer and his hash data
        $hash = sha1($email . time());
        $exists = $class->checkIfUserExists($email);
        //create entry if this email doesnt exist yet but check ip
          // if (ENABLE_SINGLE_ACCOUNT_REGISTRATION == 'enabled') {
          //    if (!$exists) {
          //      //check if user has not some other account!
          //      if ($class->userIpIsAlreadyRegistered($ip)) {
          //        $em = $class->getEmail();
          //        $msg = str_replace('{email}', $em, $translations['registration']['ip_multiple_err']);
          //        $smarty->assign('msg', $msg);
          //        $template = 'err/ip-already-registered.tpl';
          //        return;
          //      } 
          //    }
          // }

          // update already existing employer
          if ($class->checkIfEmailExists($email)) {
            $class->updateEntry($data, $hash);
            $smarty->assign('updatingEntry', true);
          } else {
            // if does not exist - create new entry 
            $class->createEntry($data, $hash); 
            $smarty->assign('updatingEntry', false);
          }

          //asign id of employer to the template
          $smarty->assign('second_step', true);
          $smarty->assign('employer_id', $class->getId());
          $smarty->assign('employer_hash', $hash);
          $smarty->assign('employer_email', $email);
          $smarty->assign('f9_job_app_email_address',$_POST['register_email']);
          $template = 'auth/register-recruiters.tpl';

      } else if ($_POST['step'] == "2") {
        //echo '4'; die();

        //ADDED 4/4/19 FIXING MERGING OF STEP 1 and 2

        //create employer, show second step form
        $class = new Employer();
        //check location/IP address. From one IP address we dont want multiple accounts
        $ip = get_client_ip();

        $name = $db->getConnection()->real_escape_string(trim($_POST['register_name']));
        $email = $db->getConnection()->real_escape_string(trim($_POST['f9_practice_email_address']));
        $password = md5(trim($_POST['register_pass1']));

        $confirmed = EMAIL_CONFIRMATION_FLAG; //by default confirmed. This option can be turned on in security settings
        
        $data = array('name' => $name, 'email' => $email, 'password' => $password,
        'confirmed' => $confirmed, 'ip' => $ip);

        //enter employer and his hash data
        $hash = sha1($email . time());
        $exists = $class->checkIfUserExists($email);
        //create entry if this email doesnt exist yet but check ip
          // if (ENABLE_SINGLE_ACCOUNT_REGISTRATION == 'enabled') {
          //    if (!$exists) {
          //      //check if user has not some other account!
          //      if ($class->userIpIsAlreadyRegistered($ip)) {
          //        $em = $class->getEmail();
          //        $msg = str_replace('{email}', $em, $translations['registration']['ip_multiple_err']);
          //        $smarty->assign('msg', $msg);
          //        $template = 'err/ip-already-registered.tpl';
          //        return;
          //      } 
          //    }
          // }

          // update already existing employer
          if ($class->checkIfEmailExists($email)) {
            $class->updateEntry($data, $hash);
            $smarty->assign('updatingEntry', true);
          } else {
            // if does not exist - create new entry 
            $class->createEntry($data, $hash); 
            $smarty->assign('updatingEntry', false);
          }

          //asign id of employer to the template

          $smarty->assign('employer_hash', $hash);

        //END 

        $email = $_POST['f9_practice_email_address'];
        $smarty->assign('employer_id', $class->getId());
        $smarty->assign('employer_email', $email);
        $smarty->assign('f9_job_app_email_address',$_POST['f9_practice_email_address']);
        $employer_email = $email;
        // var_dump($email); die();

        if (!empty($_FILES["company_logo"]['tmp_name'])) {
          $newNamePrefix = time() . '_';
          $manipulator = new ImageManipulator($_FILES['company_logo']['tmp_name']);

          $size = getimagesize($_FILES["company_logo"]['tmp_name']);
          $fileType = $size[2];
          if ($size[0] > 200) {
            //needs a resize
                $newImage = $manipulator->resample(200, 200);           
          }
          $final_path = COMPANIES_UPLOAD_FOLDER . $newNamePrefix . $_FILES['company_logo']['name'];
          $manipulator->save($final_path, $fileType);
        } else {
          $final_path = DEFAULT_LOGO_PATH;
        }

        //also if error use default image
        if (!empty($_FILES['company_logo']['error'])) {
          $final_path = DEFAULT_LOGO_PATH;
        }

        $employerClass = new Employer();
        //get employer id, get his mail, send confirmation mail, create company entry
        escape($_POST);
        $EMPLOYER_ID = $class->getId();
        $company = new Company();

        //ADDED

        $f9_practice_name = $_POST['register_name'];
        $f9_practice_phone_number = $_POST['f9_practice_phone_number'];

        //END
        
        if (isset($employerid)) {  
            $data = array('company_name' => $company_name,
              'f9_practice_name' => $f9_practice_name,
              'f9_address_1' => $f9_address_1,
              'f9_address_2' => $f9_address_2,
              'f9_city' => $f9_city,
              'f9_state' => $f9_state,
              'f9_zip' => $f9_zip,
              'f9_practice_phone_number' => $f9_practice_phone_number,
              'f9_cellphone_number' => $f9_cellphone_number,
              'f9_cellphone_provider' => $f9_cellphone_provider,
              'f9_practice_email_address' => $f9_practice_email_address,
              'f9_job_app_email_address' => $f9_practice_email_address,
              'f9_notification_preference' => $f9_notification_preference,
              'f9_practice_type' => implode(',' ,$f9_practice_type),
              'company_desc' => $company_desc, 
              'company_hq' => $company_hq,  'company_url' => $company_url, 
              'employer_id' => $EMPLOYER_ID, 'logo_path' => $final_path, 'company_street' => $company_street,
              'company_citypostcode' => $company_citypostcode,
              'f9_fname' => $f9_fname,
              'f9_lname' => $f9_lname,
              'f9_bizname' => $f9_bizname,
              'f9_suite' => $f9_suite,
              'f9_cross_st' => $f9_cross_st,
              'f9_office_software' => implode(',' , $f9_office_software),
              'f9_note' => $f9_notes

            );

            
            $num_contacts = count($_POST['c_contact_name']);
            for($x = 0; $x<$num_contacts; $x++){
              if (empty($_POST['c_contact_name'][$x])) {
                break;
              }
              $data_contacts = array(
                'contact_name' => $_POST['c_contact_name'][$x],
                'phone_number' => $_POST['c_phone_number'][$x],
                'phone_type' => $_POST['c_phone_type'][$x],
                'email_address' => $_POST['c_email_address'][$x],
                'email_type' => $_POST['c_email_type'][$x]
              );
              $company->save_contact($data_contacts, $EMPLOYER_ID);
              unset($data_contacts);
            }
        }

        //ADDED 4/4/19

        $eemail = $_POST['f9_practice_email_address'];

        // //enter employer and his hash data
        // $client_hash = sha1($employer_email . time());

        //END

        // if already exists but is not unconfirmed - update
        if ($updatingEntry) {
          $company->updateEntry($data); 
        } else {
          // if does not exist - create new entry
          //var_dump($data); die();
          $company->createCompany($data);
        }


        // $eemail = $_POST['f9_practice_email_address'];

        // var_dump($eemail); die();
        if (intval(EMAIL_CONFIRMATION_FLAG) == 0) {

          $mailer = new Mailer();
          $result = $mailer->sendConfirmationEmail($eemail, $hash);
          
          $employer_mail = new Employer();
          $employer_mail->save_email_log(array('message'=>'New Client Registration'),array('client_id'=>$EMPLOYER_ID),$eemail);

          if (!$result) {
            require_once 'page_more_content.php';
            $msg = str_replace('{EMAIL}', $email, $translations['website_general']['email_not_sent']);
            $smarty->assign('error_msg', $msg);
            $template = 'err/err-general.tpl';
            return;
          }   
        }

        $confirm_msg = str_replace('{email}', $eemail, $translations['registration']['registration_done_message']);
        $smarty->assign('msg', $confirm_msg);

        $_SESSION['user'] = $EMPLOYER_ID;
        redirect_to(BASE_URL . dashboard);
        
      } 
  } else {

    if ($captcha_enabled) {
      for( $i = 1; $i <= 6 ; $i++ ) {
        $token .= $pattern{rand( 0, 35 )};
      }
      $encryptedToken = sha1( $token );

      // save the $encryptedToken in the session
      $_SESSION['encryptedToken'] = $encryptedToken;
      $rawCaptchaBytes =  base64_encode(simple_php_captcha($token));

      $the_captcha = str_replace('{RAW}', $rawCaptchaBytes, recaptcha_get_html());
      $the_captcha = str_replace('{CAPTCHA_LABEL}', $translations['captcha']['captcha_label'], $the_captcha);
      $smarty->assign('captcha_html', $the_captcha);
    }
    $smarty->assign('ENABLE_RECAPTCHA', $captcha_enabled);

    //first initial open
    $template = 'auth/register-recruiters.tpl';
  }
  
?>
