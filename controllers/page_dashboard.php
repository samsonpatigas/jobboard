<?php 
  $smarty->assign('dashboard_footer_flag', 1);

  $DIR_CONST = '';
  if (defined('__DIR__'))
    $DIR_CONST = __DIR__;
  else
    $DIR_CONST = dirname(__FILE__);

  //is user confirmed?
  $class = new Employer();
  $result = $class->userIsConfirmed($_SESSION['user']);
  $smarty->assign('remote_portal', REMOTE_PORTAL);

  if (!$result) {
    if (isset($_SESSION['user'])) {
      unset($_SESSION['user']);
      unset($_SESSION['name']);
    }   
    redirect_to(BASE_URL . URL_ACCOUNT_NOT_CONFIRMED);
  } else {

    if (defined('PAYPAL_PLUGIN')) {
      try {
        include($DIR_CONST . '../../plugins/Paypal/invoice_notifications.php');
      } catch (Exception $e) {}
    }

    //check for notification
    $result = $class->checkForNotification($_SESSION['user']);
    if ($result)
      $smarty->assign('NOTIFICATION', true);
    else
      $smarty->assign('NOTIFICATION', false);

    if (isset($_SESSION['TMP_JOB_ID']) && $extra != "draft" && $extra != "2") {
      $tmpJob = new Job($_SESSION['TMP_JOB_ID']);
      $tmpJob->deleteById($_SESSION['TMP_JOB_ID']);
      unset($_SESSION['TMP_JOB_ID']);
      $_SESSION['TMP_JOB_ID'] = null;
    }

    $smarty->assign('name', $_SESSION['name']);
    $smarty->assign('ID', $_SESSION['user']); //current client id
    //here, switch dashboard page
    $smarty->assign('project_name', SITE_NAME);
    //$smarty->assign('EMAIL', $class->getEmployerEmail($_SESSION['user']));

    // var_dump($DASHBOARD_ROUTING);die();
    

    $banned = new Applicant();

    $banned_data = $banned->company_applicants($_SESSION['user']);    
    
    //does view exist?
    if (!empty($DASHBOARD_ROUTING[$id])) {
      $smarty->assign('banned_data', $banned_data);

      $path = $DIR_CONST . '/../_tpl/' . $settings['theme'] . '/' . 'dashboard' . '/views/' . $DASHBOARD_ROUTING[$id];

      if (!file_exists($path)) {
        $smarty->assign('view', 'dashboard-404.tpl');
      } else {
        $smarty->assign('view', $DASHBOARD_ROUTING[$id]);   
        if ($id == URL_DASHBOARD_SETTINGS)
          $smarty->assign('JS_ID', 'settings-li');
        else if ($id == URL_DASHBOARD_EDIT_COMPANY) {
          $class = new Company();
          $data = $class->getCompanyByEmployerId($_SESSION['user']);
          if (empty($data)) {
            //registration failure. user does not have a company associate with him
            //unconfirm him and log him out
            $class->unconfirmAndLogout($_SESSION['user']);
          } else {
            $smarty->assign('company', $data);
          }
          $smarty->assign('JS_ID', 'company-li');
        }
        else if ($id == URL_DASHBOARD_DEACTIVATE_ACCOUNT) {
          $smarty->assign('JS_ID', 'account-li');

        } else if ($id == URL_DASHBOARD_INVOICES) {

          if (defined('PAYPAL_PLUGIN')) {
            try {
              include($DIR_CONST . '../../plugins/Paypal/invoices.php');
            } catch (Exception $e) {}
          }
        }
        else if ($id == URL_DASHBOARD_OVERVIEW) {
          $overview = new Employer();
          $data = $overview->getEmployerOverview($_SESSION['user']);
          $news = $overview->getNews();
          $smarty->assign('news', $news);

          $smarty->assign('overview_data', $data);
          $smarty->assign('JS_ID', 'dashboard-li');
        } else if ($id == URL_DASHBOARD_APPLICATIONS){

          $smarty->assign('applications_modal_init', '1');
          $class->notificationSeen($_SESSION['user']);

          //get jobs
          $apps_job = new Job();
          $data = $apps_job->getJobTitlesByEmployerId($_SESSION['user']);
          $interested = $apps_job->getJobUnderReviewId($_SESSION['user']);
          $smarty->assign('APP_PAGE', 1);
          $smarty->assign('jobs_select', $data);
          $smarty->assign('interested', $interested);
          
          $smarty->assign('JS_ID', 'match-li');
        } 
        else if ($id == URL_DASHBOARD_SEARCHABLE){

          $smarty->assign('applications_modal_init', '1');
          $class->notificationSeen($_SESSION['user']);

          //get jobs
          $apps_job = new Job();
          $data = $apps_job->getJobTitlesByEmployerId($_SESSION['user']);
          $interested = $apps_job->getJobUnderReviewId($_SESSION['user']);

          $smarty->assign('APP_PAGE', 1);
          $smarty->assign('jobs_select', $data);
          $smarty->assign('interested', $interested);
          
          $smarty->assign('JS_ID', 'match-li');

        }
        //ENDPOINT
        else if ($id == URL_DASHBOARD_STATISTICS) {
          $job = new Job();
          $params = explode("-", $last);
          $views = $params[0];
          $applications = $params[1];

          if (intval($views) != 0)
            $conversion_rate = round(($applications/$views)*100, 2);
          else 
            $conversion_rate = 0;

          $smarty->assign('conversion_rate', round($conversion_rate));
          $conversion_title = str_replace("{RATE}", round($conversion_rate), $translations['statistics']['gauge_title']);
          $smarty->assign('conversion_title', $conversion_title);

          $smarty->assign('barchart_headline', $translations['statistics']['barchart_headline']);
          $smarty->assign('barchart_x_title', $translations['statistics']['barchart_x_title']);
          $smarty->assign('barchart_y_title', $translations['statistics']['barchart_y_title']);

          $smarty->assign('views', $views);
          $smarty->assign('applications', $applications);
          $smarty->assign('graph_headline', $translations['statistics']['piechart_title']);

          $stats = $job->getJobStatisticsById($extra);

          if (count($stats) < 2)
            $stats = array();

          $smarty->assign('stats', $stats);
          //prepare data for javascript
          $statsDate = array();
          foreach ($stats as $stat) {
            $exp = explode("-", $stat['date']);
            $obj = new stdClass;
            $obj->year = $exp[0];
            $obj->month = ltrim($exp[1], "0");
            $obj->day = ltrim($exp[2], "0");
            $statsDate[$stat['id']] = $obj;
          }

          //for small data do not need to strech graph to 100%
          if (count($stats) <= 3)
            $smarty->assign('firstGraphWidth', "75%");
          else
            $smarty->assign('firstGraphWidth', "100%");

          $smarty->assign('statsDate', $statsDate);
          
        }
        else if ($id == URL_DASHBOARD_JOBS) {
          $smarty->assign('init_modal_popup_jobs', '1');
          //get list of jobs
          $dash_job = new Job();
          $app = new JobApplication();

          $dash_jobs = $dash_job->getEmployerJobs($_SESSION['user']);
          $apps_array = array();

          //get job applications count
          foreach ($dash_jobs as $j) {
            $apps_array[$j['id']] = $app->getJobApplicationsCount($j['id']);
          }
          $smarty->assign('apps_array', $apps_array);
          $smarty->assign('dash_jobs', $dash_jobs);
        }
        else if ($id == URL_DASHBOARD_PREVIEW) {
          $job = new Job();
          $preview_job = $job->previewJob($extra);
          // var_dump($preview_job);die();
          $smarty->assign('job', $preview_job);
          $template = 'dashboard/view/preview-job.tpl';
        }
        else if ($id == URL_DASHBOARD_MATCHED_APPLICANTS) { // sam
          // list jobs that have applicants that matches to it
          $job = new Job();
          $active_jobs = $job->getJobTitlesByEmployerId($_SESSION['user']);
          
          $smarty->assign('applications_modal_init', '1');
          $class->notificationSeen($_SESSION['user']);

          //get jobs
          $data = $job->getJobTitlesByEmployerId($_SESSION['user']);

          
          if (isset($_SESSION['JOB_ID']))
            $smarty->assign('select_job_id', $_SESSION['JOB_ID']);

          /*global $db;
          $sql = 'SELECT DISTINCT a.fullname FROM '.DB_PREFIX.' applicant as a LEFT JOIN '.DB_PREFIX.' job_applications as b
          ON a.id = b.applicant_id ORDER BY fullname ASC';
          $result = $db->query($sql);

          $smarty->assign('all_candidates', $result);

          $smarty->assign('APP_PAGE', 1);
          $smarty->assign('jobs_select', $data);

          $template = 'dashboard/view/applications.tpl';*/
          
          /*
          $applicants = new Applicant();
          $active_job_count = count($active_jobs);
          foreach ($active_jobs as $key=>$active_job) {
            // var_dump($key);
            $matched_jobs[$active_job] = $applicants->getApplicantsByPosition($key);            
          }*/

          //abganzon
          /*global $db;
          $searchval = $_POST['search_match'];
          $search_btn = $_POST['job_select'];
          $btn_clear = $_POST['btn_clear'];*/
          
          /*$sql = 'SELECT DISTINCT a.f9_photo, a.f9_first_name, a.f9_last_name, a.phone , a.f9_country, a.f9_category, c.name FROM '.DB_PREFIX.' applicant as a 
            LEFT JOIN jobs as b ON a.f9_category = b.category_id
            LEFT JOIN categories as c ON c.id = b.category_id
            WHERE a.f9_category = b.category_id ORDER BY a.f9_first_name';

            $response = $db->query($sql);

            $smarty->assign('active_jobs', $active_jobs);
            $smarty->assign('matched_jobs', $matched_jobs);
            $smarty->assign('data',$response);

            $template = 'dashboard/view/matched-applicants.tpl';
            
          if(isset($search_btn)){
                $sql = 'SELECT DISTINCT a.f9_photo, a.f9_first_name, a.f9_last_name, a.phone , a.f9_country, a.f9_category, c.name FROM '.DB_PREFIX.' applicant as a 
                LEFT JOIN jobs as b ON a.f9_category = b.category_id
                LEFT JOIN categories as c ON c.id = b.category_id
                WHERE a.f9_category = b.category_id AND c.name ="'.$searchval.'" ORDER BY a.f9_first_name';
              $response = $db->query($sql);

              if(!empty($response->num_rows)){
                $smarty->assign('active_jobs', $active_jobs);
                $smarty->assign('matched_jobs', $matched_jobs);
                $smarty->assign('data',$response);

              $template = 'dashboard/view/matched-applicants.tpl';
              }
              else{
                $smarty->assign('error','No applicant match');
                $template = 'dashboard/view/matched-applicants.tpl';
            }

          }
          if(isset($btn_clear)){
            $sql = 'SELECT DISTINCT a.f9_photo, a.f9_first_name, a.f9_last_name, a.phone , a.f9_country, a.f9_category, c.name FROM '.DB_PREFIX.' applicant as a 
            LEFT JOIN jobs as b ON a.f9_category = b.category_id
            LEFT JOIN categories as c ON c.id = b.category_id
            WHERE a.f9_category = b.category_id ORDER BY a.f9_first_name';

            $response = $db->query($sql);

            $smarty->assign('active_jobs', $active_jobs);
            $smarty->assign('matched_jobs', $matched_jobs);
            $smarty->assign('data',$response);

            $template = 'dashboard/view/applications.tpl';
          }*/

        }
        else if ($id == URL_DASHBOARD_DELETE_JOB) {
          $job = new Job();
          $job->deleteById($extra);
          clear_main_cache();
          redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_JOBS);
        }
        else if ($id == URL_DASHBOARD_DEACTIVATE_MODAL){
          $job = new Job();
          $job->Deactivate($extra);
          redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_JOBS);
        }
        else if ($id == URL_DASHBOARD_RENEW_MODAL){
          $job = new Job();
          $job->Renew($extra);
          //var_dump($extra); die();

          redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_JOBS);

          /*global $db;

          $sql = 'SELECT a.company, b.name as "category_name", c.name as "job_type", d.name as "location_asci", 
          a.f9_date_posted, a.f9_post_peroid as "f9_date_post_peroid", a.f9_gender, a.f9_language, a.f9_position, a.f9_specialties,
          a.f9_city, a.f9_state, a.f9_zip, a.description, a.f9_admin_notes
          FROM '.DB_PREFIX.' jobs as a 
          LEFT JOIN categories as b ON a.category_id = b.id
          LEFT JOIN types as c ON a.type_id = c.id
          LEFT JOIN cities as d ON a.city_id = d.id
          WHERE a.id = ' .$extra;
          $result = $db->query($sql);

          $smarty->assign('job_data', $result);
          redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_POST . '/2');*/
        }
        else if ($id == URL_DASHBOARD_EDIT_JOB) {
          //does this job belongs to employer?
          $job = new Job($extra);
          $result = $job->checkOwner($_SESSION['user']);
          if (!$result)
            redirect_to("/" . URL_DASHBOARD . "/" . URL_DASHBOARD_JOBS);

          $data = $job->GetInfo();
          $smarty->assign('job', $data);
          $cats = $job->getCategoriesWithIds();
          $job_types = $job->getJobTypesWithIds();
          if (REMOTE_PORTAL == 'deactivated') {
            //get cities
            $cities = $job->getCitiesWithId();
            $smarty->assign('cities', $cities);
          }
          $smarty->assign('cats', $cats);
          $smarty->assign('types', $job_types);
        }
        else if ($id == URL_DASHBOARD_CVDATABASE) {
        
          $smarty->assign('TAGL_RESTYLE', true);

          if (isset($extra) && intval($extra) == 2){
            $smarty->assign('view', 'cvdatabase-unlock.tpl');  
          }

          if (defined('PROFILES_PLUGIN')) {
            try {
              include($DIR_CONST . '../../plugins/Profiles/cv_database.php');
            } catch (Exception $e) {}
          }
        }
        else if ($id == URL_DASHBOARD_ACCOUNT) {

          if (PAYMENT_MODE != '3') {
            redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_OVERVIEW);
          }

          $package_data = $class->getEmployerAccount($_SESSION['user']);
          $no_resources_msg = false;

          // show out of credit msg
          if (intval($package_data['jobs_left']) < 1 && intval($package_data['cv_downloads_left']) < 1) {
              $no_resources_msg = $translations['post_step1']['out_of_credit'];
          }

          // wording
          if (intval($package_data['jobs_left']) == 0 || intval($package_data['jobs_left']) > 1) {
            $suffix = $translations['post_step1']['jobs_left'];
          } else {
            $suffix = $translations['post_step1']['job_left'];
          }

          if (intval($package_data['cv_downloads_left']) == 0 || intval($package_data['cv_downloads_left']) > 1) {
            $suf = $translations['post_step1']['cvs_left'];
          } else {
            $suf = $translations['post_step1']['cv_left'];
          }

          $smarty->assign('no_resources_msg', $no_resources_msg);

          $smarty->assign('jobs_left_msg', rtrim($suffix, "."));
          $smarty->assign('cvs_left_msg', rtrim($suf, "."));

          $smarty->assign('acc_data', $package_data);
          $smarty->assign('JS_ID', 'account-li');

        } else if ($id == URL_DASHBOARD_ACCOUNT_ORDER) {

          if (isset($extra) && $extra == URL_DASHBOARD_ACCOUNT_ORDER_COMPLETED) {
            $smarty->assign('order_completed', true);
          } else {
              $sql = 'SELECT id, name, jobs_left, job_period, cv_downloads, price FROM packages';
              $result = $db->query($sql);
              $data = array();

              while ($row = $result->fetch_assoc()) {
                $data[$row['id']] = $row['name'];
              }

              $package_data = $class->getEmployerAccount($_SESSION['user']);

              $sql = 'SELECT id, name, jobs_left, job_period, cv_downloads, price FROM packages WHERE id =' . intval($package_data['package_id']);
              $result = $db->query($sql);
              $row = $result->fetch_assoc();

              $price = intval($row['price']);

              if (VAT_KOEFICIENT != 0) {
                $smarty->assign('VAT', true);
                $price_vat_total = format_currency(WEBSITE_CURRENCY, $price + ($price * VAT_KOEFICIENT));
                $price_vat = format_currency(WEBSITE_CURRENCY, $price * VAT_KOEFICIENT);

                $smarty->assign('PRICE_VAT_TOTAL', $price_vat_total);
                $smarty->assign('PRICE_VAT', $price_vat);

              } 

              $smarty->assign('PRICE', format_currency(WEBSITE_CURRENCY, $price));

              $smarty->assign('packages', $data);
              $smarty->assign('active_mode_id', $package_data['package_id']);
              $smarty->assign('pd', $row);  
          }

          $smarty->assign('JS_ID', 'account-li');
        }
        else if ($id == URL_DASHBOARD_POST) {
            if ($extra != "" && $extra != "draft") {
              $step = intval($extra);
              if (!is_numeric($extra) || $step < 1 || $step > 3 || $extra == "0")
                redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_POST);
              else
                $step = intval($extra);
            } else {
              $step = 1;

              if ($extra == "draft") {
                $tmpJob = new Job($_SESSION['JOB_EDITED_ID']);
                $draft_data = $tmpJob->GetInfo();
                $tmpJob->deleteById($_SESSION['JOB_EDITED_ID']);
                unset($_SESSION['JOB_EDITED_ID']);
                $_SESSION['JOB_EDITED_ID'] = null;
                $smarty->assign('draft_data', $draft_data);
                $smarty->assign('DRAFT', true);
              }
            }

            $job = new Job();
            $package_data = $class->getEmployerAccount($_SESSION['user']);
            $smarty->assign('job_period', $package_data['job_period']);

            if (PAYMENT_MODE == "3") {
              $smarty->assign('jobs_left', $package_data['jobs_left']);
            }

            // ***************** STEP 1 ============================
            if ($step == 1) {
              //show user how many jobs he has left and when does the job expire
              $cats = $job->getCategoriesWithIds();
              $job_types = $job->getJobTypesWithIds();

              if (PAYMENT_MODE == "3") {

                if (intval($package_data['jobs_left']) < 1) {
                  $msg = $translations['post_step1']['nojobs_left_msg'];
                  $smarty->assign('lock_post', true);
                }
                else {

                  if (intval($package_data['jobs_left']) == 0 || intval($package_data['jobs_left']) > 1) {
                    $suffix = $translations['post_step1']['jobs_left'];
                  } else {
                    $suffix = $translations['post_step1']['job_left'];
                  }

                  $msg = $translations['post_step1']['account_plan'] . " " . $package_data['jobs_left'] . " " . $suffix;
                } 
                $smarty->assign('msg', $msg);
              }

              if (REMOTE_PORTAL == 'deactivated') {
                //get cities
                $cities = $job->getCitiesWithId();
                $smarty->assign('cities', $cities);
              }

              $smarty->assign('cats', $cats);
              $smarty->assign('types', $job_types);
              //does this user have any tmp jobs? if yes he clicked back

            } else if ($step == 2) {
              $smarty->assign('init_modal_popup_preview', '1');
              // ***************** STEP 2 ============================
              $job_data = $job->getTmpJobInfoByEmployerId($_SESSION['user']);

              $_SESSION['TMP_JOB_ID'] = $job_data["id"];

              $package_data = $class->getEmployerAccount($_SESSION['user']);

              if (intval($package_data['jobs_left']) == 0 || intval($package_data['jobs_left']) > 1) {
                $suffix = $translations['post_step1']['jobs_left'];
              } else {
                $suffix = $translations['post_step1']['job_left'];
              }

              $smarty->assign('jobs_left_msg', $package_data['jobs_left'] . " " . rtrim($suffix, "."));

              if (PAYMENT_MODE == '2'){

                if (intval($job_data["spotlight"]) == 1){
                  $price = intval(PREMIUM_LISTING_PRICE) + intval(JOB_POSTED_PRICE);
                  $smarty->assign('SPOTLIGHT', true);
                }
                else
                  $price = intval(JOB_POSTED_PRICE);

                if (VAT_KOEFICIENT != 0) {
                  $smarty->assign('VAT', true);
                  $price_vat_total = format_currency(WEBSITE_CURRENCY, $price + ($price * VAT_KOEFICIENT));
                  $price_vat = format_currency(WEBSITE_CURRENCY, $price * VAT_KOEFICIENT);

                  $smarty->assign('PRICE_VAT_TOTAL', $price_vat_total);
                  $smarty->assign('PRICE_VAT', $price_vat);

                } 

                $smarty->assign('PRICE', format_currency(WEBSITE_CURRENCY, $price));
                if ($price > 0)
                  $smarty->assign('PAY', true);
                else
                  $smarty->assign('PAY', false);
              }
              else {
                $smarty->assign('PAY', false);
              }

              if (!$job_data)
                redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_POST);

              $smarty->assign('job_data', $job_data);
            } else if ($step == 3) {

              if (isset($_SESSION['STEP_3_VALID'])) {
                unset($_SESSION['STEP_3_VALID']); $_SESSION['STEP_3_VALID'] = null;
                if (JOB_POSTED_IS_ACTIVE == "0")
                  $smarty->assign('published_msg', $translations['dashboard_recruiter']['published_wait_aproval']);
                else
                  $smarty->assign('published_msg', $translations['dashboard_recruiter']['published_online']); 

                if (isset( $_SESSION['PAYPAL_RESULT_MESSAGE'])) {
                  $smarty->assign('paypal_result_message', $_SESSION['PAYPAL_RESULT_MESSAGE']);
                  unset($_SESSION['PAYPAL_RESULT_MESSAGE']); $_SESSION['PAYPAL_RESULT_MESSAGE'] = null;

                  global $db;

                $sql_email = 'SELECT b.created_on, c.email as "From", b.company as "By", b.title, a.fullname, a.email as "To", a.f9_position FROM applicant as a 
                  LEFT JOIN jobs as b ON a.f9_position = b.title
                  LEFT JOIN employer as c ON b.employer_id = c.id
                  WHERE date_sub(CURDATE(), INTERVAL 7 DAY) AND b.is_active = 1';
                  $result_email = $db->query($sql_email);

                  while ($row = $result_email->fetch_assoc()) {
                    $to      .= '<a>' . $row['To'] . ', </a>'; 
                    $subject = $row['title']; 
                    /*$comment = 'Your Profile have match the job entitled '. $row['title'] . ' posted by ' .$row['From'] . ' ';*/
                    $message = '
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8">
                    <title></title>
                    </head>
                    <body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;">
                    <div style="padding:10px; background:#333; font-size:24px; color:#CCC;">
                    <a href="http://www.desertdentalstaffing.com"><img src="http://jobboard.f9portal.net/uploads/logos/main-logo.png" width="36" height="30" alt="yoursitename" style="border:none; float:left;"></a></div>
                    <div style="padding:24px; font-size:17px;">Hello '.$row['fullname'].',<br /><br /> your profile have match the job titled '. $row['title'] . ' publish by ' .$row['By'] . '<br /><br /><a href="jobboard.f9portal.net/login-candidates">Click here to apply http://jobboard.f9portal.net/login-candidates</a><br /><br /></div>
                    </body>
                    </html>';
                  }
                  $To = strip_tags($to);
                  $TextMessage = strip_tags(nl2br($message),"<br>");
                  $Subject = strip_tags($subject);

                  $headers = "MIME-Version: 1.0" . "\r\n";
                  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                  // More headers
                  $headers .= 'From: <desertdentalstaffing@gmail.com>' . "Desert Dental Staffing\r\n";
                  $headers .= 'Cc: desertdentalstaffing@gmail.com' . "\r\n";

                  $today = date('Y-m-d');
                  $datetime1 = new DateTime($today);
                    $datetime2 = date('Y-m-d H:i:s', strtotime('+7 day', $today));;
                    $interval = $datetime2->diff($datetime1);
                    
                    $diff = $interval->format('%a');
                      if($diff == '7'){
                    mail($To, $Subject, $TextMessage, $headers);
                    }

                }

              } else {
                redirect_to(BASE_URL . URL_DASHBOARD . '/' . URL_DASHBOARD_POST);

              }
            }

            $smarty->assign('step', $step);
            $smarty->assign('employer_id', $_SESSION['user']);
        }
      }
     } else {
        if ($DASHBOARD_ROUTING[$id] == 'URL_DASHBOARD_EMAIL_LOGS'){         
          $smarty->assign('view', 'email_logs.tpl');
          exit;
        }
        
        if (($DASHBOARD_ROUTING[$id] == NULL || $DASHBOARD_ROUTING[$id] == "NULL") && $id != 0){
          $smarty->assign('view', 'dashboard-404.tpl');
        } else {
          $overview = new Employer();
          $data = $overview->getEmployerOverview($_SESSION['user']);
          $news = $overview->getNews();
          $smarty->assign('news', $news);
          $smarty->assign('overview_data', $data);

          $smarty->assign('view', 'overview.tpl');
          $smarty->assign('JS_ID', 'dashboard-li');
        }
     }
    $template = 'dashboard/dashboard.tpl';
  }

  
?>
