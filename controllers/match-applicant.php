<?php

global $db;
		
$sql = 'SELECT a.f9_first_name, a.f9_last_name, a.phone , a.f9_country, a.f9_category FROM '.DB_PREFIX.' applicant as a 
LEFT JOIN jobs as b ON a.f9_category = b.category_id WHERE a.f9_category = b.category_id ORDER BY a.f9_first_name';

$response = $db->query($sql);

$data = array();

if ($response) {
  while($row = fetch_assoc($response)) {
    $data[] = $row;
  }
}
echo json_encode(array('data' => $data));

?>
