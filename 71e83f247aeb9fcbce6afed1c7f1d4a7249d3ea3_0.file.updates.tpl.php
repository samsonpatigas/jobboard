<?php
/* Smarty version 3.1.30, created on 2018-08-20 18:51:42
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/updates.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b7affae5c9b44_95572983',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '71e83f247aeb9fcbce6afed1c7f1d4a7249d3ea3' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/updates.tpl',
      1 => 1532431582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5b7affae5c9b44_95572983 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb25">
			<label class="admin-label">
				JOB BOARD UPDATES
			</label>

			 <?php if ($_smarty_tpl->tpl_vars['SJS_PRODUCT']->value == 'free') {?>
				<div class="subheading">You can update your project to a full version with premium plugins, product support, new version notifications and software updates</div>
			 <?php } else { ?>
				<?php if ($_smarty_tpl->tpl_vars['LICENSE_EXISTS']->value == '1') {?>
					<div class="subheading"><?php if ($_smarty_tpl->tpl_vars['VERSION_UPDATE_URL']->value) {?><u>There is a new version of Dental <?php echo $_smarty_tpl->tpl_vars['version_data']->value->new_version;?>
. You can update your job board. </u><?php }
if ($_smarty_tpl->tpl_vars['LS']->value == '3') {?>Your site URL has not been activated yet please contact our team. <?php }
if ($_smarty_tpl->tpl_vars['LS']->value == '1' || $_smarty_tpl->tpl_vars['LS']->value == '3') {?>New version release and software updates notifications are turned ON. To contact our support visit this<a href="https://simplejobscript.com/product-support/" target="_blank"> LINK</a>. <?php }
if ($_smarty_tpl->tpl_vars['LS']->value == '2') {?>Expired licenses are no longer eligible for product support, new version notifications and software updates. <?php }?>Product information, instructions and features roadmap can be found <a href="https://simplejobscript.com/job-board-updates/" target="_blank">HERE</a>.</div>
				<?php } else { ?>
					<div class="subheading">Activate your license in order to receive product support, new version notifications and software updates</a></div>
				<?php }?>
			 <?php }?>

		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="overflow: hidden">
			  
			  <strong><?php echo $_smarty_tpl->tpl_vars['PR_VERSION']->value;?>
</strong>
			  <hr />

			  <?php if ($_smarty_tpl->tpl_vars['SJS_PRODUCT']->value == 'free') {?>

			  	<a href="https://simplejobscript.com/downloads/simplejobscript-complete/" target="_blank"><button type="button" class="mbtn btn btn-default alizarinBtn">Update</button></a>

			  <?php } elseif ($_smarty_tpl->tpl_vars['SJS_PRODUCT']->value == 'full') {?>

			  	<?php if ($_smarty_tpl->tpl_vars['LICENSE_EXISTS']->value == '1') {?>
					 <p>License Key: <strong>&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['LICENSE_KEY']->value;?>
</strong></p>

				  	 <?php if ($_smarty_tpl->tpl_vars['LICENSE_ERR_STATUS']->value == '1') {?>
				  	 	<span class="alizarin"><strong><?php echo $_smarty_tpl->tpl_vars['LICENSE_ERR']->value;?>
</strong></span>
				  	 <?php } else { ?>

					 	 <p>Status: <strong>&nbsp;&nbsp;&nbsp;<?php if ($_smarty_tpl->tpl_vars['LS']->value == '1') {?><span class="green"> activated </span><?php } elseif ($_smarty_tpl->tpl_vars['LS']->value == '0') {?> <span class="blue"> inactivated </span><?php } elseif ($_smarty_tpl->tpl_vars['LS']->value == '3') {?><span class="blue"> domain inactivated </span><?php } else { ?><span class="alizarin"> expired </span><?php }?></strong></p>

					 	 <p>Expiration: <strong>&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['l_expiry']->value;?>
</strong>&nbsp; <span class="apply-desc-span">(Y/M/D)</span></p>
				
						 <?php if ($_smarty_tpl->tpl_vars['MULTI_LICENSE']->value == 'true') {?>
					 	 <p><u>You can install and activate your job board license on <?php echo $_smarty_tpl->tpl_vars['MULTI_LICENSE_ACTIVATIONS_LEFT']->value;?>
 more <?php if ($_smarty_tpl->tpl_vars['SINGULAR_FLAG']->value) {?>domain<?php } else { ?>domains<?php }?>.</u></p>
					 	 <?php }?>

					 	 <?php if ($_smarty_tpl->tpl_vars['VERSION_UPDATE_URL']->value) {?>
					 	 	<?php if ($_smarty_tpl->tpl_vars['LS']->value == '1') {?>
					 			 <a href="<?php echo $_smarty_tpl->tpl_vars['VERSION_UPDATE_URL']->value;?>
" target="_blank"><button type="button" class="mbtn btn btn-default alizarinBtn">UPDATE version</button></a>
					 	 	<?php } else { ?>
					 	 		 <span class="alizarin"><strong> An active license and valid domain is needed in order to obtain any updates</strong></span>
					 	 	<?php }?>
					 	 <?php }?>

					 	 <?php if ($_smarty_tpl->tpl_vars['RENEW_BTN']->value == 'true') {?>
					 	  	<a href="<?php echo $_smarty_tpl->tpl_vars['RENEW_URL']->value;?>
" target="_blank"><button type="button" class="mbtn btn btn-default alizarinBtn">RENEW YOUR LICENSE</button></a>
					 	 <?php }?>


				  	 <?php }?>

			  	<?php } else { ?>
			  		<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
updates/activation">

						<label>License key:</label>

						<input required class="form-control minput" type="text" id="license_key" name="license_key" maxlength="50" value="<?php if ($_smarty_tpl->tpl_vars['populate_lk']->value) {
echo $_smarty_tpl->tpl_vars['populate_lk']->value;
}?>" />
						<br />

			  			<button type="submit" class="mbtn btn btn-default alizarinBtn">ACTIVATE</button>
			  		</form>

			  		<?php if ($_smarty_tpl->tpl_vars['err_msg']->value) {?>
			  		<br />
			  		<span class="alizarin"><strong><?php echo $_smarty_tpl->tpl_vars['err_msg']->value;?>
</strong></span>
			  		<?php }?>

			  		<?php if ($_smarty_tpl->tpl_vars['populate_lk']->value) {?>
			  			<div class="mt20">
			  				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
updates">REFRESH</a>
			  			</div>
			  		<?php }?>

			  	<?php }?>
		
			  	 
			  <?php }?>
			  	
		</div>

    </div>
</div><!-- #content -->

<?php if ($_smarty_tpl->tpl_vars['license_added_popup']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('License has been activated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
