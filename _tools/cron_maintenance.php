<?php


	require_once '../_config/cron_config.php';

	$janitor = new Maintenance();

	// delete old page hits / views older than 6 months
	$janitor->deleteOldHits();

	// delete temporary posts older than 5 days
	$janitor->deleteTmpJobs();
	
	// sam: send email to Candidates who has not logged in for 30 days
	$janitor->sendEmailToInActiveCandidates();

	// sam: deactivate Candidates who has not responded to the email after 5 days
	$janitor->deactivateInActiveCandidate();
	// what to do with expired jobs ?
	if (strcmp(EXPIRED_JOBS_ACTION, "deactivate") === 0) {
		$janitor->deactivateExpiredJobs();
	} else {
		$janitor->deleteExpiredJobs();
	}

?>