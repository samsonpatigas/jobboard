<?php 
	
	if (isset($id)) {

// ADDED NOTE

		if ($id === "note") {

			$notes = $_POST['apply_msg'];
			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET note = "'.$notes.'" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid."?success");

		}
// END

		// ADDED DDSPAYROLL YES

		if ($id === "ddspayroll1") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET ddspayroll = "1" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}
// END

				// ADDED DDSPAYROLL NO

		if ($id === "ddspayroll0") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET ddspayroll = "0" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}
// END

		// ADDED RECEIVE NOTIFICATION BY

		if ($id === "email") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET notifyby = "Email" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}

		if ($id === "text") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET notifyby = "Text message" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}

		if ($id === "both") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET notifyby = "Email,Text message" WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}

				if ($id === "none") {

			// echo $notes;

			$cid = intval($extra);
			// $sql = 'INSERT INTO applicant (note) VALUES ('.$notes.') WHERE id = ' . $cid;
			$sql = 'UPDATE applicant SET notifyby = null WHERE id = ' . $cid;
			// echo $sql;
			$db->query($sql);

			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);

		}


// END

		// ADDED FOR PUBLISH AND UNPUBLISH

		if ($id === "publish") {
			$cid = intval($extra);
			$sql = 'UPDATE applicant SET public_profile = 1 WHERE id = ' . $cid;
			$db->query($sql);
			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);
		} 
		if ($id === "unpublish") {
			$cid = intval($extra);
			$sql = 'UPDATE applicant SET public_profile = 0 WHERE id = ' . $cid;
			$db->query($sql);
			redirect_to("http://jobboard.f9portal.net/sjs-admin/candidate/".$cid);
		} 

		// END

		if ($id === "delete") {
			$cid = intval($extra);
			//remove all job applications
			$sql = 'DELETE FROM job_applications WHERE applicant_id = ' . $cid;
	    	$db->query($sql);

			//send notification email
		    $sql = 'SELECT * FROM applicant WHERE id = ' . $cid;
	        $data = $db->query($sql);
	        $candidateInfo = $data->fetch_assoc();

			$mailer = new Mailer();
			$mailer->notifyDeletedCandidate($candidateInfo['email'], $candidateInfo['fullname']);

			try {
				$DIR = '';
				if (defined('__DIR__'))
					$DIR = __DIR__;
				else
					$DIR = dirname(__FILE__);

				if (strlen($candidateInfo['cv_path']) > 1 && file_exists($DIR . '/../' . $candidateInfo['cv_path'])) {
					unlink($DIR . '/../' . $candidateInfo['cv_path']);
				}
			} catch (Exception $e) {}

			//remove candidate
			$sql = 'DELETE FROM applicant WHERE id = ' . $cid;
	    	$db->query($sql);

	    	//remove from subscribers
	    	$sql = 'DELETE FROM subscribers WHERE email = "' . $candidateInfo['email'] . '"';
	    	$db->query($sql);

	    	$sql = 'DELETE FROM subscriptions WHERE email = "' . $candidateInfo['email'] . '"';
	    	$db->query($sql);
	    	
	    	redirect_to(BASE_URL . 'candidates/deleted');
		}

		if(isset($_POST['update_email_app'])){
	    	$email = $_POST['email_app'];
	    	$update_email_app = 'UPDATE applicant SET email = "' . $email . '" WHERE id =' . $id;
	    	echo $update_email_app;
	    	$db->query($update_email_app);
	    	echo "<meta http-equiv='refresh' content='0'>";
			
	    }
	    if(isset($_POST['update_pass_app'])){
	    	$pass = md5($_POST['pass_app']);
	    	$update_pass_app = 'UPDATE applicant SET password = "' . $pass . '" WHERE id =' . $id;
	    	$db->query($update_pass_app);
	    	echo "<meta http-equiv='refresh' content='0'>";
	    }

		if ($id === "confirm") {
			$cid = intval($extra);
			$sql = 'UPDATE applicant SET confirmed = 1 WHERE id = ' . $cid;
			$db->query($sql);
		} else if ($id === "unconfirm") {
			$cid = intval($extra);
			$sql = 'UPDATE applicant SET confirmed = 0 WHERE id = ' . $cid;
			$db->query($sql);
		}

		if ($id === "confirm" || $id === "unconfirm") {
			$ID = intval($extra);
		} else {
			$ID = intval($id);
		}
		
	    $sql = 'SELECT * FROM applicant WHERE id = ' . $ID;
	    $data = $db->query($sql);

	} else {
		$data = null;
	}

	 $info = $data->fetch_assoc();

	 $sm_links = array();

     if (!empty($info['sm_link_1']) && $info['sm_link_1'] != "-") {
        $sm_links["first"] = deconstructSMlink($info['sm_link_1']);
     }

     if (!empty($info['sm_link_2']) && $info['sm_link_2'] != "-") {
        $sm_links["second"] = deconstructSMlink($info['sm_link_2']);
     }

     if (!empty($info['sm_link_3']) && $info['sm_link_3'] != "-") {
        $sm_links["third"] = deconstructSMlink($info['sm_link_3']);
     }

     if (!empty($info['sm_link_4']) && $info['sm_link_4'] != "-") {
        $sm_links["fourth"] = deconstructSMlink($info['sm_link_4']);
     }

     $info['sm_links'] = $sm_links;

	$se = explode(",", $info['skills']);
	$skills = '';
	foreach ($se as $skill) {
		$skills .=  "#" . $skill . " ";
	}

	$info['skills_formated'] = $skills;

    if ($info['cv_path'] != "") {
        $var = explode(".", $info['cv_path']);
        $ext = end($var);
        if (strcmp($ext, "pdf") == 0){
            $imgPath = 'fa fa-file-pdf-o fa-lg pdf-el';
        }
        else {
            $imgPath = 'fa fa-file-word-o fa-lg word-el';
        }  
    }

    $smarty->assign("data", $info);
    $smarty->assign("imgPath", $imgPath);
	$template = 'candidate.tpl';

?>