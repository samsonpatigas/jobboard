<?php 
	
	$sql = 'SELECT COUNT(id) as total FROM company';
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    $TOTAL = $row['total'];

    //pagination
    $paginatorLink = BASE_URL  . "public_doctor";
    $paginator = new Paginator($TOTAL, SUBSCRIBERS_PER_PAGE, @$_REQUEST['p']);
    $paginator->setLink($paginatorLink);
    $paginator->paginate();
    $offset = $paginator->getFirstLimit();

    $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company
    ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
    $data = $db->query($sql);

     if(isset($_POST['reviewed'])){

        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company WHERE is_Seen = 1 
        ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("reviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notreviewed'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company WHERE is_Seen = 0
    ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notreviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['allreviewed'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allreviewed", "background-color: #2C3E50!important;");
    }
      if(isset($_POST['published'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company WHERE public_page = 1  
        ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("publish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notpublished'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company WHERE public_page = 0  
        ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notpublish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['allpublished'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allpublish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['btnsearch'])){
        $sql = 'SELECT id,name,f9_job_app_email_address,public_page, is_Seen FROM company
                WHERE name LIKE"%'.$_POST['search'].'%" OR f9_job_app_email_address LIKE"%'.$_POST['search'].'%" ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
               
        $data = $db->query($sql);
        $smarty->assign("searchtext", $_POST['search']);
        $smarty->assign("btnsearch", "background-color: #2C3E50!important;");
    }

    $c = array();
    while ($row = $data->fetch_assoc()) {
     $c[] = $row;
    }
    
    $smarty->assign("companies", $c);
 	$smarty->assign("pages", $paginator->pages_link);
	$template = 'public_doctor.tpl';

?>
