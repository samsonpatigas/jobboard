<?php



	require_once '../_config/cron_config.php';

	$janitor = new Maintenance();

	// delete old page hits / views older than 6 months
	$janitor->deleteOldHits();

	// delete temporary posts older than 5 days
	$janitor->deleteTmpJobs();

	// what to do with expired jobs ?
	if (strcmp(EXPIRED_JOBS_ACTION, "deactivate") === 0) {
		$janitor->deactivateExpiredJobs();
	} else {
		$janitor->deleteExpiredJobs();
	}

?>