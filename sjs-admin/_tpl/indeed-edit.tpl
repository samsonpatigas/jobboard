{include file="header.tpl"} 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				{$indeed_headline}
			</label>
			<div class="subheading">Leave empty fields if you do not want to include this category.</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nmpl">
		 <form  method="post" action="{$BASE_URL_ADMIN}indeed/manage/{$indeed_category_id}" role="form" >
		    <input type="hidden" id="indeed_category_id" name="indeed_category_id" value="{$indeed_category_id}" />
		    <input type="hidden" id="field_ids" name="field_ids" value="{$indeed_field_ids}" />

		    {if $ids}
			    {foreach $ids as $id}
				    <div class="settings-row" style="overflow: hidden;">
			
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						  <div class="float-block">
				    		<label class="settings">NAME:</label>
				    		<input name="name_{$id}" id="name_{$id}" type="text" value="{$items[{$id}].name}" class="form-control minput"  />
				    	  </div>
				    	</div>

				    	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				    	  <div class="float-block">
				    		<label class="settings">VALUE:</label>
				    		<input name="value_{$id}" id="value_{$id}" type="text" value="{$items[{$id}].value}" class="form-control minput"  />
				    	 </div>
				    	</div>
				
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="height: 55px; padding-top: 20px; padding-left: 25px;">
						    <a href="{$BASE_URL_ADMIN}indeed/delete/{$indeed_category_id}-{$id}" title="Delete this item" onclick="if(!confirm('Are you sure you want to delete this item?'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
				    	</div>

					</div>
				{/foreach}
			{else}
			  <div class="settings-row" style="overflow: hidden;">
			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 red">
			  		This category is empty and will not be used on the website.
			  	</div>
			  </div>
			{/if}

			<div class="float-block" style="float: left">
				<div class="form-group" style="margin-top: 10px;">
				    <a href="{$BASE_URL_ADMIN}indeed/manage" style="text-decoration: none;">
						<button type="button" class="btn btn-default btn-warning mbtn ml20Desk" name="button" id="button" >Go back</button>
					</a>
					<button type="submit" class="btn btn-default btn-primary mbtn " name="submit" id="submit" >Save</button>
				</div>
			</div>

		 </form>
		</div>
</div>
</div>

{if $updated_popup == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Indeed settings have been updated');
   }, 1000);
</script>
{/if}

{if $deleted_popup == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Item has been deleted');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}