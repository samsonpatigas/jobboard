{include file="header.tpl"}

<div class="admin-content">
 <div class="admin-wrap-content">
 	<div class="row">
	
	<!-- <label class="admin-label">Review Dentist/Employer</label>
	<div class="subheading">Dentist/Employer name in red color indicates that employer is not public</div> -->
<!-- 	<br><br> -->
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 ">
		<form method="POST">

					<label class="admin-label">Filter by Publish: </label>
					<div class="checkbox">
						<button name="published" id="published" class="btn btn-default btn-primary mbtn" style="{$publish}" type="submit">Published</button>
				      <!-- <label><input name="published[]" id="published" class="published" type="checkbox" value="1">Published</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="notpublished" id="notpublished" class="btn btn-default btn-primary mbtn" style="{$notpublish}" type="submit">Not Published</button>
				      <!-- <label><input name="published[]" id="published" class="published" type="checkbox" value="0">Not Published</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="allpublished" id="allpublished" class="btn btn-default btn-primary mbtn" style="{$allpublish}" type="submit">All</button>
				      <!-- <label><input name="allpublished" id="allpublished" type="checkbox" value="">All</label> -->
				    </div>
			
					<label class="admin-label">Filter by Review: </label>
					<div class="checkbox">
					<button name="reviewed" id="reviewed" class="btn btn-default btn-primary mbtn" type="submit" style="{$reviewed}">Reviewed</button>
				      <!-- <label><input name="reviewed[]" id="reviewed" class="reviewed" type="checkbox" value="1">Reviewed</label> -->
				    </div>
				    <div class="checkbox">
				     <button name="notreviewed" id="notreviewed" class="btn btn-default btn-primary mbtn" style="{$notreviewed}" type="submit">Not Reviewed</button>
				      <!-- <label><input name="reviewed[]" id="notreviewed" class="reviewed" type="checkbox" value="0">Not Reviewed</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="allreviewed" id="allreviewed" class="btn btn-default btn-primary mbtn" style="{$allreviewed}" type="submit">All</button>
				      <!-- <label><input name="allreview" id="allreview" type="checkbox" value="">All</label> -->
				    </div>

				      <label class="admin-label">Filter by Search: </label>
				    <input name="search" id="search" type="text" class="mbtn" placeholder="Search" value="{$searchtext}">
				     <div class="checkbox"> 
				    	<button name="btnsearch" id="btnsearch" class="btn btn-default btn-primary mbtn" style="{$btnsearch}" type="submit">Search</button>
				    </div>
			
			</form>
	</div>
		<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
			<ul class="applicants-list ">  
				{foreach item=company from=$companies name=obj} 
				    <li class="p40"> 

				    <span><span class="{if $company.public_page == '1'}green{else}red{/if}" style="{if $company.is_Seen == '0'}font-weight: bold;{else} {/if}">{$company.name}</span>&nbsp;/&nbsp;{$company.f9_job_app_email_address}</span>

			    	<div style="float:right;">

				    	<a href="{$BASE_URL_ADMIN}publics_doctors/{$company.id}"><button type="submit" class="btn btn-default btn-primary mbtn" style="width: 85px !important; background-color: #E74C3C">Details</button></a>

				    </div>


					</li>
				{/foreach}
			</ul>
			<br />
			<div class="pagination">{$pages}</div>
    </div>
    {if $deletedPopup == 'true'}
      <script type="text/javascript">
       setTimeout(function(){
       	jobberBase.messages.add('Company has been deleted');
       }, 1000);
      </script>
    {/if}

</div>
</div>

{include file="footer.tpl"}