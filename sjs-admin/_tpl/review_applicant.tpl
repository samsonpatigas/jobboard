{include file="header.tpl"}

<!-- MODAL FOR MORE DETAILS -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Applicant Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
					
				  <li class="list-group-item"><strong>Applicant: </strong>&nbsp;{$companie.candidate_name}</li>
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;{$companie.candidate_email}</li>
				  <li class="list-group-item"><strong>Phone: </strong>&nbsp;{$companie.phone}</li>
				  <li class="list-group-item"><strong>Gender: </strong>&nbsp;{$companie.f9_gender}</li>
				  <li class="list-group-item"><strong>Address 1: </strong>&nbsp;{$companie.f9_address_1}</li>
				  <li class="list-group-item"><strong>Address 2: </strong>&nbsp;{$companie.f9_address_2}</li>
				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;{$companie.f9_city},&nbsp;{$companie.f9_state}</li>
<!-- 				  <li class="list-group-item"><strong>Country: </strong>&nbsp;{$companie.f9_country}</li> -->
				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;{$companie.f9_zip}</li>
<!-- 				  <li class="list-group-item"><strong>Language: </strong>&nbsp;{$companie.f9_language}</li> -->
<!-- 				  <li class="list-group-item"><strong>Location: </strong>&nbsp;{$companie.location}</li>
				  <li class="list-group-item"><strong>Occupation: </strong>&nbsp;{$companie.occupation}</li> -->
				  <li class="list-group-item"><strong>Skills: </strong>&nbsp;{$companie.skills}</li>
				  <li class="list-group-item"><strong>Experience: </strong>&nbsp;{$companie.f9_yrs_experience}</li>

				</ul>
				{/if}
			{/foreach}
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Applicant details</label>
				<div class="subheading"><a href="/sjs-admin/review_applicants"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;{$companie.candidate_email}</li>
				  <li class="list-group-item"><strong>Applicant: </strong>&nbsp;{$companie.candidate_name}</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;{if $companie.public_profile == '0'}<span class="red">Private</span>{else}<span class="green">Public</span>{/if}</li>
				  <li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				{/if}
			{/foreach}
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{if $companie.public_profile == '0'}
				<a href="{$BASE_URL_ADMIN}review_applicant/publish/{$companie.candidate_id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH ACCOUNT</button>
				</a>
			{else}
				<a href="{$BASE_URL_ADMIN}review_applicant/unpublish/{$companie.candidate_id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH ACCOUNT</button>
				</a>
			{/if}
		</div>

<!-- 		ADDED ADMIN NOTES -->

<style type="text/css">
	
	#note {

		margin-top: 1px;

	}

	#save {

		margin-left: 365px;
		margin-top: -10px;
	}

	#mnote {

		margin-left: 600px;
		margin-top: -398px;

	}

	#mnote p {

		margin-left: 30px;
	}

</style>

<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt25" id="note">

	{if $companie.note == null}

	<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small style="color: red;">( No notes added for this user. )</small></h4>

	<textarea id="apply_msg" name="apply_msg" class="apply_msg" placeholder="Add some notes?" maxlength="500" rows="5" cols="76" form="usrform"></textarea>

	{else}

	<h4>Admin Notes<red style="color: red;padding: 10px;">*</red></h4>

	<textarea id="apply_msg" name="apply_msg" class="apply_msg" maxlength="500" rows="5" cols="76" form="usrform">{$companie.note}</textarea>

	{/if}

		<div class="textarea-feedback tal" id="textarea_feedback"></div>

</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25" id="save">

	 <form action="{$BASE_URL_ADMIN}review_applicant/note/{$companie.candidate_id}" method="POST" id="usrform">
		
		<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">Save</button>

	</form>

</div>

<!-- ADDED 01/03/2019 -->

	<script type="text/javascript">
		
		$(".apply_msg").focus(function() {
    if(document.getElementById('apply_msg').value === ''){
        document.getElementById('apply_msg').value +='• ';
	}
});
$(".apply_msg").keyup(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        document.getElementById('apply_msg').value +='• ';
	}
	var txtval = document.getElementById('apply_msg').value;
	if(txtval.substr(txtval.length - 1) == '\n'){
		document.getElementById('apply_msg').value = txtval.substring(0,txtval.length - 1);
	}
});

	</script>

	<!-- ENDPOINT -->

<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25" id="mnote">

		<h4>Your Note<red style="color: red;padding: 10px;">*</red></h4>

		{if $companie.note != null}

			<p class="green">{$companie.note}</p>
		{else}

			<p class="red">No notes added for this user.</p>

		{/if}


</div> -->

<!-- END -->

	</div>
</div>
{include file="footer.tpl"}