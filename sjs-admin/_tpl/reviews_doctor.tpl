{include file="header.tpl"}

<!-- MODAL FOR MORE DETAILS -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Job Post Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
					
				  <li class="list-group-item"><strong>Type: </strong>&nbsp;{$companie.type_name}</li>
				  <li class="list-group-item"><strong>Category: </strong>&nbsp;{$companie.category_name}</li>
				  <li class="list-group-item"><strong>Location: </strong>&nbsp;{$companie.city_name}</li>
				  <li class="list-group-item"><strong>Position: </strong>&nbsp;{$companie.f9_position}</li>
				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;{$companie.f9_city},&nbsp;{$companie.f9_state}</li>
				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;{$companie.f9_zip}</li>
				  <li class="list-group-item"><strong>Gender: </strong>&nbsp;{$companie.f9_gender}</li>
				  <li class="list-group-item"><strong>Language: </strong>&nbsp;{$companie.f9_language}</li>
				  <li class="list-group-item"><strong>Specialties: </strong>&nbsp;{$companie.f9_specialties}</li>
				  <li class="list-group-item"><strong>Experience: </strong>&nbsp;{$companie.f9_yrs_of_experience}</li>
				  <li class="list-group-item"><strong>Salary: </strong>&nbsp;{$companie.salary}</li>
				  <li class="list-group-item"><strong>Post Period: </strong>&nbsp;{$companie.f9_post_peroid}&nbsp;days</li>
				  <li class="list-group-item"><strong>Short Description: </strong>&nbsp;{$companie.f9_short_description}</li>
				  <li class="list-group-item"><strong>Admin Notes: </strong>&nbsp;{$companie.f9_admin_notes}</li>
				  <li class="list-group-item"><strong>Expiry: </strong><span class="red">&nbsp;{$companie.expires}</span></li>

				</ul>
				{/if}
			{/foreach}
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Job details</label>
				<div class="subheading"><a href="/sjs-admin/review_doctors"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
				  <li class="list-group-item" style="text-align:center"><img src="{$BASE_URL}{$companie.company_logo_path}"></li>
				  <li class="list-group-item"><strong>Title: </strong>&nbsp;{$companie.title}</li>
				  <li class="list-group-item"><strong>Created by: </strong>&nbsp;{$companie.empname}</li>
				   <li class="list-group-item"><strong>Email: </strong>&nbsp;{$companie.email}</li>
				  <li class="list-group-item"><strong>Created on: </strong>&nbsp;{$companie.created_on}</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;{if $companie.review_status == '0'}<span class="red">Private</span>{else}<span class="green">Public</span>{/if}</li>
				  	<li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				{/if}
			{/foreach}
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{if $companie.review_status == '0'}
				<a href="{$BASE_URL_ADMIN}reviews/publish/{$companie.id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH JOB</button>
				</a>
			{else}
				<a href="{$BASE_URL_ADMIN}reviews/unpublish/{$companie.id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH JOB</button>
				</a>
			{/if}
		</div>
	</div>
</div>
{include file="footer.tpl"}