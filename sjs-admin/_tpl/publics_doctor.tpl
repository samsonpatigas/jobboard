{include file="header.tpl"}

<!-- MODAL FOR MORE DETAILS -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Dentist/Employer Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
					
				  <li class="list-group-item"><strong>Dentist/Employer: </strong>&nbsp;{$companie.name}</li>
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;{$companie.f9_job_app_email_address}</li>
<!-- 				  <li class="list-group-item"><strong>Headquarters: </strong>&nbsp;{$companie.hq}</li>
				  <li class="list-group-item"><strong>Street: </strong>&nbsp;{$companie.street}</li>
				  <li class="list-group-item"><strong>City Postcode: </strong>&nbsp;{$companie.city_postcode}</li> -->
				  <li class="list-group-item"><strong>Address 1: </strong>&nbsp;{$companie.f9_address_1}</li>
				  <li class="list-group-item"><strong>Address 2: </strong>&nbsp;{$companie.f9_address_2}</li>
				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;{$companie.f9_city},&nbsp;{$companie.f9_state}</li>
				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;{$companie.f9_zip}</li>
				  <li class="list-group-item"><strong>Telephone No: </strong>&nbsp;{$companie.f9_practice_phone_number}</li>
				  <li class="list-group-item"><strong>Cellphone No: </strong>&nbsp;{$companie.f9_cellphone_number}</li>
				  <li class="list-group-item"><strong>Practice Type: </strong>&nbsp;{$companie.f9_practice_type}</li>

				</ul>
				{/if}
			{/foreach}
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Dentist/Employer details</label>
				<div class="subheading"><a href="/sjs-admin/public_doctors"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{foreach item=companie from=$data name=obj} 
				{if $companie}
				<ul class="list-group">
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;{$companie.f9_job_app_email_address}</li>
				  <li class="list-group-item"><strong>Dentist/Employer: </strong>&nbsp;{$companie.name}</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;{if $companie.public_page == '0'}<span class="red">Private</span>{else}<span class="green">Public</span>{/if}</li>
				  <li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				{/if}
			{/foreach}
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			{if $companie.public_page == '0'}
				<a href="{$BASE_URL_ADMIN}publics_doctors/publish/{$companie.id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH ACCOUNT</button>
				</a>
			{else}
				<a href="{$BASE_URL_ADMIN}publics_doctors/unpublish/{$companie.id}">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH ACCOUNT</button>
				</a>
			{/if}
		</div>
	</div>
</div>
{include file="footer.tpl"}