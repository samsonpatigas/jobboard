<?php
/* Smarty version 3.1.30, created on 2019-04-29 14:14:03
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/seo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cc6f89be4cd37_54808531',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0c2b44c53146eac9fb02e2977fe402f81c5076ee' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/seo.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cc6f89be4cd37_54808531 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Website SEO
			</label>
			<div class="subheading">management</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/1" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/1">Homepage</a></label>
				<div class="light">Website titles, keywords and descriptions. 404- pages</div>
			</div>

		    <div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/2" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/2">Jobs</a></label>
				<div class="light">Job related SEO</div>
			</div>

			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/3" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/3">Applicant</a></label>
				<div class="light">Candidate dashboard. Login pages</div>
			</div>

			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/4" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/4">Recruiter</a></label>
				<div class="light">Login and registration pages</div>
			</div>


			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/5" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/5">Companies</a></label>
				<div class="light">Jobs at company. Companies listing</div>
			</div>


			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/6" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/6">Google Analytics</a></label>
				<div class="light">Manage Google Analytics</div>
			</div>

			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/7" ><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/7">Google Maps</a></label>
				<div class="light">Manage Google Maps</div>
			</div>
		</div>
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
