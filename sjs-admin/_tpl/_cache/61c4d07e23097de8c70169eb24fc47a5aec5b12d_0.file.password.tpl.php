<?php
/* Smarty version 3.1.30, created on 2019-03-20 16:54:36
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/password.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c92704c0a1e18_16584623',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '61c4d07e23097de8c70169eb24fc47a5aec5b12d' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/password.tpl',
      1 => 1544153609,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c92704c0a1e18_16584623 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- MODAL FOR MORE ADDING NEW ADMIN USER -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Add new administrator</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			
	<!-- 		http://jobboard.f9portal.net/sjs-admin/password/ -->
				<form action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
add-admin/" method="post">
<!-- 					<form action="<?php echo $_SERVER['REQUEST_URI'];?>
" method="post"> -->
		<!-- 			<form action="" method="post"> -->

					Username: <input required placeholder="Username" class="form-control minput" name="username" id="username" size="30" />

					Password: <input required placeholder="Password" class="form-control minput" type="password" name="password" id="password" size="30" />

					<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Register</button>

				</form>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->

		<div class="admin-content">
				 <label class="admin-label">Password change</label>
				 <div class="subheading">Change your SJS admin password</div><br />
				 <a href="/sjs-admin/settings"><div class="subheading">&larr;go back</div></a>
				<br />
				<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
					<div class="negative-feedback">
						<?php echo $_smarty_tpl->tpl_vars['error']->value;?>

					</div>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['success']->value) {?>
					<div class="positive-feedback">
						<?php echo $_smarty_tpl->tpl_vars['success']->value;?>

					</div>
				<?php }?>
				
				<form id="change_password" action="<?php echo $_SERVER['REQUEST_URI'];?>
" method="post">
					<div>
						<div class="form-group <?php if ($_smarty_tpl->tpl_vars['error']->value) {?> error<?php }?>">
							<input required placeholder="new password" class="form-control minput" type="password" name="new_password" id="new_password" size="30" />
						</div>
						<div class="form-group <?php if ($_smarty_tpl->tpl_vars['error']->value) {?> error<?php }?>">
							<input required placeholder="repeat password" class="form-control minput" type="password" name="verify_password" id="verify_password" size="30" />
						</div>
					<div class="button-holder-np form-group" >
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Change password</button>
					</div>
				</form>

<!-- 				<a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a> -->

<button type="button" class="btn btn-default btn-primary mbtn" data-toggle="modal" data-target="#myModal">Add Admin Account&nbsp;&nbsp;<i class="fa fa-user-secret" style="font-size:15px;color:black"></i></button>

				<?php if ($_smarty_tpl->tpl_vars['inserted']->value) {?>
					<div class="positive-feedback">
						<?php echo $_smarty_tpl->tpl_vars['inserted']->value;?>

					</div>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['fail']->value) {?>
					<div class="positive-feedback">
						<?php echo $_smarty_tpl->tpl_vars['fail']->value;?>

					</div>
				<?php }?>

		</div><!-- #content -->

	
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function()
			{
				$('#new_password').focus();
				
				$("#change_password").validate({
					rules: {
						new_password: { required: true },
						verify_password: { equalTo: "#new_password" }
					}
				});
			});
		<?php echo '</script'; ?>
>
		

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
