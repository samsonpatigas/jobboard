<?php
/* Smarty version 3.1.30, created on 2019-07-18 20:38:01
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30ca99403d14_72086245',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d2a8b4ef91dbb290f0ebb252442d7429f99b8ca' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/login.tpl',
      1 => 1545158754,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:login_header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d30ca99403d14_72086245 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:login_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="form-box">
		<div style="text-align: center;"><img class="site-logo" src="jobboard.f9portal.net/../../fav.png" style="width: 30%;" alt="Website's Logo"></div>
		<div class="login-recruiter-headline admin-login-top">

<style type="text/css">
	
#pos {

	margin-top: 5px;
}
.form-box{
	margin-top: 0;
}
</style>

			<label class="login-form-title"><img class="site-logo" src="jobboard.f9portal.net/../../uploads/logos/main-logo.png" style="width: 85%; margin-top: 10px;" alt="Website's Logo"></label>
			<div class="subheadline" id="pos"><p>
				Verify your identity
			</p>

		</div>
		<div class="login-container admin-wrap" >
			<form id="login" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
">
				
					<div class="form-group">
						<div class="group<?php if ($_smarty_tpl->tpl_vars['errors']->value['username']) {?> error<?php }?>">
							<input required placeholder="username" type="text" name="username" id="username" class="form-control grayInput" value="<?php echo $_POST['username'];?>
" />
						</div>
					</div>

					<div class="form-group">
						<div class="group<?php if ($_smarty_tpl->tpl_vars['errors']->value['password']) {?> error<?php }?>">
							<input required placeholder="password" type="password" name="password" class="form-control grayInput" id="password"value="" />
						</div>
					</div>

					<div class="form-group">
						<div class="negative-feedback">
						<?php echo $_smarty_tpl->tpl_vars['errors']->value['incorrect'];?>

						</div>
					</div>
					
					<div class="form-group">
						<div class="group_submit">
							<button class="btn btn-default btn-primary mbtn" type="submit" name="submit" id="submit"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
							<input type="hidden" name="action" value="login" />
						</div>
					</div>
			
			</form>
		</div><!-- #content -->
	</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
