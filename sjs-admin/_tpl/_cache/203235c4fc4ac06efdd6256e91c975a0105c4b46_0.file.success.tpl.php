<?php
/* Smarty version 3.1.30, created on 2019-07-16 14:31:43
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/success/success.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2dd1bf52bee4_22274546',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '203235c4fc4ac06efdd6256e91c975a0105c4b46' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/success/success.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d2dd1bf52bee4_22274546 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
  <div class="admin-wrap-content">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['admin']['operation_success_label'];?>
</label>
		<div class="alert alert-info fade in main-color mt10">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		     <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		    <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

		</div>
	</div>
  </div>
</div><!-- #content -->

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
