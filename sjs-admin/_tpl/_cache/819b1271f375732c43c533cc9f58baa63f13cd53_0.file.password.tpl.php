<?php
/* Smarty version 3.1.30, created on 2018-10-26 14:31:06
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/password.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd3171add67d3_06865693',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '819b1271f375732c43c533cc9f58baa63f13cd53' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/password.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd3171add67d3_06865693 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div class="admin-content">
				 <label class="admin-label">Password change</label>
				 <div class="subheading">Change your SJS admin password</div><br />
				 <a href="/sjs-admin/settings"><div class="subheading">&larr;go back</div></a>
				<br />
				<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
					<div class="negative-feedback">
						<?php echo $_smarty_tpl->tpl_vars['error']->value;?>

					</div>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['success']->value) {?>
					<div class="positive-feedback">
						<?php echo $_smarty_tpl->tpl_vars['success']->value;?>

					</div>
				<?php }?>
				
				<form id="change_password" action="<?php echo $_SERVER['REQUEST_URI'];?>
" method="post">
					<div>
						<div class="form-group <?php if ($_smarty_tpl->tpl_vars['error']->value) {?> error<?php }?>">
							<input required placeholder="new password" class="form-control minput" type="password" name="new_password" id="new_password" size="30" />
						</div>
						<div class="form-group <?php if ($_smarty_tpl->tpl_vars['error']->value) {?> error<?php }?>">
							<input required placeholder="repeat password" class="form-control minput" type="password" name="verify_password" id="verify_password" size="30" />
						</div>
					<div class="button-holder-np form-group" >
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Change password</button>
					</div>
				</form>
		</div><!-- #content -->

	
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function()
			{
				$('#new_password').focus();
				
				$("#change_password").validate({
					rules: {
						new_password: { required: true },
						verify_password: { equalTo: "#new_password" }
					}
				});
			});
		<?php echo '</script'; ?>
>
		

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
