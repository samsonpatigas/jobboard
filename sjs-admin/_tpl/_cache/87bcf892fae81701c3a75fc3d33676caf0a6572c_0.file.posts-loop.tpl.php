<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:14:43
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/posts-loop.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30d3336364c2_28883227',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '87bcf892fae81701c3a75fc3d33676caf0a6572c' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/posts-loop.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30d3336364c2_28883227 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['jobs']->value) {?>

<div class="list">

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jobs']->value, 'job', false, NULL, 'tmp', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>
	<div id="item<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" class="row settings-row p15" style="padding: 10px;">
		<div class="icons" >
			 <?php if ($_smarty_tpl->tpl_vars['job']->value['spotlight'] == 0) {?>
				<a class="mr10 mb3" id="activateSpotlight<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="javascript:void(0);" onclick="Jobber.SpotlightActivate('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
activate-spotlight/', <?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
);" title="Turn spotlight on"><i class="fa fa-star-o fa-lg yellow-fa" aria-hidden="true"></i>
			<?php } else { ?>
				<a class="mr10 mb3" id="deactivateSpotlight<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="javascript:void(0);" onclick="Jobber.SpotlightDeactivate('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
deactivate-spotlight/', <?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
);" title="Turn spotlight off"><i class="fa fa-star fa-lg yellow-fa" aria-hidden="true"></i></a>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['job']->value['is_active'] == 0) {?>
				<a  class="mr10 mb3" id="activateLink<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="javascript:void(0);" onclick="Jobber.Activate('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
activate/', <?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
, <?php if ($_smarty_tpl->tpl_vars['CURRENT_PAGE']->value == '') {?>1<?php } else { ?>0<?php }?>);" title="Activate this job"><i class="fa fa-circle-o fa-lg green-fa" aria-hidden="true"></i></a>
			<?php } else { ?>
				<a  class="mr10 mb3" id="deactivateLink<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="javascript:void(0);" onclick="Jobber.Deactivate('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
deactivate/', <?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
);" title="Deactive this job"><i class="fa fa-circle fa-lg green-fa" aria-hidden="true"></i></a>
			<?php }?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
edit-post/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
/" title="Edit this job"><i class="fa fa-gear fa-lg mr10" aria-hidden="true"></i></a>
			<a href="javascript:void(0);" onclick="Jobber.Delete('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
delete/', <?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
);" title="Delete this job"><i class="fa fa-trash fa-lg blueColor" aria-hidden="true"></i></span></a>
			<br />
			<div class="clear"></div>
			<span class="fs13 gray" ><?php echo $_smarty_tpl->tpl_vars['job']->value['post_date'];?>
</span>
		</div>

		<strong class="f14"><a style="color: #113E4D; text-decoration: none" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
/" title="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</a></strong> 
		<span class="f14"><span class="light">at</span> <span class="moderate-black" style="color: #39b6b3"><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];
if ($_smarty_tpl->tpl_vars['job']->value['is_location_anywhere']) {?>, <?php echo $_smarty_tpl->tpl_vars['translations']->value['jobs']['location_anywhere'];
} else { ?> </span><span class="light">in</span> <span class="moderate-black"><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</span><?php }?></span>
		<div>
			<span class="fs13 gray">viewed <strong><?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
 times</span></strong><?php if ($_smarty_tpl->tpl_vars['statisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]) {?>, <strong><?php echo $_smarty_tpl->tpl_vars['statisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]['numberOfApplications'];?>
 applicants</strong>, 
			last application on <strong><?php echo $_smarty_tpl->tpl_vars['statisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]['lastApplicationOn'];?>
</strong><?php }
if ($_smarty_tpl->tpl_vars['spamReportStatisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]) {?>,
			<span class="spam"> reported as spam <strong><?php echo $_smarty_tpl->tpl_vars['spamReportStatisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]['numberOfSpamReports'];?>
 times</strong>, 
				last time on <strong><?php echo $_smarty_tpl->tpl_vars['spamReportStatisticalData']->value[$_smarty_tpl->tpl_vars['job']->value['id']]['lastSpamReportOn'];?>
</strong></span><?php }?></div>
	</div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div>

<div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>

<?php } else { ?>
	<div id="no-ads">
		No jobs, yet.
	</div>
<?php }
}
}
