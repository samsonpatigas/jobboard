<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:14:43
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/category.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30d333589d21_43159399',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5b280541b272861cc258d7f0938c8a768d19164' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/category.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:posts-loop.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d30d333589d21_43159399 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		<div class="admin-content" >
		 <div class="admin-wrap-content">
			<div id="job-listings">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<label class="admin-label">
						<?php echo $_smarty_tpl->tpl_vars['translations']->value['category']['jobs_for'];?>
 <?php echo $_smarty_tpl->tpl_vars['current_category_name']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['current_type']->value) {?>/ <?php echo $_smarty_tpl->tpl_vars['current_type']->value;
}?>
					</label>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mb15">
					<div id="sort-by-type">
					<?php echo $_smarty_tpl->tpl_vars['translations']->value['category']['display_only'];?>
&nbsp; 
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['current_category']->value;?>
" title="All" >
					  <div class="box">
						Reset
					  </div>
					</a>
						<?php
$__section_tmp_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp'] : false;
$__section_tmp_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['types']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_tmp_0_total = $__section_tmp_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = new Smarty_Variable(array());
if ($__section_tmp_0_total != 0) {
for ($__section_tmp_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] = 0; $__section_tmp_0_iteration <= $__section_tmp_0_total; $__section_tmp_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']++){
?>
						<a class="<?php if ($_smarty_tpl->tpl_vars['current_type']->value == $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]['var_name']) {?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['current_category']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]['var_name'];?>
/" title="<?php echo $_smarty_tpl->tpl_vars['current_category']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]['name'];?>
" >
						  <div class="box">
							<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]['name'];?>

						  </div>
						  </a>
						<?php
}
}
if ($__section_tmp_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = $__section_tmp_0_saved;
}
?>
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="container-fluid">
						<?php $_smarty_tpl->_subTemplateRender("file:posts-loop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					</div>
				</div>

			</div>
		</div>
		</div>

<?php if ($_smarty_tpl->tpl_vars['JOB_EDITED']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Job has been edited');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
