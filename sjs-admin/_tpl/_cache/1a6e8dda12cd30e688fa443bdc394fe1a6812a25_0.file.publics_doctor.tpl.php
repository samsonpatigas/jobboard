<?php
/* Smarty version 3.1.30, created on 2019-06-13 14:11:30
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/publics_doctor.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d024b82e3f243_90081336',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1a6e8dda12cd30e688fa443bdc394fe1a6812a25' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/publics_doctor.tpl',
      1 => 1543989474,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d024b82e3f243_90081336 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- MODAL FOR MORE DETAILS -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Dentist/Employer Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
				<ul class="list-group">
					
				  <li class="list-group-item"><strong>Dentist/Employer: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['name'];?>
</li>
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_job_app_email_address'];?>
</li>
<!-- 				  <li class="list-group-item"><strong>Headquarters: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['hq'];?>
</li>
				  <li class="list-group-item"><strong>Street: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['street'];?>
</li>
				  <li class="list-group-item"><strong>City Postcode: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['city_postcode'];?>
</li> -->
				  <li class="list-group-item"><strong>Address 1: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_address_1'];?>
</li>
				  <li class="list-group-item"><strong>Address 2: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_address_2'];?>
</li>
				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_city'];?>
,&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_state'];?>
</li>
				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_zip'];?>
</li>
				  <li class="list-group-item"><strong>Telephone No: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_practice_phone_number'];?>
</li>
				  <li class="list-group-item"><strong>Cellphone No: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_cellphone_number'];?>
</li>
				  <li class="list-group-item"><strong>Practice Type: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_practice_type'];?>
</li>

				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Dentist/Employer details</label>
				<div class="subheading"><a href="/sjs-admin/public_doctors"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
				<ul class="list-group">
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_job_app_email_address'];?>
</li>
				  <li class="list-group-item"><strong>Dentist/Employer: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['name'];?>
</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;<?php if ($_smarty_tpl->tpl_vars['companie']->value['public_page'] == '0') {?><span class="red">Private</span><?php } else { ?><span class="green">Public</span><?php }?></li>
				  <li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php if ($_smarty_tpl->tpl_vars['companie']->value['public_page'] == '0') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
publics_doctors/publish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH ACCOUNT</button>
				</a>
			<?php } else { ?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
publics_doctors/unpublish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH ACCOUNT</button>
				</a>
			<?php }?>
		</div>
	</div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
