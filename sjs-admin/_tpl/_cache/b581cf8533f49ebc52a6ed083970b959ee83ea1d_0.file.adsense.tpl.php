<?php
/* Smarty version 3.1.30, created on 2018-10-26 14:33:22
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/adsense.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd317a227bf87_76230930',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b581cf8533f49ebc52a6ed083970b959ee83ea1d' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/adsense.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd317a227bf87_76230930 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">
				Google Adsense
			</label>
			<div class="subheading">See <a href="https://simplejobscript.com/downloads/adsense-manager/" target="_blank">setup steps</a> and how to start.</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
		
			<form id="adsense-form" name="adsense-form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
adsense/update" role="form" >
				 <div class="form-group mb50">
					<label for="home_sidebar">Home sidebar rectangle (optimal 300x250)</label>
					<textarea  class="form-control grayInput noTinymceTA" maxlength="2000" name="home_sidebar" id="home_sidebar" rows="4"><?php if ($_smarty_tpl->tpl_vars['current_form']->value) {
echo $_smarty_tpl->tpl_vars['current_form']->value['home_sidebar_value'];
}?></textarea>
				</div>

				<div class="form-group mb50">
					<label for="home_leaderboard">Home listing leaderboard (optimal 728x90). Displayed between every XY (specify at the bottom) ads.</label>
					<textarea  class="form-control grayInput noTinymceTA" maxlength="2000" name="home_leaderboard" id="home_leaderboard" rows="4" ><?php if ($_smarty_tpl->tpl_vars['current_form']->value) {
echo $_smarty_tpl->tpl_vars['current_form']->value['home_leaderboard_value'];
}?></textarea>
				</div>

				<div class="form-group mb50">
					<label for="detail_rectangle">Job detail rectangle (optimal 300x250)</label>
					<textarea  class="form-control grayInput noTinymceTA" maxlength="2000" name="detail_rectangle" id="detail_rectangle" rows="4" ><?php if ($_smarty_tpl->tpl_vars['current_form']->value) {
echo $_smarty_tpl->tpl_vars['current_form']->value['detail_rectangle_value'];
}?></textarea>
				</div>

				 <div class="form-group mb50">
					<label for="backoffice_rectangle">Recruiter backoffice sidebar rectangle (optimal 300x250)</label>
					<textarea  class="form-control grayInput noTinymceTA" maxlength="2000" name="backoffice_rectangle" id="backoffice_rectangle" rows="4" ><?php if ($_smarty_tpl->tpl_vars['current_form']->value) {
echo $_smarty_tpl->tpl_vars['current_form']->value['backoffice_rectangle_value'];
}?></textarea>
				</div>

				 <div class="form-group mb50">
					<label for="adsense_count">Display ads between every XY entries</label>
					<input  required name="adsense_count" id="adsense_count" maxlength="50" type="text" value="<?php echo $_smarty_tpl->tpl_vars['adsense_count']->value;?>
" class="form-control minput"  />
				</div>

				<div class="form-group mt30">
				   <button onclick="jobberBase.messages.add('Adsense saved');" type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Save</button>
				</div>
			</form>

		</div>

    </div>
</div><!-- #content -->

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
