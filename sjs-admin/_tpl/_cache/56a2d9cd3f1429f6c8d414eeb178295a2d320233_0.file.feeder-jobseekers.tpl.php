<?php
/* Smarty version 3.1.30, created on 2018-11-08 17:48:20
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-jobseekers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be476e47b1d96_70516878',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '56a2d9cd3f1429f6c8d414eeb178295a2d320233' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-jobseekers.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5be476e47b1d96_70516878 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content " >
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label">ADD JOBSEEKERS</label>
		<a href="/sjs-admin/feeder"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a><br />
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	 <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/candidates" enctype="multipart/form-data" >
	 	<input type="hidden" id="external_links" name="external_links" value="0" />

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb40">
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 deskPr100">
				<div class="form-group mb40">
					<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['name'];?>
" required id="apply_name" name="apply_name" type="text" class="form-control minput" />
				</div>

				<div class="form-group mb40">
					<input placeholder="Email (*)" required id="apply_email" name="apply_email" type="email" maxlength="100" class="form-control minput" />
				</div>

				<div class="form-group mb40">
					<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['portfolio'];?>
" id="portfolio" placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['portfolio_placeholder'];?>
" name="portfolio" type="text" maxlength="100" class="form-control minput" />
				</div>

				<div class="form-group mb40">
					<input required id="occupation" name="occupation" placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['occupation_placeholder'];?>
" type="text" maxlength="200" class="form-control minput" />
					<div class="textarea-feedback" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['occupation_desc'];?>
</div>
				</div>
				
				<div class="form-group">
					<textarea placeholder="About the Jobseeker (*)" required class="form-control" rows="5" id="apply_msg" name="apply_msg"></textarea>
					<div class="textarea-feedback" id="textarea_feedback"></div>
				</div>

				<div class="form-group mb40">
					<label id="bannerLabel" for="cv"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['cv_label'];?>
</label>
					<input accept=".doc,.docx, .pdf" name="cv" id="cv" class="inputfile form-control minput" type="file" />

					<div class="textarea-feedback" ><?php echo $_smarty_tpl->tpl_vars['cv_hint']->value;?>
</div>
					<div id="uploadPreview"></div>
					<div id="err" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['cv_err'];?>
</div>
				</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mb40 deskPr100">
				<div class="form-group mb40">
					<input required placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['password'];?>
" type="password" id="pass1" name="pass1"  maxlength="50" class="form-control minput" />
				</div>
				<div class="form-group mb40">
					<input required placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['password2'];?>
" type="password" id="pass2" name="pass2"  maxlength="50" class="form-control minput" />
					<div id="feedback-err" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_passes'];?>
</div>
				</div>
				<div class="form-group mb40">
					<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['phone'];?>
" type="tel" id="apply_phone" name="apply_phone"  maxlength="50" class="form-control minput" />
				</div>
				<div class="form-group mb40">
					<input placeholder="Location (*)" type="text" id="apply_location" name="apply_location"  maxlength="400" class="form-control minput" />
				</div>

				<div class="form-group mb40">
					<div class="input textarea clearfix skillsTaggle"></div>
				</div>

		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group mb20">
				<a id="addLink" onclick="return Jobber.addExternalLink();" href="#"><?php echo $_smarty_tpl->tpl_vars['translations']->value['js']['add_social_media'];?>
</a>
			</div>

			<div id="addLinkBlock" class="mb30"></div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<button type="submit" class="btn btn-default btn-primary mbtn" onclick="return Jobber.createProfileValidationAdmin(<?php echo $_smarty_tpl->tpl_vars['MAX_CV_SIZE']->value;?>
);">SUBMIT</button>
		</div>

	</form>
	</div>
 </div>
</div><!-- #content -->


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function() {

		$('#cv').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#bannerLabel').html(fname);
			else
				$('#bannerLabel').html($('#bannerLabel').html());
        });
	});
<?php echo '</script'; ?>
>


<?php if ($_smarty_tpl->tpl_vars['CANDIDATE_ADDED']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Jobseeker has been added to the system');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
