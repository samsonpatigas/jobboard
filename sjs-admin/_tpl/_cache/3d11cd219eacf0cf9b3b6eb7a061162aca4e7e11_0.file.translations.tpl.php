<?php
/* Smarty version 3.1.30, created on 2019-07-15 15:14:01
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/translations.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2c8a293221f1_47581294',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d11cd219eacf0cf9b3b6eb7a061162aca4e7e11' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/translations.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d2c8a293221f1_47581294 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="admin-content">
	  <div class="admin-wrap-content">
		  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  	<label class="admin-label mb15">Translations</label>
		  </div>			

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb50">		
					<h4  class="upc" style="margin-bottom: 10px;"><a class="blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
translations/edit/<?php echo @constant('LANG_CODE');?>
/">&rarr;Edit translations</a></h4>
					<div class="subheading">Manage translations (strings on the site + emails).</div>
				</div>


					<div id="translation-langs">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<h4 class="upc fs13" style="margin-bottom: 20px;">Add a new language</h4>
							<form id="new-lang-frm" method="post" action="" >
									<table class="table table-condensed settings-table">
										<tr class="light-gray fs13"style="opacity: 0.65" ><th>Name (e.g. English)</th><th>Code (e.g. en)</th><th>&nbsp;</th></tr>
										<tr>
											<td><input class="mr15 minput form-control" type="text" id="new-lang-name" /></td>
											<td><input class="mr15 minput form-control" type="text" id="new-lang-code" size="7" maxlength="2" /></td>
											<td><button style="width: 75px" id="add-lang-trigger">Add</button></td>
										</tr>
									</table>
							</form>
						</div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 						<h4  class="upc fs13" style="margin-bottom: 20px;">Existing languages</h4>
						<div class="table-responsive">
							<table class="table table-condensed settings-table" cellspacing="0">
								<tr class=" upc fs13" style="opacity: 0.65"><th>Name</th><th>Code</th><th>&nbsp;</th></tr>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'lang');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value) {
?>
								<tr class="<?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
">
									<td width="200">
										<span class="light-gray fs13"><?php echo $_smarty_tpl->tpl_vars['lang']->value['name'];?>
</span>
										<div class="lang-edit"><form><input class="mr15 minput form-control new-lang-name" type="text" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['name'];?>
" /></form></div>
									</td>
									<td width="100">
										<span class="light-gray fs13"><?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
</span>
										<div class="lang-edit"><form><input class="mr15 minput form-control new-lang-code" type="text" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
" size="7" maxlength="2" /></form></div>
									</td>
									 <td width="100">
										<div style="padding-top: 30px; padding-left: 20px;"><a href="#" class="lang-edit-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
"><i class="fa fa-gear fa-lg blueColor mr5" aria-hidden="true"></i></a></div>
									</td>
									<td width="100">
										<div style="padding-top: 30px; padding-left: 20px;"><a href="#" class="lang-delete-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['lang']->value['id'];?>
"><i class="fa fa-trash fa-lg blueColor" aria-hidden="true"></i></a></div>
									</td>
				 					<td align="right" class="no-decor">
										<div style="padding-top: 30px; padding-left: 20px;" class="lang-edit"><button style="width: 75px" class="lang-save-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['lang']->value['id'];?>
">Save</button> &nbsp;or&nbsp; <a class="lang-cancel-trigger" href="#" rel="<?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
">Cancel</a></div>
									</td>  
								</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</table>
						</div>
					</div>
					</div>		

	</div>
	</div>
		
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
