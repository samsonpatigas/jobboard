<?php
/* Smarty version 3.1.30, created on 2019-07-03 18:37:49
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/candidates_history_reports.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d1ce7ed694099_66871803',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7a3fd17d0d51f53fe61f6fd240f559fef97d477' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/candidates_history_reports.tpl',
      1 => 1541688223,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d1ce7ed694099_66871803 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="admin-content">
<div class="admin-wrap-content">	
	<!-- <link href="http://54.213.230.63/css/app.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable({
            "ajax": "http://jobboard.f9portal.net/sjs-admin/page_candidates_history_api.php",
            "columns": [
                { "data": "created_on" },
                { "data": "title" },
                { "data": "name" },
                { "data": "fullname" },
                { "data": "status" }
            ]
        });
    });
    <?php echo '</script'; ?>
>
    <!-- Scripts -->
    <?php echo '<script'; ?>
>
        setTimeout(function(){
        location.reload();
        },216000);
        <?php echo '</script'; ?>
>
</head>
<body class="is-logged-in has-sidebar">
    <div id="app" style="margin-top: -40px;">
        <section id="section" class="h-pad-xl v-pad-xs">
            <div id="title">
                <h2 class="v-margin-sm" style="
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
                -webkit-border-radius: .25rem;
                border-radius: .25rem;
                padding: 20px;
                background: linear-gradient(40deg,#3ab8bc,#39b6b3);
                color: #fff;
                font-size: 20px;
                font-weight: 500;
                ">Candidates History & Reports</h2>
            </div>
            <div id="content">
                <div class="fw-container">
                    <div class="fw-body">
                        <div class="content">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Created on</th>
                                        <th>Jobs</th>
                                        <th>Locations</th>
                                        <th>Candidates</th>
                                        <th>Status</th>
                                       
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Created on</th>
                                        <th>Jobs</th>
                                        <th>Locations</th>
                                        <th>Candidates</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer"></footer>
    </div>
</body>
</div>
	</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
