<?php
/* Smarty version 3.1.30, created on 2019-07-15 15:14:37
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/translations-edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2c8a4d9b36d0_60632375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffcc971419befc6960aae3a2dbe23132d9828e26' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/translations-edit.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d2c8a4d9b36d0_60632375 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_cycle')) require_once '/home3/fninpor1/public_html/jobboard/_lib/smarty/libs/plugins/function.cycle.php';
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<style>
.table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th,.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th  {
	border: none !important;
}
</style>


	<div class="admin-content">
	<div class="admin-wrap-content">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<label class="admin-label">
			Edit Translations for:&nbsp;&nbsp;
			<select id="translations-lang" class="minput form-control">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'lang');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value) {
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
"<?php if ($_smarty_tpl->tpl_vars['lang']->value['code'] == $_smarty_tpl->tpl_vars['current_lang']->value) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['name'];?>
</option>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</select>
		</label>
		<br /><br />
		
		<a href="#" class="new-item" id="new-translation-section-trigger"><i class="fa fa-plus-circle blueColor mr5" aria-hidden="true"></i>&nbsp;Add new section</a>
		
		<div id="new-translation-section"><br />
			<form>
			 		<strong>Name</strong><br />
					<input class="minput form-control" type="text" id="new-section-item" size="17" />
					<div class="mt15">
				 		<button id="add-section-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['current_lang_id']->value;?>
">Create</button>
						or <a href="#" id="cancel-add-section-trigger">Cancel</a>
					</div>
			</form>
		</div><br /><br />
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="list translations">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['translations_raw']->value, 'tr_section');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tr_section']->value) {
?>
					<div class="row <?php echo smarty_function_cycle(array('values'=>'even-border, odd'),$_smarty_tpl);?>
">
						<div style="float:right;padding:10px"><a href="#" title="Delete this section" class="translation-section-delete" rel="<?php echo $_smarty_tpl->tpl_vars['tr_section']->value['id'];?>
"><i class="fa fa-trash fa-lg blueColor" aria-hidden="true"></i></a></div>

						<h4 class="genericBig"><span>Section:</span> <?php echo $_smarty_tpl->tpl_vars['tr_section']->value['section'];?>
</h4>
						<a href="#" class="new-item new-translation-trigger"><span title="Add new category" class="mr5 fui-plus-circle blueColor fs13" ></span>&nbsp;Add new item</a>
						
						<div class="new-translation mt15" id="new-translation-<?php echo $_smarty_tpl->tpl_vars['tr_section']->value['id'];?>
">
 							<form>
								<!-- <div class="table-responsive" style="border: none !important;">
									<table class="table">
										<tr><th>Item</th><th>Value</th><th>&nbsp;</th></tr>
										<tr>
											<td><input type="text" class="form-control minput new-translation-item" size="20" /></td>
											<td><input type="text" class="form-control minput new-translation-value" size="40" /></td>
										</tr>
										<tr><td style="padding: 10px;"><button class="new-translation-add-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['tr_section']->value['id'];?>
">Add</button> or <a href="#" class="new-transaction-cancel-trigger">Cancel</a></td></tr>
									</table>
								</div> -->
								<div class="row-fluid">
									<div class="col-lg-3 itemSibling">
										Item <br />
										<input type="text" class="form-control minput new-translation-item" size="20" />
									</div>
									<div class="col-lg-3 valueSibling">
									    Value<br />
										<input type="text" class="form-control minput new-translation-item" size="20" />
									</div>

									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt15">
									<button class="new-translation-add-trigger" rel="<?php echo $_smarty_tpl->tpl_vars['tr_section']->value['id'];?>
">Add</button> or <a href="#" class="new-transaction-cancel-trigger">Cancel</a></div>
								</div>
							</form> 
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt15">
						<div class="table-responsive">
							<table width="100%" id="translations_<?php echo $_smarty_tpl->tpl_vars['tr_section']->value['id'];?>
" class="table">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tr_section']->value['items'], 'tr_item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tr_item']->value) {
?>
								<tr>
									<td valign="top"><strong><?php echo $_smarty_tpl->tpl_vars['tr_item']->value['item'];?>
:</strong></td>
									<td>
										<?php if ($_smarty_tpl->tpl_vars['tr_item']->value['field_type'] == 'textarea') {?>
										<textarea style="background-color: #f2f2f2; border: solid 1px #d9d9d9;border-radius: 3px;" cols="70" rows="10" rel="<?php echo $_smarty_tpl->tpl_vars['tr_item']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tr_item']->value['value']);?>
</textarea>
										<?php } else { ?>
										<input style="background-color: #f2f2f2; border: solid 1px #d9d9d9;border-radius: 3px;" id="item-<?php echo $_smarty_tpl->tpl_vars['tr_item']->value['id'];?>
" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tr_item']->value['value']);?>
" size="70" rel="<?php echo $_smarty_tpl->tpl_vars['tr_item']->value['id'];?>
" />
										<?php }?>
										 &nbsp;<a href="#" title="Delete this item" class="translation-item-delete" rel="<?php echo $_smarty_tpl->tpl_vars['tr_item']->value['id'];?>
"><i class="ml5 fa fa-trash fa-lg blueColor" aria-hidden="true"></i></a>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</table>
						</div>
						</div>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</div>
		</div>
		</div>
	</div>
	</div>
		
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
