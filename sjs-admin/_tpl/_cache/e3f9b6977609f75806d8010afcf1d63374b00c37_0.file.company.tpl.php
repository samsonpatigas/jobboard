<?php
/* Smarty version 3.1.30, created on 2019-07-15 09:18:38
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/company.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2c36de7e9066_06993037',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3f9b6977609f75806d8010afcf1d63374b00c37' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/company.tpl',
      1 => 1542710013,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d2c36de7e9066_06993037 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<label class="admin-label">Practice & Listing details</label>
	<div class="subheading">You can log into employer's dashboard and manage the account on his behalf<?php if ($_smarty_tpl->tpl_vars['data']->value['emp_id'] == '144') {?><div class="positive-feedback green">Admin profile works with DATA FEEDER. Is not listed publicly and cannot be deleted.<?php }?></div><a href="/sjs-admin/companies"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a></div>
	</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
	<?php if ($_smarty_tpl->tpl_vars['data']->value) {?>

		<ul class="list-group">
		  <li class="list-group-item"><strong>Employer email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
</li>
		  <li class="list-group-item"><strong>Employer name: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['company_name'];?>
</li>
		 <!--  <li class="list-group-item"><strong>Company name: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['company_name'];?>
</li> -->
		  <li class="list-group-item"><strong>Account status: </strong>&nbsp;
		  <?php if ($_smarty_tpl->tpl_vars['data']->value['confirmed'] == '1') {?>
		  	<span class="green">activated</span>
		  <?php } else { ?>
		    <span class="red">deactivated</span>
		  <?php }?></li>

		  <?php if ($_smarty_tpl->tpl_vars['PROFILE_PLUGIN']->value == '1' && $_smarty_tpl->tpl_vars['PAYPAL_PLUGIN']->value == '1' && $_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '2') {?>
			  <li class="list-group-item"><strong>
			  	Resume database access: </strong>&nbsp;
				  <?php if ($_smarty_tpl->tpl_vars['data']->value['cvdb_access'] == '1') {?>
				  	<a class="mr10 mb3" id="cvdb-icon-activated" href="javascript:void(0);" onclick="Jobber.DeactivateCvdb('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cvdb-deactivate/', <?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cvdb-activate/');" title="Remove access"><i class="fa fa-circle fa-lg green-fa" aria-hidden="true"></i></a>
				  <?php } else { ?>
				  	<a  class="mr10 mb3" id="cvdb-icon-deactivated" href="javascript:void(0);" onclick="Jobber.ActivateCvdb('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cvdb-activate/', <?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cvdb-deactivate/');" title="Grant access"><i class="fa fa-circle-o fa-lg green-fa" aria-hidden="true"></i></a>
				  <?php }?>
			  </li>
		  <?php }?>

		   <?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '3') {?>
		   	<li class="list-group-item"><strong>Package plan: </strong>&nbsp;<strong class="bl"><?php echo $_smarty_tpl->tpl_vars['package_data']->value['name'];?>
</strong> - 
		   		<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
company/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
/plan-management" title="Manage employer's package plan" style="opacity: 0.5;">[manage]</a>
		   	</li>
		   <?php }?>

		   <?php if ($_smarty_tpl->tpl_vars['data']->value['confirmed'] == '1') {?>
		  <li class="list-group-item"><a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
login-as/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['employer_name'];?>
">&nbsp;Login in as this employer &rarr;</a></li>
		   <?php }?>
		</ul>

	<?php }?>
</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	<?php if ($_smarty_tpl->tpl_vars['data']->value['confirmed'] == '1') {?>
		<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
company/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
/unconfirm" title="Unconfirm account">
			<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">DEACTIVATE ACCOUNT</button>
		</a>
	<?php } else { ?>
		<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
company/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
/confirm" title="Confirm account">
			<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">ACTIVATE ACCOUNT</button>
		</a>
	<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['data']->value['emp_id'] != '144') {?>
		<a class="ml20Desk" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
company/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
/delete" title="Delete company" onclick="if(!confirm('Are you sure you want to delete this company?'))return false;"><button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">DELETE COMPANY</button></a>
		<?php }?>

		<form method="POST" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
company/<?php echo $_smarty_tpl->tpl_vars['data']->value['emp_id'];?>
">
		<div class="row" style="margin-top: 30px;">
		<div class="col-md-6">
			<input required name="email" id="email" maxlength="400" type="email" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
" class="form-control minput"  />
			<button name="update_email" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">Update Email</button>
		</div>
		<div class="col-md-6">
			
			<input name="pass" id="pass" maxlength="400" type="Password" class="form-control minput"  placeholder="Enter new Password" />
			<span toggle="#pass" class="fa fa-fw fa-eye field-icon toggle-password" style="float: right;margin-right: 160px;margin-top: -28px;position: relative;z-index: 2;"></span>
			<button name="update_pass" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">Update Password</button>
		</div>
		</div>
	</form>
</div>


<?php if ($_smarty_tpl->tpl_vars['PROFILE_PLUGIN']->value == '1') {?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt15">
	<label style="text-transform: uppercase; margin-top: 10px;">Invoices for this company</label>
	<hr />
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
		<div class="table-responsive job-table">
			<table class="table table-striped table-condensed manage-jobs-table normal-gray">
			    <thead>
			      <tr>
			        <th><strong><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['date_column'];?>
</strong></th>
			        <th><strong><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['invoice_column'];?>
</strong></th>
			      </tr>
			    </thead>
			    <tbody>
			      <?php if (!empty($_smarty_tpl->tpl_vars['invoices']->value)) {?>
				      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['invoices']->value, 'invoice');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['invoice']->value) {
?>
				      <tr>
				      	<td><?php echo $_smarty_tpl->tpl_vars['invoice']->value['date'];?>
</td>
				      	<td><a href="<?php echo $_smarty_tpl->tpl_vars['invoice']->value['path'];?>
" target="_blank"><i class="fa fa-file-pdf-o fa-lg pdf-el" aria-hidden="true"></i></a></td>
				      </tr>
				      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			      <?php } else { ?>
			      	   #there are no invoices for this company
			      	   <br /><br />
			      <?php }?>
			    </tbody>
			</table>
		</div>
	</div>
<?php }?>

</div>
</div>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(".toggle-password").click(function() {
		  $(this).toggleClass("fa-eye fa-eye-slash");
		  var input = $($(this).attr("toggle"));
		  if (input.attr("type") == "password") {
		    input.attr("type", "text");
		  } else {
		    input.attr("type", "password");
		  }
		});
	<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
