<?php
/* Smarty version 3.1.30, created on 2019-07-01 14:00:06
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/types.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d1a03d6426df6_15977611',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ee9b7677aaa38b89f094ad5f00219b827a3c3f00' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/types.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d1a03d6426df6_15977611 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div class="admin-content">
		  <div class="admin-wrap-content" >
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label class="admin-label">Job types</label>
					<div class="subheading">Write Permalink in lowercase as a single word (eg. "part-time").</div>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div id="typesContainer">
					<?php
$__section_tmp_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp'] : false;
$__section_tmp_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['types']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_tmp_0_total = $__section_tmp_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = new Smarty_Variable(array());
if ($__section_tmp_0_total != 0) {
for ($__section_tmp_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] = 0; $__section_tmp_0_iteration <= $__section_tmp_0_total; $__section_tmp_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']++){
?>
						<div class="typeItem" rel="<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]->getId();?>
">
							<div class="typeWrapper">

								<a href="#" title="Delete job type" class="deleteType"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
								<label><span class="gray">Name:</span><input class="light-text minput" type="text" size="25" name="name[<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]->getId();?>
]" value="<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]->getName();?>
" /></label>
								<a href="#" title="Save changes" class="saveType mtSave"><i class="fa fa-save fa-lg blue-fa" aria-hidden="true"></i></a>

								<label class="sec-column"><span class="gray typesPadding">Permalink:</span><input class="light-text minput" type="text" size="25" id="nr" name="var_name[<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]->getId();?>
]" value="<?php echo $_smarty_tpl->tpl_vars['types']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)]->getVarName();?>
" /></label>
							</div>
						</div>
					<?php
}
}
if ($__section_tmp_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = $__section_tmp_0_saved;
}
?>
					</div>
				<p></p>
				<p>
					<a href="#" title="Add new type"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a></p>
				</div>
		 </div><!-- #content -->
		</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
