<?php
/* Smarty version 3.1.30, created on 2018-10-11 14:46:15
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/pages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbf5427105b55_03651068',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7259fc779b8ae220025fc4a4f3093f197cc38a21' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/pages.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bbf5427105b55_03651068 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div class="admin-content">
		 <div class="admin-wrap-content" style="padding-right: 35px !important;">
			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobMargin">
				<label class="admin-label">Pages</label>
			</div>
			<br />

			<div class="ml15">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="list">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
?>
					<div class="row settings-row p15" >
						<div class="icons">
							<a class="mr5" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
/" target="_blank" title="View this page"><i class="fa fa-eye fa-lg mr5" aria-hidden="true"></i></a>
							<a class="mr5" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/edit/<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
/" title="Edit this page"><i class="fa fa-gear fa-lg mr5" aria-hidden="true"></i></a>

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/delete/<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
/" title="Delete this page" onclick="if(!confirm('Are you sure you want to delete this page? Make CONTENT BACKUP before as you might loose it'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
						</div>
						<span class="dark-font"><?php echo $_smarty_tpl->tpl_vars['page']->value['page_title'];?>
</span>  - <span><?php if ($_smarty_tpl->tpl_vars['page']->value['is_external'] == '0') {?>( <?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
 )<?php } else { ?>( <?php echo $_smarty_tpl->tpl_vars['page']->value['external_url'];?>
 )<?php }?> </span>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<p style="margin: 20px -10px;">
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/add/" title="Add a new page" style="margin-top:"><span title="Add new page" class="blueColor fui-plus-circle iconSize" ></span></a>
				</p>
				</div>
			</div></div>

		</div><!-- #content -->
	</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if ($_smarty_tpl->tpl_vars['updated']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Page has been updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['deleted']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Page has been deleted');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['added']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('New page has been created');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }
}
}
