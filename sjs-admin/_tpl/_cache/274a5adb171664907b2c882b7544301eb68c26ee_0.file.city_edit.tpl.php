<?php
/* Smarty version 3.1.30, created on 2019-04-29 14:14:54
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/city_edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cc6f8cee6c2c8_02272436',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '274a5adb171664907b2c882b7544301eb68c26ee' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/city_edit.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cc6f8cee6c2c8_02272436 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="admin-content">
	 <div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">Locations - <?php if ($_smarty_tpl->tpl_vars['action']->value == 'add') {?>New location<?php } else { ?>"Edit <?php echo $_smarty_tpl->tpl_vars['city']->value['name'];?>
"<?php }?></label>
			<div class="subheading" >Write Permalink in lowercase as a single word (eg. "UK", or "united-kingdom"). ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/">&larr;go back</a> )</div>
			<br />
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<form id="city_form" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/<?php if ($_smarty_tpl->tpl_vars['action']->value == 'add') {?>add/<?php } else { ?>edit/<?php }?>" method="post">
				<div class="row-fluid">
					<div class="left window">
						<div class="block_inner">
							<div class="group<?php if ($_smarty_tpl->tpl_vars['errors']->value['name']) {?> error<?php }?> mb15">
								<label>Name</label>
								<input type="text" class="form-control minput" name="name" value="<?php echo $_smarty_tpl->tpl_vars['city']->value['name'];?>
" />
								<?php if ($_smarty_tpl->tpl_vars['errors']->value['name']) {?><span class="suggestion">Location name can not be empty</span>
								<?php }?>
							</div>
							<div class="group<?php if ($_smarty_tpl->tpl_vars['errors']->value['asciiName']) {?> error<?php }?>">
								<label>Permalink</label>
								<input type="text" class="form-control minput" name="ascii_name" value="<?php echo $_smarty_tpl->tpl_vars['city']->value['ascii_name'];?>
"/>
								<?php if ($_smarty_tpl->tpl_vars['errors']->value['asciiName']) {?><span class="suggestion"><?php echo $_smarty_tpl->tpl_vars['errors']->value['asciiName'];?>
</span>
								<?php }?>
							</div>
							<div class="button-holder-admin mt30" style="margin-left: 0px;">
								<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="save"><?php if ($_smarty_tpl->tpl_vars['action']->value == 'add') {?>Save entry<?php } else { ?>Save changes<?php }?></button>
								<?php if ($_smarty_tpl->tpl_vars['action']->value == 'edit') {?><input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['city']->value['id'];?>
" /><?php }?>
								
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
