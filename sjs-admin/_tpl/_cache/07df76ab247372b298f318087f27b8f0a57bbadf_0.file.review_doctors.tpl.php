<?php
/* Smarty version 3.1.30, created on 2019-07-18 20:56:23
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/review_doctors.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30cee71b4e52_28846542',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '07df76ab247372b298f318087f27b8f0a57bbadf' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/review_doctors.tpl',
      1 => 1543367943,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d30cee71b4e52_28846542 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="admin-content">
 <div class="admin-wrap-content">
 	<div class="row">
	
		<!-- <div class="col-sm-10" style="padding: 1em 48px;margin: 20px 0px;box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;">
			<label class="admin-label">Review Jobs</label>
			<div class="subheading">Publish or unpublish jobs</div>
		</div> -->
	<!-- <br><br> -->
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 ">
			<form method="POST">


					<label class="admin-label">Filter by Publish: </label>
					<div class="checkbox">
						<button name="published" id="published" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['publish']->value;?>
" type="submit">Published</button>
				      <!-- <label><input name="published[]" id="published" class="published" type="checkbox" value="1">Published</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="notpublished" id="notpublished" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['notpublish']->value;?>
" type="submit">Not Published</button>
				      <!-- <label><input name="published[]" id="published" class="published" type="checkbox" value="0">Not Published</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="allpublished" id="allpublished" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['allpublish']->value;?>
" type="submit">All</button>
				      <!-- <label><input name="allpublished" id="allpublished" type="checkbox" value="">All</label> -->
				    </div>
			
					<label class="admin-label">Filter by Review: </label>
					<div class="checkbox">
					<button name="reviewed" id="reviewed" class="btn btn-default btn-primary mbtn" type="submit" style="<?php echo $_smarty_tpl->tpl_vars['reviewed']->value;?>
">Reviewed</button>
				      <!-- <label><input name="reviewed[]" id="reviewed" class="reviewed" type="checkbox" value="1">Reviewed</label> -->
				    </div>
				    <div class="checkbox">
				     <button name="notreviewed" id="notreviewed" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['notreviewed']->value;?>
" type="submit">Not Reviewed</button>
				      <!-- <label><input name="reviewed[]" id="notreviewed" class="reviewed" type="checkbox" value="0">Not Reviewed</label> -->
				    </div>
				    <div class="checkbox">
				    	<button name="allreviewed" id="allreviewed" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['allreviewed']->value;?>
" type="submit">All</button>
				      <!-- <label><input name="allreview" id="allreview" type="checkbox" value="">All</label> -->
				    </div>
				    
				    <label class="admin-label">Filter by Search: </label>
				    <input name="search" id="search" type="text" class="mbtn" placeholder="Search" value="<?php echo $_smarty_tpl->tpl_vars['searchtext']->value;?>
">
				     <div class="checkbox"> 
				    	<button name="btnsearch" id="btnsearch" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['btnsearch']->value;?>
" type="submit">Search</button>
				    </div>
			
			</form>
	</div>

		<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
<!-- ADDED			<form method="POST"> -->
			<ul class="applicants-list ">  
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['companies']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				    <li class="p40"> 
				    	<span><span class="<?php if ($_smarty_tpl->tpl_vars['companie']->value['review_status'] == '1') {?>green <?php } else { ?>red <?php }?>" style="<?php if ($_smarty_tpl->tpl_vars['companie']->value['is_Seen'] == '0') {?>font-weight: bold;<?php } else { ?> <?php }?>"><?php echo $_smarty_tpl->tpl_vars['companie']->value['title'];?>
</span>&nbsp;/&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['name'];?>
</span><br>
						    <i><?php echo $_smarty_tpl->tpl_vars['companie']->value['email'];?>
</i>
				    <div style="float:right;">
						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
reviews/<?php echo $_smarty_tpl->tpl_vars['companie']->value['id'];?>
"><button type="submit" class="btn btn-default btn-primary mbtn" style="width: 85px !important; background-color: #E74C3C">Details</button></a>
					</div> 
					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</ul>
			<br />
<!-- 		</form>
	name="up_details" id="up_details"
		END -->
			<div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['deletedPopup']->value == 'true') {?>
      <?php echo '<script'; ?>
 type="text/javascript">
       setTimeout(function(){
       	jobberBase.messages.add('Candidate has been notified and deleted');
       }, 1000);
      <?php echo '</script'; ?>
>
    <?php }?>
 </div>
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


  
	<?php echo '<script'; ?>
 type="text/javascript">
	  $('.reviewed').on('change', function() {
	   if($('.reviewed:checked').length > 1) {
	       this.checked = false;
	   }
	});
	  <?php echo '</script'; ?>
>
  

    
	<?php echo '<script'; ?>
 type="text/javascript">
	  $('.published').on('change', function() {
	   if($('.published:checked').length > 1) {
	       this.checked = false;
	   }
	});
	  <?php echo '</script'; ?>
>
  <?php }
}
