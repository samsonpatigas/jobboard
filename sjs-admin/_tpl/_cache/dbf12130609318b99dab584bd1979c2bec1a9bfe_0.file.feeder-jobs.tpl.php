<?php
/* Smarty version 3.1.30, created on 2018-12-03 00:28:25
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-jobs.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c0478a9155935_35698851',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dbf12130609318b99dab584bd1979c2bec1a9bfe' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-jobs.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c0478a9155935_35698851 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content " >
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label">POST A JOB</label>
		<a href="/sjs-admin/feeder"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a><br />
		<div class="alert alert-info fade in main-color">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		     <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		    Jobs are posted by the "Admin company". Change and customize for your project.
		</div>
		<?php if ($_smarty_tpl->tpl_vars['EMP_MISSING_ERR']->value) {?>
		<div class="negative-feedback red">Administration company and employer profile is missing ! Please create employer with name "Admin" and a company
		associated with him. That profile will be owner of the jobs posted.</div>
		<?php }?>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	 <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/jobs" enctype="multipart/form-data" >

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mb20 deskPr100">
			<!-- job types-->
			<div class="form-group grayLabel mb40">
				<label for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_type_label'];?>
</label>
				<select id="type_select" name="type_select" class="form-control minput">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['types']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
						<option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['value']->value;
$_prefixVariable1=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['type_name'] == $_prefixVariable1) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</select>
			</div>

			<!-- catgory-->
			<div class="form-group grayLabel mb40">
				<label for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_category_label'];?>
</label>
				<select id="cat_select" name="cat_select" class="form-control minput">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cats']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
						<option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['id']->value;
$_prefixVariable2=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['category_id'] == $_prefixVariable2) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</select>
			</div>

			<div class="form-group grayLabel mb40">
				<label for="description"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_location_label'];?>
</label>
				<select id="location_select" name="location_select" class="form-control minput">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
					<option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['id']->value;
$_prefixVariable3=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['city_id'] == $_prefixVariable3) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</select>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mb20 deskPr100">
			<div class="form-group grayLabel mb40">
				<label for="title"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_title_label'];?>
</label>
				<input required name="title" id="title" maxlength="400" type="text" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['title'];
}?>" class="form-control minput"  />
			</div>

			<div class="form-group grayLabel mb40">
				<label for="salary"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_label'];?>
<span class="italic"> <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_desc'];?>
</span></label>
				<input name="salary" id="salary" type="text" class="form-control minput" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['salary'];
}?>"  />
			</div>

			<div class="form-group mb30 grayLabel mb40">
				<label for="premium_select"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['premium_text'];?>
 </label>
				<select id="premium_select" name="premium_select" class="form-control minput">
						<option value="0"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['standard_label'];?>
</option>
						<option value="1">Premium</option>
				</select>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mlpl0">
				<div class="form-group mb20">
					<label for="description"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_desc_label'];?>
 (*)</label>
					<textarea id="description" name="description"><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mlpl0">
		 		<div class="form-group mb20 ">
				 	<input type="checkbox" onchange="applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" checked /><label style="margin-left: 10px;" class="switch-label mt25"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_label'];?>
</label><span class="apply-desc-span"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_desc'];?>
</span>
				 </div>

				 <div id="apply-desc-block" class="form-group mb20  displayNone" >
					<label class="green"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['howto_apply_label'];?>
</label>
					<input id="howtoTA" class="form-control minput" rows="5" cols="5" id="howtoapply" name="howtoapply" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
"></input>
				 </div>

			<button type="submit" class="btn btn-default btn-primary mbtn mt30" onclick="return Jobber.validatePasswords();" >POST</button>

			 </div>
			 <br /><br />
		</div>

	</form>
	</div>
 </div>
</div><!-- #content -->


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function(){
		var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
		tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 300, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });
	});

	function applyChanged(val) {
		if (val == false) {
			$('#apply-desc-block').removeClass('displayNone');
		} else {
			$('#howtoTA').val('');
			$('#apply-desc-block').addClass('displayNone');
		}
	}

	function validateDesc() {
		if (tinymce.activeEditor.getContent() == "") {
			alert("Please, fill in the job description");
			return false;
		} else return true;
	}

<?php echo '</script'; ?>
>
	

<?php if ($_smarty_tpl->tpl_vars['JOB_ADDED']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Job has been posted');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
