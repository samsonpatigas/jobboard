<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:14:47
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/edit-post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30d337588d77_45046058',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e6211ba9f540d6f023f6392f6faa39bae41eb24' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/edit-post.tpl',
      1 => 1562686883,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d30d337588d77_45046058 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    <div class="admin-content">
       <div class="admin-wrap-content">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <label class="admin-label">
        Edit job        
      </label>
      <div>
        Date Posted: <?php echo $_smarty_tpl->tpl_vars['job']->value['f9_date_posted'];?>

        
      </div>
      <span class="back-area" >
        <a href="<?php echo $_smarty_tpl->tpl_vars['HTTP_REFERER']->value;?>
" style="float: right;"><button type="button" class="btn btn-default back-button" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</button></a>
       </span>
    </div>

    <br />

    <div class="container-fluid ">

      <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
post-edited/" role="form">
        <input type="hidden" id="job_id" name="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" />
        <input type="hidden" id="referer" name="referer" value="<?php echo $_smarty_tpl->tpl_vars['referer']->value;?>
" />
        <input name="f9_date_posted" id="f9_date_posted" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_date_posted'];?>
" />
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb20">

          <!-- catgory-->
          <div class="form-group mb20">
            <label class="grayLabel" for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_type_label'];?>
</label>
            <select id="type_select" name="type_select" class="form-control minput">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['types']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <option <?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['job']->value['type_name']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </select>
          </div>

          <!-- job types-->
          <div class="form-group mb20">
            <label class="grayLabel" for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_category_label'];?>
</label>
            <select id="cat_select" name="cat_select" class="form-control minput">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cats']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['category_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </select>
          </div>

          <?php if ($_smarty_tpl->tpl_vars['remote_portal']->value == 'deactivated') {?>
            <div class="grayLabel form-group mb20">
              <label for="description">Location</label>
              <select id="location_select" name="location_select" class="form-control minput">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['city_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

              </select>
            </div>
          <?php }?>

          <div class="grayLabel form-group mb20">
            <label for="city">City</label>
            <input required name="f9_city" id="f9_city" maxlength="400" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_city'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Service Type: </label>
              <select id="f9_service_type" name="f9_service_type" class="form-control minput">
                <option value="Desert Dental Staffing Job Board Posting" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_service_type'] == 'Desert Dental Staffing Job Board Posting') {?>selected<?php }?>>Desert Dental Staffing Job Board Posting</option>
                <option value="Concierge Service" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_service_type'] == 'Concierge Service') {?>selected<?php }?>>Concierge Service</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Post Period: </label>
              <select id="f9_post_peroid" name="f9_post_peroid" class="form-control minput">
                <option value="30" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == '30') {?>selected<?php }?>>30 days</option>
                <option value="60" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == '60') {?>selected<?php }?>>60 days</option>
                <option value="90" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == '90') {?>selected<?php }?>>90 days</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Spanish Bilingual: </label>
              <select id="f9_bilingual" name="f9_bilingual" class="form-control minput">
                <option value="Yes" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_bilingual'] == 'Yes') {?>selected<?php }?>>Yes</option>
                <option value="No" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_bilingual'] == 'No') {?>selected<?php }?>>No</option>
              </select>
          </div>

          <div class="grayLabel form-group mb20">
            <label for="city">Expiry: </label>
            <input required name="expires" id="expires" maxlength="400" type="text" class="form-control minput" value="<?php echo date("Y/m/d",$_smarty_tpl->tpl_vars['job']->value['expires']);?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label for="skills">Skills: </label>
            <label style="display: block;">       
              <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Custom Temps') !== false) {?>checked<?php }?> />
              <span style="line-height: 2;">Custom Temps</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Cerec System') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Cerec System</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Surgical Implants') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Surgical Implants</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'X-ray Certified in AZ') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">X-ray Certified in AZ</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Coronal Polish Certified in AZ') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Coronal Polish Certified in AZ</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'IV Sedation') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">IV Sedation</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Insurance Processing') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Insurance Processing</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Treatment Presentation') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Treatment Presentation</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Anesthesia Certified') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Anesthesia Certified</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'AR') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">AR</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'Laser Certified') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">Laser Certified</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_skills'],'EFDA (Certified Assistant in Arizona)') !== false) {?>checked<?php }?>/>
              <span style="line-height: 2;">EFDA (Certified Assistant in Arizona)</span>                  
            </label>
          </div>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="grayLabel form-group mb20">
            <label for="title"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_title_label'];?>
</label>
            <input required name="title" id="title" maxlength="400" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label for="salary"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_label'];?>
</label>
            <input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="salary" id="salary" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Positions: </label>
              <select id="f9_position" name="f9_position" class="form-control minput">
                  <option value="Hygienist" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Hygienist') {?>selected<?php }?>>Hygienist</option>
                  <option value="Dental Assistant" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Dental Assistant') {?>selected<?php }?>>Dental Assistant</option>
                  <option value="Cross-trained (Front Office/Dental Assistant)" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Cross-trained (Front Office/Dental Assistant)') {?>selected<?php }?>>Cross-trained (Front Office/Dental Assistant)</option>
                  <option value="Front Office" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Front Office') {?>selected<?php }?>>Front Office</option>
                  <option value="Dentist" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Dentist') {?>selected<?php }?>>Dentist</option>
              </select>
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Practice Type: </label>
              <select id="f9_practice_type" name="f9_practice_type" class="form-control minput">
                <option value="General" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'General') {?>selected<?php }?>>General</option>
                <option value="Ortho" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Ortho') {?>selected<?php }?>>Ortho</option>
                <option value="Perio" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Perio') {?>selected<?php }?>>Perio</option>
                <option value="Oral Surgery" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Oral Surgery') {?>selected<?php }?>>Oral Surgery</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Position Type: </label>
              <select id="f9_position_type" name="f9_position_type" class="form-control minput">
                <option value="Permanent Full Time" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position_type'] == 'Permanent Full Time') {?>selected<?php }?>>Permanent Full Time</option>
                <option value="Permanent Part Time" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position_type'] == 'Permanent Part Time') {?>selected<?php }?>>Permanent Part Time</option>
              </select> 
          </div>
          <?php $_smarty_tpl->_assignInScope('arr_soft', explode(",",$_smarty_tpl->tpl_vars['job']->value['f9_office_software']));
?>    
          <?php $_smarty_tpl->_assignInScope('arr2_soft', array("Dentrix","EagleSoft","Open Dental","SoftDent","Dentimax"));
?>
          <?php $_smarty_tpl->_assignInScope('result', array_diff($_smarty_tpl->tpl_vars['arr_soft']->value,$_smarty_tpl->tpl_vars['arr2_soft']->value));
?>
          <div class="grayLabel form-group">
              <label for="date_posted">Software Experience: </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_office_software'],'Dentrix') !== false) {?>checked<?php }?>>
                <span style="padding-top: 5px;">Dentrix</span>  
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_office_software'],'EagleSoft') !== false) {?>checked<?php }?>>
                <span style="padding-top: 5px;">EagleSoft</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_office_software'],'Open Dental') !== false) {?>checked<?php }?>> 
                <span style="padding-top: 5px;">Open Dental</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_office_software'],'SoftDent') !== false) {?>checked<?php }?>> 
                <span style="padding-top: 5px;">SoftDent</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['job']->value['f9_office_software'],'Dentimax') !== false) {?>checked<?php }?>> 
                <span style="padding-top: 5px;">Dentimax</span>
              </label>
              <label style="display: block;">
                <span style="padding-top: 5px;">Others</span><br/>
                <input name="f9_office_software[]" value="<?php echo implode(",",$_smarty_tpl->tpl_vars['result']->value);?>
" class="checkbox-custom" type="text" />                
              </label>
          </div>

          <div class="grayLabel form-group ">
            <label>Hourly Range (Min):</label>
            <input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="f9_pay_min" id="f9_pay_min" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_pay_min'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label>Hourly Range (Max):</label>
            <input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="f9_pay_max" id="f9_pay_max" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_pay_max'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label>State :</label>
            <input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="f9_state" id="f9_state" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_state'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label>Zip Code :</label>
            <input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="f9_zip" id="f9_zip" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_zip'];?>
" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Years of experience: </label>
              <select id="f9_yrs_of_experience" name="f9_yrs_of_experience" class="form-control minput">
                <option value="New Graduate" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == 'New Graduate') {?>selected<?php }?>>New Graduate</option>
                <option value="6 Months – 2 Years" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '6 Months – 2 Years') {?>selected<?php }?>>6 Months – 2 Years</option>
                <option value="3 – 5 Years" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '3 – 5 Years') {?>selected<?php }?>>3 – 5 Years</option>
                <option value="6 Years Plus" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '6 Years Plus') {?>selected<?php }?>>6 Years Plus</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Gender: </label>
              <select id="f9_gender" name="f9_gender" class="form-control minput">
                <option value="Male" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_gender'] == 'Male') {?>selected<?php }?>>Male</option>
                <option value="Female" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_gender'] == 'Female') {?>selected<?php }?>>Female</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Specialties: </label>
              <select id="f9_specialties" name="f9_specialties" class="form-control minput">
                <option value="Dental Assistant" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == 'Dental Assistant') {?>selected<?php }?>>Dental Assistant</option>
                <option value="Hygienist" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == 'Hygienist') {?>selected<?php }?>>Hygienist</option>
                <option value="Front Office" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == 'Front Office') {?>selected<?php }?>>Front Office</option>
                <option value="Dentist" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == 'Dentist') {?>selected<?php }?>>Dentist</option>
              </select> 
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="description"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_desc_label'];?>
</label>
              <textarea id="description" name="description"><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="admin_notes">Admin Notes:</label>
              <textarea id="f9_admin_notes" name="f9_admin_notes"><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_admin_notes'];?>
</textarea>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="position_notes">Position Notes:</label>
              <textarea id="f9_position_notes" name="f9_position_notes"><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_position_notes'];?>
</textarea>
            </div>
          </div>                   
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20">
              <input type="checkbox" onchange="applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?>checked<?php }?> />
              <label style="margin-left: 10px; margin-bottom" class="switch-label mt25 grayLabel"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_label'];?>
</label>
              <span class="apply-desc-span"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_desc'];?>
</span>
            </div>

             <div id="apply-desc-block" class="form-group mb20 <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?> displayNone<?php }?>" >
              <label class="green"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['howto_apply_label'];?>
</label>
              <input id="howtoTA" class="form-control minput" rows="5" cols="5" id="howtoapply" name="howtoapply" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
" />
             </div>
          </div>
          <br /><br />
        </div>
        <br />

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group mb20">
            <button  type="submit" onclick="//return validateDesc();" class="btn btn-default btn-primary mbtn" name="submit" id="submit" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['change_password_submit'];?>
</button>
            <a href="<?php echo $_smarty_tpl->tpl_vars['HTTP_REFERER']->value;?>
"><button type="button" class="right-btn btn btn-default btn-warning mbtn" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['cancel'];?>
</button></a>
          </div>
        </div>
      </form>
      <br />
      </div>
  </div><!-- /content -->
  </div>


<?php echo '<script'; ?>
 type="text/javascript">
  $(document).ready(function(){
    var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
    tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 300, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });
  });

  function applyChanged(val) {
    if (val == false) {
      $('#apply-desc-block').removeClass('displayNone');
    } else {
      $('#howtoTA').val('');
      $('#apply-desc-block').addClass('displayNone');
    }
  }

  function validateDesc() {
    if (tinymce.activeEditor.getContent() == "") {
      alert("Please, fill in the job description");
      return false;
    } else return true;
  }

<?php echo '</script'; ?>
>
    

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
