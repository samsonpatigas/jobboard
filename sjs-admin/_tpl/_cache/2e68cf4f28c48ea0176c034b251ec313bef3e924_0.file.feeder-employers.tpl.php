<?php
/* Smarty version 3.1.30, created on 2018-10-29 15:59:06
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-employers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd72e4a8554e7_77993249',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e68cf4f28c48ea0176c034b251ec313bef3e924' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-employers.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd72e4a8554e7_77993249 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content " >
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label">ADD EMPLOYERS</label>
		<a href="/sjs-admin/feeder"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a><br />
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	 <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/employers" enctype="multipart/form-data" >

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 deskPr100">
			<h4 class="general-headline gray-border-bottom">Company data</h4>

	 		<div class="form-group mb40">
			<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_name'];?>
" required name="company_name" id="company_name" maxlength="300" type="text" class="form-control grayInput minput "  />
			</div>

			<div class="form-group mb40">
				<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_hq_label'];?>
" required name="company_hq" id="company_hq" maxlength="300" type="text" class="form-control grayInput minput"  />
			</div>

			<div class="form-group mb40">
				<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_url_label'];?>
" name="company_url" id="company_url" maxlength="1000" type="text" class="form-control grayInput minput"  />
			</div>

			<div class="form-group mb40">
				<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_street_label'];?>
" name="company_street" id="company_street" maxlength="300" type="text" class="form-control grayInput minput"  />
			</div>

			<div class="form-group mb40">
			<input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_citypostcode_label'];?>
" name="company_citypostcode" id="company_citypostcode" maxlength="300" type="text" class="form-control grayInput minput"  />
			</div>

			<div class="form-group mb40">
				<label id="bannerLabel" for="company_logo"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_logo_label'];?>
</label>
			 	<input type="file" name="company_logo" id="company_logo" class="form-control inputfile minput" />
			 	<div class="textarea-feedback" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_logo_hint'];?>
</div>
			 	<div id="uploadPreview"></div>
			 	<div id="logo-err" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_msg'];?>
</div>
			 	<div id="logo-err2" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_samesize'];?>
</div>
			 	<div id="logo-ok" class="positive-feedback displayNone mt20"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_ok'];?>
</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  deskPr100">
			<h4 class="general-headline gray-border-bottom">Employer</h4>
	 		<div class="form-group mb40">
				<input placeholder="Employer name" name="register_name" id="register_name" maxlength="400" type="text" class="form-control grayInput minput"  />
				<div id="err-reg-name" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_name'];?>
</div>
			</div>
			<div class="form-group mb40">
				<input required placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['email'];?>
" name="register_email" id="register_email" maxlength="400" type="email" class="form-control grayInput minput"  />
				<div id="err-reg-email" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_email'];?>
</div>
			</div>

			<div class="form-group mb40">
				<input required placeholder="Password (*)" name="register_pass1" id="register_pass1" maxlength="50" type="password" class="form-control grayInput minput"  />
			</div>

			<div class="form-group mb40">
				<input required placeholder="Repeat password (*)" name="register_pass2" id="register_pass2" maxlength="50" type="password" class="form-control grayInput minput"  />
				<div id="err-reg-pass2" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_passes'];?>
</div>
			</div>

		</div>

			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="form-group mb40">
					<label>Company pages :</label>
					<textarea name="company_desc" id="company_desc" ></textarea>
					<div class="textarea-feedback" id="textarea_feedback"></div>
				</div>
			</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<button type="submit" class="btn btn-default btn-primary mbtn" onclick="return Jobber.validatePasswords();">SUBMIT</button>
		</div>

	</form>
	</div>
 </div>
</div><!-- #content -->


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function() {

		var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
		tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 300, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });

		$('#company_logo').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#bannerLabel').html(fname);
			else
				$('#bannerLabel').html($('#bannerLabel').html());
        });
	});
<?php echo '</script'; ?>
>


<?php if ($_smarty_tpl->tpl_vars['COMPANY_ADDED']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Employer has been added to the system');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
