<?php
/* Smarty version 3.1.30, created on 2019-06-03 09:58:31
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/payment-settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cf4e137373aa0_83308697',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '935ab2f33806daaa0f13eb7a35003616c0479197' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/payment-settings.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cf4e137373aa0_83308697 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">
				Website Payments
			</label>

			<div class="subheading mt10" id="mode-desc">
				<?php if ($_smarty_tpl->tpl_vars['active_mode_id']->value == 1) {?>
				Activate <strong>free</strong> mode. Companies can post jobs and access resume database without any restrictions.
				<?php } elseif ($_smarty_tpl->tpl_vars['active_mode_id']->value == 2) {?>
				Activate <strong>fees</strong> mode. After activation set up the prices in the "Fees Settings" section. Companies will be charged flat fees to post jobs, premium ads and access resume database.
				<?php } elseif ($_smarty_tpl->tpl_vars['active_mode_id']->value == 3) {?>
				Activate <strong>packages</strong> mode. After activation set up the pricing plans in the "Packages Settings" section. Each company will be assigned an account plan with pre-defined resources (job ads, job ad duration, resume downloads).
				<?php }?>
				Read more <a href="https://simplejobscript.com/paypal-and-invoices/" target="_blank">here</a> 
			</div>

			<hr />
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt15">

			<form id="psf" name="psf" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/mode" role="form" >
						<input type="hidden" id="old_mode_id" name="old_mode_id" value="<?php echo $_smarty_tpl->tpl_vars['active_mode_id']->value;?>
" />

						<div class="form-group">
								<label for="description">Current Payment Mode: &nbsp;</label>
								<select id="payment_mode_select" onchange="jobberBase.paymentModeChanged(this.value, <?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
);" name="payment_mode_select" class="form-control minput">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['modes']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
									<option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['active_mode_id']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

								</select>
						</div>

						<div class="form-group mt30">
						   <button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Activate</button>
						</div>

			</form>

		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt15" id="pm-btn-div">
				<?php if ($_smarty_tpl->tpl_vars['active_mode_id']->value == 2) {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/fees"><button type="submit" class="btn btn-default btn-primary mbtn alizarinBtn" >Fees Settings</button></a>
				<?php } elseif ($_smarty_tpl->tpl_vars['active_mode_id']->value == 3) {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/packages"><button type="submit" class="btn btn-default btn-primary mbtn alizarinBtn" >Packages Settings</button></a>
				<?php }?>
		</div>

    </div>
</div>


<?php if ($_smarty_tpl->tpl_vars['POPUP']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Payment Mode Activated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
