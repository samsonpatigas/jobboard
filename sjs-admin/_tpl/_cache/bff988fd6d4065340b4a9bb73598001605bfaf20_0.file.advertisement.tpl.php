<?php
/* Smarty version 3.1.30, created on 2019-04-29 14:14:39
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/advertisement.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cc6f8bfd1fda3_59701955',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bff988fd6d4065340b4a9bb73598001605bfaf20' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/advertisement.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cc6f8bfd1fda3_59701955 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
  <div class="admin-wrap-content" >
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20">
			<label class="admin-label">
				Banner manager
			</label>
			<div class="subheading">Recommended banner sizes: [listing, detail, employer-dashboard, candidate-dashboard] - squared (eg. 285x285), [leaderboard] - 728x90, [joblisting] - 480x90. Use only 1 banner within 1 area.</div>
		</div><br /><br />

	<br /><br />
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="table-responsive">
		<table class="table">
		    <thead>
		      <tr>
		      	<th>Banner</th>
		        <th>Area</th>
		        <th>Campaign</th>
		        <th>Created on</th>
		        <th>Status</th>
		        <th>Url</th>
		        <th>Clicks</th>
		        <th>Delete</th>
		      </tr>
		    </thead>
		    <tbody>
			    <?php if ($_smarty_tpl->tpl_vars['count']->value == 0) {?>
			    	<tr><td>No campaigns for the moment</td></tr>
			    <?php }?>
		    	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['campaigns']->value, 'obj', false, 'val');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['val']->value => $_smarty_tpl->tpl_vars['obj']->value) {
?>
		      	<tr>
		      		 <td><div class="listing-logo"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['obj']->value['banner_path'];?>
" alt="Ad banner" /></div></td>
		       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['area_name'];?>
</td>
		       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['c_name'];?>
</td>
		       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['date_formated'];?>
</td>
		       		 <?php if ($_smarty_tpl->tpl_vars['obj']->value['is_active'] == '1') {?>
		       		 <td><input class="ml10" type="checkbox" name="c_switch" id="c_switch" data-size="mini" checked onchange="jobberBase.changeCampaignStatus(this.checked, <?php echo $_smarty_tpl->tpl_vars['obj']->value['c_id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
change_campaign_ajax.php');"> </td>
		       		 <?php } else { ?>
		       		 <td><input class="ml10" type="checkbox" name="c_switch" id="c_switch" data-size="mini" onchange="jobberBase.changeCampaignStatus(this.checked, <?php echo $_smarty_tpl->tpl_vars['obj']->value['c_id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
change_campaign_ajax.php');"></td>
		       		 <?php }?>

		       		 <td><a href="http://<?php echo $_smarty_tpl->tpl_vars['obj']->value['url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['obj']->value['url'];?>
</a></td>
		       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['clicks'];?>
</td>
		       		 <td><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
advertisement/delete/<?php echo $_smarty_tpl->tpl_vars['obj']->value['c_id'];?>
" title="Delete this campaign" onclick="if(!confirm('Are you sure you want to delete this campaign?'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
		      	</tr>
		      	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		    </tbody>
		  </table>
	 </div>
	<br /><br />
	</div>

	<div class="col-lg-6 col-md-6">
	<label class="admin-label">
			Create new campaign
	</label><br /><br />

		<form id="campaign-form" name="campaign-form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
advertisement/create" role="form" enctype="multipart/form-data">


				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group mb30">
						<label for="area_select">Area</label>
					 	<select id="area_select" name="area_select" class="form-control minput">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['areas']->value, 'obj', false, 'val');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['val']->value => $_smarty_tpl->tpl_vars['obj']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['obj']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['obj']->value['name'];?>
</option>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</select>
					</div>
					<div class="form-group mb30">
						<label for="area_select">Name</label>
						<input placeholder="" required name="name" id="name" maxlength="50" type="text" class="form-control minput"  />
					</div>
					<div class="form-group mb30">
			 			<input class="mr15" type="checkbox" name="campaign_active_switch" id="campaign_active_switch" data-size="mini" checked><label style="margin-top: 0px; position: absolute;" class="switch-label">Activate campaign</label>
			 		</div>
			 	</div>

			 	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			 		<div class="form-group mb30">
						<label for="url">Url</label>
						<input required name="url" id="url" maxlength="100" type="text" class="form-control minput"  />
					</div>

			 		<div class="form-group mb30">
						<label id="bannerLabel" for="banner">Upload banner</label>
						<input required accept=".jpg,.jpeg,.png, .gif, .bmp" type="file" name="banner" id="banner" class="form-control inputfile minput" />
						<div class="subheading mt3" >Max 2MB. (jpg, png, gif, bmp)</div>
						<?php if ($_smarty_tpl->tpl_vars['FILE_ERR']->value == 'true') {?>
							<div class="negative-feedback">
								Invalid file extension.
							</div>
						<?php }?>
					</div>
				</div>	

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt20">
					<div class="form-group">
					   <button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Submit</button>
					</div>
				</div>
		</form>
	</div>
    
</div><!-- #content -->
</div>

<?php echo '<script'; ?>
 type="text/javascript">
	 $('#banner').change(function() {
		 var fname = $('input[type=file]').val().split('\\').pop();
		 if( fname )
			$('#bannerLabel').html(fname);
		 else
			$('#bannerLabel').html($('#bannerLabel').html());
		 });
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
