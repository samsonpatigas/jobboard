<?php
/* Smarty version 3.1.30, created on 2018-10-26 14:32:31
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/applicants.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd3176fccab58_79890450',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e17a761f2e76f4184e7639fb6fc13d5fe1f88df' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/applicants.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd3176fccab58_79890450 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<label class="admin-label">
			<?php if ($_smarty_tpl->tpl_vars['applicants_count']->value > 0) {
echo $_smarty_tpl->tpl_vars['applicants_count']->value;?>
 job applications<?php } else { ?>No job applications, yet<?php }?>
		</label>
	</div>

     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	    <div id="accordion-list"> 

			<ul class="applicants-list">  
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['applicants']->value, 'applicant', false, NULL, 'cvs', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['applicant']->value) {
?> 
				    <li> 

					    <div style="float:right; margin-left: 15px;">
					    	<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
job-applications/delete/<?php echo $_smarty_tpl->tpl_vars['applicant']->value['id'];?>
/" title="Delete this applicant" onclick="if(!confirm('Are you sure you want to delete this application?'))return false;"><i class="fa fa-trash fa-lg blueColor" aria-hidden="true"></i></a>
					    </div>

	                    <div style="float:right; margin-left: 15px;" title="Application message">
	                    	<a data-toggle="modal" data-target="#apply_modal_<?php echo $_smarty_tpl->tpl_vars['applicant']->value['id'];?>
" href="#" onclick="return false;"><i class="fa fa-address-card-o fa-lg blueColor" aria-hidden="true"></i></a>
						</div>

						<?php if ($_smarty_tpl->tpl_vars['applicant']->value['cv_path'] != '') {?>
				            <div style="float:right" title="Download cv"><a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['applicant']->value['cv_path'];?>
"><i class="<?php echo $_smarty_tpl->tpl_vars['cvs']->value[$_smarty_tpl->tpl_vars['applicant']->value['id']];?>
" aria-hidden="true"></i></a></div>
				        <?php }?>

				        <div style="float:right; margin-right: 10px;" title="Application message">
	                    	<?php if ($_smarty_tpl->tpl_vars['applicant']->value['status'] == 0) {?>
	                    		PENDING
	                    	<?php } elseif ($_smarty_tpl->tpl_vars['applicant']->value['status'] == 1) {?>
	                    	 	<span class="profile-rejected">REJECTED</span>
	                    	<?php } else { ?>
	                    		<span class="profile-reviewed">REVIEWED</span>
	                    	<?php }?>
						</div>

				        <span class="applicant-name"><?php echo $_smarty_tpl->tpl_vars['applicant']->value['name'];?>
</span>
				        <div style="font-size: 13px;">
				            <div class="gray">applied 
							<?php if ($_smarty_tpl->tpl_vars['applicant']->value['title']) {?>to <a style="font-size: 13px;margin: 0px 5px 0px 5px;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
job/<?php echo $_smarty_tpl->tpl_vars['applicant']->value['job_id'];?>
/"><span class="upc">"<?php echo $_smarty_tpl->tpl_vars['applicant']->value['title'];?>
"</span></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['applicant']->value['company']) {?> at <strong class="upc" style="color: #39b6b3; ">&nbsp;<?php echo $_smarty_tpl->tpl_vars['applicant']->value['company'];?>
</strong><?php }?>
				        </div>
				    </li>

					<div id="apply_modal_<?php echo $_smarty_tpl->tpl_vars['applicant']->value['id'];?>
" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Candidate profile</h4>
					      </div>
					      <div class="modal-body">
					      		<span class="modal-label">Applied on</span><br />
					      		<?php echo $_smarty_tpl->tpl_vars['applicant']->value['created_on'];?>

					      </div>
					      <div class="modal-body">
					      		<span class="modal-label">Email</span><br />
					      		<?php echo $_smarty_tpl->tpl_vars['applicant']->value['email'];?>

					      </div>

					      <?php if ($_smarty_tpl->tpl_vars['applicant']->value['phone']) {?>
					      <div class="modal-body">
					      		<span class="modal-label">Phone</span><br />
					      		<a href="tel:<?php echo $_smarty_tpl->tpl_vars['applicant']->value['phone'];?>
"><?php echo $_smarty_tpl->tpl_vars['applicant']->value['phone'];?>
</a>
					      </div>
					      <?php }?>

					      <?php if ($_smarty_tpl->tpl_vars['applicant']->value['website']) {?>
					   	  <div class="modal-body">
					      		<span class="modal-label">Website</span><br />
					      		<a href="http://<?php echo $_smarty_tpl->tpl_vars['applicant']->value['website'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['applicant']->value['website'];?>
</a>
					      </div>
					      <?php }?>

					      <?php if ($_smarty_tpl->tpl_vars['applicant']->value['sm_links']) {?>
					      <div class="modal-body">
					      		<span class="modal-label"><?php echo $_smarty_tpl->tpl_vars['translations']->value['js']['social_media_label'];?>
</span><br />
								 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['applicant']->value['sm_links'], 'SM_OBJ');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['SM_OBJ']->value) {
?>
									 	 <a class="mr12" href="<?php if ($_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp == 'true') {?>tel:<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp_numb;
} else {
echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->linkToShow;
}?>" target="_blank"><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->icon;?>
 fa-lg mt10" aria-hidden="true"></i></a>
								 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					      </div>
					      <?php }?>

					      <div class="modal-body">
					      		<span class="modal-label">Application message</span><br /><br />
					      		<?php echo $_smarty_tpl->tpl_vars['applicant']->value['message'];?>

					      </div>
					      <div class="modal-footer">
	        				<button style="float: left" type="button" class="btn btn-default btn-warning mbtn" data-dismiss="modal">Close</button>
	      				</div>
					    </div>

					  </div>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</ul>
			<div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>
	    </div>
    </div>
</div><!-- #content -->
</div>

<?php if ($_smarty_tpl->tpl_vars['deleted_popup']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Job application has been deleted');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
