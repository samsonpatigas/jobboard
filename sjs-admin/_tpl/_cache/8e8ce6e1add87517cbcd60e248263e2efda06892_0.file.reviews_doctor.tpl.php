<?php
/* Smarty version 3.1.30, created on 2019-07-18 20:56:29
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/reviews_doctor.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30ceed881d19_49270211',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e8ce6e1add87517cbcd60e248263e2efda06892' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/reviews_doctor.tpl',
      1 => 1543884272,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d30ceed881d19_49270211 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- MODAL FOR MORE DETAILS -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Job Post Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
				<ul class="list-group">
					
				  <li class="list-group-item"><strong>Type: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['type_name'];?>
</li>
				  <li class="list-group-item"><strong>Category: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['category_name'];?>
</li>
				  <li class="list-group-item"><strong>Location: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['city_name'];?>
</li>
				  <li class="list-group-item"><strong>Position: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_position'];?>
</li>
				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_city'];?>
,&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_state'];?>
</li>
				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_zip'];?>
</li>
				  <li class="list-group-item"><strong>Gender: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_gender'];?>
</li>
				  <li class="list-group-item"><strong>Language: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_language'];?>
</li>
				  <li class="list-group-item"><strong>Specialties: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_specialties'];?>
</li>
				  <li class="list-group-item"><strong>Experience: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_yrs_of_experience'];?>
</li>
				  <li class="list-group-item"><strong>Salary: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['salary'];?>
</li>
				  <li class="list-group-item"><strong>Post Period: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_post_peroid'];?>
&nbsp;days</li>
				  <li class="list-group-item"><strong>Short Description: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_short_description'];?>
</li>
				  <li class="list-group-item"><strong>Admin Notes: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_admin_notes'];?>
</li>
				  <li class="list-group-item"><strong>Expiry: </strong><span class="red">&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['expires'];?>
</span></li>

				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Job details</label>
				<div class="subheading"><a href="/sjs-admin/review_doctors"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
				<ul class="list-group">
				  <li class="list-group-item" style="text-align:center"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['companie']->value['company_logo_path'];?>
"></li>
				  <li class="list-group-item"><strong>Title: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['title'];?>
</li>
				  <li class="list-group-item"><strong>Created by: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['empname'];?>
</li>
				   <li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['email'];?>
</li>
				  <li class="list-group-item"><strong>Created on: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['created_on'];?>
</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;<?php if ($_smarty_tpl->tpl_vars['companie']->value['review_status'] == '0') {?><span class="red">Private</span><?php } else { ?><span class="green">Public</span><?php }?></li>
				  	<li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php if ($_smarty_tpl->tpl_vars['companie']->value['review_status'] == '0') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
reviews/publish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH JOB</button>
				</a>
			<?php } else { ?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
reviews/unpublish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH JOB</button>
				</a>
			<?php }?>
		</div>
	</div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
