<?php
/* Smarty version 3.1.30, created on 2019-07-15 06:56:07
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/job-details.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2c15773dc405_39721945',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '830c31a129ac3124bc2a1c8463d3ecd910e06b57' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/job-details.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d2c15773dc405_39721945 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_highlight_keywords')) require_once '/home3/fninpor1/public_html/jobboard/_lib/smarty/libs/plugins/modifier.highlight_keywords.php';
?>
<div id="job-details">
	<h3 class="blue-font">
	 <?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
 
	</h3>
	<p>
		<span class="fading">at</span>
		<?php if ($_smarty_tpl->tpl_vars['job']->value['url'] && $_smarty_tpl->tpl_vars['job']->value['url'] != 'http://') {?>
		<a target="_blank" href="http://<?php echo $_smarty_tpl->tpl_vars['job']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</a>
		<?php } else { ?>
		<strong><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</strong>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'activated') {?>
		<strong>(<?php echo $_smarty_tpl->tpl_vars['translations']->value['jobs']['location_anywhere'];?>
)</strong>
		<?php } else { ?>
		<span class="fading">in</span> <span class="dark-font" ><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</span>
		<?php }?>
	</p>
	<div id="job-description" class="container-fluid">
	<?php echo smarty_modifier_highlight_keywords($_smarty_tpl->tpl_vars['job']->value['description'],$_SESSION['keywords_array']);?>

	</div><br />
</div><!-- #job-details -->
<?php }
}
