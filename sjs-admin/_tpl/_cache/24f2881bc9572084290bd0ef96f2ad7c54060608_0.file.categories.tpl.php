<?php
/* Smarty version 3.1.30, created on 2018-10-11 14:45:58
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/categories.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbf54166ba0f3_18052372',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24f2881bc9572084290bd0ef96f2ad7c54060608' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/categories.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bbf54166ba0f3_18052372 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div class="admin-content">
			<div class="admin-wrap-content" >
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb25">
					<label class="admin-label">Categories</label>
					<div class="subheading">Drag and drop by green border handle to change the order. Title, Desc and Keyword = SEO Meta tags. Write URL in lowercase as a single word (eg. "mobile-development").</div>
				</div>
				<br /><br />

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div id="categoriesContainer">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
						<div class="categoryItem" rel="<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
							<div class="categoryHandle"></div>
							<div class="categoryWrapper">
								<div class="row-fluid">
									<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" >
										<label><span class="gray typesPadding">Name:</span><input class="ml5 form-control minput" type="text" size="60" name="name[<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>
" /></label>
										<label><span class="gray typesPadding">Title:</span><input class="form-control minput" type="text" size="60" name="title[<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['title'];?>
" /></label>
										<label><span class="gray typesPadding">Desc:&nbsp;</span><input class="ml5 form-control minput" type="text" size="60" name="desc[<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['description'];?>
"/></label>
										<label><span class="gray typesPadding">Keywords:</span><input class="ml5 form-control minput" type="text" size="60" name="keys[<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['keywords'];?>
" /></label>
										<label><span class="gray typesPadding">URL:</span><input class="ml5 form-control minput" type="text" size="60" name="url[<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['var_name'];?>
" /></label>
									</div>

									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
										<a href="#" title="Delete this category" class="deleteCategory"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<a href="#" title="Save changes" class="saveCategory mr10 mtSave"><i class="fa fa-save fa-lg blue-fa" aria-hidden="true"></i></a>
									</div>

								</div>
							</div>
						</div>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					</div>
				<p></p>
				<p>
					<a href="#" title="Add new category"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
				</div>
		</div><!-- #content -->
		</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
