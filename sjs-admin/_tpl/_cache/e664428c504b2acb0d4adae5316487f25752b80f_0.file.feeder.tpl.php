<?php
/* Smarty version 3.1.30, created on 2018-12-03 00:32:40
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c0479a82f4708_85348023',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e664428c504b2acb0d4adae5316487f25752b80f' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c0479a82f4708_85348023 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content ">
	<div class="admin-wrap-content " >
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label">DATA FEEDER</label>
		<div class="alert alert-info fade in main-color">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		     <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		  	Post new jobs, create employer & jobseeker profiles [jobseeker = premium plugin]
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb10 mlpl0">
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/jobs">
				<button type="button" class="mbtn btn btn-default alizarinBtn">POST JOBS</button>
			</a>
		</div>

		<?php if ($_smarty_tpl->tpl_vars['PROFILE_PLUGIN']->value == '1') {?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb10 mlpl0">
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/candidates">
				<button type="button" class="mbtn btn btn-default alizarinBtn">ADD JOBSEEKERS</button>
			</a>
		</div>
		<?php }?>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0 mb10">
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder/employers">
				<button type="button" class="mbtn btn btn-default alizarinBtn">ADD EMPLOYERS</button>
			</a>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder-settings">
				<button type="button" class="btn btn-default btn-primary mbtn">SETTINGS</button>
			</a>
		</div>
	</div>

</div><!-- #content -->
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
