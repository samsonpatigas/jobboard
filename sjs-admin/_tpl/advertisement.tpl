{include file="header.tpl"} 

<div class="admin-content">
  <div class="admin-wrap-content" >
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20">
			<label class="admin-label">
				Banner manager
			</label>
			<div class="subheading">Recommended banner sizes: [listing, detail, employer-dashboard, candidate-dashboard] - squared (eg. 285x285), [leaderboard] - 728x90, [joblisting] - 480x90. Use only 1 banner within 1 area.</div>
		</div><br /><br />

	<br /><br />
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="table-responsive">
		<table class="table">
		    <thead>
		      <tr>
		      	<th>Banner</th>
		        <th>Area</th>
		        <th>Campaign</th>
		        <th>Created on</th>
		        <th>Status</th>
		        <th>Url</th>
		        <th>Clicks</th>
		        <th>Delete</th>
		      </tr>
		    </thead>
		    <tbody>
			    {if $count == 0}
			    	<tr><td>No campaigns for the moment</td></tr>
			    {/if}
		    	{foreach item=obj key=val from=$campaigns}
		      	<tr>
		      		 <td><div class="listing-logo"><img src="{$BASE_URL}{$obj.banner_path}" alt="Ad banner" /></div></td>
		       		 <td>{$obj.area_name}</td>
		       		 <td>{$obj.c_name}</td>
		       		 <td>{$obj.date_formated}</td>
		       		 {if $obj.is_active == '1'}
		       		 <td><input class="ml10" type="checkbox" name="c_switch" id="c_switch" data-size="mini" checked onchange="jobberBase.changeCampaignStatus(this.checked, {$obj.c_id}, '{$BASE_URL}change_campaign_ajax.php');"> </td>
		       		 {else}
		       		 <td><input class="ml10" type="checkbox" name="c_switch" id="c_switch" data-size="mini" onchange="jobberBase.changeCampaignStatus(this.checked, {$obj.c_id}, '{$BASE_URL}change_campaign_ajax.php');"></td>
		       		 {/if}

		       		 <td><a href="http://{$obj.url}" target="_blank">{$obj.url}</a></td>
		       		 <td>{$obj.clicks}</td>
		       		 <td><a href="{$BASE_URL_ADMIN}advertisement/delete/{$obj.c_id}" title="Delete this campaign" onclick="if(!confirm('Are you sure you want to delete this campaign?'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
		      	</tr>
		      	{/foreach}
		    </tbody>
		  </table>
	 </div>
	<br /><br />
	</div>

	<div class="col-lg-6 col-md-6">
	<label class="admin-label">
			Create new campaign
	</label><br /><br />

		<form id="campaign-form" name="campaign-form" method="post" action="{$BASE_URL_ADMIN}advertisement/create" role="form" enctype="multipart/form-data">


				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group mb30">
						<label for="area_select">Area</label>
					 	<select id="area_select" name="area_select" class="form-control minput">
						{foreach from=$areas key=val item=obj}
							<option value="{$obj.id}">{$obj.name}</option>
						{/foreach}
						</select>
					</div>
					<div class="form-group mb30">
						<label for="area_select">Name</label>
						<input placeholder="" required name="name" id="name" maxlength="50" type="text" class="form-control minput"  />
					</div>
					<div class="form-group mb30">
			 			<input class="mr15" type="checkbox" name="campaign_active_switch" id="campaign_active_switch" data-size="mini" checked><label style="margin-top: 0px; position: absolute;" class="switch-label">Activate campaign</label>
			 		</div>
			 	</div>

			 	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			 		<div class="form-group mb30">
						<label for="url">Url</label>
						<input required name="url" id="url" maxlength="100" type="text" class="form-control minput"  />
					</div>

			 		<div class="form-group mb30">
						<label id="bannerLabel" for="banner">Upload banner</label>
						<input required accept=".jpg,.jpeg,.png, .gif, .bmp" type="file" name="banner" id="banner" class="form-control inputfile minput" />
						<div class="subheading mt3" >Max 2MB. (jpg, png, gif, bmp)</div>
						{if $FILE_ERR == 'true'}
							<div class="negative-feedback">
								Invalid file extension.
							</div>
						{/if}
					</div>
				</div>	

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt20">
					<div class="form-group">
					   <button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Submit</button>
					</div>
				</div>
		</form>
	</div>
    
</div><!-- #content -->
</div>

<script type="text/javascript">
	 $('#banner').change(function() {
		 var fname = $('input[type=file]').val().split('\\').pop();
		 if( fname )
			$('#bannerLabel').html(fname);
		 else
			$('#bannerLabel').html($('#bannerLabel').html());
		 });
</script>

{include file="footer.tpl"}

