{include file="header.tpl"}

<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">
				Website Payments
			</label>

			<div class="subheading mt10" id="mode-desc">
				{if $active_mode_id == 1}
				Activate <strong>free</strong> mode. Companies can post jobs and access resume database without any restrictions.
				{elseif $active_mode_id == 2}
				Activate <strong>fees</strong> mode. After activation set up the prices in the "Fees Settings" section. Companies will be charged flat fees to post jobs, premium ads and access resume database.
				{elseif $active_mode_id == 3}
				Activate <strong>packages</strong> mode. After activation set up the pricing plans in the "Packages Settings" section. Each company will be assigned an account plan with pre-defined resources (job ads, job ad duration, resume downloads).
				{/if}
				Read more <a href="https://simplejobscript.com/paypal-and-invoices/" target="_blank">here</a> 
			</div>

			<hr />
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt15">

			<form id="psf" name="psf" method="post" action="{$BASE_URL_ADMIN}payment-settings/mode" role="form" >
						<input type="hidden" id="old_mode_id" name="old_mode_id" value="{$active_mode_id}" />

						<div class="form-group">
								<label for="description">Current Payment Mode: &nbsp;</label>
								<select id="payment_mode_select" onchange="jobberBase.paymentModeChanged(this.value, {$BASE_URL_ADMIN});" name="payment_mode_select" class="form-control minput">
								{foreach from=$modes key=id item=value}
									<option {if $id == $active_mode_id}selected{/if} value="{$id}">{$value}</option>
								{/foreach}
								</select>
						</div>

						<div class="form-group mt30">
						   <button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Activate</button>
						</div>

			</form>

		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt15" id="pm-btn-div">
				{if $active_mode_id == 2}
				<a href="{$BASE_URL_ADMIN}payment-settings/fees"><button type="submit" class="btn btn-default btn-primary mbtn alizarinBtn" >Fees Settings</button></a>
				{elseif $active_mode_id == 3}
				<a href="{$BASE_URL_ADMIN}payment-settings/packages"><button type="submit" class="btn btn-default btn-primary mbtn alizarinBtn" >Packages Settings</button></a>
				{/if}
		</div>

    </div>
</div>


{if $POPUP == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Payment Mode Activated');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}