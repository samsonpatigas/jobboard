{include file="header.tpl"}

<!-- MODAL FOR MORE ADDING NEW ADMIN USER -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Add new administrator</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>

            <div class="row">
                <div class="col-xs-12 col-md-15">
			
	<!-- 		http://jobboard.f9portal.net/sjs-admin/password/ -->
				<form action="{$BASE_URL_ADMIN}add-admin/" method="post">
<!-- 					<form action="{$smarty.server.REQUEST_URI}" method="post"> -->
		<!-- 			<form action="" method="post"> -->

					Username: <input required placeholder="Username" class="form-control minput" name="username" id="username" size="30" />

					Password: <input required placeholder="Password" class="form-control minput" type="password" name="password" id="password" size="30" />

					<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Register</button>

				</form>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->

		<div class="admin-content">
				 <label class="admin-label">Password change</label>
				 <div class="subheading">Change your SJS admin password</div><br />
				 <a href="/sjs-admin/settings"><div class="subheading">&larr;go back</div></a>
				<br />
				{if $error}
					<div class="negative-feedback">
						{$error}
					</div>
				{/if}

				{if $success}
					<div class="positive-feedback">
						{$success}
					</div>
				{/if}
				
				<form id="change_password" action="{$smarty.server.REQUEST_URI}" method="post">
					<div>
						<div class="form-group {if $error} error{/if}">
							<input required placeholder="new password" class="form-control minput" type="password" name="new_password" id="new_password" size="30" />
						</div>
						<div class="form-group {if $error} error{/if}">
							<input required placeholder="repeat password" class="form-control minput" type="password" name="verify_password" id="verify_password" size="30" />
						</div>
					<div class="button-holder-np form-group" >
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Change password</button>
					</div>
				</form>

<!-- 				<a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a> -->

<button type="button" class="btn btn-default btn-primary mbtn" data-toggle="modal" data-target="#myModal">Add Admin Account&nbsp;&nbsp;<i class="fa fa-user-secret" style="font-size:15px;color:black"></i></button>

				{if $inserted}
					<div class="positive-feedback">
						{$inserted}
					</div>
				{/if}

				{if $fail}
					<div class="positive-feedback">
						{$fail}
					</div>
				{/if}

		</div><!-- #content -->

	{literal}
		<script type="text/javascript">
			$(document).ready(function()
			{
				$('#new_password').focus();
				
				$("#change_password").validate({
					rules: {
						new_password: { required: true },
						verify_password: { equalTo: "#new_password" }
					}
				});
			});
		</script>
		{/literal}

{include file="footer.tpl"}