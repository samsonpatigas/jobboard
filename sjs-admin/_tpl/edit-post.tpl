{include file="header.tpl"}

    <div class="admin-content">
       <div class="admin-wrap-content">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <label class="admin-label">
        Edit job        
      </label>
      <div>
        Date Posted: {$job.f9_date_posted}
        
      </div>
      <span class="back-area" >
        <a href="{$HTTP_REFERER}" style="float: right;"><button type="button" class="btn btn-default back-button" >{$translations.dashboard_recruiter.back}</button></a>
       </span>
    </div>

    <br />

    <div class="container-fluid ">

      <form method="post" action="{$BASE_URL_ADMIN}post-edited/" role="form">
        <input type="hidden" id="job_id" name="job_id" value="{$job.id}" />
        <input type="hidden" id="referer" name="referer" value="{$referer}" />
        <input name="f9_date_posted" id="f9_date_posted" type="hidden" value="{$job.f9_date_posted}" />
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb20">

          <!-- catgory-->
          <div class="form-group mb20">
            <label class="grayLabel" for="type">{$translations.dashboard_recruiter.post_type_label}</label>
            <select id="type_select" name="type_select" class="form-control minput">
              {foreach from=$types key=id item=value}
                <option {if $value == $job.type_name}selected{/if} value="{$id}">{$value}</option>
              {/foreach}
            </select>
          </div>

          <!-- job types-->
          <div class="form-group mb20">
            <label class="grayLabel" for="type">{$translations.dashboard_recruiter.post_category_label}</label>
            <select id="cat_select" name="cat_select" class="form-control minput">
              {foreach from=$cats key=id item=value}
                <option {if $id == $job.category_id}selected{/if} value="{$id}">{$value}</option>
              {/foreach}
            </select>
          </div>

          {if $remote_portal == 'deactivated'}
            <div class="grayLabel form-group mb20">
              <label for="description">Location</label>
              <select id="location_select" name="location_select" class="form-control minput">
              {foreach from=$cities key=id item=value}
                <option {if $id == $job.city_id}selected{/if} value="{$id}">{$value}</option>
              {/foreach}
              </select>
            </div>
          {/if}

          <div class="grayLabel form-group mb20">
            <label for="city">City</label>
            <input required name="f9_city" id="f9_city" maxlength="400" type="text" class="form-control minput" value="{$job.f9_city}" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Service Type: </label>
              <select id="f9_service_type" name="f9_service_type" class="form-control minput">
                <option value="Desert Dental Staffing Job Board Posting" {if $job.f9_service_type == 'Desert Dental Staffing Job Board Posting'}selected{/if}>Desert Dental Staffing Job Board Posting</option>
                <option value="Concierge Service" {if $job.f9_service_type == 'Concierge Service'}selected{/if}>Concierge Service</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Post Period: </label>
              <select id="f9_post_peroid" name="f9_post_peroid" class="form-control minput">
                <option value="30" {if $job.f9_post_peroid == '30'}selected{/if}>30 days</option>
                <option value="60" {if $job.f9_post_peroid == '60'}selected{/if}>60 days</option>
                <option value="90" {if $job.f9_post_peroid == '90'}selected{/if}>90 days</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Spanish Bilingual: </label>
              <select id="f9_bilingual" name="f9_bilingual" class="form-control minput">
                <option value="Yes" {if $job.f9_bilingual == 'Yes'}selected{/if}>Yes</option>
                <option value="No" {if $job.f9_bilingual == 'No'}selected{/if}>No</option>
              </select>
          </div>

          <div class="grayLabel form-group mb20">
            <label for="city">Expiry: </label>
            <input required name="expires" id="expires" maxlength="400" type="text" class="form-control minput" value="{date("Y/m/d",$job.expires)}" />
          </div>

          <div class="grayLabel form-group ">
            <label for="skills">Skills: </label>
            <label style="display: block;">       
              <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Custom Temps') !== false}checked{/if} />
              <span style="line-height: 2;">Custom Temps</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Cerec System') !== false}checked{/if}/>
              <span style="line-height: 2;">Cerec System</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Surgical Implants') !== false}checked{/if}/>
              <span style="line-height: 2;">Surgical Implants</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'X-ray Certified in AZ') !== false}checked{/if}/>
              <span style="line-height: 2;">X-ray Certified in AZ</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Coronal Polish Certified in AZ') !== false}checked{/if}/>
              <span style="line-height: 2;">Coronal Polish Certified in AZ</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'IV Sedation') !== false}checked{/if}/>
              <span style="line-height: 2;">IV Sedation</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Insurance Processing') !== false}checked{/if}/>
              <span style="line-height: 2;">Insurance Processing</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Treatment Presentation') !== false}checked{/if}/>
              <span style="line-height: 2;">Treatment Presentation</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Anesthesia Certified') !== false}checked{/if}/>
              <span style="line-height: 2;">Anesthesia Certified</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'AR') !== false}checked{/if}/>
              <span style="line-height: 2;">AR</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'Laser Certified') !== false}checked{/if}/>
              <span style="line-height: 2;">Laser Certified</span>
            </label>
            <label style="display: block;">
              <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" {if strpos($job.f9_skills, 'EFDA (Certified Assistant in Arizona)') !== false}checked{/if}/>
              <span style="line-height: 2;">EFDA (Certified Assistant in Arizona)</span>                  
            </label>
          </div>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="grayLabel form-group mb20">
            <label for="title">{$translations.dashboard_recruiter.post_title_label}</label>
            <input required name="title" id="title" maxlength="400" type="text" class="form-control minput" value="{$job.title}" />
          </div>

          <div class="grayLabel form-group ">
            <label for="salary">{$translations.dashboard_recruiter.salary_label}</label>
            <input {if $lock_post}disabled{/if} name="salary" id="salary" maxlength="100" type="text" class="form-control minput" value="{$job.salary}" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Positions: </label>
              <select id="f9_position" name="f9_position" class="form-control minput">
                  <option value="Hygienist" {if $job.f9_position == 'Hygienist'}selected{/if}>Hygienist</option>
                  <option value="Dental Assistant" {if $job.f9_position == 'Dental Assistant'}selected{/if}>Dental Assistant</option>
                  <option value="Cross-trained (Front Office/Dental Assistant)" {if $job.f9_position == 'Cross-trained (Front Office/Dental Assistant)'}selected{/if}>Cross-trained (Front Office/Dental Assistant)</option>
                  <option value="Front Office" {if $job.f9_position == 'Front Office'}selected{/if}>Front Office</option>
                  <option value="Dentist" {if $job.f9_position == 'Dentist'}selected{/if}>Dentist</option>
              </select>
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Practice Type: </label>
              <select id="f9_practice_type" name="f9_practice_type" class="form-control minput">
                <option value="General" {if $job.f9_practice_type == 'General'}selected{/if}>General</option>
                <option value="Ortho" {if $job.f9_practice_type == 'Ortho'}selected{/if}>Ortho</option>
                <option value="Perio" {if $job.f9_practice_type == 'Perio'}selected{/if}>Perio</option>
                <option value="Oral Surgery" {if $job.f9_practice_type == 'Oral Surgery'}selected{/if}>Oral Surgery</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Position Type: </label>
              <select id="f9_position_type" name="f9_position_type" class="form-control minput">
                <option value="Permanent Full Time" {if $job.f9_position_type == 'Permanent Full Time'}selected{/if}>Permanent Full Time</option>
                <option value="Permanent Part Time" {if $job.f9_position_type == 'Permanent Part Time'}selected{/if}>Permanent Part Time</option>
              </select> 
          </div>
          {$arr_soft = explode(",",$job.f9_office_software)}    
          {$arr2_soft = array("Dentrix","EagleSoft", "Open Dental", "SoftDent", "Dentimax")}
          {$result=array_diff($arr_soft,$arr2_soft)}
          <div class="grayLabel form-group">
              <label for="date_posted">Software Experience: </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software" {if strpos($job.f9_office_software, 'Dentrix') !== false}checked{/if}>
                <span style="padding-top: 5px;">Dentrix</span>  
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox" {if strpos($job.f9_office_software, 'EagleSoft') !== false}checked{/if}>
                <span style="padding-top: 5px;">EagleSoft</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox" {if strpos($job.f9_office_software, 'Open Dental') !== false}checked{/if}> 
                <span style="padding-top: 5px;">Open Dental</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox" {if strpos($job.f9_office_software, 'SoftDent') !== false}checked{/if}> 
                <span style="padding-top: 5px;">SoftDent</span>
              </label>
              <label style="display: block;">
                <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox" {if strpos($job.f9_office_software, 'Dentimax') !== false}checked{/if}> 
                <span style="padding-top: 5px;">Dentimax</span>
              </label>
              <label style="display: block;">
                <span style="padding-top: 5px;">Others</span><br/>
                <input name="f9_office_software[]" value="{implode(",",$result)}" class="checkbox-custom" type="text" />                
              </label>
          </div>

          <div class="grayLabel form-group ">
            <label>Hourly Range (Min):</label>
            <input {if $lock_post}disabled{/if} name="f9_pay_min" id="f9_pay_min" maxlength="100" type="text" class="form-control minput" value="{$job.f9_pay_min}" />
          </div>

          <div class="grayLabel form-group ">
            <label>Hourly Range (Max):</label>
            <input {if $lock_post}disabled{/if} name="f9_pay_max" id="f9_pay_max" maxlength="100" type="text" class="form-control minput" value="{$job.f9_pay_max}" />
          </div>

          <div class="grayLabel form-group ">
            <label>State :</label>
            <input {if $lock_post}disabled{/if} name="f9_state" id="f9_state" maxlength="100" type="text" class="form-control minput" value="{$job.f9_state}" />
          </div>

          <div class="grayLabel form-group ">
            <label>Zip Code :</label>
            <input {if $lock_post}disabled{/if} name="f9_zip" id="f9_zip" maxlength="100" type="text" class="form-control minput" value="{$job.f9_zip}" />
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Years of experience: </label>
              <select id="f9_yrs_of_experience" name="f9_yrs_of_experience" class="form-control minput">
                <option value="New Graduate" {if $job.f9_yrs_of_experience == 'New Graduate'}selected{/if}>New Graduate</option>
                <option value="6 Months – 2 Years" {if $job.f9_yrs_of_experience == '6 Months – 2 Years'}selected{/if}>6 Months – 2 Years</option>
                <option value="3 – 5 Years" {if $job.f9_yrs_of_experience == '3 – 5 Years'}selected{/if}>3 – 5 Years</option>
                <option value="6 Years Plus" {if $job.f9_yrs_of_experience == '6 Years Plus'}selected{/if}>6 Years Plus</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Gender: </label>
              <select id="f9_gender" name="f9_gender" class="form-control minput">
                <option value="Male" {if $job.f9_gender == 'Male'}selected{/if}>Male</option>
                <option value="Female" {if $job.f9_gender == 'Female'}selected{/if}>Female</option>
              </select> 
          </div>

          <div class="grayLabel form-group ">
            <label for="date_posted">Specialties: </label>
              <select id="f9_specialties" name="f9_specialties" class="form-control minput">
                <option value="Dental Assistant" {if $job.f9_specialties == 'Dental Assistant'}selected{/if}>Dental Assistant</option>
                <option value="Hygienist" {if $job.f9_specialties == 'Hygienist'}selected{/if}>Hygienist</option>
                <option value="Front Office" {if $job.f9_specialties == 'Front Office'}selected{/if}>Front Office</option>
                <option value="Dentist" {if $job.f9_specialties == 'Dentist'}selected{/if}>Dentist</option>
              </select> 
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="description">{$translations.dashboard_recruiter.post_desc_label}</label>
              <textarea id="description" name="description">{$job.description}</textarea>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="admin_notes">Admin Notes:</label>
              <textarea id="f9_admin_notes" name="f9_admin_notes">{$job.f9_admin_notes}</textarea>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20 grayLabel">
              <label for="position_notes">Position Notes:</label>
              <textarea id="f9_position_notes" name="f9_position_notes">{$job.f9_position_notes}</textarea>
            </div>
          </div>                   
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
            <div class="form-group mb20">
              <input type="checkbox" onchange="applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" {if $job.apply_online == '1'}checked{/if} />
              <label style="margin-left: 10px; margin-bottom" class="switch-label mt25 grayLabel">{$translations.dashboard_recruiter.apply_label}</label>
              <span class="apply-desc-span">{$translations.dashboard_recruiter.apply_desc}</span>
            </div>

             <div id="apply-desc-block" class="form-group mb20 {if $job.apply_online == '1'} displayNone{/if}" >
              <label class="green">{$translations.dashboard_recruiter.howto_apply_label}</label>
              <input id="howtoTA" class="form-control minput" rows="5" cols="5" id="howtoapply" name="howtoapply" value="{$job.apply_desc}" />
             </div>
          </div>
          <br /><br />
        </div>
        <br />

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group mb20">
            <button  type="submit" onclick="//return validateDesc();" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >{$translations.dashboard_recruiter.change_password_submit}</button>
            <a href="{$HTTP_REFERER}"><button type="button" class="right-btn btn btn-default btn-warning mbtn" >{$translations.dashboard_recruiter.cancel}</button></a>
          </div>
        </div>
      </form>
      <br />
      </div>
  </div><!-- /content -->
  </div>

{literal}
<script type="text/javascript">
  $(document).ready(function(){
    var theme = "{/literal}{$THEME}{literal}";
    tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 300, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });
  });

  function applyChanged(val) {
    if (val == false) {
      $('#apply-desc-block').removeClass('displayNone');
    } else {
      $('#howtoTA').val('');
      $('#apply-desc-block').addClass('displayNone');
    }
  }

  function validateDesc() {
    if (tinymce.activeEditor.getContent() == "") {
      alert("Please, fill in the job description");
      return false;
    } else return true;
  }

</script>
{/literal}    

{include file="footer.tpl"}
