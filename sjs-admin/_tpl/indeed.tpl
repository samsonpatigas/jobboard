{include file="header.tpl"} 

<div class="admin-content">
	<div class="admin-wrap-content" >
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
		<label class="admin-label">
			Indeed Settings
		</label>
		<div class="subheading">Read <a href="https://simplejobscript.com/indeed-setup-guide/" target="_blank">guide</a>. If the <a href="{$BASE_URL_ADMIN}cities/list" target="_blank">locations</a> are countries - select homepage dropdown to "countries". If they are cities, select the "cities". It will help Indeed to pull more relevant jobs.</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="banBtnDiv">
			<a href="{$BASE_URL_ADMIN}indeed/manage"><button class="btn btn-default btn-primary mbtn" style=" background-color: #E74C3C">MANAGE SEARCH</button></a>
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<form id="indeed-form" name="indeed-form" method="post" action="{$BASE_URL_ADMIN}indeed/settings" role="form" >

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">PLUGIN STATUS:</label>

					<select class="form-control minput" id="indeed_activate_select" name="indeed_activate_select">
								<option {if $data.activate_select == 'activated'}selected{/if} value="activated">Activated</option>
								<option {if $data.activate_select == 'deactivated'}selected{/if} value="deactivated">Deactivated</option>
					</select>

		    	</div>
			</div>

			<div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">HOMEPAGE DROPDOWN SEARCH BY:</label>

					<select class="form-control minput" id="indeed_homepage_dropdown_select" name="indeed_homepage_dropdown_select">
								<option {if $data.homepage_dropdown == 'cities'}selected{/if} value="cities">Cities</option>
								<option {if $data.homepage_dropdown == 'countries'}selected{/if} value="countries">Countries</option>
					</select>

		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">PUBLISHER ID (*):</label>
		    		<input name="indeed_publisher_id" id="indeed_publisher_id" type="text" value="{$data.publisher_id}" class="form-control minput"  />
		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
					<label class="settings">SHOW BOTH INDEED AND MY JOBS:</label>
					<input {if $data.show_both_jobs_flag == '1'}checked{/if} name="indeed_show_both_jobs_flag" type="checkbox" />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT QUERY:</label>
		    		<input name="indeed_query" id="indeed_query" type="text" value="{$data.query}" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT COUNTRY (*):</label>
		    		<input name="indeed_country" id="indeed_country" type="text" value="{$data.country}" class="form-control minput"  />
		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT CITY (*):</label>
		    		<input name="indeed_location" id="indeed_location" type="text" value="{$data.location}" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT JOB TYPE:</label>
		    		<input name="indeed_jobtype" id="indeed_jobtype" type="text" value="{$data.jobtype}" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">JOBS OLD (IN DAYS):</label>
		    		<input name="indeed_jobs_old" id="indeed_jobs_old" type="text" value="{$data.jobs_old}" class="form-control minput"  />
		    	</div>
			</div>

			<div class="float-block">
				<div class="form-group" style="margin-top: 10px;">
					<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Save</button>
				</div>
			</div>

		</form>
	</div>
    
</div><!-- #content -->
</div>

{if $updated_popup == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Indeed settings have been updated');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}

