{include file="header.tpl"}
<div class="admin-content">
	<div class="admin-wrap-content">	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<link href="http://54.213.230.63/css/app.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable({
            "ajax": "http://jobboard.f9portal.net/sjs-admin/page_employer_history_api.php",
            "columns": [
                { "data": "created_on" },
                { "data": "title" },
                { "data": "name" },
                { "data": "fullname" },
                { "data": "status" }
            ]
        });
    });
    </script>
    <!-- Scripts -->
    <script>
        setTimeout(function(){
        location.reload();
        },216000);
        </script>
</head>

<body class="is-logged-in has-sidebar">
    <div id="app">
        <section id="section" class="h-pad-xl v-pad-xs">
            <div id="title">
                <h2 class="v-margin-sm">Candidates History</h2>
            </div>
            <div id="content">
                <div class="fw-container">
                    <div class="fw-body">
                        <div class="content">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Created on</th>
                                        <th>Candidates</th>
                                        <th>Jobs</th>
                                        <th>Locations</th>
                                        <th>Status</th>
                                       
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Created on</th>
                                        <th>Candidates</th>
                                        <th>Jobs</th>
                                        <th>Locations</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer"></footer>
    </div>
</body>
</div>
	</div>
</div>

{include file="footer.tpl"}
