{include file="header.tpl"} 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Manage Indeed Search
			</label>
			<div class="subheading">To disable category just leave it empty. When searching by city or postcode, default country must be set to match these cities. (eg for New York, "us" etc)</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="settings-row">
				<a class="right blue" href="{$BASE_URL_ADMIN}indeed/manage/1" title="Indeed Job Types"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="{$BASE_URL_ADMIN}indeed/manage/1">Job Types</a></label>
				<div class="light">Filter Indeed Jobs By Job Types</div>
			</div>

			<div class="settings-row">
				<a class="right blue" href="{$BASE_URL_ADMIN}indeed/manage/2" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="{$BASE_URL_ADMIN}indeed/manage/2">Countries</a></label>
				<div class="light">Filter Indeed Jobs By Country</div>
			</div>

			<div class="settings-row">
				<a class="right blue" href="{$BASE_URL_ADMIN}indeed/manage/3" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="{$BASE_URL_ADMIN}indeed/manage/3">Cities</a></label>
				<div class="light">Filter Indeed Jobs By Cities</div>
			</div>

		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt25">
    		<a href="{$BASE_URL_ADMIN}indeed/add" title="Add new search item" href="#" ><i class="fa fa-plus-circle fa-lg blueColor mr5" aria-hidden="true"></i></a>
		</div>

</div>
</div>

{include file="footer.tpl"}