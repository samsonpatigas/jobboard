{include file="login_header.tpl"}

	<div class="form-box">
		<div style="text-align: center;"><img class="site-logo" src="jobboard.f9portal.net/../../fav.png" style="width: 30%;" alt="Website's Logo"></div>
		<div class="login-recruiter-headline admin-login-top">

<style type="text/css">
	
#pos {

	margin-top: 5px;
}
.form-box{
	margin-top: 0;
}
</style>

			<label class="login-form-title"><img class="site-logo" src="jobboard.f9portal.net/../../uploads/logos/main-logo.png" style="width: 85%; margin-top: 10px;" alt="Website's Logo"></label>
			<div class="subheadline" id="pos"><p>
				Verify your identity
			</p>

		</div>
		<div class="login-container admin-wrap" >
			<form id="login" method="post" action="{$BASE_URL_ADMIN}">
				
					<div class="form-group">
						<div class="group{if $errors.username} error{/if}">
							<input required placeholder="username" type="text" name="username" id="username" class="form-control grayInput" value="{$smarty.post.username}" />
						</div>
					</div>

					<div class="form-group">
						<div class="group{if $errors.password} error{/if}">
							<input required placeholder="password" type="password" name="password" class="form-control grayInput" id="password"value="" />
						</div>
					</div>

					<div class="form-group">
						<div class="negative-feedback">
						{$errors.incorrect}
						</div>
					</div>
					
					<div class="form-group">
						<div class="group_submit">
							<button class="btn btn-default btn-primary mbtn" type="submit" name="submit" id="submit"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
							<input type="hidden" name="action" value="login" />
						</div>
					</div>
			
			</form>
		</div><!-- #content -->
	</div>

{include file="footer.tpl"}
