<?php
	require_once '../_config/config.php';
	require_once '../_lib/class.Types.php';
	
		$sql = "SELECT a.mysql_timestamp, a.payer_name, a.payer_surname, a.amount, a.currency_code  FROM payment as a
	LEFT JOIN employer as b ON a.employer_id = b.id
	ORDER BY a.mysql_timestamp DESC";

		$result = $db->query($sql);

		$a = array();
		while ($row = $result->fetch_assoc()){
			if($row['amount'] != ""){
				$row['amount'] = '<i class="fa  fa-dollar" aria-hidden="true" style="color:#777;"> '.$row['amount'].'</i>';
			}
			$a['data'][] = $row;
		}
		echo (json_encode($a));
?>