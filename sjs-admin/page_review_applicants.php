<?php 
	
    $sql = 'SELECT COUNT(id) as total FROM applicant';
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    $TOTAL = $row['total'];

    //pagination
    $paginatorLink = BASE_URL  . "review_applicants";
    $paginator = new PaginatorAdmin($TOTAL, SUBSCRIBERS_PER_PAGE, @$_REQUEST['p']);
    $paginator->setLink($paginatorLink);
    $paginator->paginate();
    $offset = $paginator->getFirstLimit();

    $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
    $data = $db->query($sql);

    if(isset($_POST['reviewed'])){

        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c WHERE c.is_Seen = 1 ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("reviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notreviewed'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c WHERE c.is_Seen = 0 ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notreviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['allreviewed'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allreviewed", "background-color: #2C3E50!important;");
    }
      if(isset($_POST['published'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c WHERE c.public_profile = 1 ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("publish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notpublished'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c WHERE c.public_profile = 0 ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notpublish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['allpublished'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allpublish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['btnsearch'])){
        $sql = 'SELECT c.id as "candidate_id", c.fullname as "candidate_name", c.email as"candidate_email", c.public_profile as "public_profile", c.is_Seen, c.confirmed as "candidate_confirmed" FROM applicant c
                WHERE c.fullname LIKE"%'.$_POST['search'].'%" OR c.email LIKE"%'.$_POST['search'].'%" ORDER BY id DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
                
        $data = $db->query($sql);
        $smarty->assign("searchtext", $_POST['search']);
        $smarty->assign("btnsearch", "background-color: #2C3E50!important;");
    }

    $cdts = array();
    while ($row = $data->fetch_assoc()) {
     $cdts[] = $row;
    }



    $smarty->assign("candidates", $cdts);
    $smarty->assign("pages", $paginator->pages_link);

    $template = 'review_applicants.tpl';

?>