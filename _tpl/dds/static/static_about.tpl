<div style="background-color:#e96656; padding: 15px 15px 15px 25px; ">
	<h1 style="color: white; text-shadow: 1px 1px 1px #000;">About Desert Dental Staffing</h1>
	<h3 style="color: white; text-shadow: 1px 1px 1px #000;">Our Mission and The People Who Make it Happen</h3>
</div>
<br />
<p><span style=color: #000000; font-size: 16px;>At Desert Dental Staffing, our focused selection process is designed to save you time and money. Since 1998 we have represented the top dental professionals in the valley. </span></p>
<br />
<br />
<h3>Our Unique Process</h3>
<br />
<p><span style=color: #000000; font-size: 16px;>When you meet with one of our advisors, we will ask you specific questions about you, your practice, and your team. Then, using thorough and extensive interview techniques, including analysis of behavioral styles, we send ONLY the candidate we feel is right for your needs. This unique approach ensures long term success from your team members and whenever experienced professionals are needed, whether permanent or temporary, we will utilize your critical information to fit your specific needs each and every time.&nbsp;</span></p>
<br />
<br />
<h3>Call Today!</h3>
<br />
<h4><a href="tel:6028404703">(602) 840-4703&nbsp;</a></h4>
<br />
<br />
<br />
<br />
<h3 style=color: #828282; font-size: 26px;>Our Team</h3>
<br />
<div class="container-fluid">
  <div  id="staff1" class="row">
    <div class="col-lg-3" >	 	
      <img src=http://www.desertdentalstaffing.com/img/gina3.jpg  style="height: auto; width: auto;" />
    </div>
    <div class="col-lg-9" style="padding-left: 4%;">
       <h3>Gina</h3>
      <div>
       <h4>President-Owner</h4>
      </div>
      <br />
      <div>
      	<p><span style=color: #000000; font-size: 16px;>Gina has decades of experience managing practices, sales and dental staffing. She has a degree in business from Western Michigan University and has completed the qualifications for the DISC certification, which has earned her the title of Certified DISC Coach. She also has written monthly editorials for Doctor of Dentistry Magazine, advising practices about staffing. Gina currently serves as a program advisor for Phoenix College dental assisting school. She works helping students design resumes as well as refine their presentation, communication and interview skills. For 6 years in a row (2010 through 2016), Desert Dental Staffing has been ranked as a Top 10 Who's Who Business Industry Leader in the category of Employment Services. Gina was chosen to be profiled in the latest edition. </span></p>
      </div>
    </div>
  </div>
  <hr style="background-color: #7527a0; height: 3px; border: 0;">
  <div id="staff2" class="row">
  	<div class="col-lg-3">	 	
      <img src=http://www.desertdentalstaffing.com/img/lynnette3.jpg  style="height: auto; width: auto;" />
    </div>
    <div class="col-lg-9" style="padding-left: 4%;">
       <h3>Lynnette</h3>
      <div>
      	<h4>Director of Marketing</h4>
      </div>
      <br />
      <div>
      	<p><span style=color: #000000; font-size: 16px;>Lynnette brings over 20 years of sales/marketing & recruitment experience to Desert Dental Staffing. Lynnette’s goals are simple, she’s looking to help clients find a better way of hiring employees and applicants find the perfect job through OUR exceptional staffing service. She is also spearheading our newest service that brings our clients/vendors together through advertising. Our advertising program offers a great opportunity to market your products and services to our dentists and applicants. Lynnette's passion in her personal life is to impress and inspire others, through the love of nature, food, fellowship & faith. </span></p>
      </div>
    </div>
  </div>
  <hr style="background-color: #7527a0; height: 3px; border: 0;">
   <div id="staff3" class="row">
  	<div class="col-lg-3">	 	
      <img src=http://www.desertdentalstaffing.com/img/kris.jpg  style="height: auto; width: auto;" />
    </div>
    <div class="col-lg-9" style="padding-left: 4%;">
       <h3>Kris</h3>
      <div>
       <h4>Temporary Placement Specialist</h4>
      </div>
      <br />
      <div>
      	<p><span style=color: #000000; font-size: 16px;>Kris grew up in Mesa Arizona. She attended ASU where she met her husband and graduated with an education degree. George and Kris have three children and one ornery Jack Russell. After teaching for over 20 years, she graduated from dental assisting school she worked as an orthodontic assistant and a scheduling coordinator. Her social and communication skills help to create that “perfect fit” between doctors and employees. </span></p>
      </div>
    </div>
  </div>
  <hr style="background-color: #7527a0; height: 3px; border: 0;">
   <div id="staff4" class="row">
  	<div class="col-lg-3">	 	
      <img src=http://www.desertdentalstaffing.com/img/jenny.jpg style="height: auto; width: auto;" />
    </div>
    <div class="col-lg-9" style="padding-left: 4%;">
       <h3>Jenny</h3>
      <div>
       <h4>Administrator Coordinator</h4>
      </div>
      <br />
      <div>
      	<p><span style=color: #000000; font-size: 16px;>Jenny Sunwoo was born in Seoul, South Korea when she was 9 years old she moved to Los Angeles, California. Growing up in the United States her interest has always been in health care which brought her to University of California San Diego where she would major in Biology with a Minor in Political Science. Her educational success did not end there. She went on to pursue a Masters of Arts in Biomedical Science, where she showed promise in the field of research. Jenny’s passion in dentistry lead her to become a Dental Candidate at Midwestern University Class of 2021 where she will once again continue her life of education.  </span></p>
      	<p><span style=color: #000000; font-size: 16px;>Besides school and her career focus goals, her hobbies include trying new foods, and exploring different cities. Her role at Desert Dental is to help employees find temporary jobs. Her enthusiastic yet calming personality will help you feel less intimidated during your job search. If you get a chance to work with Jenny, you will see that she is very compassionate and is willing to work hard for you to get your placement. </span></p>
      </div>
    </div>
  </div>
</div>
<br />
