{section name=index loop=$dash_jobs}
  <div class="modal fade" id="renewModal_{$dash_jobs[index].id}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%; margin-left: 0px;">
        <!--<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body">
          <p>Do you want to renew this listing?</p>
          <div class="job-process">
            <div class="detail-font" >
              <div class="row checkboxes" style="margin-top: 10px;">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Post period</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_post_peroid" name="f9_post_peroid" value="{$job.f9_post_peroid}">
                    <option value="30 days">(30 days)</option>
                    <option value="60 days">(60 days)</option>
                    <option value="90 days">(90 days)</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
    
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">YES I DO</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
        </div>
      </div>
      
    </div>
  </div>
{/section}