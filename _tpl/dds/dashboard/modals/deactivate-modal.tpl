{section name=index loop=$dash_jobs}
  <div class="modal fade" id="myModal_{$dash_jobs[index].id}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%; margin-left: 0px;">
        <!--<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body">
          <p>Do you want to renew this listing?</p>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mlpl0">

    <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.company}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.category_name}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.job_type}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.location_asci}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_date_posted}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_post_peroid}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_gender}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_language}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Position</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_position}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_specialties}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_city}  {$job_data.f9_state}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_zip}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.description}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_admin_notes}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <form action="" method="post">
            <a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_DEACTIVATE_MODAL}/{$job.id}"><button id="deactivate" name="deactivate" class="btn btn-default" type="submit">YES I DO</button></a> 
            <button name="cancel" class="btn btn-default" data-dismiss="modal">NO</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
{/section}
