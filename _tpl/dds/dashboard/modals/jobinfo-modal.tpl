
{section name=index loop=$dash_jobs}
  <div id="modal_{$dash_jobs[index].id}" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{$dash_jobs[index].title}</h4>
      </div>

   <div class="modal-body job-details">
       <div class="clear-both"></div>

        <div class="detail-font" >
          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Posted by</h3>
                    </div>
                  </div>
                </div>
                
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <!-- <div class="row">
                        <div class="col-md-2">
                          <img src="/{$dash_jobs[index].company_logo_path}" style="width: 38px!important;border-radius: 2em;"> 
                            </div>
                              <div class="col-md-6"> -->
                                <p>{$dash_jobs[index].company}</p>
                         <!--  </div>
                        </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].category_name}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].job_type}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].location_asci}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_date_posted}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_post_peroid} days</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_gender}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_language}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Positions</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_position}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_specialties}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_city}  {$dash_jobs[index].f9_state}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_zip}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].description}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -4px!important;float:left;">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p>{$dash_jobs[index].f9_admin_notes}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer preview-footer">
        <div class="row">
          <div class="col-md-4">
            <button type="button" class="btn mbtn fl" style="width: 170px!important;;height: 40px;line-height: 0;" data-dismiss="modal">{$translations.js.close}</button>
          </div>
           <div class="col-md-4">
            {if $dash_jobs[index].salary}<p class="price-apply">Salary <c style="color: #7527a0;">{$dash_jobs[index].salary} </c></p>{/if}
          </div>
           <div class="col-md-4">
              <p>Expiry date <c style="color: #7527a0;">{$dash_jobs[index].expires_date} </c></p>
          </div>
        </div>
      </div>
    </div>

  </div>
  </div>
 {/section}