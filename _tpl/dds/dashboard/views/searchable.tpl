<div class="row board mb25">
    <div id="title">
      <h2 class="v-margin-sm" style="
      box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
      -webkit-border-radius: .25rem;
      border-radius: .25rem;
      padding: 20px;
      background-color: #7527a0;
      color: #fff;
      font-size: 20px;
      font-weight: 500;
      ">Searchable Candidate List</h2>
    </div>
</div>
<br /><br />

<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0 mb25">
    <form role="form">
    <!--    <div class="row"> -->
                <label class="mb15" for="job_select"><!-- {$translations.dashboard_recruiter.select_jobs_title} -->Show applied candidates by jobs :</label>
                <div class="col-md-12">
                    <div class="col-md-7" style="margin-left: -30px;">
                        <select id="job_select" name="job_select" class="form-control minput1">
                    {foreach from=$jobs_select key=id item=value}
                        {if $select_job_id}
                            {if $select_job_id == $id}
                            <option selected value="{$id}">{$value}</option>
                            {else}
                            <option value="{$id}">{$value}</option>
                            {/if}
                        {else}
                            <option value="{$id}">{$value}</option>
                        {/if}
                        }
                    {/foreach}
                </select>
                    </div>
                    <div class="col-md-1" style="margin-left: -70px;margin-top: 4px;">
                        <i id="apps_spinner" class="ml15 displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
                    </div>
            </div>
            <!--    <label class="mb15" for="job_select">Show all interesting candidates:</label>
                <i id="apps_spinner" class="ml15 displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
                <select id="interested" name="interested" class="form-control minput1">
                    {foreach from=$interested key=id item=value}
                        {if $select_job_id}
                            {if $select_job_id == $id}
                            <option selected value="{$id}">{$value}</option>
                            {else}
                            <option value="{$id}">{$value}</option>
                            {/if}
                        {else}
                            <option value="{$id}">{$value}</option>
                        {/if}
                        }
                    {/foreach}
                </select>
        </div> -->
        
    </form>
</div>


<!--
    <script type="text/javascript" language="javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"> 
-->    

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" class="init">
  
    $(document).ready(function() {
        $('#example').DataTable( {

        columnDefs: [
        {
            targets: [-1,-2,-3,-4,-5],
            className: 'dt-center'
        },
        {
            // -18 is removed to show phone number
          "targets": [-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16,-17,-19,-20,-21,-24],
          "visible": false
        }
      ],
            ajax: 'http://jobboard.f9portal.net/_lib/page_searchable_api.php',
        });
    });
    </script>

    <script>
      setTimeout(function(){
      location.reload();
      },216000);
    </script>

<script type="text/javascript">
    
$(document).ready(function(){

    var table = $('#example').DataTable();
 
    $('#example').on( 'click', 'tbody #review', function () {
      var data_row = table.row($(this).closest('tr')).data();
      var data_row_id = table.row($(this).closest('tr')).data()[0];
      console.log(data_row);
      console.log(data_row_id);
      //console.log( table.row( this ).data() );
      //console.log( table.row( this ).data()[0] );

      $.post("http://jobboard.f9portal.net/interest.php", {
          interested: data_row_id
      }, function(data,status){
          console.log("Review Update:"+data);
      });
    });

    $('#example').on( 'click', 'tbody #reject', function () {
      var data_row = table.row($(this).closest('tr')).data();
      var data_row_id = table.row($(this).closest('tr')).data()[0];
      console.log(data_row);
      console.log(data_row_id);
      //console.log( table.row( this ).data() );
      //console.log( table.row( this ).data()[0] );

      $.post("http://jobboard.f9portal.net/reject.php", {
          rejected: data_row_id
      }, function(data,status){
          console.log("Reject Update:"+data);
      });
    });

   $('#example').on( 'click', 'tbody #banned', function () {
      var data_row = table.row($(this).closest('tr')).data();
      var data_row_id = table.row($(this).closest('tr')).data()[0];
      console.log(data_row);
      console.log(data_row_id);

      $.post("http://jobboard.f9portal.net/banned_client.php", {
          banned: data_row_id
      }, function(data,status){
          console.log("Banned Update:"+data);
      });
    });


    $('#example').on( 'click', 'tbody #about', function () {
      var data_row = table.row($(this).closest('tr')).data();
      var data_row_id = table.row($(this).closest('tr')).data()[0];
      var data_row_name = table.row($(this).closest('tr')).data()[1];
      var data_row_position = table.row($(this).closest('tr')).data()[2];
      var data_row_email = table.row($(this).closest('tr')).data()[20];
      var data_row_about = table.row($(this).closest('tr')).data()[5];
      var data_row_phone = table.row($(this).closest('tr')).data()[6];

      var name = data_row_name;  
      $('#name').html(name); 

      var position = data_row_position;  
      $('#position').html(position); 

      var phone = data_row_phone;
      $('#phone').html(phone); 

      var email = data_row_email;  
      $('#email').html(email);

       var about = data_row_about;  
      $('#aboutme').html(about);   
    });
});

</script>

<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #7527a0; color: #fff;">
        <h4 class="modal-title" style="font-size: 20px;">Candidate Profile</h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="row">
          <div class="col-md-6">
              <ul class="list-group">
                <li class="list-group-item"><b>Fullname:</b> <label id="name"></label></li>
                <li class="list-group-item"><b>Position:</b> <label id="position"></label></li>
                <li class="list-group-item"><b>Phone:</b> <label id="phone"></label></li>
                <li class="list-group-item"><b>Email:</b> <label id="email"></label></li>
              </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<body class="is-logged-in has-sidebar">
  <div id="app" style="margin-top: -40px;">
    <section id="section" class="h-pad-xl v-pad-xs">
      <div id="content">
          <div class="fw-container">
            <div class="fw-body">
              <div class="content">
                <table id="example" class="display" style="width:100%">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Candidates</th>
                      <th>Position</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Zip</th>
                      <th>Phone</th>
                      <th>Gender</th>
                      <th>Bilingual</th>
                      <th>Field</th>
                      <th>Experience</th>
                      <th>Prosoft</th>
                      <th>Workarea</th>
                      <th>Tempday</th>
                      <th>Datefrom</th>
                      <th>Dateuntil</th>
                      <th>Permaday</th>
                      <th>Skillsprof</th>
                      <th>Wage</th>
                      <th>Miles</th>
                      <th>Resume</th>
                      <th>About</th>
                      <th>Interested</th>
                      <th>Reject</th>
                      <th>Banned</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
      </div>
    </section>
      <footer id="footer"></footer>
  </div>
</body>
