
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edit-company">
  <form method="post" action="{$BASE_URL}{$URL_JOB_POSTED}/" role="form">
    <input type="hidden" id="step" name="step" value="1" />
    <input type="hidden" id="employer_id" name="employer_id" value="{$employer_id}" />
    <input type="hidden" id="jobs_left" name="jobs_left" value="{$jobs_left}" />
    <input type="hidden" id="job_period" name="job_period" value="{$job_period}" />

{if not $lock_post}


          <div class="row " id="step-1">
           <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0">
  
            <div class="row checkboxes">
              <div class="col-md-3 col-xs-12">
                <h3 class="process-heading">Type <red style="color: red">*</red></h3>
              </div>

              <div class="col-md-5 col-xs-12">
                <select id="type_select" name="type_select">
                  {foreach from=$types key=id item=value}
                    <option {if $DRAFT}{if $draft_data.type_name == {$value}}selected{/if}{/if} value="{$id}">{$value}</option>
                  {/foreach}
                </select>
              </div>
            </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Category <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="cat_select" name="cat_select">
                    {foreach from=$cats key=id item=value}
                      <option {if $DRAFT}{if $draft_data.category_id == {$id}}selected{/if}{/if} value="{$id}">{$value}</option>
                    {/foreach}
                  </select>
                </div>
              </div>

              {if $REMOTE_PORTAL == 'deactivated'}
               <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Location <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="location_select" name="location_select">
                  {foreach from=$cities key=id item=value}
                    <option {if $DRAFT}{if $draft_data.city_id == {$id}}selected{/if}{/if} value="{$id}">{$value}</option>
                  {/foreach}
                  </select>
                </div>
              </div>
              {/if}

              {if $PAYMENT_MODE == '2'}
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">{$translations.dashboard_recruiter.premium_text} <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="premium_select" name="premium_select">
                    <option value="0">{$translations.dashboard_recruiter.standard_label}</option>
                    <option value="1">{$translations.dashboard_recruiter.premium_label} {$FORMATED_CURRENCY}</option>
                  </select>
                </div>
              </div>
              {/if}

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Date posted <!-- <red style="color: red">*</red> --></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input type="date" id="f9_date_posted" name="f9_date_posted" value="{if $DRAFT == 'true'}{$draft_data.f9_date_posted}{/if}" class="job-title" readonly>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Title <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <!-- <input required name="title" id="title" maxlength="400" type="text" value="{if $DRAFT == 'true'}{$draft_data.title}{/if}" class="job-title"  /> -->
                  <select id="title" name="title">
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Hygienist">Hygienist</option>
                      <option value="Front Office">Front Office</option>
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Positions<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_position" name="f9_position" {if $DRAFT}{if $draft_data.f9_specialties}selected{/if}{/if}>
                      <option value="Hygienist">Hygienist</option>
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Cross-trained (Front Office/Dental Assistant)">Cross-trained (Front Office/Dental Assistant)</option>
                      <option value="Front Office">Front Office</option>
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Practice Type:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_practice_type" name="f9_practice_type">
                    <option value="General">General</option>
                    <option value="Ortho">Ortho</option>
                    <option value="Perio">Perio</option>
                    <option value="Endo">Endo</option>
                    <option value="Oral Surgery">Oral Surgery</option>
                  </select>            
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Position Type:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Full Time">Permanent Full Time</label>
                  <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Part Time">Permanent Part Time</label>
                </div>
              </div>
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Service Type:</h3>
                </div>
                <div class="col-md-8 col-xs-12">
                  <div class="radio">
                    <label style="text-align:left;"><input type="radio" name="f9_service_type" id="f9_service_type" value="Desert Dental Staffing Job Board Posting">Desert Dental Staffing Job Board Posting</label>
                  </div> 
                  <div class="radio">  
                    <label style="text-align:left;"><input type="radio" name="f9_service_type" id="f9_service_type" value="Concierge Service">Concierge Service</label>
                  </div>
                </div>
              </div>              
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Post period <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_post_peroid" name="f9_post_peroid" {if $DRAFT}{if $draft_data.f9_post_peroid}selected{/if}{/if}>
                    <option value="30">(30 days)</option>
                    <option value="60">(60 days)</option>
                    <option value="90">(90 days)</option>
                  </select>
                </div>
              </div>
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Spanish Bilingual:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="Yes">Yes</label>
                  <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="No">No</label>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Skills:</h3>
                </div>
                <div class="col-md-8 col-xs-12">
                  <p>TIP:  Select only the ABSOLUTE requirements needed for the position to maximize the number of resumes received</p>
                  <label>       
                    <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Custom Temps</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Cerec System</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Surgical Implants</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">X-ray Certified in AZ</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Coronal Polish Certified in AZ</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">IV Sedation</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Insurance Processing</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Treatment Presentation</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Anesthesia Certified</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">AR</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Laser Certified</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">EFDA (Certified Assistant in Arizona)</h4>                  
                  </label>
                </div>      
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Software Experience:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                    <label>
                      <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software">
                      <h4 style="padding-top: 5px;">Dentrix</h4>  
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox">
                      <h4 style="padding-top: 5px;">EagleSoft</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Open Dental</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">SoftDent</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Dentimax</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Others" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Others</h4>
                    </label>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">City <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_city" id="f9_city" maxlength="400" type="text" value="{if $DRAFT == 'true'}{$draft_data.f9_city}{/if}" class="job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Hourly Range (Min)<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_pay_min" id="f9_pay_min" maxlength="400" type="text" value="" class="input-f9_pay_min job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Hourly Range (Max)<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_pay_max" id="f9_pay_max" maxlength="400" type="text" value="" class="input-f9_pay_max job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">State <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">        
                  <select required id="f9_state" name="f9_state" class="dropdown" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    padding: 10px;
                    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    min-height: 47px;
                    width: 75%;
                    border-radius: 5px;
                    padding: 0% 3% 0% 6%;
                    border: none;
                    float: left;
                    margin-bottom: 2%;
                    font-size: 14px;
                    padding: 10px;
                    background-color:white;
                    border-color: gainsboro;
                    border-width: 1px;
                    border-style: solid;
                    ">
                    {$states= ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']}
                      <option value="" disabled selected>Select your option</option>
                      {foreach from=$states item=x}
                        <option value="{$x}">{$x}</option>
                      {/foreach}
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Zip code <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_zip" id="f9_zip" maxlength="5" type="text" value="{if $DRAFT == 'true'}{$draft_data.f9_zip}{/if}" class="job-title"  />
                </div>
              </div>


              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">{$translations.dashboard_recruiter.salary_label} <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input name="salary" class="input-salary job-title" id="salary" value="{if $DRAFT == 'true'}{$draft_data.salary}{/if}"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Years of experience <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_yrs_of_experience" name="f9_yrs_of_experience">
                    <option value="New Graduate">New Graduate</option>
                    <option value="6 Months – 2 Years">6 Months – 2 Years</option>
                    <option value="3 – 5 Years">3 – 5 Years</option>
                    <option value="6 Years Plus">6 Years Plus</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Language <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_language" id="f9_language" maxlength="400" type="text" value="{if $DRAFT == 'true'}{$draft_data.f9_language}{/if}" class="job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Gender <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_gender" name="f9_gender" {if $DRAFT}{if $draft_data.f9_gender}selected{/if}{/if}>
                     <option value="Male">Male</option>
                       <option value="Female">Female</option>
                  </select>
                    </div>
                </div>

                <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Specialties <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_specialties" name="f9_specialties" {if $DRAFT}{if $draft_data.f9_specialties}selected{/if}{/if}>
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Hygienist">Hygienist</option>
                      <!-- <option value="EFTA Certified Assistant">EFTA Certified Assistant</option> -->
                      <option value="Front Office">Front Office</option>
                      <!-- <option value="Crostrained (Front Office/Dental Assistant)">Crostrained (Front Office/Dental Assistant)</option> -->
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>



              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Short Description <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="description" name="description" class="process-textarea" rows="12" cols="1" spellcheck="true">{if $DRAFT == 'true'}{$draft_data.description}{/if}</textarea>
                </div>
              </div>
              <br />

              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Admin Notes <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="f9_admin_notes" name="f9_admin_notes" class="process-textarea" rows="12" cols="1" spellcheck="true">{if $DRAFT == 'true'}{$draft_data.f9_admin_notes}{/if}</textarea>
                </div>
              </div>
              <br />
              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Position Notes <red style="color: red">*</red></h3>
                  <p>Enter additional information about the position.  Text entered here will be displayed in the Job Board posting.</p>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="f9_admin_notes" name="f9_position_notes" class="process-textarea" rows="12" cols="1" spellcheck="true">{if $DRAFT == 'true'}{$draft_data.f9_position_notes}{/if}</textarea>
                </div>
              </div>
              </div>
        
              <div class="acceptance mb50">
                <label>
                  <input class="checkbox-custom" type="checkbox" onchange="SimpleJobScript.applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" checked />

                  <h4>{$translations.dashboard_recruiter.apply_label}<span class="apply-desc-span" >{$translations.dashboard_recruiter.apply_desc}</span></h4>

                </label>
              </div>
              <br />
              
               <div id="apply-desc-block" class="form-group mb20 displayNone" >
                <label class="green tal mb20">{$translations.dashboard_recruiter.howto_apply_label}</label>
                <input id="howtoTA" class="form-control minput" id="howtoapply" name="howtoapply" value="{$job.apply_desc}"></input>
               </div>
        
          </div>

          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0 mt25">
            <button type="submit" onclick="return SimpleJobScript.validateDesc();" class="btn mbtn fl" name="submit" id="submit" >{$translations.dashboard_recruiter.post_submit}</button>
          </div>
        </div>


{else}

  <div class="col-md-10 col-sm-12 locked mt25">
    <div class="modal1">
      <div class="modal fade in" id="myModal" role="dialog" style="display: block;">
        <div class="modal-dialog">
      
          <div class="modal-content">
            <div class="modal-body">
              <h4 class="modal-title md-hide mb25">1/2</h4>
              <p class="tac fs16">{$msg}</p>
              <img class="anim-pic" src="{$BASE_URL}_tpl/default/1.5/images/lock-image.png">
              <a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_ACCOUNT_ORDER}">
                <button type="button" class="btn btn-default btn-lock">{$translations.post_step1.renew}</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  

{/if}

  </form>
</div>
<script src="/_tpl/dds/1.5/js/cleave/cleave.min.js"></script>
<script src="/_tpl/dds/1.5/js/cleave/cleave-phone.us.js"></script>
{literal}
  <script type="text/javascript">
    $(document).ready( function() {
        var now = new Date();
        var month = (now.getMonth() + 1);
        var day = now.getDate();
        if (month < 10) 
            month = "0" + month;
        if (day < 10) 
            day = "0" + day;
        var today = now.getFullYear() + '-' + month + '-' + day;
        $('#f9_date_posted').val(today);
        document.getElementById('f9_date_posted').setAttribute("min",today);

        $("input[name='f9_service_type']").change(function(){
            selected_value = $("#f9_service_type:checked").val();
            if(selected_value == 'Concierge Service'){
              $('#f9_post_peroid').append($('<option>', {
                value: 3650,
                text: 'No Expiry'
              }));
              $('option[value="3650"]').prop("selected", true);
            }else{
              $("#f9_post_peroid option[value='3650']").remove();
            }
        });
    });

    var cleave = new Cleave('.input-f9_pay_min', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    var cleave = new Cleave('.input-f9_pay_max', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    var cleave = new Cleave('.input-salary', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    document.getElementById('f9_zip').addEventListener('input', function (e) {
      var x = e.target.value.replace(/[^0-9]/g, '')
          e.target.value = x;
            });

  </script>
{/literal}