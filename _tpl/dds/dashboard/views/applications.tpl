
<style type="text/css">
.modal-body {
padding: 5% 3% !important;
}
.modal-dialog {
	margin-top: 10% !important;
}
</style>

<div class="row board mb25">
  <h2>Candidate List</h2>
 <!--  <p>{$translations.dashboard_recruiter.jobs_apps_desc}</p> -->
</div>
<br /><br />

<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0 mb25">
	<form role="form">
	<!-- 	<div class="row"> -->
				<label class="mb15" for="job_select"><!-- {$translations.dashboard_recruiter.select_jobs_title} -->Show applied candidates by jobs :</label>
				<div class="col-md-12">
					<div class="col-md-7" style="margin-left: -30px;">
						<select id="job_select" name="job_select" class="form-control minput1">
					{foreach from=$jobs_select key=id item=value}
						{if $select_job_id}
							{if $select_job_id == $id}
							<option selected value="{$id}">{$value}</option>
							{else}
							<option value="{$id}">{$value}</option>
							{/if}
						{else}
							<option value="{$id}">{$value}</option>
						{/if}
						}
					{/foreach}
				</select>
					</div>
					<div class="col-md-1" style="margin-left: -70px;margin-top: 4px;">
						<i id="apps_spinner" class="ml15 displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
					</div>
			</div>
			<!-- 	<label class="mb15" for="job_select">Show all interesting candidates:</label>
				<i id="apps_spinner" class="ml15 displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
				<select id="interested" name="interested" class="form-control minput1">
					{foreach from=$interested key=id item=value}
						{if $select_job_id}
							{if $select_job_id == $id}
							<option selected value="{$id}">{$value}</option>
							{else}
							<option value="{$id}">{$value}</option>
							{/if}
						{else}
							<option value="{$id}">{$value}</option>
						{/if}
						}
					{/foreach}
				</select>
		</div> -->
		
	</form>
</div>
<br /><br />

<div class="col-md-12 col-xs-12 col-xs-6">
	<label style="margin-top: 17px;margin-left: 15px;"><input class="checkbox-custom checkbox_app" type="checkbox" name="interested" id="interested" value="1"><c style="line-height: 2;">Show Interested</c></label>
	<br /><br />
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	<div class="table-responsive applications-table">
		<table id="customers" class="table table-striped">
		    <thead>
		      <tr>
		        <th><strong>Candidates</strong></th>
		        <th><strong>Position</strong></th>
		        <!-- <th><strong>{$translations.apply.skills}</strong></th> -->
		        <!-- <th><strong>{$translations.apply.located}</strong></th> -->
		        <th><strong>{$translations.applications.cv}</strong></th>
		        <th><strong>{$translations.apply.table_message}</strong></th>
		        <th><strong><!-- {$translations.apply.table_review} -->Interested</strong></th>
		        <th><strong>{$translations.apply.table_delete}</strong></th>
		        <th style="text-align: center!important;"><strong>Score</strong></th>
		      </tr>
		    </thead>
		    <tbody id="ajax-content"></tbody>
		    <tfoot id="ajax-content-sub"></tfoot>
		</table>
	</div>
</div>
<br><br>
<!-- 
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" class="init">
  
    $(document).ready(function() {
        $('#example').DataTable( {
            ajax: 'http://jobboard.f9portal.net/_lib/page_searchable_api.php',
            columns: [
                { data: 'name' },
                { data: 'title' },
                { data: 'f9_position' },
                { data: 'phone' },
                { data: 'email' },
                { data: 'public_profile' }
            ],
            order: [[2, 'asc']],
            rowGroup: {
                dataSrc: 2
            }
        } );
    } );
    </script>

    <script>
        setTimeout(function(){
        location.reload();
        },216000);
        </script>
</head>
<body class="is-logged-in has-sidebar">
    <div id="app" style="margin-bottom: 15px;">
        <section id="section" class="h-pad-xl v-pad-xs">
            <div id="content">
                <div class="fw-container">
                    <div class="fw-body">
                        <div class="content">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Gender</th>
                                        <th>Position</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Profile Status</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Gender</th>
                                        <th>Position</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Profile Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer"></footer>
    </div>
</body>
</div>
 -->


{literal}
	<script type="text/javascript">
		$('#interested').change(function(){
		  if($(this).prop("checked")) {
		    $('#ajax-content-sub tr, #ajax-content tr').each(function(e){
				if($(this).data('status')!=2) $(this).toggle('hide');
				 $("i").removeClass("fa-thumbs-o-up");
			});
		  } else {
		     $('#ajax-content-sub tr, #ajax-content tr').each(function(e){
				if($(this).data('status')!=2) $(this).toggle('show');
				$("i").addClass("fa-thumbs-o-up");
			});
		  }
		});
	</script>
{/literal}