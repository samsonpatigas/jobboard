
<div class="row board ml0 pl0">

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <h2>{$translations.dashboard_recruiter.editjob_headline}</h2>
    <p>{$translations.dashboard_recruiter.editjob_about_tag}</p>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_JOBS}"><button type="button" class="btn btn-green tabletmt3p deskFr" >{$translations.dashboard_recruiter.back}</button></a>
  </div>
</div>

<br /><br />

<div class="job-process">
  <form method="post" action="{$BASE_URL}{$URL_JOB_EDITED}" role="form">
    <input type="hidden" id="job_id" name="job_id" value="{$job.id}" />

    <div class="col-sm-12 col-xs-12" >

      <div class="detail-font" >
        <div>
          Job ID: <strong class="process-heading"> {$job.id + 10000} </strong>
        </div>
        <div>
          Created: <strong class="process-heading">{$job.created_on}</strong>
        </div>
        <div>
          Status: 
          {if $job.is_active == 1}
            <strong class="process-heading">Active</strong>
          {else}
            <strong class="process-heading">Inactive</strong>
          {/if}
        </div>        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">{$translations.dashboard_recruiter.post_type_label}</h3>
          </div>
          <div class="col-md-5 col-xs-12">                
            <select id="type_select" name="type_select">
              {foreach from=$types key=id item=value}
                <option {if $value == $job.type_name}selected{/if} value="{$id}">{$value}</option>
              {/foreach}
            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">{$translations.dashboard_recruiter.post_category_label}</h3>
          </div>
          <div class="col-md-5 col-xs-12">                
            <select id="cat_select" name="cat_select">
              {foreach from=$cats key=id item=value}
                <option {if $id == $job.category_id}selected{/if} value="{$id}">{$value}</option>
              {/foreach}
            </select>
          </div>
        </div>
        {if $remote_portal == 'deactivated'}
          <div class="row checkboxes">
            <div class="col-md-3 col-xs-12">
              <h3 class="process-heading">{$translations.dashboard_recruiter.post_location_label}</h3>
            </div>
            <div class="col-md-5 col-xs-12">                
              <select id="location_select" name="location_select">
                {foreach from=$cities key=id item=value}
                  <option {if $id == $job.city_id}selected{/if} value="{$id}">{$value}</option>
                {/foreach}
              </select>
            </div>
          </div>
        {/if}
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Date posted:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input type="date" id="f9_date_posted" name="f9_date_posted" value="{$job.f9_date_posted}" class="job-title">
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Job title:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="title" name="title">
              <option {if $job.title == 'Dental Assistant'} selected="selected"{/if} value="Dental Assistant">Dental Assistant</option>
              <option {if $job.title == 'Hygienist'} selected="selected"{/if} value="Hygienist">Hygienist</option>
              <option {if $job.title == 'Front Office'} selected="selected"{/if} value="Front Office">Front Office</option>
              <option {if $job.title == 'Dentist'} selected="selected"{/if} value="Dentist">Dentist</option>
            </select>      
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Positions:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_position" name="f9_position" value="{$job.f9_position}">
              <option {if $job.f9_position == 'Hygienist'} selected {/if} value="Hygienist">Hygienist</option>
              <option {if $job.f9_position == 'Dental Assistant'} selected {/if} value="Dental Assistant">Dental Assistant</option>
              <option {if $job.f9_position == 'Front Office'} selected {/if} value="Front Office">Front Office</option>
              <option {if $job.f9_position == 'Cross-trained (Front Office/Dental Assistant)'} selected {/if} value="Cross-trained (Front Office/Dental Assistant)">Cross-trained (Front Office/Dental Assistant)</option>
              <option {if $job.f9_position == 'Dentist'} selected {/if} value="Dentist">Dentist</option>
            </select>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Practice Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_practice_type" name="f9_practice_type" value="{$job.f9_practice_type}">
              <option {if $job.f9_practice_type == 'General'} selected="selected"{/if} value="General">General</option>
              <option {if $job.f9_practice_type == 'Ortho'} selected="selected"{/if} value="Ortho">Ortho</option>
              <option {if $job.f9_practice_type == 'Perio'} selected="selected"{/if} value="Perio">Perio</option>
              <option {if $job.f9_practice_type == 'Oral Surgery'} selected="selected"{/if} value="Oral Surgery">Oral Surgery</option>
            </select>            
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">City:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_city" id="f9_city" maxlength="400" type="text" value="{$job.f9_city}" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">State:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_state" id="f9_state" maxlength="400" type="text" value="{$job.f9_state}" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Zip code:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_zip" id="f9_zip" maxlength="400" type="text" " value="{$job.f9_zip}" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">{$translations.dashboard_recruiter.salary_label}</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input name="salary" class="job-title" id="salary" type="text" value="{$job.salary}"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Years of experience:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_yrs_of_experience" name="f9_yrs_of_experience">
              <option {if $job.f9_yrs_of_experience == 'New Graduate'}selected="selected"{/if} value="New Graduate">New Graduate</option>
              <option {if $job.f9_yrs_of_experience == '6 Months – 2 Years'}selected="selected"{/if} value="6 Months – 2 Years">6 Months – 2 Years</option>
              <option {if $job.f9_yrs_of_experience == '3 – 5 Years'}selected="selected"{/if} value="3 – 5 Years">3 – 5 Years</option>
              <option {if $job.f9_yrs_of_experience == '6 Years Plus'}selected="selected"{/if} value="6 Years Plus">6 Years Plus</option>
            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Language:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_language" id="f9_language" maxlength="400" type="text" value="{$job.f9_language}" class="job-title"  />
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Gender:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_gender" name="f9_gender" value="$job.f9_gender">
               <option value="Male" {if $job.f9_gender == "Male"} selected {/if}>Male</option>
               <option value="Female" {if $job.f9_gender == "Female"} selected {/if}>Female</option>
            </select>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Specialties:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_specialties" name="f9_specialties" value="$job.f9_specialties">
               <option {if $job.f9_specialties == "Dental Assistant"} selected {/if} value="Dental Assistant">Dental Assistant (DA)</option>
               <option {if $job.f9_specialties == "FO"} selected {/if} value="FO">(FO)</option>
               <option {if $job.f9_specialties == "Doctor of Dental Surgery"} selected {/if} value="Doctor of Dental Surgery">Doctor of Dental Surgery (DDS)</option>
               <option {if $job.f9_specialties == "Registered Dental Hydrant"} selected {/if} value="Registered Dental Hydrant">Registered Dental Hydrant (RDH)</option>
            </select>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Position Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Full Time" {if $job.f9_position_type == 'Permanent Full Time'}{'checked="checked"'}{/if} >Permanent Full Time</label>
            <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Part Time" {if $job.f9_position_type == 'Permanent Part Time'}{'checked="checked"'}{/if}>Permanent Part Time</label>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Service Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="padding-bottom: 10px;"><input {if $job.f9_service_type == 'Desert Dental Staffing Job Board Posting'} checked="checked" {/if} type="radio" name="f9_service_type" id="f9_service_type" value="Desert Dental Staffing Job Board Posting">Desert Dental Staffing Job Board Posting</label>
            <label style="padding-bottom: 10px;"><input {if $job.f9_service_type == 'Concierge Service'} checked="checked" {/if} type="radio" name="f9_service_type" id="f9_service_type" value="Concierge Service">Concierge Service</label>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Post period:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_post_peroid" name="f9_post_peroid" value="{$job.f9_post_peroid}">
              <option value="30" {if $job.f9_post_peroid == "30"} selected {/if}>(30 days)</option>
              <option value="60" {if $job.f9_post_peroid == "60"} selected {/if}>(60 days)</option>
              <option value="90" {if $job.f9_post_peroid == "90"} selected {/if}>(90 days)</option>
            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Spanish Bilingual:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="Yes" {if $job.f9_bilingual == "Yes"} checked {/if}>Yes</label>
            <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="No" {if $job.f9_bilingual == "No"} checked {/if}>No</label>
          </div>
        </div>

        {assign var="askills" value=","|explode:$job.f9_skills}
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Skills:</h3>
          </div>
          <div class="col-md-8 col-xs-12">
            <p>TIP:  Select only the ABSOLUTE requirements needed for the position to maximize the number of resumes received</p>
            {assign var="key" value='Custom Temps'|array_search:$askills}
            <label>              
              <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Custom Temps</h4>
            </label>
            {assign var="key" value='Cerec System'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Cerec System</h4>
            </label>
            {assign var="key" value='Surgical Implants'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Surgical Implants</h4>
            </label>
            {assign var="key" value='X-ray Certified in AZ'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">X-ray Certified in AZ</h4>
            </label>
            {assign var="key" value='Coronal Polish Certified in AZ'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Coronal Polish Certified in AZ</h4>
            </label>
            {assign var="key" value='IV Sedation'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">IV Sedation</h4>
            </label>
            {assign var="key" value='Insurance Processing'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Insurance Processing</h4>
            </label>
            {assign var="key" value='Treatment Presentation'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Treatment Presentation</h4>
            </label>
            {assign var="key" value='Anesthesia Certified'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Anesthesia Certified</h4>
            </label>
            {assign var="key" value='AR'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">AR</h4>
            </label>
            {assign var="key" value='Laser Certified'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">Laser Certified</h4>
            </label>
            {assign var="key" value='EFDA (Certified Assistant in Arizona)'|array_search:$askills}
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}/>
              <h4 style="line-height: 2;">EFDA (Certified Assistant in Arizona)</h4>                  
            </label>
          </div>      
        </div>

        
        {assign var="aofficesoftware" value=","|explode:$job.f9_office_software}
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Software Experience:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
              {assign var="key" value='Dentrix'|array_search:$aofficesoftware}
              <label>
                <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software" {if $key !== false} checked {/if}>
                <h4 style="padding-top: 5px;">Dentrix</h4>  
              </label>
              {assign var="key" value='EagleSoft'|array_search:$aofficesoftware}
              <label>
                <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}>
                <h4 style="padding-top: 5px;">EagleSoft</h4>
              </label>
              {assign var="key" value='Open Dental'|array_search:$aofficesoftware}
              <label>
                <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}> 
                <h4 style="padding-top: 5px;">Open Dental</h4>
              </label>
              {assign var="key" value='SoftDent'|array_search:$aofficesoftware}
              <label>
                <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}> 
                <h4 style="padding-top: 5px;">SoftDent</h4>
              </label>
              {assign var="key" value='Dentimax'|array_search:$aofficesoftware}
              <label>
                <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox" {if $key !== false} checked {/if}> 
                <h4 style="padding-top: 5px;">Dentimax</h4>
              </label>
              <label>
                <input name="f9_office_software[]" value="Others" class="checkbox-custom" type="checkbox"> 
                <h4 style="padding-top: 5px;">Others</h4>
              </label>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Expiry Date:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
              <span style="{if $job.expires < time()}color:red{/if}">
                {date("m/d/Y",$job.expires)}
              </span>
            </div>
          </div>
        </div>
              

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Renew:</h3>
          </div>
        
          <div class="col-md-5 col-xs-12">
            <select value="{date('Y-m-d', $job.expires)}" id="expires" name="expires" {if $job.expires > time()}disabled="disabled"{/if}>
              <option value="{date('Y-m-d', $job.expires)}" selected >Select Number of Days</option>
              <option value="{date('Y-m-d', $job.expires + (30*24*60*60))}">(30 days)</option>
              <option value="{date('Y-m-d', $job.expires + (60*24*60*60))}">(60 days)</option>
              <option value="{date('Y-m-d', $job.expires + (90*24*60*60))}">(90 days)</option>
            </select>
          </div> 
        </div>

        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Short Description</h3>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="description" name="description"  value="{$job.description}" class="process-textarea" rows="12" cols="1">{$job.description}</textarea>
          </div>
        </div>
        <br />

        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Admin Notes</h3>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="f9_admin_notes" name="f9_admin_notes" class="process-textarea" rows="12" cols="1">{$job.f9_admin_notes}</textarea>
          </div>
        </div>
        <br />
        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Position Notes</h3>
            <p>Enter additional information about the position.  Text entered here will be displayed in the Job Board posting.</p>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="f9_position_notes" name="f9_position_notes" class="process-textarea" rows="12" cols="1">{$job.f9_position_notes}</textarea>
          </div>
        </div>
        <br/>
        <div class="acceptance">
          <label>
            <input class="checkbox-custom" type="checkbox" onchange="SimpleJobScript.applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" {if $job.apply_online == '1'}checked{/if} />
            <h4>{$translations.dashboard_recruiter.apply_label}<span class="apply-desc-span" >{$translations.dashboard_recruiter.apply_desc}</span></h4>
          </label>
        </div>
      
        <div id="apply-desc-block" class="row checkboxes {if $job.apply_online == '1'} displayNone{/if}">
          <div class="col-md-12 col-xs-12 mtm20" >
            <h3 class="process-heading green">{$translations.dashboard_recruiter.howto_apply_label}</h3>    
            <input value="http://{$job.apply_desc}" id="howtoTA" class="process-textarea" id="howtoapply" name="howtoapply">
          </div>
        </div>
      </div>

      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt25 mb50">
        <button type="submit" onclick="return SimpleJobScript.validateDesc();" class="btn mbtn fl" name="submit" id="submit" >{$translations.website_general.text_save}</button>
      </div>
    </div>
  </form>
</div>

{literal}
  <script type="text/javascript">
    $(document).ready( function() {
      $("input[name='f9_service_type']").change(function(){
          selected_value = $("#f9_service_type:checked").val();
          if(selected_value == 'Concierge Service'){
            $('#f9_post_peroid').append($('<option>', {
              value: 3650,
              text: 'No Expiry'
            }));
            $('option[value="3650"]').prop("selected", true);
          }else{
            $("#f9_post_peroid option[value='3650']").remove();
          }
      });


    });
  </script>
{/literal}
  <script type="text/javascript">
    {if $job.expires > time()}
      $('#expires').find(":selected").text('Job has not expired');
    {/if}
  </script>