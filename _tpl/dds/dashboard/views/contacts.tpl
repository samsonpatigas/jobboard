<div class="row board">
  <h2>Contacts</h2>...{$ID}
  <div id="cons">

	</div>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #7527a0; color: #fff;">
        <h4 class="modal-title" style="font-size: 20px;">Candidate Profile</h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="row">
          <div class="col-md-6">

              <ul class="list-group">
                <li class="list-group-item"><b>Fullname:</b> <label id="name"></label></li>
                <li class="list-group-item"><b>Position:</b> <label id="position"></label></li>
                <li class="list-group-item"><b>Phone:</b> <label id="phone"></label></li>
                <li class="list-group-item"><b>Email:</b> <label id="email"></label></li>
                <li class="list-group-item"><b>About:</b> <label id="aboutme"></label></li>
              </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"> 
  $.getJSON('http://jobboard.f9portal.net/get_contacts?emp={$ID}', function(data) {
  //$.getJSON('http://jobboard.f9portal.net/get_contacts?emp=9196', function(data) {
    contacts = data;
    console.log(contacts);

    for (let contact of contacts) {
      document.getElementById('cons').innerHTML += '<tr>'+
        '<td>'+contact.contact_name+'</td>'+
        '<td>'+contact.phone_number+'</td>'+
        '<td>'+contact.phone_type+'</td>'+
        '<td>'+contact.email_address+'</td>'+
        '<td>'+contact.email_type+'</td>' +
        '<td><a href="/edit_contact?id=' + contact.id + '">Edit</a> <a href="/delete_contact?id=' + contact.id + '">Delete</a>'
        +'</tr>';
    }
  });
</script>