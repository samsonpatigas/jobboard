<link rel="stylesheet" href="/_tpl/dds/1.5/css/reset.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" integrity="sha256-vK3UTo/8wHbaUn+dTQD0X6dzidqc5l7gczvH+Bnowwk=" crossorigin="anonymous" />
<div class="row board ml0 pl0">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <h2>Job Preview</h2>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_JOBS}"><button type="button" class="btn btn-green tabletmt3p deskFr" >{$translations.dashboard_recruiter.back}</button></a>
  </div>
</div><br/>
<div class="columns">
  <div class="column">
    <h3>{$job.title|strip_tags:false} in {$job.location_asci|strip_tags:false}</h3><a class="button is-primary" href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_EDIT_JOB}/{$job.id}">Edit this Job Post</a>
  </div>
</div>
<div class="columns">
  <div class="column">
      <table class="table">
        <tr>
          <td class="has-text-weight-bold">Company:</td>
          <td>{$job.company|strip_tags:false}</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Job Type:</td>
          <td>{$job.job_type|strip_tags:false}</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Category:</td>
          <td>{$job.category_name|strip_tags:false}</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Salary</td>
          <td>${$job.salary}</td>
        </tr>
      </table>
  </div>
  <div class="column">
    <table class="table">
      <tr>
        <td class="has-text-weight-bold">Date Posted:</td>
        <td>{$job.f9_date_posted}</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">Position:</td>
        <td>{$job.f9_position}</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">City:</td>
        <td>{$job.f9_city}</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">Zip Code:</td>
        <td>{$job.f9_zip}</td>
      </tr>
    </table>
  </div>
</div>



