<style type="text/css">
.modal-body {
padding: 5% 3% !important;
}
.modal-dialog {
  margin-top: 10% !important;
}
</style>

<div class="row board">
  <h2>{$translations.dashboard_recruiter.jobs_headline}</h2>
  <p>{$translations.dashboard_recruiter.jobs_about_tag}</p>
</div>
<br /><br />

<div class="panel-group">
    <div class="panel panel-default" >
      <div id="arrow" class="">
        <h4 class="panel-title arrow-down" data-toggle="collapse" href="#collapse1" onclick="this.classList.toggle('active')">ACTIVE JOBS</h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
        <div class="row board mt0">
          <div class="table-responsive job-table">
            
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th>{$translations.dashboard_recruiter.jobs_title}</th>
                    <th>{$translations.dashboard_recruiter.jobs_status}</th>
                    <th>{$translations.dashboard_recruiter.jobs_views}</th>
                    <th style="width: 20%;">{$translations.dashboard_recruiter.jobs_applications}</th>
                    <!--<th>{$translations.dashboard_recruiter.jobs_statistics}</th>-->
                    <th>{$translations.dashboard_recruiter.info}</th>
                    <!-- <th>{$translations.dashboard_recruiter.jobs_edit}</th>-->
                    <th>Deactivate</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody> 
                {foreach from=$dash_jobs item=job}
                  <tr>
                    {if $job.is_active == '1'}
                      <td>{$job.title}</td>
                      <td>{$translations.dashboard_recruiter.jobs_active}</td>
                      <td>{$job.views_count}</td>
                      <td>{$apps_array[$job.id]}</td>
                      <!--<td class="opaque"><a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_STATISTICS}/{$job.id}/{$job.views_count}-{$apps_array[$job.id]}"><i class="fa fa-bar-chart-o fa-lg" aria-hidden="true"></i></a></td>-->
                      <td><a data-toggle="modal" data-target="#modal_{$job.id}" href="#"><i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>
                      <!--<td class="opaque">
                      <a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_EDIT_JOB}/{$job.id}"><i class="fa fa-wrench fa-lg" aria-hidden="true"></i></a> 
                      </td>-->
                      <!--  <td class="opaque"><a data-toggle="modal" data-target="#myModal_{$job.id}"><i class="fa fa-ban fa-lg" aria-hidden="true" ></i></a></td> -->
                      <td class="opaque"><a id="deactivateConfirm-{$job.id}" href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_DEACTIVATE_MODAL}/{$job.id}"><i class="fa fa-ban fa-lg" aria-hidden="true" ></i></a></td>
                      <td class="opaque"><a href="/{$URL_DASHBOARD}/URL_DASHBOARD_PREVIEW/{$job.id}">Preview</a></td>
                    {/if}
                  </tr>
                {/foreach}
                </tbody>
            </table>
          </div>
        </div>
      </div>
      </div>
  </div>
   
  <!--INACTIVE JOBS-->
    <div class="panel panel-default">
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapse2" onclick="this.classList.toggle('active1')">INACTIVE JOBS</h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">

    <div class="row board mt0">
          <div class="table-responsive job-table">
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th>{$translations.dashboard_recruiter.jobs_title}</th>
                    <th>{$translations.dashboard_recruiter.jobs_status}</th>
                    <th>Expiry Date</th>
                    <th>{$translations.dashboard_recruiter.jobs_views}</th>
                    <th style="width: 20%;">{$translations.dashboard_recruiter.jobs_applications}</th>
                    <!--<th>{$translations.dashboard_recruiter.jobs_statistics}</th>-->
                    <th>Activate</th>
                    <th>{$translations.dashboard_recruiter.jobs_edit}</th>
                    <th>{$translations.apply.table_jobs_delete}</th>
                  </tr>
                </thead>
                <tbody>

              {foreach from=$dash_jobs item=job}
                  <tr>
              {if $job.is_active == '0'}
                <td><s>{$job.title}</s></td>
                <td class="red">{$translations.dashboard_recruiter.jobs_inactive}</td>
                <td>{date("m-d-Y", $job.expires)}</td>
                <td>{$job.views_count}</td>
                <td>{$apps_array[$job.id]}</td>
                <!--<td class="opaque"><a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_STATISTICS}/{$job.id}/{$job.views_count}-{$apps_array[$job.id]}"><i class="fa fa-bar-chart-o fa-lg" aria-hidden="true"></i></a></td>-->
                <td class="opaque"><a id="renewConfirm-{$job.id}" href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_RENEW_MODAL}/{$job.id}"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></a></td>
                <td class="opaque"><a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_EDIT_JOB}/{$job.id}"><i class="fa fa-wrench fa-lg" aria-hidden="true"></i></a></td>
                <td class="opaque"><a id="deleteConfirm-{$job.id}" href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_DELETE_JOB}/{$job.id}"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
                  {/if}
                  </tr>
                 {/foreach}
                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>


    <!--EXpired-->
    <div class="panel panel-default">
      <div class="">
        <h4 class="panel-title arrow-down2" data-toggle="collapse" href="#collapse3" onclick="this.classList.toggle('active2')">EXPIRED</h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">

    <div class="row board mt0">
          <div class="table-responsive job-table">
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th>{$translations.dashboard_recruiter.jobs_title}</th>
                    <th>{$translations.dashboard_recruiter.jobs_status}</th>
                    <th>{$translations.dashboard_recruiter.jobs_views}</th>
                    <th style="width: 20%;">{$translations.dashboard_recruiter.jobs_applications}</th>
                    <!--<th>{$translations.dashboard_recruiter.jobs_statistics}</th>-->
                    <th>{$translations.dashboard_recruiter.info}</th>
                    <th>{$translations.dashboard_recruiter.jobs_edit}</th>
                    <th>{$translations.apply.table_jobs_delete}</th>
                  </tr>
                </thead>
                <tbody>
              
                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>

  </div>


  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

{foreach from=$dash_jobs item=job}
<script type="text/javascript">
  $("#deleteConfirm-{$job.id}").confirm({
        text: "{$translations.dashboard_recruiter.delete_job_message}",
        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "{$translations.dashboard_recruiter.yes_i_do}",
        cancelButton: "{$translations.dashboard_recruiter.text_no}",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
</script>
{/foreach}


{foreach from=$dash_jobs item=job}
<script type="text/javascript">
  $("#deactivateConfirm-{$job.id}").confirm({
        text: "Do you want to deactivate this job?",
        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "{$translations.dashboard_recruiter.yes_i_do}",
        cancelButton: "{$translations.dashboard_recruiter.text_no}",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
</script>
{/foreach}


{foreach from=$dash_jobs item=job}
<script type="text/javascript">
  $("#renewConfirm-{$job.id}").confirm({
        text: "Do you want to renew this job?", 

        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "{$translations.dashboard_recruiter.yes_i_do}",
        cancelButton: "{$translations.dashboard_recruiter.text_no}",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
</script>
{/foreach}

<!-- {literal}
<script type="text/javascript">
  $( document ).ready(function() {
    document.getElementById("arrow").style.backgroundImage = "url('/uploads/logos/uparrow.png')";
     $('#arrow').click(function(){
        document.getElementById("arrow").style.backgroundImage = "url('/uploads/logos/arrow.png')";
     });
  });
</script>
{/literal} -->
