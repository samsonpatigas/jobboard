<div class="row board">
  <h2>Contacts</h2>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Contact Name</th>
          <th scope="col">Phone Number</th>
          <th scope="col">Phone Type</th>
          <th scope="col">Email Address</th>
          <th scope="col">Email Type</th>
          <th><button id="about" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i> Add</button></th>
        </tr>
      </thead>
      <tbody id='cons'>
      </tbody>
    </table>
</div>
<div id="app">
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #7527a0; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Add Contacts</h4>
        </div>
        <div class="modal-body">
          <br>
          <form 
            action="https://jobboard.f9portal.net/save_contact"
            method="post"
            ref="form"
          >
          <div class="row">
            <div class="col-md-6">
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Contact Name:
                  </div>
                  <div class="col-md-8">
                    <input name="contact_name" id="contact_name" placeholder="Name" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Phone Number:
                  </div>
                  <div class="col-md-8">
                    <input name="phone_number" id="phone_number" placeholder="Phone Number" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Phone Type:
                  </div>
                  <div class="col-md-8">
                    <select name="phone_type" id="phone_type">
                      <option value="Main">Main</option>
                      <option value="Mobile">Mobile</option>
                    </select>
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Email Address:
                  </div>
                  <div class="col-md-8">
                   <input name="email_address" id="email_address" placeholder="Email Addess" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Email Type:
                  </div>
                  <div class="col-md-8">
                    <select name="email_type" id="email_type">
                      <option value="Main">Main</option>
                      <option value="Billing">Billing</option>
                      <option value="Office Mgr">Office Mgr</option>
                    </select>
                    <input name="employer_id" id="employer_id" type="hidden" value="{$ID}">
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" onclick="app.submit()">Save</button>
                </div>                
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="myModalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #7527a0; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Edit Contacts</h4>
        </div>
        <div class="modal-body">
          <br>
          <form          
            action="https://jobboard.f9portal.net/update_contact"
            method="post"
            ref="update_form"
          >
          <div class="row">
            <div class="col-md-6">
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Contact Name:
                  </div>
                  <div class="col-md-8">
                    <input name="e_contact_name" id="e_contact_name" placeholder="Name" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Phone Number:
                  </div>
                  <div class="col-md-8">
                    <input name="e_phone_number" id="e_phone_number" placeholder="Phone Number" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Phone Type:
                  </div>
                  <div class="col-md-8">
                    <select name="e_phone_type" id="e_phone_type">
                      <option value="Main">Main</option>
                      <option value="Mobile">Mobile</option>
                    </select>
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Email Address:
                  </div>
                  <div class="col-md-8">
                   <input name="e_email_address" id="e_email_address" placeholder="Email Addess" />
                  </div>
                </div>
                <div class="row" style="height: 35px;">
                  <div class="col-md-4">
                    Email Type:
                  </div>
                  <div class="col-md-8">
                    <select name="e_email_type" id="e_email_type">
                      <option value="Main">Main</option>
                      <option value="Billing">Billing</option>
                      <option value="Office Mgr">Office Mgr</option>
                    </select>
                    <input name="e_id" id="e_id" type="hidden" value="">
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" onclick="app.update()">Update</button>
                </div>                
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
  //index.php->get_contacts 
  $.getJSON('http://jobboard.f9portal.net/get_contacts?emp={$ID}', function(data) {
  // $.getJSON('http://jobboard.f9portal.net/get_contacts?emp=9149', function(data) {
   contacts = data;
   console.log(contacts);

  for (let contact of contacts) {
    document.getElementById('cons').innerHTML += '<tr>'+
      '<td>'+contact.contact_name+'</td>'+
      '<td>'+contact.phone_number+'</td>'+
      '<td>'+contact.phone_type+'</td>'+
      '<td>'+contact.email_address+'</td>'+
      '<td>'+contact.email_type+'</td>' +
      '<td><button style="width:100px;" id="about" data-toggle="modal" data-target="#myModalEdit" onclick="app.editContact('+ contact.id + ')" href=""><i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i> Edit </button><a class="btn" style="width:100px;border:0px;line-height:2.5;border-radius:1px" href="/delete_contact?id=' + contact.id + '">Delete</a>'
      +'</tr>';
  }
  });
  const app = new Vue({
  el: '#app',
  data: {},
  methods:{
    submit : function(){
      this.$refs.form.submit()
    },
    update : function(){
      this.$refs.update_form.submit()
    },
    editContact: function(emp){
      $.getJSON('http://jobboard.f9portal.net/api_get_contact?emp=' + emp, function(data) {
        $('input#e_contact_name').val(data.contact_name);
        $('input#e_phone_number').val(data.phone_number);
        $('select#e_phone_type').val(data.phone_type);
        $('input#e_email_address').val(data.email_address);
        $('select#e_email_type').val(data.email_type);
        $('input#e_id').val(data.id);

        document.body.style.overflow = 'scroll';
      });
    }
  }
});
</script>

