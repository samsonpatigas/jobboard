{include file="1.5/layout/sjs-header.tpl"}
<!-- ADDED ERROR MSG LIST -->

<style type="text/css"> 
  .error-message {
    background-color: #fce4e4;
    border: 1px solid #fcc2c3;
    float: left;
    width: 100%;
    padding: 20px 30px;
  }

  .error-text {
    color: #cc0033;
    font-family: Helvetica, Arial, sans-serif;
    font-size: 13px;
    font-weight: 400;
    line-height: 20px;
    text-shadow: 1px 1px rgba(250,250,250,.3);
  }

  .error-info {  
    float: center;
  }

  disabled-checkbox{
    border-color: gray;
    background-color: lightgray;
  }

  .enabled-checkbox{
    border-color: #7527a0;
    background-color: white;  
  }

  #applicantCvLabel:hover {
    background-color: #7527a0;
    color: white;
  }

  #applicantCvLabel {
    padding: 6px !important;
  }

  .create-profile .row .col-md-6 {
      padding-left: 1% !important;
  }

  @media screen and (max-width: 1024px) {
    .create-profile .row input[type="checkbox"] {
        width: 25px !important;
    }

    .create-profile .row input#f9_gender, .create-profile .row input.checkbox-gx {
        width: 25px!important;
    }

    .sat {
        padding-left: 5px;
    }
    .wed, .sat, .thu {
        width: 75px!important;
    }

    .tempText, .permText {
      margin-left: 10px;
    }

  }

  @media screen and (min-width: 1024px){
/*    .hourlyRate {
      margin-left: -75px;
    }*/

    #feedback-err_birthdate {
      margin-left: -5px!important;
        margin-top: 10px!important;
    }

/*    #feedback-err_cb_tempdays {
        margin-left: -65px;
    }

    #feedback-err_cb_permdays {
       margin-left: -65px;
    }*/
  }

  .checkbox-custom:checked:after, .create-profile .row .tos input:checked:after, #jobpopup .tos input:checked:after {
    top:-5px!important;
  }

  #feedback-err_f9_first_name, #feedback-err_f9_last_name {
    padding: 0px;
  }

  .radio-tos {
    width: 18px!important;
    height: 18px!important;
  }

  .label-tos {
    margin-top: 5px;
  }
</style>

<!-- ENDPOINT -->

<div class="contact-us create-profile">
  <div class="container">
    <h2 style="margin-left: 0%;">{$translations.apply.create_profile_headline}</h2>
        <!-- HIDING IT FOR NOW ONLY PREPARING THIS INCASE NEEDED -->
    <div class="error-message" id="errors" style="width=100%">
      <span class="error-info" style="font-weight: 400";>
        <i class="fa fa-warning" style="color:red;"></i> Incorrect or missing information, Please scroll down to correct your entries.
        <ul id="error-message-text" style="text-align:left; list-style-type: circle;"></ul>
      </span>   
    </div>

    <script type="text/javascript">
      var x = document.getElementById("errors");
        // x.style.display = "block"; //show
        x.style.display = "none"; //hide
      var errorText = document.getElementById('error-message-text');
      errorText.style.display = "none";
    </script>

    <form id="form_upload" role="form" method="post" action="{$BASE_URL}{$URL_REGISTER_APPLICANTS}" enctype="multipart/form-data">
      <input type="hidden" id="external_links" name="external_links" value="0" />

      <div class="row" style="width: 100%;">
        <div class="col-md-6 col-xs-12">

          <h3>Photo (Optional)</h3>
          <img src="{$BASE_URL}{$applicant.f9_photo}" id="image" class="img" style="width:240px; height:240px;-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);-moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);">
          
          <label id="applicantCvLabel" for="f9_photo" class="fullD" style="cursor:pointer; width: 240px !important;float: left;height: 50px;line-height: 2.5;border-radius: 0px!Important;">{$translations.profile.form_upload}</label>
          
          <input accept=".gif,.JPG,.jpg,.jpeg,.png" onchange="showImage.call(this)" type="file" name="f9_photo" id="f9_photo" class="form-control inputfile minput" />
          
          <h3>Email<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultemail'></p></h3>
          <input required type="email" id="apply_email" name="apply_email" maxlength="300" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err_apply_email" class="negative-feedback displayNone">* Email format is invalid or Field is empty.</div>
          <div id="email_apply_email_exist" class="negative-feedback displayNone">* Email already Registered.</div>

          <h3>Repeat email<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultremail'></p></h3>
          <input required type="email" id="re_email" name="re_email" maxlength="300" onfocus="checkEmail()" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err_re_email" class="negative-feedback displayNone">* Email format is invalid or Field is empty.</div>
          <div id="feedback-err-email" class="negative-feedback displayNone">* Email format is invalid or mismatch</div>

          <h3 style="margin-bottom:0px;">Password<red style="color: red;padding: 10px;">*</red><p style="color: black;font-size: 80%; margin-bottom:0px; margin-top: 10px;">Minimum 8-16 characters, 1 uppercase letter, 1 lowercase letter, and 1 number.</p><p style="margin-top: 10px;" id='password'></p></h3>
          <input required type="password" id="pass1" name="pass1"  maxlength="50" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err-pass1" class="negative-feedback displayNone">This password is Invalid.</div>
          <p id="valpass" style="color: red; opacity: 0.75; font-size: 80%;margin-top: 10px;float: left;margin-left: 10px;"></p>

          <h3>Repeat Password<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='rpassword'></p></h3>
          <input required type="password" id="pass2" name="pass2"  maxlength="50" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err-pass2" class="negative-feedback displayNone">Password Mismatch</div>

            <div style="display: none;">
              <h3>{$translations.apply.name}</h3>
              <input id="apply_name" name="apply_name" type="text" maxlength="500" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
            </div>
            <h3>{$translations.apply.f9_first_name}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultfname'></p></h3>
            <input required id="f9_first_name" name="f9_first_name" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
            <div id="feedback-err_f9_first_name" class="col-md-12 negative-feedback displayNone">First Name is Empty</div>

            <h3>{$translations.apply.f9_middle_name}</h3>
            <input id="f9_middle_name" name="f9_middle_name" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">      

            <h3>{$translations.apply.f9_last_name}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultlname'></p></h3>
            <input required id="f9_last_name" name="f9_last_name" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
            <div id="feedback-err_f9_last_name" class="col-md-12 negative-feedback displayNone">Last Name is Empty</div>

            <h3>Date of Birth<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultdob'></p></h3>
            <input required class="input-birthdate" id="birthdate" name="birthdate" type="text" maxlength="10" placeholder="MM/DD/YYYY" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
            <div id="feedback-err_birthdate" class="negative-feedback-form displayNone col-md-12">Invalid Date, enter as MM/DD/YYYY</div>
            <!-- DATE MASKING -->
            <!-- ADDED FOR DOB VALIDATION -->
            {literal}

              <script type="text/javascript">
                /*document.getElementById('birthdate').addEventListener('input', function (e) {
                  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,2})(\d{0,4})/);
                  e.target.value = !x[2] ? x[1] : x[1] + '/' + x[2] + (x[3] ? '/' + x[3] : '');
                });*/

                document.getElementById('birthdate').addEventListener('input', function (e) {
                  var x = e.target.value.replace(/[^0-9\/]/g, '')
                  e.target.value = x;
                  console.log(x);
                });

                document.getElementById('birthdate').addEventListener('blur', function (e) {
                  var pdate = e.target.value.split('/');
                  var mm = parseInt(pdate[0]);
                  var dd = parseInt(pdate[1]);
                  var yy = parseInt(pdate[2]);

                  if(mm>12){
                    mm = 12;
                  }
                  if(dd>31){
                    dd = 31;
                  }
                  if(yy>9 && yy<100){
                    yy = yy + 1900
                  }
                  if(mm<10){
                    mm = '0' + mm.toString();
                  }
                  if(dd<10){
                    dd = '0' + dd.toString();
                  }
                  var fd = mm.toString().concat('/', dd.toString(), '/', yy.toString());
                  console.log(mm);
                  console.log(dd);
                  console.log(yy);

                  if(!(isNaN(mm) || isNaN(dd) || isNaN(yy))) {
                    if(yy>1000){
                      console.log('yy:' + yy);
                      $('#birthdate').val(fd);
                    }
                  }else{
                    console.log('error');
                    //$("#error-message-text").append('<li style="font-weight:400">Incomplete Birthdate Entry</li>');
                    $('#errors').show();
                  }                 
                });
              </script>
            {/literal}

            <h3>{$translations.apply.f9_gender}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultgender'></p></h3>
          <!--<input required id="f9_gender" name="f9_gender" type="text" maxlength="500">-->
            <div class="row" style="float: left;">
              <div class="col-md-6">
                <div class="tos">
                  <label><input name="f9_gender" id="f9_gender" value="Male" type="radio" class="checkbox-g" />
                  <h4>Male</h4></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="tos">
                  <label><input name="f9_gender" id="f9_gender" value="Female" type="radio" class="checkbox-g" />
                  <h4>Female</h4></label>
                </div>
              </div>
            </div>
            <div id="feedback-err_cb_f9_gender" class="negative-feedback displayNone">Please check a box</div>

            <h3>Street Address<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultstreetaddress'></p></h3>
            <input required id="f9_address_1" name="f9_address_1" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
            <div id="feedback-err_f9_address_1" class="negative-feedback displayNone">Street Address is Empty</div>

            <h3>Unit #</h3>
            <input id="f9_address_2" name="f9_address_2" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">

            <div style="display: none;">
              <h3>{$translations.apply.portfolio}</h3>
              <input type="text" id="portfolio" name="portfolio" maxlength="500">
              

              <h3>{$translations.apply.location}</h3>
              <input type="text" id="apply_location" name="apply_location" >
            </div>

            <h3>{$translations.apply.f9_city}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultcity'></p></h3>
            <input required id="f9_city" name="f9_city" type="text" maxlength="500" style="font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
            
            <div id="feedback-err_f9_city" class="negative-feedback col-md-6 displayNone">City is Empty</div>

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>
  <style type="text/css">

    select.dropdown{
        height: 50px;
        padding: 10px;
        border: 0;
        font-size: 15px;
        width: 200px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    label.wrap:after {
      content: '\f078';
      font: normal normal normal 17px/1 FontAwesome;
      color: #871f78;
        position: absolute;
        right: 0;
        left: 385px;
        top: 18px;
        z-index: 1;
        width: 10%;
        height: 100%;
        pointer-events: none;
    }

    select{
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        text-align: left;
      text-align-last: left;
    }

  option {
    text-align: left;
    /* reset to left*/
  }

  </style>
            <h3>{$translations.apply.f9_state}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='state'></p></h3>
            <label class="wrap">
            <select required id="f9_state" name="f9_state" class="dropdown" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    padding: 10px;
                    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    min-height: 47px;
                    width: 75%;
                    border-radius: 5px;
                    padding: 0% 3% 0% 6%;
                    border: none;
                    float: left;
                    margin-bottom: 2%;
                    font-size: 14px;
                    padding: 10px;
                    background-color:white;
                    border-color: gainsboro;
                    border-width: 1px;
                    border-style: solid;
                    ">
                    {$states= ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']}
                      <option value="" disabled selected>Select your option</option>
                      {foreach from=$states item=x}
                        <option value="{$x}">{$x}</option>
                      {/foreach}
                  </select>
            </label>
            <div id="feedback-err_f9_state" class="negative-feedback displayNone">State is Empty</div>


          <h3>{$translations.apply.f9_zip}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultzip'></p></h3>
          <input required id="f9_zip" name="f9_zip" type="text" maxlength="5" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err_f9_zip" class="negative-feedback col-md-6 displayNone">Enter 5 digit zip code</div>
          <!-- COMMENTED AS SUGGESTED IN UPDATES   -->    
          
            <h3>What are your major cross-streets<br><small>(Highly Recommended)</small><p style="margin-top: 10px;" id='major'></p></h3>
            <textarea name="f9_cross_st" id="f9_cross-st" rows="4" cols="50" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" maxlength="500" placeholder="What are your major cross-streets?"></textarea>
            
          <h3>Cell Phone Provider<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='carrier'></p></h3>
          <label class="wrap">
            <select required id="phone_carrier" name="phone_carrier" class="dropdown"
              style="
                -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  padding: 10px;
                  box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  min-height: 47px;
                  width: 75%;
                  border-radius: 5px;
                  padding: 0% 3% 0% 6%;
                  border: none;
                  float: left;
                  margin-bottom: 2%;
                  font-size: 14px;
                  padding: 10px;
                background-color:white;
                border-color: gainsboro;
                border-width: 1px;
                border-style: solid;
              ">
                <option value="" disabled selected>Select your option</option>      
                <option value="Sprint">Sprint</option>
                <option value="Verizon">Verizon</option>
                <option value="T-Mobile">T-Mobile</option>
                <option value="MetroPCS">MetroPCS</option>
                <option value="AT&T">AT&T</option>
                <option value="Boost Mobile">Boost Mobile</option>
                <option value="Mint Mobile">Mint Mobile</option>
                <option value="Simple Mobile">Simple Mobile</option>
                <option value="FreedomPop">FreedomPop</option>
                <option value="Tello">Tello</option>
                <option value="UNREAL Mobile">UNREAL Mobile</option>
                <option value="Twigby">Twigby</option>
                <option value="US Mobile">US Mobile</option>
                <option value="Consumer Cellular">Consumer Cellular</option>
                <option value="Total Wireless">Total Wireless</option>
                <option value="FreeUp Mobile">FreeUp Mobile</option>
                <option value="Red Pocket">Red Pocket</option>
                <option value="TextNow">TextNow</option>
                <option value="Straight Talk">Straight Talk</option>
                <option value="XFINITY Mobile">XFINITY Mobile</option>
                <option value="Ting">Ting</option>
                <option value="H20 Wireless">H20 Wireless</option>
                <option value="TracFone">TracFone</option>
                <option value="Net10">Net10</option>
                <option value="Ultra Mobile">Ultra Mobile</option>
                <option value="Virgin Mobile">Virgin Mobile</option>
                <option value="Page Plus">Page Plus</option>
              </select>
          </label>
          <div id="feedback-err_phone_carrier" class="negative-feedback displayNone">Cellphone Provider is Empty</div>

          <h3>Cell Phone<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultcp'></p></h3>
          <input required type="text" id="apply_phone" name="apply_phone" placeholder="10-digit mobile number" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          <div id="feedback-err_apply_phone" class="negative-feedback col-md-6 displayNone">Cellphone is Empty</div>

          {literal}
          <script type="text/javascript">
            document.getElementById('apply_phone').addEventListener('input', function (e) {
            var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
            e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
            });
          </script>
          {/literal}

          <div style="display: none;">
            <h3>{$translations.apply.f9_isProfile}</h3>
            <input id="f9_isProfile" name="f9_isProfile" type="text" maxlength="500">
          </div>

          <h3>{$translations.apply.f9_yrs_experience}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='years'></p></h3>
          <label class="wrap">
            <select required id="f9_yrs_experience" name="f9_yrs_experience" class="dropdown" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              min-height: 47px;
              width: 75%;
              border-radius: 5px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 2%;
              font-size: 14px;
              padding: 10px;
              background-color:white;
              border-color: gainsboro;
              border-width: 1px;
              border-style: solid;">
              <option value="" disabled selected>Select your option</option>
              <option value="New Graduate">New Graduate</option>
              <option value="6 months-2 years">6 Months - 2 Years</option>
              <option value="3-5 years">3 - 5 Years</option>
              <option value="6 years plus">6 Years Plus</option>
            </select>
          </label>
          <div id="feedback-err_f9_yrs_experience" class="negative-feedback displayNone">Years of Experience is Empty</div>

          <!-- ADDED CHANGE POSITIONS TO CHECKBOXES -->
          <h3>Are you bilingual Spanish?<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultbilingual'></p></h3>
          <div class="row" style="float: left;">
            <div class="col-md-6">
              <div class="tos">
                <label><input name="span_bilingual" value="Yes" type="radio" class="checkbox-gx" style="font-size:23px;" required />
                <h4>Yes</h4></label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tos">
                <label><input name="span_bilingual" value="No" type="radio" class="checkbox-gx" required />
                    <h4>No</h4>
                </label>
              </div>
            </div>
          </div>
          <div id="feedback-err_cb_span_bilingual" class="negative-feedback displayNone">Please Check a box</div>
          <h3>Positions<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id="posresult"></p></h3>
          <div class="row" style="float: left;">
            <label class="wrap">
            <select required id="f9_position" name="f9_position" class="dropdown" style="
              
                -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  padding: 10px;
                  box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  min-height: 47px;
                  width: 75%;
                  border-radius: 5px;
                  padding: 0% 3% 0% 6%;
                  border: none;
                  float: left;
                  margin-bottom: 2%;
                  font-size: 14px;
                  padding: 10px;
                background-color:white;
                border-color: gainsboro;
                border-width: 1px;
                border-style: solid;">
              <option value="" disabled selected>Select your option</option>
              <option value="Dental Assistant">Dental Assistant</option>
              <option value="Hygienist">Hygienist</option>
              <option value="Cross-trained (Front Office/Dental Assistant)">Cross-trained (Front Office/Dental Assistant)</option>              
              <option value="Front Office">Front Office</option>
              <option value="Dentist">Dentist</option>                         
            </select>
          </label>
            <div id="feedback-err_f9_position" class="col-md-6 negative-feedback displayNone">Check at least one box</div>       
          </div>

          <!-- ENDPOINT -->       
          <h3>Receive Notifications by :<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resultnotify'></p></h3>
            {if strpos($applicant.notifyby, 'Email') !== false}
              {$notifybye = 'checked'}
            {else}
              {$notifybye = ''}
            {/if}
            {if strpos($applicant.notifyby, 'Text Message') !== false}
              {$notifybyt = 'checked'}
            {else}
                  {$notifybyt = ''}
            {/if}         
          <div class="row" style="float: left;">
            <div class="col-md-6">
              <div class="tos">
                  <input name="notifyby[]" id="notifyby" value="Email" class="checkbox-custom" type="checkbox" {$notifybye} />
                  <h4 style="line-height: 2;">Email</h4>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tos">
                  <input name="notifyby[]" id="notifyby" value="Text Message" class="checkbox-custom" type="checkbox" {$notifybyt} /> 
                  <h4 style="line-height: 2;">Text Message</h4>
              </div>
            </div>
          </div>
          <div id="feedback-err_cb_notifyby" class="negative-feedback displayNone">Must select at least one way to notify you</div>

        </div>

        <div class="col-md-6 col-xs-12">
        
          <h3>Software that you are Proficient in <red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='proresult'></p></h3>
          <div class="row" style="float: left;">
              <div class="col-md-6">
              <div class="tos">
              <label><input name="pro_soft[]" value="Dentrix" type="checkbox" id="pro_soft" />
                <h4>Dentrix</h4></label>  
              </div>
              <div class="tos">
              <label><input name="pro_soft[]" value="EagleSoft" type="checkbox" />
                    <h4>EagleSoft</h4></label>
              </div>
              <div class="tos">
              <label><input name="pro_soft[]" value="Open Dental" type="checkbox" />
                    <h4>Open Dental</h4></label>
              </div>
            </div>
              <div class="col-md-6">
              <div class="tos">
              <label><input name="pro_soft[]" value="SoftDent" type="checkbox" />
                    <h4>SoftDent</h4></label>
              </div>
              <div class="tos">
              <label><input name="pro_soft[]" value="Dentimax" type="checkbox" />
                          <h4>Dentimax</h4></label>
              </div>  
            </div>
            <input type="text" name="pro_soft[]" placeholder="Others..." style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
          </div>
          <div id="feedback-err_cb_pro_soft" class="negative-feedback displayNone">Please check at least one Box</div>
        
          <h3>Areas of Dentistry you have worked in <red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='arearesult'></p></h3>
          <div class="row" style="float: left;">
              <div class="col-md-6">
              <div class="tos">
              <label>
                <input name="workarea[]" value="General" type="checkbox" id="workarea" />
                <h4>General</h4></label>  
              </div>
              <div class="tos">
              <label>
                <input name="workarea[]" value="Perio" type="checkbox" />
                <h4>Perio</h4></label>
              </div>
              <div class="tos">
              <label>
                <input name="workarea[]" value="Oral Max Surgery" type="checkbox" />
                <h4>Oral Max Surgery</h4></label>
              </div>
            </div>
              <div class="col-md-6">
              <div class="tos">
              <label>
                <input name="workarea[]" value="Endo" type="checkbox" />
                <h4>Endo</h4></label>
              </div>
              <div class="tos">
              <label>
                <input name="workarea[]" value="Pedo" type="checkbox" />
                <h4>Pedo</h4></label>
              </div>
              <div class="tos">
              <label>
                <input name="workarea[]" value="Ortho" type="checkbox" />
                <h4>Ortho</h4></label>
              </div>
            </div>
          </div>
          <div id="feedback-err_cb_workarea" class="negative-feedback displayNone">Please check at least one Box</div>

          <h3>Skills you are PROFICIENT in <red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='skillsresult'></p></h3>
          <div class="row" style="float: left; width: 115%;">
              <div class="col-md-6">
            <div class="tos">
                <label>
                  <input name="skills_prof[]" value="Custom Temps" type="checkbox" id="skills_prof"></input> 
                  <h4>Custom Temps</h4>
                </label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Cerec System" type="checkbox"></input> 
                <h4>Cerec System</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Surgical Implants" type="checkbox"></input> 
                <h4>Surgical Implants</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="X-ray Certified in AZ" type="checkbox"></input> 
                <h4>X-ray Certified in AZ</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Coronal Polish Certified in AZ" type="checkbox"></input> 
                <h4>Coronal Polish Certified in AZ</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="IV Sedation Certified" type="checkbox"></input> 
                <h4>IV Sedation Certified</h4></label>
              </div>
              </div>
              <div class="col-md-6">
              <div class="tos">
                <label><input name="skills_prof[]" value="AR" type="checkbox"></input> 
                <h4>AR</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Insurance Processing" type="checkbox"></input> 
                <h4>Insurance Processing</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Treatment Presentation" type="checkbox"></input> 
                <h4>Treatment Presentation</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Anethesia Certified" type="checkbox"></input> 
                <h4>Anethesia Certified</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="Laser Certified" type="checkbox"></input> 
                <h4>Laser Certified</h4></label>
              </div>
              <div class="tos">
                <label><input name="skills_prof[]" value="EFDA" type="checkbox"></input> 
                <h4>EFDA</h4></label>
              </div>
            </div>
          </div>
          <div id="feedback-err_cb_skills_prof" class="negative-feedback displayNone">Please check at least one Box</div>
          
          <div style="display: none;">
          <h3>{$translations.apply.occupation_placeholder}</h3>
          <input placeholder="{$translations.apply.occupation_desc}" type="text" id="occupation" name="occupation" maxlength="500">
          </div>

          <h3>Type of Jobs <span style="color: red;padding: 10px;">*</span><p style="margin-top: 10px;" id="type_of_jobs" style="color:red;"></p></h3>
                  
          <div class="col-md-6" style="margin-left: -5px;">
            <div class="col-md-6">
              <input onclick="TempDays();" class="form-check-input radio-tos" type="radio" name="typepos" id="typepos" value="Temporary" {if ($applicant.typepos == 'Temporary')}checked{/if}>
              <label class="form-check-label label-tos tempText" for="typepos">
                Temporary
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <input class="form-check-input radio-tos" type="radio" name="typepos" id="typepos" value="Permanent" onclick="PermaDays();" {if ($applicant.typepos == 'Permanent')}checked{/if}>
            <label class="form-check-label label-tos permText" for="typepos">
              Permanent
            </label>
          </div>
          <div id="feedback-err_cb_typepos" class="negative-feedback displayNone">Please Select Type of Job</div>
          <br/>
          <div class="tos">
                  <h3>
                    Temporary days you are consistently available each week
                    <p style="margin-top: 10px;" id="tempdaymsg" style="color:red;"></p>
                  </h3>                    
          </div>                        
          <div class="mon tos" style="width: 75px;">
            <label>
                      <input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday0" value="Monday" type="checkbox" disabled></input> 
                      <h4 id="temp_day_name0" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Mon</h4>
            </label>  
          </div>
          <div class="tue tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday1" value="Tuesday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name1" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Tue</h4>
            </label>
          </div>
          <div class="wed tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday2" value="Wednesday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name2" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Wed</h4>
            </label>
          </div>
          <div class="thu tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday3" value="Thursday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name3" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Thu</h4>
            </label>
          </div>
          <div class="fri tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday4" value="Friday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name4" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Fri</h4>
            </label>
          </div>
          <div class="sat tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday5" value="Saturday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name5" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Sat</h4>
            </label>
          </div>
          <div class="sun tos" style="width: 70px;">
            <label>
              <input style="font-weight: 400; border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday6" value="Sunday" type="checkbox" disabled></input> 
              <h4 id="temp_day_name6" style="margin-left: 6px;margin-top: 6px;color:#CCCCCC">Sun</h4>
            </label>
          </div>
          <div id="feedback-err_cb_tempdays" class="negative-feedback displayNone">If Temporary Job Type above is selected, you must check the temporary days you are available</div>

          <style type="text/css">
            select.dropdown{
              height: 50px;
              padding: 10px;
              border: 0;
              font-size: 15px;
              width: 200px;
              -webkit-appearance: none;
              -moz-appearance: none;
              appearance: none;
            }

            label.wrapanother:after {
              content: '\f078';
              font: normal normal normal 17px/1 FontAwesome;
              color: #871f78;
                position: absolute;
                right: 0;
                left: 185px;
                /*top: 18px;*/
                bottom: -82px;
                z-index: 1;
                width: 10%;
                height: 100%;
                pointer-events: none;
            }

            select{
              -webkit-appearance: none;
              -moz-appearance: none;
              appearance: none;
              text-align: left;
              text-align-last: left;
            }

            option {
              text-align: left;
              /* reset to left*/
            }
          </style>

            <h3>Permanent days you are consistently available each week
            <p style="margin-top: 10px;" id="permadaymsg" style="color:red;"></p>
            </h3>

            <div class="mon tos" style="width: 75px;">
              <label>
                        <input name="permaday[]" id="permaday0" value="Monday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                        <h4 id="permaday_name0" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Mon</h4>
              </label>  
            </div>
            <div class="tue tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday1" value="Tuesday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name1" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Tue</h4>
              </label>
            </div>
            <div class="wed tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday2" value="Wednesday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name2" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Wed</h4>
              </label>
            </div>
            <div class="thu tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday3" value="Thursday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name3" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Thu</h4>
              </label>
            </div>
            <div class="fri tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday4" value="Friday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name4" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Fri</h4>
              </label>
            </div>
            <div class="sat tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday5" value="Saturday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name5" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Sat</h4>
              </label>
            </div>
            <div class="sun tos" style="width: 70px;">
              <label>
                <input name="permaday[]" id="permaday6" value="Sunday" type="checkbox" style="font-weight: 400; border-color: gray; background-color: lightgray;" disabled></input> 
                <h4 id="permaday_name6" style="margin-left: 6px;margin-top: 6px; color:#CCCCCC">Sun</h4>
              </label>
            </div>
            <div id="feedback-err_cb_permdays" class="negative-feedback displayNone">If Permanent Job Type above is selected, you must check the permanent days you are available.</div>
            <br/ >
            <div class="row">
            </div>
            <div class="row">
              <div class="hourlyRate col-md-5" style="width: 255px;">
                <h3 style="margin-left: -10px;">Minimum Hourly Rate (Permanent jobs only) <red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='resulthr' style="color: red; font-size: 70%;"></p></h3>
                <input required type="number" name="wage_sel" id="wage_sel" placeholder="$0.00" step="1" min="1" style="font-weight: 400; width: 90%;">
              </div>
            </div>
            <div class="row">
              <div id="feedback-err_wage_sel" class="negative-feedback displayNone">Field is empty</div>
            </div>
            <div class="row">
              <div class="col-md-5" style="width: 255px; height: 150px;">
                <h3 style="margin-left: -10px;">How far are you willing to drive?<red style="color: red;padding: 10px;">*</red><red id="miles"></red></h3><!-- <p id="miles" style="top:100px;"></p> -->
                <label class="wrapanother" id="wrapanother">
                  <select required id="miles_sel" name="miles_sel" class="dropdown" style="
                    -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    padding: 10px;
                    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    min-height: 47px;
                    border-radius: 5px;
                    background-color: white;
                    padding: 0% 3% 0% 6%;
                    border: none;
                    float: left;
                    /*margin-bottom: 7%;
                    margin-top: 7%;*/
                    bottom: -8px;
                    font-size: 14px;
                    padding: 10px;">
                    <option value="" disabled selected>Select your option</option>
                    <option value="5 miles">5 miles</option>
                    <option value="10 miles">10 miles</option>
                    <option value="20 miles">20 miles</option>
                    <option value="35 miles">35 miles</option>
                    <option value="36+ miles">36+ miles</option>                          
                  </select>
                </label>
              </div>
            </div>
            <div class="row">
              <div id="feedback-err_miles_sel" class="negative-feedback displayNone">Please Select How far are you willing to drive </div>
            </div>
        </div>

      </div>

      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div style="display: none;">
          <h3>{$translations.js.skills_label}</h3>
          <div class="profile-taggl minput textarea clearfix skillsTaggle"></div>
          </div>

          <h3>About you</h3>
          <textarea id="apply_msg" name="apply_msg" maxlength="500" rows="8" cols="50" style="margin-top: 10px;font-weight: 400; margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;"></textarea>
          <div class="textarea-feedback tal" id="textarea_feedback"></div>

          <div class="clear-both"></div>
  
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="SMLinkDiv">
            <a id="addLink" class="green" onclick="return SimpleJobScript.addExternalLink();" href="#">{$translations.js.add_social_media}</a>
            <div id="addLinkBlock"></div>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-6 col-xs-12">            
            {if $ENABLE_RECAPTCHA}
              {$captcha_html}
              <div id="captcha_err" class="negative-feedback displayNone ml0" >{$translations.apply.captcha_empty_err}</div>
            {/if}

            <div class="tos">
              <label><input required name="termscondition[]" id="termscondition" type="checkbox" class="checkbox-custom"></input> 
              <h4>{$translations.registration.accept_part1} <a target="_blank" href="{$BASEURL}{TERMS_CONDITIONS_URL}"> {$translations.registration.accept_part2}</a></h4></label>
            </div>
            <div id="feedback-err_cb_termscondition" class="negative-feedback displayNone">You must check the box stating that you agree to the terms and conditions</div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-xs-12">
          <button type="submit" class="btn" id="save" 
            onclick="
            validate_fields(); 
            /*validate_checkboxes();*/ 
            check_input_fields(event);
            checkEmail(); 
            return SimpleJobScript.createProfileValidation({$MAX_CV_SIZE});
            ">{$translations.website_general.top_menu_register_label}</button>
        </div>
      </div>

      </div>
    </form>
</div>
<script src="/_tpl/dds/1.5/js/cleave/cleave.min.js"></script>
<script src="/_tpl/dds/1.5/js/cleave/cleave-phone.us.js"></script>
{literal}
<script type="text/javascript">
  $(document).ready(function() {    
    SimpleJobScript.I18n = {/literal}{$translationsJson}{literal};
    SimpleJobScript.initApplyValidation();
    
    $('#cv').change(function() {
      var fname = $('input[type=file]').val().split('\\').pop();
      if( fname )
        $('#cvLabel').html(fname);
      else
        $('#cvLabel').html($('#cvLabel').html());
        });
  });


var cleave = new Cleave('.input-birthdate', {
    date: true,
    delimiter: '/',
    datePattern: ['m', 'd', 'Y']
});

  function showImage(){
    if(this.files && this.files[0]){
    var obj = new FileReader();
      obj.onload = function(data){
        var image = document.getElementById("image");
        image.src = data.target.result;
      }
      obj.readAsDataURL(this.files[0]);
    }else{
      $("#image").attr("src","uploads/images/Generic-Profile.jpg");
    }
  }
  $("#image").attr("src","uploads/images/Generic-Profile.jpg");
</script>   
{/literal}

{literal}
<script type="text/javascript">
function checkEmail() {
  
  $.ajax({ url: '/email_verification_ajax_applicants.php',
    data: "email=" + $('#apply_email').val(),
    type: 'post'
  }).done(function(msg) {
    var json = JSON.parse(msg);
    if (json.result == "1") {
      /*$("#save").attr("disabled", "disabled");*/
      //$('#apply_email').focus();
      $('#email_apply_email_exist').removeClass('displayNone');
      var x = document.getElementById("errors");
          x.style.display = "block"; 
          window.scrollTo({
                 top: 80,
                 behavior: 'smooth'
                 });
          return false;
    }else{
      $('#email_apply_email_exist').addClass('displayNone');
      /*$("#save").removeAttr("disabled");*/      
    }
  });
}
</script>
{/literal}

<!-- ADDED NEW 3/25/19 , Validate only in Submission -->

{literal}
<script type="text/javascript">
function validate_fields(){
  //DOB
  var dob = document.getElementById('birthdate').value;
  //Zip Code
  var zip = document.getElementById('f9_zip').value;
  if(zip != ""){
    var $result = $("#resultzip");
    $result.text("");
    var zipReg = /^\d{5}$/;                       
    if (!zipReg.test(zip)) {
      // $result.text("5-digits Zip Code. (99999)");
      // $result.css({"color": "red", "font-size": "80%"});
      //MSG
      var x = document.getElementById("errors");
      // x.style.display = "block"; //show
      var zip  =  document.getElementById('f9_zip');
      // zip.setCustomValidity("5-digits Zip Code. (99999)");
      //$('#f9_zip').focus();
    } else {
      $result.text("");
      $result.css({"color": "green", "font-size": "80%"});
      var zip  =  document.getElementById('f9_zip');
      zip.setCustomValidity("");
    }
  }else{
    var $result = $("#resultzip");
    //$result.text("Fill in this field.");
    // $result.css({"color": "red", "font-size": "80%"});
    //MSG
    var x = document.getElementById("errors");
    // x.style.display = "block"; //show
    var zip  =  document.getElementById('f9_zip');
    // zip.setCustomValidity("Fill in this field.");
    //$result.focus();
  }

  //CELLPHONE

  var phone = document.getElementById('apply_phone').value;

  if(phone != ""){
    var $result = $("#resultcp");
    $result.text("");
    if (phone.length != 14) {
      //$result.text("Enter a 10-digit number.");
      //$result.css({"color": "red", "font-size": "80%"});
      //MSG
      var x = document.getElementById("errors");
      //x.style.display = "block"; //show
      var p  =  document.getElementById('apply_phone');
      //p.setCustomValidity("This is not a valid Cellphone Number.");
    } else {
      $result.text("");
      $result.css({"color": "green", "font-size": "80%"});
      var p  =  document.getElementById('apply_phone');
      p.setCustomValidity("");
    }
  }else{
    var $result = $("#resultcp");
    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});
    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show
    var p  =  document.getElementById('apply_phone');
    //p.setCustomValidity("Fill in this field.");
    $("#error-message-text").append('<li style="font-weight:400">Enter a 10-digit number.</li>');
    //$('#apply_phone').focus();
  }
  //Email
  var email = document.getElementById('apply_email').value;
  if(email == ""){
    var $result = $("#resultemail");

    // $result.text("Fill in this field.");
    // $result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    // x.style.display = "block"; //show

    var p  =  document.getElementById('apply_email');
    // p.setCustomValidity(" ");
    //$('#apply_email').focus();
  }else{
    var $result = $("#resultemail");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

    var p  =  document.getElementById('apply_email');
    p.setCustomValidity("");
  }

  //Repeat Email

  var remail = document.getElementById('re_email').value;

  if(remail == ""){

    var $result = $("#resultremail");

    // $result.text("Fill in this field.");
    // $result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    var p  =  document.getElementById('re_email');
    // p.setCustomValidity("Fill in this field.");
    //$('#re_email').focus();
  }else{

    var $result = $("#resultremail");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

    var p  =  document.getElementById('re_email');
    p.setCustomValidity("");

  }

  //First Name

  var fname = document.getElementById('f9_first_name').value;

  if(fname == ""){

    var $result = $("#resultfname");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    var p  =  document.getElementById('f9_first_name');
    //p.setCustomValidity("Fill in this field.");
    //$('#f9_first_name').focus();
  }else{

    var $result = $("#resultfname");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

    var p  =  document.getElementById('f9_first_name');
    p.setCustomValidity("");

  }

  //Last Name

  var lname = document.getElementById('f9_last_name').value;

  if(lname == ""){

    var $result = $("#resultlname");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    var p  =  document.getElementById('f9_last_name');
    //p.setCustomValidity("Fill in this field.");
    //$('#f9_last_name').focus();
  }else{

    var $result = $("#resultlname");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

    var p  =  document.getElementById('f9_last_name');
    p.setCustomValidity("");

  }

  //Gender

  var gender = $('input[name="f9_gender"]:checked').length;

  if(gender == 0){

    var $result = $("#resultgender");

    //$result.text("Must select at least one Gender.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    x.style.display = "block"; //show
    $("#error-message-text").append('<li style="font-weight:400">Select a Gender</li>');
    //$('#f9_gender').focus();
  }else{

    var $result = $("#resultgender");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //Street Address

  var stadd = document.getElementById('f9_address_1').value;

  if(stadd == ""){

    var $result = $("#resultstreetaddress");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    //$('#f9_address_1').focus();
  }else{

    var $result = $("#resultstreetaddress");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //City

  var city = document.getElementById('f9_city').value;

  if(city == ""){

    var $result = $("#resultcity");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    //$('#f9_city').focus();

  }else{

    var $result = $("#resultcity");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //Bilingual

  var bilingual = $('input[name="span_bilingual"]:checked').length;

  if(bilingual == 0){

    var $result = $("#resultbilingual");

    //$result.text("Must select at least one answer.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    //$('#span_bilingual').focus();
  }else{
    var $result = $("#resultbilingual");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //How far are you willing to drive

  var miles = document.getElementById('miles_sel').value;

  if(miles == ""){

    var $result = $("#miles");

    //$result.text("Must select one.");
    //$result.css({"color": "red", "font-size": "80%", "font-weight": "bold"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show
    //$("#miles").focus();
  }else{

    var $result = $("#miles");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //State

  var state = document.getElementById('f9_state').value;

  if(state == ""){

    var $result = $("#state");

    //$result.text("Must select one.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show
    //$result.focus();
  }else{
    var $result = $("#state");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //Years of Experience

  var years = document.getElementById('f9_yrs_experience').value;

  if(years == ""){

    var $result = $("#years");

    //$result.text("Must select one.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

  }else{

    var $result = $("#years");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //Password

  var pass1 = document.getElementById('pass1').value;

  if(pass1 == ""){

    var $result = $("#password");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    var p  =  document.getElementById('pass1');
    //p.setCustomValidity("Fill in this field.");
    //$('#pass1').focus();
  }else{

    var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;

    if (!passReg.test(pass1)) {

      var p  =  document.getElementById('pass1');
      p.setCustomValidity("Minimum 8-16 characters, 1 uppercase letter, 1 lowercase letter, and 1 number.");

    } else {

      var $result = $("#password");

      $result.text("");
      $result.css({"color": "green", "font-size": "80%"});

      var p  =  document.getElementById('pass1');
      p.setCustomValidity("");

    }


  }

  //Repeat Password

  var pass2 = document.getElementById('pass2').value;

  if(pass2 == ""){

    var $result = $("#rpassword");

    //$result.text("Fill in this field.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG
    var x = document.getElementById("errors");
    //x.style.display = "block"; //show

    var p  =  document.getElementById('pass2');
    //p.setCustomValidity("Fill in this field.");
    //$('#pass2').focus();
  }else{

    var $result = $("#rpassword");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

    var p  =  document.getElementById('pass2');
    p.setCustomValidity("");


  }

  //Cellphone Carrier/ Cell Phone Provider

  var carrier = document.getElementById('phone_carrier').value;

  if(carrier == ""){

    var $result = $("#carrier");

    //$result.text("Must select one.");
    //$result.css({"color": "red", "font-size": "80%"});

    //MSG   
    $("#error-message-text").append('<li style="font-weight:400">Select a Cell Phone Provider</li>');
    var x = document.getElementById("errors");
    x.style.display = "block"; //show

  }else{
    var $result = $("#carrier");

    $result.text("");
    $result.css({"color": "green", "font-size": "80%"});

  }

  //Minimum Hourly Rate

  var wage_sel = document.getElementById('wage_sel').value;

  console.log(wage_sel);

  if(wage_sel != ""){

    var $result = $("#resulthr");
    $result.text("");
    var hrReg = /^[1-9][0-9]*$/;
    
    if (!hrReg.test(wage_sel)) {
      //$result.text("Must be a number.");
      //$result.css({"color": "red", "font-size": "70%"});

      var hr  =  document.getElementById('wage_sel');
      //hr.setCustomValidity("This is not a valid value.");

    } else {
      $result.text("");
      $result.css({"color": "green", "font-size": "70%"});

      var hr  =  document.getElementById('wage_sel');
      hr.setCustomValidity("");
    }

  }else{

    var $result = $("#resulthr");

      //$result.text("Fill in this field.");
        //$result.css({"color": "red", "font-size": "80%"});

        //MSG
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var hr  =  document.getElementById('wage_sel');
      //hr.setCustomValidity("Fill in this field.");

  } 

  //What are your major cross-streets

  var cross = document.getElementById('f9_cross-st').value;

  if(cross == ""){

    var $result = $("#major");

      //$result.text("Fill in this field.");
        //$result.css({"color": "red", "font-size": "80%"});

        //MSG
      var x = document.getElementById("errors");
      //x.style.display = "block"; //show
      //$('#f9_cross_st').focus();
  }else{

    var $result = $("#major");

      $result.text("");
        $result.css({"color": "green", "font-size": "80%"});

  }                 

}


  function validate_checkboxes(){
    if($('#f9_gender:checked').length == 0) {
      //$("#error-message-text").append('<li style="font-weight:400">Select your Gender</li>');

      var x = document.getElementById("errors");
      x.style.display = "block"; //show
    }

    if($('input[name="pro_soft[]"]:checked').length == 0) {
      
      var $proresult = $("#proresult");
      //$proresult.text("Must select at least one Software that you are Proficient in.");
      //$proresult.css({"color": "red", "font-size": "80%"});

      //MSG
      //$("#error-message-text").append('<li style="font-weight:400">select at least one Software that you are Proficient in.</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var pro_soft  =  document.getElementById('pro_soft');
      //pro_soft.setCustomValidity("Must select at least one Software that you are Proficient in.");

    }else{
      var $proresult = $("#proresult");
      $proresult.text("");
        $proresult.css({"color": "red", "font-size": "80%"});

      var pro_soft  =  document.getElementById('pro_soft');
      pro_soft.setCustomValidity("");

    }

    /*if($('input[name="f9_position[]"]:checked').length == 0) {*/
      if($('input[name="f9_position"]').length == 0) {
      
      var $posresult = $("#posresult");
      //$posresult.text("Must select at least one Position.");
        //$posresult.css({"color": "red", "font-size": "80%"});

      //MSG
      //$("#error-message-text").append('<li style="font-weight:400">Select at least one Position</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var position  =  document.getElementById('position');
      //position.setCustomValidity("Must select at least one Position.");
      
    }else{

      var $posresult = $("#posresult");
      $posresult.text("");
        $posresult.css({"color": "red", "font-size": "80%"});

      var position  =  document.getElementById('position');
      position.setCustomValidity("");

    }

    if($('input[name="notifyby[]"]:checked').length == 0) {
      
      var $resultnotify = $("#resultnotify");
      //$resultnotify.text("Must select at least one way to notify you.");
        //$resultnotify.css({"color": "red", "font-size": "80%"});

      //MSG
      //$("#error-message-text").append('<li style="font-weight:400">Email format is invalid or mismatch</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var notifyby  =  document.getElementById('notifyby');
      //notifyby.setCustomValidity("Must select at least one way to notify you.");

    }

    //ADDED

    if($('input[name="workarea[]"]:checked').length == 0) {

      var $arearesult = $("#arearesult");
      //$arearesult.text("Must select at least one Areas of Dentistry you have worked in.");
        //$arearesult.css({"color": "red", "font-size": "80%"});

        //MSG
      //$("#error-message-text").append('<li style="font-weight:400">Select at least one Areas of Dentistry you have worked in.</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var workarea  =  document.getElementById('workarea');
      //workarea.setCustomValidity("Must select at least one Areas of Dentistry you have worked in.");
    }else{

      var $arearesult = $("#arearesult");
      $arearesult.text("");
        $arearesult.css({"color": "red", "font-size": "80%"});

      var workarea  =  document.getElementById('workarea');
      workarea.setCustomValidity("");

    }

    if($('input[name="skills_prof[]"]:checked').length == 0) {

      var $skillsresult = $("#skillsresult");
      //$skillsresult.text("Must select at least one Skills you are Proficient in.");
        //$skillsresult.css({"color": "red", "font-size": "80%"});

        //MSG
      //$("#error-message-text").append('<li style="font-weight:400">Select at least one Skills you are Proficient in.</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var skills_prof  =  document.getElementById('skills_prof');
      //skills_prof.setCustomValidity("Must select at least one Skills you are Proficient in.");
    }else{
      var $skillsresult = $("#skillsresult");
      $skillsresult.text("");
        $skillsresult.css({"color": "red", "font-size": "80%"});

      var skills_prof  =  document.getElementById('skills_prof');
      skills_prof.setCustomValidity("");

    }

    if($('input[name="typepos[]"]:checked').length < 1) {

      var $typepos = $("#type_of_jobs");
      //$typepos.text("Must select at least one Type of Job.");
      //$typepos.css({"color": "red", "font-size": "80%"});

        //MSG
      //$("#error-message-text").append('<li style="font-weight:400">Select at least one Type of Job.</li>');
      var x = document.getElementById("errors");
      x.style.display = "block"; //show

      var temreq  =  document.getElementById('temreq');
      //temreq.setCustomValidity("Must select at least one Type of Job.");

    }else{

      var $typepos = $("#type_of_jobs");
      $typepos.text("");
        $typepos.css({"color": "red", "font-size": "80%"});

      var temreq  =  document.getElementById('temreq');
      temreq.setCustomValidity("");

    }


    if($('input[name="tempday[]"]:checked').length == 0 && $('#temreq:checked').length ==1) {
      var $tempday = $("#tempdaymsg");
      $tempday.text("If Temporary Job Type above is selected, you must check the temporary days you are available.");
      $tempday.css({"color": "red", "font-size": "80%", "opacity": "0.75", "font-size": "0.89em"});

      //MSG
      var x = document.getElementById("errors");
      x.style.display = "block"; //show
      window.scrollTo({
          top: 80,
          behavior: 'smooth'
        });

      var tempday  =  document.getElementById('tempday0');
      tempday.setCustomValidity("Select Temporary Job Type above if checking temp days you are available.");
      
    }else{
      var tempday = $("#tempdaymsg");
      tempday.text("");
        tempday.css({"color": "red", "font-size": "80%"});
      var tempday  =  document.getElementById('tempday0');
      tempday.setCustomValidity("");
    }


    if($('input[name="permaday[]"]:checked').length < 1 && $('#permdays:checked').length ==1) {
      var $permaday = $("#permadaymsg");
      $permaday.text("If Permanent Job Type above is selected, you must check the permanent days you are available.");
      $permaday.css({"color": "red", "font-size": "80%", "opacity": "0.75", "font-size": "0.89em"});

      var x = document.getElementById("errors");
      x.style.display = "block"; //show
      window.scrollTo({
          top: 80,
          behavior: 'smooth'
        });

      var permaday_msg  =  document.getElementById('permaday0');
      permaday_msg.setCustomValidity("Select Permanent Job Type above if checking temp days you are available");
    }else{
      var permaday = $("#permadaymsg");
      permaday.text("");
        permaday.css({"color": "red", "font-size": "80%"});
      //MSG
      var x = document.getElementById("errors");
      x.style.display = "block"; //show
    
      var permaday  =  document.getElementById('permaday0');
      permaday.setCustomValidity("");
    }
    //END
  }



  function showResume(){
    var resume = $('#cv').val();
    $('#cv-hint').text(resume);
  }
  function showCoverLetter(){
    var resume = $('#cover_letter').val();
    $('#cover_letter-hint').text(resume);
  }
  function showXray(){
    var resume = $('#x_ray').val();
    $('#x-ray-hint').text(resume);
  }
  function showCoronalPolish(){
    var resume = $('#coronal_polish').val();
    $('#coronal-polish-hint').text(resume);
  }
  function showLaser(){
    var resume = $('#laser').val();
    $('#laser-hint').text(resume);
  }
  function showAnesthesia(){
    var resume = $('#anesthesia').val();
    $('#anesthesia-hint').text(resume);
  }
  function showEfta(){
    var resume = $('#efta').val();
    $('#efta-hint').text(resume);
  }
  function showHygiene(){
    var resume = $('#hygiene_license').val();
    $('#hygiene-license-hint').text(resume);
  }
  function showDentist(){
    var resume = $('#dentist_license').val();
    $('#dentist-license-hint').text(resume);
  }
  function showW9(){
    var resume = $('#w9').val();
    $('#w9-hint').text(resume);
  }
  function showW4(){
    var resume = $('#w4').val();
    $('#w4-hint').text(resume);
  }
  function showA4(){
    var resume = $('#a4').val();
    $('#a4-hint').text(resume);
  }
  function showDirectDepost(){
    var resume = $('#direct_deposit').val();
    $('#direct-deposit-hint').text(resume);
  }
  function showI9(){
    var resume = $('#i9').val();
    $('#i9-hint').text(resume);
  }

  function TempDays(){
    if(document.forms['form_upload'].elements['typepos'].value == 'Temporary'){
      for (i = 0; i < 7; i++) { 
        $('#tempday' + i).attr('style',false);
        $('#tempday' + i).attr('disabled',false);
        $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#000000');
      }
      for (i = 0; i < 7; i++) { 
        $('#permaday'+i).attr('style','border-color: gray; background-color: lightgray;');
        $('#permaday' + i).attr('disabled',true);
        $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
        //ADDED TO UNCHECK CHECK ITEMS
        $('#permaday' + i).attr('checked',false);
      }         
    }else{
      for (i = 0; i < 7; i++) { 
          $('#tempday'+i).attr('style','border-color: gray; background-color: lightgray;');
        $('#tempday' + i).attr('disabled',true);
        $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
        //ADDED TO UNCHECK CHECK ITEMS
        $('#tempday' + i).attr('checked',false);
      }
    }
  }

  function PermaDays(){
    if(document.forms['form_upload'].elements['typepos'].value == 'Permanent'){
      for (i = 0; i < 7; i++) { 
        $('#permaday' + i).attr('style',false);
        $('#permaday' + i).attr('disabled',false);
        $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#000000');
      }
      for (i = 0; i < 7; i++) { 
          $('#tempday'+i).attr('style','border-color: gray; background-color: lightgray;');
        $('#tempday' + i).attr('disabled',true);
        $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
        //ADDED TO UNCHECK CHECK ITEMS
        $('#tempday' + i).attr('checked',false);
      }       
    }else{
      for (i = 0; i < 7; i++) { 
        $('#permaday'+i).attr('style','border-color: gray; background-color: lightgray;');
        $('#permaday' + i).attr('disabled',true);
        $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
        //ADDED TO UNCHECK CHECK ITEMS
        $('#permaday' + i).attr('checked',false);
      }
    }
  }

  function check_input_fields(event){
    var input_fields = 
      {
        'apply_email': 'Email',
        'pass1':'Password', 
        'f9_first_name':'First Name', 
        'f9_last_name': 'Last Name', 
        'birthdate' : 'Date of Birth',
        'cb_f9_gender' : 'Select Your Gender',
        'f9_address_1' : 'Street Address', 
        'f9_city' : 'City',
        'f9_state' : 'State', 
        'f9_zip' : 'Zip', 
        'phone_carrier' : 'Cell Phone Provider',
        'apply_phone' : 'Cell Phone Number', 
        'f9_yrs_experience' : 'Years of experience',
        'cb_span_bilingual' : 'Are you bilingual Spanish',        
        'f9_position' : 'Positions',                         
        'cb_notifyby[]' : 'Receive Notification',
        'cb_pro_soft[]' : 'Software that you are Proficient in',
        'cb_workarea[]' : 'Areas of Dentistry you have worked in',
        'cb_skills_prof[]' : 'Skills you are PROFICIENT in',
        'cb_typepos' : 'Type of Jobs',
        'cb_termscondition[]' : 'Terms and Condition',
        'wage_sel' : 'Minimum Hourly Rate (Fill in a valid dollar amount)',
        'miles_sel' : 'How far are you willing to drive'
      };

    var err_display = 0;
    $("#error-message-text").empty();
    $.each(input_fields, function( index, value ) {
      if(index.substring(0,2)=='cb'){   
        console.log(index);                                                                     
        if(!$('input[name="' + index.substring(3,index.length) + '"]').is(':checked')) {        
          //$("#error-message-text").append('<li style="font-weight:400">' + value + '</li>');
          if(index.substr((index.length-2),2)=='[]') {
            err_name = index.substr(0,index.length-2);
            console.log(err_name);
            $('#' + 'feedback-err_' + err_name).removeClass('displayNone');
          }else{
            $('#' + 'feedback-err_' + index).removeClass('displayNone');
          }         
          err_display = 1;
        }else{
          if(index.substr((index.length-2),2)=='[]') {
            err_name = index.substr(0,index.length-2);
            console.log(err_name);
            $('#' + 'feedback-err_' + err_name).addClass('displayNone');
          }else{
            $('#' + 'feedback-err_' + index).addClass('displayNone');
          }
        }
      }else{
        if(($('#' + index).val()==0 || $('#' + index).val()==null)){
          //$("#error-message-text").append('<li style="font-weight:400">' + value + '</li>');
          $('#' + 'feedback-err_' + index).removeClass('displayNone');
          err_display = 1;
        }else{
          $('#' + 'feedback-err_' + index).addClass('displayNone');
        }
      }
    });

    if (err_display == 1){
      var x = document.getElementById("errors");
      x.style.display = "block"; 
      //window.scrollTo(0,80);
      window.scrollTo({
          top: 80,
          behavior: 'smooth'
        });
      event.preventDefault();
    }else{
      var x = document.getElementById("errors");
      x.style.display = "none"; 
    }
  } 
</script>   
{/literal}

{literal}
<script type="text/javascript">
  $('.checkbox-p').on('change', function() {
  if($('.checkbox-p:checked').length > 2) {
    this.checked = false;
  }
  });
</script>
{/literal}

{literal}
<script type="text/javascript">
  $('.checkbox-g').on('change', function() {
  if($('.checkbox-g:checked').length > 1) {
    this.checked = false;
  }
});
</script>
{/literal}

{literal}
  <script type="text/javascript">
    $('.checkbox-gx').on('change', function() {
     if($('.checkbox-g:checked').length > 1) {
         this.checked = false;
     }
  });
    </script>
{/literal}

{literal}
  <script type="text/javascript">
    $(document).ready(function(){
      $('#pass1').on('change keydown paste input', function () {
        var $pass1 = this.value;
        validatePass($pass1);
      });
      $("#apply_email").focusout(function(){
        var apply_email = $('#apply_email').val();
        var re_email = $('#re_email').val();

        function validateEmail(email) {
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }
        if (!validateEmail(apply_email)) {
          if($('#apply_email').val() !== ""){
            $('#feedback-err_apply_email').removeClass('displayNone');
          }
          //$("#error-message-text").append('<li style="font-weight:400">Email format is invalid or mismatch</li>');
        }else{
          $('#feedback-err_apply_email').addClass('displayNone');
          $('#feedback-err-email').addClass('displayNone');
          $('#feedback-err-email_re_email').addClass('displayNone');
          /*$("#save").removeAttr("disabled"); */
        }
      });
      if($('#apply_email').val() == ""){
        $('#feedback-err-email_apply_email').addClass('displayNone');
      }
    });

    function validatePass(pass) {
      var $result = $("#valpass");
      $result.text("");
      console.log('validatePass');
      var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;
      if (!passReg.test(pass)) {
          $result.text("This password is Invalid.");
          $result.css({
            "color": "red",
            "font-size": "90%",
            "font-weight": "bold"
          });
        }else{
          $result.text("");
          $result.css({
            "color": "green",
            "font-size": "80%"
          });
        }
      }
  </script>
{/literal}

{literal}
  <script type="text/javascript">
      $(document).ready(function(){
      $("#re_email").blur(function(){
        var apply_email = $('#apply_email').val();
        var re_email = $('#re_email').val();

        if(apply_email != re_email){
            if(re_email != ''){
              $('#feedback-err-email').removeClass('displayNone');
            }
        }else{
          $('#feedback-err-email').addClass('displayNone');
          $('#feedback-err-email_re_email').addClass('displayNone');
        } 
      });
    });
    </script>
{/literal}

{literal}
  <script type="text/javascript">
      $(document).ready(function(){       
      $("#pass1").blur(function(){

        var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;
        var passval = $('#pass1').val();

        if(!passReg.test(passval)){
          // $('#feedback-err-pass1').removeClass('displayNone'); error display should only be on top
          
          // console.log("Error");
          //$('#pass1').focus();
        }
        else {
          $('#feedback-err-pass1').addClass('displayNone');

          // console.log("Valid");
          }

        });

          var inputElements = document.getElementsByTagName('input');

            for(var i = 0; i < inputElements.length; i++)
            {
              inputElements[i].addEventListener( "invalid",
                  function( event ) {
                  event.preventDefault();
                 });
            }

          var selectElements = document.getElementsByTagName('select');

            for(var i = 0; i < selectElements.length; i++)
            {
              selectElements[i].addEventListener( "invalid",
                  function( event ) {
                  event.preventDefault();
                 });
            }
            
      });
    </script>
{/literal}

{literal}
  <script type="text/javascript">
      $(document).ready(function(){
      $("#pass2").blur(function(){
          if ($('#pass1').val() !== $('#pass2').val()) {
              $('#feedback-err-pass2').removeClass('displayNone');
          }else{
            $('#feedback-err-pass2').addClass('displayNone');
          }
      });

      $('#f9_zip').blur(function(){
          if ($('#f9_zip').val().length !== 5) {
            $('#feedback-err_f9_zip').removeClass('displayNone');
            $('#save').click(function (evt) {
                        evt.preventDefault();
                          $('#feedback-err_f9_zip').removeClass('displayNone');
                            var x = document.getElementById("errors");
                            x.style.display = "block"; 
                              window.scrollTo({
                                top: 80,
                                behavior: 'smooth'
                              });
                      });
          }else{
            $('#feedback-err_f9_zip').addClass('displayNone');
            $('#save').unbind('click');
          }
      });
    });

    $(document).ready(function(){
      $("#birthdate").blur(function(){
          var birthdateVal = $('#birthdate').val();
          var birthdatePattern = /^([0-9]{2})+\/([0-9]{2})+\/([0-9]{4})+/;
          var birthdateIsValid = birthdatePattern.test(birthdateVal);
          if (birthdateVal.length !== 10 || birthdateIsValid == false) {
            $('#feedback-err_birthdate').removeClass('displayNone');
            console.log(birthdateIsValid);
            $('#save').click(function (evt) {
                        evt.preventDefault();
                        $('#feedback-err_birthdate').removeClass('displayNone');
                        var x = document.getElementById("errors");
                        x.style.display = "block"; 
                          window.scrollTo({
                            top: 80,
                            behavior: 'smooth'
                          });
                    });
          }else{
            $('#feedback-err_birthdate').addClass('displayNone');
            console.log(birthdateIsValid);
            $('#save').unbind('click');
          }
      }); 
    });


    document.getElementById('wage_sel').addEventListener('blur', function (e) {
      var valueNum = e.target.value;
      //newCurrency = valueNum.toFixed(2);
      //e.target.value = newCurrency;
      var wageFormat = /^([0-9]+)\.([0-9]+)/;
      var wageFormatDecimal = wageFormat.test(valueNum);
      console.log(typeof(valueNum));

      if (valueNum == 0){
        var errText = $('#feedback-err_wage_sel').text();
        var newErrText = errText.replace('Field is empty', 'Fill in a valid dollar amount');
        document.getElementById('feedback-err_wage_sel').innerHTML = newErrText;
        $('#feedback-err_wage_sel').removeClass('displayNone');
      } else {
        $('#feedback-err_wage_sel').addClass('displayNone');
        if (wageFormatDecimal == true) {
         /* var newCurrency = valueNum.toFixed(2);*/
          var newCurrency = valueNum.slice(0, (valueNum.indexOf(".")) + 3);
          e.target.value = newCurrency;
        }
      }
  });

 /*   $("input[type='button']").click(function(){
            var radioValue = $("input[name='typepos']:checked").val();
            if (radioValue != 0){
              console.log('radio buttong cliked');
            }
          });*/


    /*$("input[name='typepos']").click(function(){
        if(!$('input[name="tempday"]').is(':checked')) {
           $("input[name='tempday']").click(function(){
              console.log('tempday clicked');
           });
         } else {
          console.log('yes tempday')
         }
      });*/

    $('#save').click(function (evt) {

          if(document.forms['form_upload'].elements['typepos'].value == 'Temporary'){
              $('#feedback-err_cb_permdays').addClass('displayNone');
              if(!$('input[name="tempday[]"]').is(':checked')){
              evt.preventDefault();
              $('#feedback-err_cb_tempdays').removeClass('displayNone');
              var x = document.getElementById("errors");
               x.style.display = "block"; 
               window.scrollTo({
                 top: 80,
                 behavior: 'smooth'
                 });
            } else {
                $('#feedback-err_cb_tempdays').addClass('displayNone');
            }
          } else if(document.forms['form_upload'].elements['typepos'].value == 'Permanent') {
              $('#feedback-err_cb_tempdays').addClass('displayNone');
              if(!$('input[name="permaday[]"]').is(':checked')){
              evt.preventDefault();
              $('#feedback-err_cb_permdays').removeClass('displayNone');
              var x = document.getElementById("errors");
               x.style.display = "block"; 
               window.scrollTo({
                 top: 80,
                 behavior: 'smooth'
                 });
            } else {
              $('#feedback-err_cb_permdays').addClass('displayNone');
            }
          }

       });


/*    if($('input[name="typepos"]').is(':checked')) {
      console.log('radio buttong cliked')
    }*/
       document.getElementById('f9_zip').addEventListener('input', function (e) {
        var x = e.target.value.replace(/[^0-9]/g, '')
            e.target.value = x;
                });


  </script>
{/literal}

  
{include file="1.5/layout/sjs-footer.tpl"}