{include file="1.5/layout/sjs-header.tpl"}

<style type="text/css">

@media (min-width: 992px) and (max-width: 1200px) {
	.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
		min-height: 0!important;
	}
}


</style>

<div class="main-content signup-page">
	<div class="adjusted-signup-page" style="min-height: 550px; ">
		<div class="container">
			<h2>{$translations.sign_up.headline}</h2>
			<div class="row">
				<div class="col-md-2 .hide-sm"></div>

				{if $PROFILES_PLUGIN == 'true' and $jobs_candidates_on_flag == '1'}
				<div class="col-lg-4 col-md-6 col-xs-12 candidate">
					<img src="{$BASE_URL}_tpl/default/1.5/images/candidate.png">
					<a href="{$BASE_URL}{$URL_BUFFER_APPLICANTS}" class="su-emp"><!--{$translations.sign_up.candidates_registration}-->JOB SEEKER REGISTRATION</a>
					<a href="{$BASE_URL}{$URL_LOGIN_APPLICANTS}" class="si-emp">{$translations.registration.sign_in}</a>
				</div>
				{/if}


				{if $PROFILES_PLUGIN == 'true' and $jobs_candidates_on_flag == '1'}
				<div class="col-lg-4 col-md-6 col-xs-12  employer">
		
				
					<img src="{$BASE_URL}_tpl/default/1.5/images/employer.png">
					<!-- ADDED UPDATE ORIG -->

<!-- 						<a href="{$BASE_URL}{$URL_BUFFER_DOCTORS}" class="su-emp">CLIENT REGISTRATION
						</a> -->

						<!-- END -->

						<a href="{$BASE_URL}{$URL_REGISTER_RECRUITERS}" class="su-emp">CLIENT REGISTRATION
						</a>

						<a href="{$BASE_URL}{$URL_LOGIN_RECRUITERS}" class="si-emp">
							{$translations.registration.sign_in}
						</a>
				</div>
				{/if}

				<div class="col-md-2 .hide-sm"></div>

			</div>
		</div>
	</div>
</div>

{include file="1.5/layout/sjs-footer.tpl"}