{include file="1.5/layout/login-header.tpl"}

{if $second_step}
<div class="main-content login-page register-employer-page-step-2">
	<div class="adjusted-login-page">
		<div class="container" style="margin-top: 75px;">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<!-- <img src="{$BASE_URL}{$SITE_LOGO_PATH}"> -->
					<h2>Client Registration</h2>
					
					{include file="auth/register-recruiters-step2-form-content.tpl"}
				</div>
			</div>
		</div>
	</div>
</div>
	{* Commented below since page 1 was merged with Page 2 *}
	{* {else}
		<div class="main-content login-page register-employer-page">
			<div class="adjusted-login-page">
				<div class="container" style="margin-top: 75px;">
					<div class="row">
						<div class="col-md-12 col-xs-12">		
							<h2>Client Registration (Page 1 of 2)<br><small style="font-size: 20px">Each location must register separately</small></h2>
							 {include file="auth/register-recruiters-step1-form-content.tpl"} 
						</div>
					</div>
				</div>
			</div>
		</div>  *}
{/if}


{include file="1.5/layout/login-footer.tpl"}
