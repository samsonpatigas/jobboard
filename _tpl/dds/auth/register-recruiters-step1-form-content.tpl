
<form id="register-form" name="register-form" method="post" action="{$BASE_URL}{$URL_REGISTER_RECRUITERS}" role="form">
	<input type="hidden" name="step" id="step" value="1">

	<div id="reg-name-fg">
		<h4>Practice name<font color="red">*</font></h4>
		<input type="text" name="register_name" id="register_name" maxlength="500" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" required >
		<div id="err-reg-name" class="negative-feedback-form displayNone">{$translations.login.err_name}</div>
	</div>
	<div class="clear-both"></div>

	<div id="reg-email-fg">
		<h4>Email Address <font color="red">*</font></h4>
		<input required type="email" name="register_email" id="register_email" maxlength="500" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
		<div id="err-reg-email" class="negative-feedback-form displayNone">{$translations.login.err_email}</div>
	</div>
	<div class="clear-both"></div>


	<div id="reg-pass1-fg">
		<h4>Password <font color="red">*</font></h4>
		<p id='resultpass' style="color: red;font-size: 80%;">Minimum 8-16 characters, 1 uppercase letter, 1 lowercase letter, and 1 number.</p>
		<br>
		<input required type="password" onfocus="SimpleJobScript.checkIfEmailExists();" name="register_pass1" id="register_pass1" maxlength="500" style="margin-bottom:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">

		<!-- ERROR FEEDBACK NOTE IF CONDITIONS ISN'T SATISFIED -->
		<p id='valpass' style="color: red;font-size: 80%;margin-top: 10px;float: left;margin-left: 10px;"></p>
		<!-- END -->

					<!-- ADDED FOR PASSWORD VALIDATION -->

		{literal}

			<script type="text/javascript">
								   	
				$('#register_pass1').on('change keydown paste input', function(){

					var $pass1 = this.value;
					validatePass($pass1);

				});

				function validatePass(pass) {

					var $result = $("#valpass");
					$result.text("");

					// Validate with special characters

					// var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/;

					// Validate without special characters

					var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;

					if (!passReg.test(pass)) {

						$result.text("This password is Invalid.");
   						$result.css({"color": "red", "font-size": "90%", "font-weight": "bold"});

   						var p  =  document.getElementById('register_pass1');
						p.setCustomValidity("Minimum 8-16 characters, 1 uppercase letter, 1 lowercase letter, and 1 number.");

					} else {

						$result.text("");
    					$result.css({"color": "green", "font-size": "80%"});

    					var p  =  document.getElementById('register_pass1');
						p.setCustomValidity("");

					}
				}

			</script>
								   
		{/literal}

	<!-- 	ENPOINT -->

	</div>
	<div class="clear-both"></div>

	<div id="reg-pass2-fg">
		<h4>Repeat password <font color="red">*</font></h4>
		<input required type="password" name="register_pass2" id="register_pass2" maxlength="500" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
		<div id="err-reg-pass2" class="negative-feedback-form displayNone">{$translations.login.err_passes}</div>
	</div>
	<div class="clear-both"></div>

	{if $ENABLE_RECAPTCHA}
	<div class="captcha-block">
		{$captcha_html}
		<div id="captcha_err" class="negative-feedback-form displayNone" >{$translations.apply.captcha_empty_err}</div>
	</div>
	{/if}

	<div class="row">
		<div class="col-md-12 col-xs-12">	
			<div class="tos">
				<label><input required type="checkbox" class="checkbox-custom"></input> 
				<h4>{$translations.registration.accept_part1} <a target="_blank" href="{$BASEURL}{TERMS_CONDITIONS_URL}"> {$translations.registration.accept_part2}</a></h4></label>
			</div>
		</div>
	</div>
	<br /><br />

	<!-- FIXATION OF CUSTOM-CHECKBOX -->

	<style type="text/css">
		
	.checkbox-custom:checked::after {

		content: '\2714';
		font-size: 23px;
		font-weight: 600;
		position: absolute;
		top: -12px;
		left: 2px;
		color: #337ab7;

	}

	</style>

	<!-- END -->

	<div class="row">
		<div class="col-md-12 col-xs-12">	
			<button 
				name="proceedToStep2btnId" 
				id="proceedToStep2btnId" 
				type="submit" 
				onclick="return SimpleJobScript.registerFormValidation();"
				style="
					min-height: 45px;
					min-width: 230px;
					background-color: #7527a0;
					color: white;
					border-radius: 10px;
					text-transform: uppercase;
					border: solid;
					border-width: 1px;
					margin-top: 40px;
				">
				{$translations.registration.submit}
			</button>
		</div>
	</div>

</form> 

<div class="row">
	<div class="col-md-12 col-xs-12">	
		<br /><br /><br /><br />
	</div>
</div>