{include file="1.5/layout/sjs-header.tpl"}
<div class="main-content signup-page">
	<div class="adjusted-signup-page" style="min-height: 550px;">
		<div class="container">
			<h2>Are you registering as a Job Seeker?</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet elit ex, sit amet vulputate nisi accumsan at. Sed varius enim quis cursus efficitur. Aliquam tincidunt fermentum volutpat. Donec placerat orci diam, quis dapibus mi tempor a. Donec egestas metus eget gravida viverra. Curabitur iaculis magna quam, non eleifend arcu varius at. Suspendisse faucibus nisi non sagittis imperdiet. Aliquam viverra diam vitae quam mollis, at accumsan augue laoreet. Morbi tellus orci, semper eget aliquet in, tincidunt non ex.</p>
			<p>In sollicitudin lectus lacus, eget placerat ipsum malesuada vitae. Nulla eget magna ac est scelerisque scelerisque et vel risus. Duis pharetra ipsum ante, ac bibendum nunc imperdiet sed. Proin at metus et ipsum blandit pellentesque bibendum ac ante. Aliquam ut enim vitae augue dignissim pharetra at et enim. Fusce massa neque, blandit ut scelerisque vestibulum, euismod vitae sapien. Suspendisse aliquam velit sollicitudin urna accumsan aliquet. Maecenas rhoncus est at convallis vehicula. Nulla facilisi. Donec at pharetra diam, ac porta nisi. Suspendisse egestas ligula vitae ipsum sagittis, eget luctus massa auctor. Proin posuere scelerisque nibh, at fringilla tortor tincidunt quis. Aliquam non nibh sit amet lectus efficitur venenatis. Praesent tristique nibh eu enim scelerisque egestas.</p>
			<div class="row">
				<div class="col-md-2 .hide-sm"></div>
				<div class="col-lg-4 col-md-6 col-xs-12">
					<a href="{$BASE_URL}{$URL_REGISTER_APPLICANTS}" class="su-emp">YES</a>
				</div>
				<div class="col-lg-4 col-md-6 col-xs-12">
					<a href="{$BASE_URL}" class="su-emp">NO</a>
				</div>
				<div class="col-md-2 .hide-sm"></div>
			</div>
		</div>
	</div>
</div>
{include file="1.5/layout/sjs-footer.tpl"}