<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            {$translations.profile.apps_desc}
        </p>
    </div>
</div>

{$i=0}
{section name=application loop=$apps} 
{$i = $i + 1}

 <div class="panel-group">
    <div class="panel panel-default" >
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapse{$i}" onclick="this.classList.toggle('active1')">{$apps[application].job_title} @ {$apps[application].job_company}
          <a ></a>
        </h4>
      </div>
      <div id="collapse{$i}" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="" aria-hidden="true"></i>

            <table class="table" style="width: auto;">
              <tr>
                <th style="border:none;color: #7527a0;">Date Applied</th>
                <th style="border:none;color: #7527a0;">Date Posted</th>
              </tr>
              <tr>
                <td style="border:none;">{$apps[application].created_on}</td>
                <td style="border:none;">{$apps[application].date_posted}</td>
              </tr>
              <tr>
                <th style="border:none;color: #7527a0;">Title</th>
                <th style="border:none;color: #7527a0;">City</th>
              </tr>
              <tr>
                <td style="border:none;">{$apps[application].job_title}</td>
                <td style="border:none;">{$apps[application].city_name}</td>
              </tr>
            </table>

          </div>
        
          <div class="col-md-4 col-sm-4">
                    <div class="listing-type">
                        <p>Status: </p>
                        {if $apps[application].status == '0'}
                            <span class="profile-pending hideMobile">OPEN</span>
                        {else if $apps[application].status == '1'}
                            <span class="profile-rejected hideMobile">CLOSED</span>
                        {else}
                            <span class="profile-reviewed hideMobile">{$translations.profile.review_label}</span>
                        {/if}
                    </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
 {/section}
<br>

<div class="row">
<div class="col-md-12 col-sm-12 mb20">
  <h2>Available jobs</h2>   
  <p>Below are the jobs still available for application</p>
 </div>
</div>
<br>
<br>
{$j=0}
{section name=notapplied loop=$jobs} 
{$j = $j + 1}

 <div class="panel-group">
    <div class="panel panel-default" >
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapses{$j}" onclick="this.classList.toggle('active1')">{$jobs[notapplied].job_title} @ {$jobs[notapplied].job_company}
          <a ></a>
        </h4>
      </div>
      <div id="collapses{$j}" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="" aria-hidden="true"></i>

            <table class="table" style="width: auto;">
              <tr>
                <th style="border:none;color: #7527a0;">Date Posted</th>
                <th style="border:none;color: #7527a0;">Title</th>
              </tr>
              <tr>
                <td style="border:none;">{$jobs[notapplied].created_on}</td>
                <td style="border:none;">{$jobs[notapplied].job_title}</td>
              </tr>
            </table>

          </div>
          <div class="col-md-4 col-sm-4">
                    <div class="listing-type">
                        <p>Status: </p>
                        {if $jobs[notapplied].status == '0'}
                            <span class="profile-pending hideMobile">INACTIVE</span>
                        {else $jobs[notapplied].status == '1'}
                            <span class="profile-rejected hideMobile">ACTIVE</span>
                        {/if}
                    </div> 
                    <div>
                        <form action="{$BASE_URL}{$URL_JOB}/{$jobs[notapplied].url_title}/{$jobs[notapplied].job_id}">
                          <input type="submit" value="View Details" class="btn mbtn zeromlplLeft" style="margin-top: 10px;" />
                        </form>
                    </div> 
            </div>
        </div>
      </div>
    </div>
  </div>
 {/section}

