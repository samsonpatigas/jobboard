
<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            {$translations.profile.passchange_desc}
        </p>
    </div>
</div>

<form role="form" action="/{$URL_PROFILE}/pass-edited" method="post" >
    <div class="row">
        <div class="col-md-6 col-sm-12">

                    <p id='resultpass' style="color: black;font-size: 80%; width: auto;">Minimum 8-16 characters, 1 uppercase letter, 1 lowercase
            letter, and 1 number.</p>

                   <input placeholder="{$translations.login.new_pass}" name="pass1" id="pass1" maxlength="50" type="password" class="minput" required />
                   <p id="valpass" style="color: red; opacity: 0.75; font-size: 80%;margin-top: 10px;float: left;margin-left: 10px;"></p> 

                   <input placeholder="{$translations.login.new_pass2}" name="pass2" id="pass2" maxlength="50" type="password" class="minput" required /> 

                    <div id="passrecovery-feedback-err" class="negative-feedback mt0 displayNone">{$translations.login.err_passes}</div>

        </div>

        <div class="col-md-6 col-sm-12">
            
        </div>

    </div>

    <div class="row mb50">
        <div class="col-md-6 col-sm-12 pushTop40">
            <button type="submit_forgotten_pass"  onclick="return SimpleJobScript.applicantPasswordValidation();" class="btn mbtn zeromlplLeft mt50" name="submit" id="submit" required><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
        </div>
    </div>

    <style type="text/css">
        
   #dbtn
    {
    border-color: #f44336;
      background-color: #f44336;
      color: white;
    }

    #dbtn:hover
    {
      background-color: white;
      color: #f44336;
    }

    #valpass {
        margin-top: -10px!important;
        margin-bottom: 20px;
    }

    </style>

    <div class="row mb50">
        <div class="col-md-6 col-sm-12 pushTop40">
            <a class="greenLink" href="{BASE_URL}{$URL_PROFILE}/{$URL_PROFILE_DELETE}">

                <button type="button"  class="btn mbtn zeromlplLeft" id="dbtn">{$translations.profile.delete_acc_label}</button>


            </a>
        </div>
    </div>
</form>
{literal}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#pass1').on('change keydown paste input', function () {
                var $pass1 = this.value;
                console.log($pass1);
                validatePass($pass1);
            });

        function validatePass(pass) {
            var $result = $("#valpass");
            $result.text("");
            console.log('validatePass');
            var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;
            if (!passReg.test(pass)) {
                    $result.text("This password is Invalid");
                    $result.css({
                        "color": "red",
                        "font-size": "90%",
                        "font-weight": "bold"
                    });
                    $("#submit").attr("disabled", "disabled");
                }else{
                    $result.text("");
                    $result.css({
                        "color": "green",
                        "font-size": "80%"
                    });
                    $("#submit").removeAttr("disabled"); 
                }
            }
        });
    </script>
{/literal}
