  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>

  <style type="text/css">
    select.dropdown{
      height: 50px;
      padding: 10px;
      border: 0;
      font-size: 15px;
      width: 200px;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
    }

    label.wrap:after {
      content: '\f078';
      font: normal normal normal 17px/1 FontAwesome;
      color: #871f78;
      position: absolute;
      right: 0;
      left: 270px;
      top: 18px;
      z-index: 1;
      width: 10%;
      height: 100%;
      pointer-events: none;
    }

    div.down:after {
      content: '\f078';
      font: normal normal normal 17px/1 FontAwesome;
      color: #871f78;
      position: absolute;
      right: 0;
      left: 185px;
      margin-top: 5px;
      top: 72px;
      z-index: 1;
      width: 10%;
      height: 100%;
      pointer-events: none;
    }

    select{
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      text-align: left;
      text-align-last: left;
    }

    option {
      text-align: left;
      /* reset to left*/
    }
    
    .error-message {
      background-color: #fce4e4;
      border: 1px solid #fcc2c3;
      float: left;
      width: 600px;
      padding: 20px 30px;
    }

    .error-text {
      color: #cc0033;
      font-family: Helvetica, Arial, sans-serif;
      font-size: 13px;
      font-weight: bold;
      line-height: 20px;
      text-shadow: 1px 1px rgba(250,250,250,.3);
    }

    .error-info {
      margin-left: 150px;
    }

    .cv-hint {
      font-size: small;
      margin-left: 0px;
    }
    .row_padding{
      padding-bottom: 3px;
      padding-top: 3px;
    }
    .back_color{
      padding-top: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
      background: #ede7f7;
    }

    #cvLabel:hover{
      background-color: #7527a0;
      color:white;
    }

    .rcorner{
      font-weight: 400;
      margin-botton:0px;
      padding: 10px;
      min-height: 37px;
      border-color: gainsboro;
      border-width: 1px;
      border-style: solid;
      border-radius: 5px;
      width: 300px;
    }

    .middle:hover {
      background-color: #ffffff;
      border: 1px solid #7527a0;
      cursor: pointer;
    }

    .middle:hover div {
      color: #7527a0;
    }

    @media screen and (min-width: 1024px){
      #errors{
        width: 1200px;
      }
      #feedback-err_cb_pro_soft{
        margin-top: -30px!important;
      }

      .f9posClass {
        margin-left: 0!important;
      }
    }

    @media (min-width: 992px){
      .docUpload {
        height: 88px;
      }

      .saveDoc {
        display: none;
      }

    }
    
    @media screen and (max-width: 798px){
      .rcorner, .selectLabel {
        width: 250px!important;
      }

      label.wrap:after {
        left: 220px;
      }

      div.down:after {
        left: 220px;
        top: 80px;
      }

      .permRow {
        position: relative;
        left: 130px;
        bottom: 30px;
      }

      .tempMargin{
        margin-top: -40px!important;
      }

      .genderPos, .bilingualPos {
        position: relative;
        bottom: 25px;
        left: 20px;
      }

      .genderRowPos, .bilingualRowPos {
        position: relative;
        bottom: 62px;
        left: 138px;
        margin-bottom: -100px;
      }

      .bilingualRowPos {
        position: relative;
        bottom: 62px;
        left: 138px;
        margin-bottom: -70px;
      }
    }
  </style>

  <div class="row">
    <div class="col-md-12 col-sm-12 mb20">
      <p class="profile-subheadline" style="display:none">
        {$translations.profile.edit_subheadline} {$translations.profile.edit_label}
      </p>
      <div 
      class="error-message" 
      id="errors"
      >
      <span 
      class="error-info"
      style="margin-left: 0px;"
      >
      <i class="fa fa-warning" style="color:red;"></i>
      Incorrect or missing information. Please scroll down to correct you entries.
      <ul id="error-message-text" style="text-align:left; list-style-type: circle; font-weight:400; margin-top:15px;"></ul>
    </span>
  </div>
  <script type="text/javascript">  
    var x = document.getElementById("errors");
          // x.style.display = "block"; //show
          x.style.display = "none"; //hide
          var errorText = document.getElementById('error-message-text');
          errorText.style.display = "none";
        </script>
      </div>
    </div>
    
    <form id="form_upload" name="form_upload" role="form" action="/{$URL_PROFILE}/profile-edited" method="post" enctype="multipart/form-data">
      <input type="hidden" id="external_links" name="external_links" value="{$applicant.sm_links|@count}" />
      <section class="py-4">
        <div class="row">
          <div class="col-md-12">
            <ul id="tabsJustified" class="nav nav-tabs">
              <li class="nav-item" style="border-right: solid 1px #ddd;border-left: solid 1px #ddd;"><a href="" data-target="#home1" data-toggle="tab" class="nav-link small text-uppercase active">
              Contact Information</a></li>
              <li class="nav-item" style="border-right: solid 1px #ddd;"><a href="" data-target="#profile1" data-toggle="tab" class="nav-link small text-uppercase">
                {if $applicant.cv_path eq '' && $applicant.cover_letter eq '' && $applicant.x_ray eq '' && $applicant.coronal_polish eq '' && $applicant.laser eq '' && $applicant.anesthesia eq '' && $applicant.efta eq '' && $applicant.hygiene_license eq '' && $applicant.dentist_license eq '' && $applicant.w9 eq '' && $applicant.w4 eq '' && $applicant.a4 eq '' && 
                $applicant.direct_deposit eq '' && $applicant.i9 eq ''} 
                <i class="fa fa-warning" style="color:red;"></i>{else}<i class="fa fa-check"></i>{/if} Documents</a></li>
              </ul>
              <br>
              <div id="tabsJustifiedContent" class="tab-content">
                <div id="home1" class="tab-pane  active ">
                  <div class="tos">

                    <!-- first tab -->
                    <div class="row tal">
                      <div class="col-md-5 col-sm-12">
                        <label class="fullW">{$translations.profile.f9_photo} (Highly Recommended):</label>                   
                        <div class="alert alert-danger alert-dismissible" id="myAlert" style="font-size: 14px;display:none; width: 250px;margin-bottom: 0px;border-radius:0px;border-top: solid 2px #fd5f60;color: #ff0003;padding: 8px;">
                          <!--<a href="#" class="close">&times;</a>-->
                          <span class="fa fa-exclamation-circle fa-fw"></span>
                          Click save to update your image...
                        </div>

                        <div class="container-hover">
                          <img src="{$BASE_URL}{$applicant.f9_photo}" id="image" name="image" class="img" style="width:240px; height:240px;-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);-moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);">
                          <label for="f9_photo">
                            <div class="middle">
                              <div class="text">Upload Photo</div>
                            </div>
                          </label>
                        </div>
                        <br>
                        <input accept=".gif,.JPG,.jpg,.jpeg,.png" onchange="showImage.call(this)" type="file" name="f9_photo" id="f9_photo" class="form-control inputfile minput"/>
                        <br />

                        <label class="fullW">Email: <red style="color: red;padding: 10px;">*</red><p id='result'></p> </label>
                        <input required class="minput opaque rcorner" value="{$applicant.email}" name="apply_email" id="apply_email" maxlength="50" type="text"/>
                        <br />

                        <input required class="minput opaque" value="{$applicant.email}" name="current_email" id="current_email" maxlength="50" type="text" hidden/>

                        {literal}
                        <script type="text/javascript">
                          $('#apply_email').on('focusout', function(){
                            var $email = this.value;
                            validateEmail($email);
                          });

                          function validateEmail(email) {
                            var $result = $("#result");
                            $result.text("");
                            var emailReg = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/;
                            if (!emailReg.test(email)) {
                              $result.text("This email is Invalid.");
                              $result.css({"color": "red", "font-size": "80%"});
                              var dobs  =  document.getElementById('apply_email');
                              dobs.setCustomValidity("");
                            } else {
                              $.getJSON('http://jobboard.f9portal.net/existing_email?email='+email, function(data) {                                
                                cemail = data;

                                if(cemail == 1){
                                  var current  =  document.getElementById('current_email').value;
                                  if(current == email){
                                    $result.text("");
                                    $result.css({"color": "green", "font-size": "80%"});
                                    var dobs  =  document.getElementById('apply_email');
                                    dobs.setCustomValidity("");
                                  }else{
                                    $result.text("Email Already Exist");
                                    $result.css({"color": "red", "font-size": "80%"});

                                    var dobs  =  document.getElementById('apply_email');
                                    dobs.setCustomValidity("");
                                  }
                                }else{

                                  $result.text("");
                                  $result.css({"color": "green", "font-size": "80%"});

                                  var dobs  =  document.getElementById('apply_email');
                                  dobs.setCustomValidity("");
                                }
                              });
                            }
                          }
                        </script>
                        {/literal}

                        <label class="fullW">{$translations.profile.f9_first_name}:</label>
                        <input class="minput opaque rcorner" value="{$applicant.f9_first_name}" name="f9_first_name" id="f9_first_name" maxlength="50" type="text"  /> 
                        <br />  

                        <label class="fullW">{$translations.profile.f9_middle_name}:</label>
                        <input class="minput opaque rcorner" value="{$applicant.f9_middle_name}" name="f9_middle_name" id="f9_middle_name" maxlength="50" type="text" placeholder="No value/data" /> 
                        <br />

                        <label class="fullW">{$translations.profile.f9_last_name}:</label>
                        <input class="minput opaque rcorner" value="{$applicant.f9_last_name}" name="f9_last_name" id="f9_last_name" maxlength="50" type="text"  /> 
                        <br />

                        <label class="fullW">Date of Birth : <red style="color: red;padding: 10px;">*</red><p id='resultdob'></p></label>
                        <input required class="minput opaque rcorner" type="text" value="{$applicant.birthdate}" name="birthdate" id="birthdate" maxlength="10" placeholder="MM/DD/YYYY">
                        <div id="feedback-err_birthdate" class="negative-feedback-form displayNone col-md-12">Invalid Date, enter as MM/DD/YYYY</div>
                        <br>

                        {literal}
                        <script type="text/javascript">

                          document.getElementById('birthdate').addEventListener('input', function (e) {
                            var x = e.target.value.replace(/[^0-9\/]/g, '')
                            e.target.value = x;
                            console.log(x);
                          });

                          document.getElementById('birthdate').addEventListener('blur', function (e) {
                            var pdate = e.target.value.split('/');
                            var mm  = parseInt(pdate[0]);
                            var dd = parseInt(pdate[1]);
                            var yy = parseInt(pdate[2]);

                            if(mm>12){
                              mm = 12;
                            }
                            if(dd>31){
                              dd = 31;
                            }
                            if(yy>9 && yy<100){
                              yy = yy + 1900
                            }
                            if(mm<10){
                              mm = '0' + mm.toString();
                            }
                            if(dd<10){
                              dd = '0' + dd.toString();
                            }

                            var fd = mm.toString().concat('/', dd.toString(), '/', yy.toString());
                            console.log(mm);
                            console.log(dd);
                            console.log(yy);
                            if(!(isNaN(mm) || isNaN(dd) || isNaN(yy))) {
                              if(yy>1000){
                                console.log('yy:' + yy);
                                $('#birthdate').val(fd);
                                $('#feedback-err_birthdate').addClass('displayNone');
                              }
                            }else{
                              console.log('error');
                      //$("#error-message-text").append('<li style="font-weight:400">Incomplete Birthdate Entry</li>');
                      //$('#birthdate').val(fd);
                      //$('#errors').show();
                      $('#feedback-err_birthdate').removeClass('displayNone');
                      $('#submit').click(function (evt) {
                        evt.preventDefault();
                        var x = document.getElementById("errors");
                        x.style.display = "block"; 
                        window.scrollTo({
                          top: 80,
                          behavior: 'smooth'
                        });

                      });
                      return false;
                    }
                  });
                </script>
                {/literal}

                <label class="fullW">{$translations.profile.f9_gender}:<red style="color: red;padding: 10px;">*</red>
                  <p id="error_message_f9_gender" style="color:red; display:none;">Gender is Empty</p>
                </label>
                {if $applicant.f9_gender == 'Male'}

                <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">             <div class="tos">
                  <input id="f9_gender" name="f9_gender" checked="checked" value="Male" type="radio"></input><h4 class="genderPos" style="line-height: 2;">Male</h4>
                </div>

              </div>

              <div class="col-md-6 genderRowPos" style="margin-top: 10px;margin-left: -15px;">
                <div class="tos">
                  <input id="f9_gender" name="f9_gender" value="Female" type="radio"></input><h4 class="genderPos" style="line-height: 2;">Female</h4>

                </div>
              </div>

              {else}

              <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;"><div class="tos">
                <input id="f9_gender" name="f9_gender" value="Male" type="radio"></input><h4 class="genderPos" style="line-height: 2;">Male</h4>
              </div>

            </div>
            <div class="col-md-6 genderRowPos" style="margin-top: 10px;margin-left: -15px;">
              <div class="tos">
                <input id="f9_gender" name="f9_gender" checked="checked" value="Female" type="radio"><h4 class="genderPos" style="line-height: 2;">Female</h4>
              </div>
            </div>
            {/if}
            <br />
            <label class="fullW" style="margin-top: 20px">Street Address :<red style="color: red;padding: 10px;">*</red>
              <p id="error_message_f9_address_1" style="color:red; display:none;">Street Address is Empty</p>
            </label>
            <input required class="minput opaque rcorner" value="{$applicant.f9_address_1}" name="f9_address_1" id="f9_address_1" maxlength="50" type="text"  /> 
            <br />
            <!--                   <label class="fullW">{$translations.profile.f9_address_2}:</label> -->
            <label class="fullW" style="margin-top: 20px">Unit # :</label>
            <input class="minput opaque rcorner" value="{$applicant.f9_address_2}" name="f9_address_2" id="f9_address_2" maxlength="50" type="text" placeholder="No value/data"  /> 
            <br />
            <!-- COMMENT FOR NOW -->
            <!-- ORIG -->
                  <!--
                    <label class="fullW">Cellphone carrier:</label>
                    <input class="minput opaque" value="{$applicant.phone_carrier}" name="phone_carrier" id="phone_carrier" maxlength="50" type="text"  />
                    <br />
                  -->
                  <label class="fullW">Cellphone Provider:</label>
                  <label class="wrap">
                  <select class="selectLabel" id="phone_carrier" name="phone_carrier" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  padding: 10px;
                  box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  min-height: 47px;
                  width: 300px;
                  border-radius: 5px;
                  padding: 0% 3% 0% 6%;
                  border: none;
                  float: left;
                  margin-bottom: 7%;
                  font-size: 14px;
                  padding: 10px;
                  background-color: white;">

                  {$array = ['Sprint','Verizon','T-Mobile','MetroPCS','AT&T','Boost Mobile','Mint Mobile','Simple Mobile','FreedomPop','Tello','UNREAL Mobile','Twigby','US Mobile','Consumer Cellular','Total Wireless','FreeUp Mobile','Red Pocket','TextNow','Straight Talk','XFINITY Mobile','Ting','H20 Wireless','TracFone','Net10','Ultra Mobile','Virgin Mobile','Page Plus']}

                  {foreach from=$array item=n}
                  {if $n == $applicant.phone_carrier}
                  {$selected = 'selected'}
                  {else}
                  {$selected = ''}
                  {/if}

                  <option value="{$n}" {$selected}>{$n}</option>

                  {/foreach}

                </select>
                </label>
                <br />

                <label class="fullW">{$translations.apply.f9_yrs_experience}<red style="color: red;padding: 10px;">*</red><p style="margin-top: 10px;" id='years'></p></label>
                <label class="wrap">
                  <select class="selectLabel" required id="f9_yrs_experience" name="f9_yrs_experience" class="dropdown" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  padding: 10px;
                  box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  min-height: 47px;
                  width: 300px;
                  border-radius: 5px;
                  padding: 0% 3% 0% 6%;
                  border: none;
                  float: left;
                  margin-bottom: 20px;
                  font-size: 14px;
                  padding: 10px;
                  background-color: white;">
                    <option value="" disabled selected>Select your option</option>
                    <option value="New Graduate" {if ($applicant.f9_yrs_experience == 'New Graduate')} selected {/if}>New Graduate</option>
                    <option value="6 months-2 years" {if ($applicant.f9_yrs_experience == '6 months-2 years')} selected {/if}>6 Months - 2 Years</option>
                    <option value="3-5 years" {if ($applicant.f9_yrs_experience == '3-5 years')} selected {/if}>3 - 5 Years</option>
                    <option value="6 years plus" {if ($applicant.f9_yrs_experience == '6 years plus')} selected {/if}>6 Years Plus</option>
                  </select>
                </label>
                <div id="feedback-err_f9_yrs_experience" class="negative-feedback displayNone">Years of Experience is Empty</div>

                <label class="fullW">{$translations.profile.f9_city}:<red style="color: red;padding: 10px;">*</red>
                  <p id="error_message_f9_city" style="color:red; display:none;">City is Empty</p>
                </label>
                <input required class="minput opaque rcorner" value="{$applicant.f9_city}" name="f9_city" id="f9_city" maxlength="50" type="text"  /> 
                <br />
                <label class="fullW">{$translations.profile.f9_state}:<red style="color: red;padding: 10px;">*</red>
                  <p id="error_message_f9_state" style="color:red; display:none">Select State</p>
                </label>
                <label class="wrap">
                  <select class="selectLabel" required id="f9_state" name="f9_state" class="dropdown rcorner" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  padding: 10px;
                  box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                  min-height: 47px;
                  width: 300px;
                  border-radius: 5px;
                  padding: 0% 3% 0% 6%;
                  border: none;
                  float: left;
                  margin-bottom: 20px;
                  font-size: 14px;
                  padding: 10px;
                  background-color: white;">
                  {$states= ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']}

                  {foreach from=$states item=x}
                  {if $x == $applicant.f9_state}
                  {$selected = 'selected'}
                  {else}
                  {$selected = ''}
                  {/if}

                  <option value="{$x}" {$selected}>{$x}</option>

                  {/foreach}

                </select>
              </label>

              <!-- END -->

              <br />

              <label class="fullW">{$translations.profile.f9_zip} Code: <red style="color: red;padding: 10px;">*</red>
                <p id="error_message_f9_zip" style="color:red; display: none;">Zip Code Empty</p>
              </label>
              <input required class="minput opaque rcorner" value="{$applicant.f9_zip}" name="f9_zip" id="f9_zip" maxlength="5" type="text" /> 
              <br />

              <!-- ADDED FOR ZIP VALIDATION -->

              {literal}

              <script type="text/javascript">

                $('#f9_zip').on('focusout', function(){
                  var $zip = this.value;
                  validateZip($zip);
                });

                function validateZip(zip) {
                  var $result = $("#error_message_f9_zip");
                  $result.text("");
                  var zipReg = /^\d{5}$/;
                  if (!zipReg.test(zip)) {
                    $result.text("");
                    $result.css({"color": "red", "font-size": "80%"});
                          //$("#error-message-text").append('<li>5-digits Zip Code. (99999)</li>');
                          var zip  =  document.getElementById('f9_zip');
                          zip.setCustomValidity("");
                          $("#error_message_f9_zip").text("This is not a valid Zip Code.");
                          $("#error_message_f9_zip").show();
                        } else {
                          $result.text("");
                          $result.css({"color": "green", "font-size": "80%"});
                          var zip  =  document.getElementById('f9_zip');
                          zip.setCustomValidity("");
                          $("#error_message_f9_zip").text("");
                          $("#error_message_f9_zip").hide();
                        }
                      }

                    </script>
                    {/literal}

                    <!-- ENDPOINT -->
                    <label>What are your major cross-streets<br><small>(Highly Recommended)</small><p style="margin-top: 10px;" id='major'></p></label>
                    <textarea name="f9_cross_st" id="f9_cross-st" rows="4" cols="50" style="margin-botton:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" maxlength="500" placeholder="What are your major cross-streets?"></textarea>
                    <br/>
                    <label class="fullW">Cell Phone: <red style="color: red;padding: 10px;">*</red><p id='resultcp'></p></label>
                    <input required class="minput opaque rcorner" value="{$applicant.phone}" name="phone" id="phone" type="text" placeholder="(999) 999-9999"/>

                    <br />

                    <!-- <input type="text" id="phone" placeholder="(555) 555-5555"/> -->

                    <!-- END -->

                    <!-- ADDED FOR CELLPHONE NUMBER VALIDATION -->

                    {literal}

                    <script type="text/javascript">

                      document.getElementById('phone').addEventListener('input', function (e) {
                        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
                        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
                      });

                      if (document.getElementById('phone').value != null){

                        console.log("im here!");
                        console.log(document.getElementById('phone').value);

                      var e = document.getElementById('phone');

                      var x = e.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
                      e.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');

                        console.log(e.value);

                      }else{

                      console.log("im here! also");
                    };

                  </script>

                  <script type="text/javascript">

                    $('#phone').on('focusout', function(){
                      var $phone = this.value;
                          // console.log($phone.length);
                          validatePhone($phone);
                        });

                    function validatePhone(phone) {
                      var $result = $("#resultcp");
                      $result.text("");

                          // var phoneReg = /^{14}$/;
                          
                          // if (!phoneReg.test(phone)) {
                          //         $result.text("Enter a 10-digit number.");
                          //    $result.css({"color": "red", "font-size": "80%"});
                          // } else {
                          //         $result.text("This number is Valid.");
                          //    $result.css({"color": "green", "font-size": "80%"});
                          // }

                          if (phone.length != 14) {
                            $result.text("Enter a 10-digit number.");
                            $result.css({"color": "red", "font-size": "80%"});

                            var p  =  document.getElementById('phone');
                            p.setCustomValidity("");

                          } else {
                            $result.text("");
                            $result.css({"color": "green", "font-size": "80%"});

                            var p  =  document.getElementById('phone');
                            p.setCustomValidity("");

                          }

                        }

                      </script>

                      {/literal}

                     <label class="fullW">{$translations.apply.f9_yrs_experience}:<red style="color: red;padding: 10px;">*</red></label>
                     <label class="wrap">
                      <select class="selectLabel" required id="f9_yrs_experience" name="f9_yrs_experience" class="dropdown" style="
                      -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      padding: 10px;
                      box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      min-height: 47px;
                      width: 300px;
                      border-radius: 5px;
                      padding: 0% 3% 0% 6%;
                      border: none;
                      float: left;
                      margin-bottom: 20px;
                      font-size: 14px;
                      padding: 10px;
                      background-color: white;">

                      {if $applicant.f9_yrs_experience == 'New Graduate'}

                      <option value="New Graduate" selected>New Graduate</option>
                      <option value="6 months-2 years">6 Months - 2 Years</option>
                      <option value="3-5 years">3 - 5 Years</option>
                      <option value="6 years plus">6 Years Plus</option>

                      {/if}  

                      {if $applicant.f9_yrs_experience == '6 months-2 years'}

                      <option value="New Graduate">New Graduate</option>
                      <option value="6 months-2 years" selected>6 Months - 2 Years</option>
                      <option value="3-5 years">3 - 5 Years</option>
                      <option value="6 years plus">6 Years Plus</option>

                      {/if} 

                      {if $applicant.f9_yrs_experience == '3-5 years'}

                      <option value="New Graduate">New Graduate</option>
                      <option value="6 months-2 years">6 Months - 2 Years</option>
                      <option value="3-5 years" selected>3 - 5 Years</option>
                      <option value="6 years plus">6 Years Plus</option>

                      {/if} 

                      {if $applicant.f9_yrs_experience == '6 years plus'}

                      <option value="New Graduate">New Graduate</option>
                      <option value="6 months-2 years">6 Months - 2 Years</option>
                      <option value="3-5 years">3 - 5 Years</option>
                      <option value="6 years plus" selected>6 Years Plus</option>

                      {/if}

                    </select>
                  </label>

                  <!-- ENDPOINT -->

                  <label class="fullW">Are you Spanish Bilingual ?<red style="color: red;padding: 10px;">*</red></label>

                  {if $applicant.span_bilingual == 'Yes'}
                  <div class="col-md-6" style="margin-top: 10px;margin-left: -15px; margin-bottom: 15px;">
                    <div class="tos">
                    <input id="span_bilingual" name="span_bilingual" checked="checked" value="Yes" type="radio" /><h4 class="bilingualPos" style="line-height: 2;">Yes</h4>
                  </div>
                </div>
                <div class="col-md-6 bilingualRowPos" style="margin-top: 10px;margin-left: -15px;">
                  <div class="tos">
                    <input id="span_bilingual" name="span_bilingual" value="No" type="radio" /><h4 class="bilingualPos" style="line-height: 2;">No</h4>
                  </div>
                </div>
                {else}
                <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;margin-bottom: 15px;">
                  <div class="tos">
                  <input id="span_bilingual" name="span_bilingual" value="Yes" type="radio" /><h4 class="bilingualPos" style="line-height: 2;">Yes</h4>
                </div>
              </div>

              <div class="col-md-6 bilingualRowPos" style="margin-top: 10px;margin-left: -15px;">
                <div class="tos">
                  <input id="span_bilingual" name="span_bilingual" checked="checked" value="No" type="radio"><h4 class="bilingualPos" style="line-height: 2;">No</h4>
                </div>
              </div>
              {/if}

                  <label class="fullW">Positions : <red style="color: red;padding: 10px;">*</red><p id="posresult"></p></label>
                  <label class="wrap">
                  <div class="row f9posClass" style="float: left;">
                    <select class="selectLabel" required id="f9_position" name="f9_position" class="dropdown" style="
                      -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      padding: 10px;
                      box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                      min-height: 47px;
                      width: 300px;
                      border-radius: 5px;
                      padding: 0% 3% 0% 6%;
                      border: none;
                      float: left;
                      margin-bottom: 20px;
                      font-size: 14px;
                      padding: 10px;
                      background-color: white;">
                      <option value="" disabled selected>Select your option</option>
                      <option value="Dental Assistant" {if $applicant.f9_position == 'Dental Assistant'}selected{/if}>Dental Assistant</option>
                      <option value="Hygienist" {if $applicant.f9_position == 'Hygienist'}selected{/if}>Hygienist</option>
                      <option value="Front Office" {if $applicant.f9_position == 'Front Office'}selected{/if}>Front Office</option>
                      <option value="Cross-trained (Front Office/Dental Assistant)" {if $applicant.f9_position == 'Cross-trained (Front Office/Dental Assistant)'}selected{/if}>Cross-trained (Front Office/Dental Assistant)</option>
                      <option value="Dentist" {if $applicant.f9_position == 'Dentist'}selected{/if}>Dentist</option>                         
                    </select>
                    <div id="feedback-err_cb_f9_position" class="col-md-6 negative-feedback displayNone">Check at least one box</div>       
                  </div>
                    <br />                   
                    </label>
                    <label class="fullW" style="margin-top: 28px">Receive Notifications by :</label>

                    {if strpos($applicant.notifyby, 'Email') !== false}
                      {$notifybye = 'checked'}
                    {else}
                      {$notifybye = ''}
                    {/if}

                    {if strpos($applicant.notifyby, 'Text Message') !== false}
                      {$notifybyt = 'checked'}
                    {else}
                      {$notifybyt = ''}
                    {/if}

                    <!-- ENDPOINT -->

                    <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
                      <div class="tos">
                        <input name="notifyby[]" id="notifyby" value="Email" class="checkbox-custom" type="checkbox" {$notifybye}></input> 
                        <h4 style="line-height: 2;">Email</h4>  
                      </div>
                    </div>
                    <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
                      <div class="tos">
                        <input name="notifyby[]" id="notifyby" value="Text Message" class="checkbox-custom" type="checkbox" {$notifybyt}></input> 
                        <h4 style="line-height: 2;">Text Message</h4>
                      </div>
                    </div>
                    <div id="feedback-err_cb_notifyby" class="negative-feedback displayNone">Must select at least one way to notify you</div>
                    <br>

                    <!-- ADDED -->

                    <div style="display: none;">
                     <label>{$translations.profile.form_name}:</label>
                     <input class="minput opaque" value="{$applicant.fullname}" name="fullname" id="fullname" maxlength="50" type="text"  /> 
                     <br />

                     <label>{$translations.apply.occupation_label}:</label>
                     <input class="minput opaque" value="{$applicant.occupation}" name="occupation" id="occupation" maxlength="200" type="text"  /> 
                     <br />
                   </div>

                   <div style="display: none;">
                     <label class="fullW">{$translations.dashboard_recruiter.post_location_label}</label>
                     <input class="minput opaque" value="{$applicant.location}" name="location" id="location" maxlength="400" type="text"  />
                     <br />


                     <label>{$translations.profile.form_weblink}:</label>
                     <input class="minput opaque" value="{$applicant.weblink}" name="weblink" id="weblink" maxlength="50" type="text"  />
                     <!-- <br /> -->

                    <!-- <div class="tos">
                        <label class="fullW"><input name="public_profile" id="public_profile" {if $applicant.public_profile == 1}checked{/if} type="checkbox" class="checkbox-custom"></input> 
                        <h4 class="pt9">Suspend this Account</h4></label>
                      </div>  -->
                      <br />
                    </div>

                    <!-- COMMENTED DUE TO REQUEST IN UPDATES -->
                    
                    <div class="tos">
                      <label class="fullW" style="margin-top: 35px; margin-left: 15px">
  <!--                      <input name="subscription_flag" id="subscription_flag" {if $subs_data == 1}checked{/if} type="checkbox" class="checkbox-custom"></input> 
    <h4 class="pt9">{$translations.profile.form_subscription}</h4></label>   -->    
  </div>

  <!-- ENDPOINT -->

  <!-- ADDED reposition save btn -->

  <div class="col-md-12 col-sm-12 mt50" style="z-index: 5;">

    <div class="row mb50">
      <div class="col-md-6 col-sm-12" style="width: 600px; margin-top: -40px;">

        <!--        <div class="col-md-6 col-sm-12" style="width: 600px; margin-top: -40px;"> -->
          <a id="esmId" class="green" onclick="return SimpleJobScript.showSMfields();" >{$translations.js.edit_social_media}</a>
          <!--    </div> -->

          <div id="showSMfieldsBlockId" class="displayNone">

            <br>

            <div id="SMsubBlock">
             {foreach $applicant.sm_links as $obj}
             <div id="sm_fg_{$obj@iteration}" class="form-group mb30">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl0 ml0 mb20">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pl0 ml0">
                  <div class="form-group">
                   <input value="{$obj->linkToShow}" name="sm_url_{$obj@iteration}" id="sm_url_{$obj@iteration}" type="text" class="form-control grayInput minput"  /> 
                 </div>
               </div>

               <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pl0 ml20-desk">
                <div class="form-group">
                 <select id="sm_select_{$obj@iteration}" name="sm_select_{$obj@iteration}" class="form-control minput">
                   {foreach $SM_PROFILES as $ITEM}
                   <option {if $obj->smId == $ITEM.id}selected{/if} value="{$ITEM.id}">{$ITEM.name}</option>
                   {/foreach}
                 </select>
               </div>
             </div>

             <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
              <a id="sm_close_{$obj@iteration}" class="green" href="#sm_close_{$obj@iteration}" onclick="return SimpleJobScript.removeProfileSMfield('{$obj@iteration}');"><i class="fa fa-close mt18" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
        {/foreach}
      </div>

      <div class="form-group mb30">
        <a id="addSMLink" class="green" onclick="return SimpleJobScript.addProfileSMLink();" >{if $applicant.sm_links|@count lt 4}{$translations.js.add_social_media}{else}<span class="limr">{$translations.js.limit_reached}</span>{/if}</a>
      </div>

    </div>

    <button 
      type="submit" 
      class="btn mbtn zeromlplLeft mt50" 
      name="submit" 
      id="submit"
      onclick="check_input_fields(); validate_input_fields(event);"
    >
      {$translations.website_general.text_save}
    </button>
          
  </div>
  </div>

  </div>

  <!-- END FOR REPOSITION OF SAVE BTN -->

  </div>

  <div class="col-md-7 col-sm-12 m-mobt8">
    <label>About you <red style="color: red;padding: 10px;">*</red></label><br />
    <textarea class="noTinymceTA minput opaque pTextArea rcorner" id="msg" name="msg" maxlength="500" rows="8" cols="50">{$applicant.message}</textarea>
    <br />

    <div style="display: none;">
      <label class="fullW">{$translations.profile.skills}</label>
      <div class="input textarea clearfix profileEditTaggle minput edit-profile"></div>
    </div>

    <br />

    <label class="fullW" style="margin-top: 20px">Software that you are Proficient in :<red style="color: red;padding: 10px;">*</red><p id='proresult'></p></label>
    {assign var="arr_soft" value=","|explode:$applicant.pro_soft}
      
    {$arr2_soft = array("Dentrix","EagleSoft", "Open Dental", "SoftDent", "DentiMax")}
    {$result=array_diff($arr_soft,$arr2_soft)}

    {if strpos($applicant.pro_soft, 'Dentrix') != false}
      {$dchecked = 'checked'}
    {else}
      {$dchecked = ''}
    {/if}

    {if strpos($applicant.pro_soft, 'EagleSoft') != false}
      {$eachecked = 'checked'}
    {else}
      {$eachecked = ''}
    {/if}

    {if strpos($applicant.pro_soft, 'Open Dental') != false}
      {$opchecked = 'checked'}
    {else}
      {$opchecked = ''}
    {/if}

    {if strpos($applicant.pro_soft, 'SoftDent') != false}
      {$schecked = 'checked'}
    {else}
      {$schecked = ''}
    {/if}

    {if strpos($applicant.pro_soft, 'Dentimax') != false}
      {$dechecked = 'checked'}
    {else}
      {$dechecked = ''}
    {/if}

      <div class="col-md-6"  style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="pro_soft[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="pro_soft" {$dchecked}>
          <h4 style="line-height: 2;">Dentrix</h4>  
        </div>
        <div class="tos">
          <input name="pro_soft[]" value="EagleSoft" class="checkbox-custom" type="checkbox" {$eachecked}>
          <h4 style="line-height: 2;">EagleSoft</h4>
        </div>
        <div class="tos">
          <input name="pro_soft[]" value="Open Dental" class="checkbox-custom" type="checkbox" {$opchecked}> 
          <h4 style="line-height: 2;">Open Dental</h4>
        </div>
      </div>
      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="pro_soft[]" value="SoftDent" class="checkbox-custom" type="checkbox" {$schecked}> 
          <h4 style="line-height: 2;">SoftDent</h4>
        </div>
        <div class="tos">
          <input name="pro_soft[]" value="Dentimax" class="checkbox-custom" type="checkbox" {$dechecked}> 
          <h4 style="line-height: 2;">Dentimax</h4>
        </div>
      </div>
      
      <input class="minput opaque rcorner" type="text" name="pro_soft[]" placeholder="Others..." style="margin-top: 10px" value="" />
      <div id="feedback-err_cb_pro_soft" class="negative-feedback displayNone">Please check at least one Box</div>
      <br>

      <label class="fullW">Areas of Dentistry you have worked in :<red style="color: red;padding: 10px;">*</red><p id='arearesult'></p></label>

      {if strpos($applicant.workarea, 'General') != false}
        {$gchecked = 'checked'}
      {else}
        {$gchecked = ''}
      {/if}

      {if strpos($applicant.workarea, 'Perio') != false}
        {$pchecked = 'checked'}
      {else}
        {$pchecked = ''}
      {/if}

      {if strpos($applicant.workarea, 'Oral Max Surgery') != false}
        {$ochecked = 'checked'}
      {else}
        {$ochecked = ''}
      {/if}

      {if strpos($applicant.workarea, 'Endo') != false}
        {$echecked = 'checked'}
      {else}
        {$echecked = ''}
      {/if}

      {if strpos($applicant.workarea, 'Pedo') != false}
        {$pechecked = 'checked'}
      {else}
        {$pechecked = ''}
      {/if}

      {if strpos($applicant.workarea, 'Ortho') != false}
        {$orchecked = 'checked'}
      {else}
        {$orchecked = ''}
      {/if}

      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="workarea[]" value="General" class="checkbox-custom" type="checkbox" id="workarea" {$gchecked}></input> 
          <h4 style="line-height: 2;">General</h4>  
        </div>
        <div class="tos">
          <input name="workarea[]" value="Perio" class="checkbox-custom" type="checkbox" {$pchecked}></input> 
          <h4 style="line-height: 2;">Perio</h4>
        </div>
        <div class="tos">
          <input name="workarea[]" value="Oral Max Surgery" class="checkbox-custom" type="checkbox" {$ochecked}></input> 
          <h4 style="line-height: 2;">Oral Max Surgery</h4>
        </div>
      </div>
      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="workarea[]" value="Endo" class="checkbox-custom" type="checkbox" {$echecked}></input> 
          <h4 style="line-height: 2;">Endo</h4>
        </div>
        <div class="tos">
          <input name="workarea[]" value="Pedo" class="checkbox-custom" type="checkbox" {$pechecked}></input> 
          <h4 style="line-height: 2;">Pedo</h4>
        </div>
        <div class="tos">
          <input name="workarea[]" value="Ortho" class="checkbox-custom" type="checkbox" {$orchecked}></input> 
          <h4 style="line-height: 2;">Ortho</h4>
        </div>
      </div>
      <div id="feedback-err_cb_workarea" class="negative-feedback displayNone">Please check at least one Box</div>
      <br>

      <label class="fullW" style="margin-top: 20px">Skills you are Proficient in :<red style="color: red;padding: 10px;">*</red><p id='skillsresult'></p></label>
     <div class="fullW" 
        style="
          margin-top: -10;
          font-size: smaller;
          font-weight: 100;
          color: blueviolet;">
        TIP:  Select only the ABSOLUTE requirements needed for the position to maximize the number of resumes received
     </div>

      {if strpos($applicant.skills_prof, 'Custom Temps') != false}
        {$cuchecked = 'checked'}
      {else}
        {$cuchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Cerec System') != false}
        {$cechecked = 'checked'}
      {else}
        {$cechecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Surgical Implants') != false}
        {$suchecked = 'checked'}
      {else}
        {$suchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'X-ray Certified in AZ') != false}
        {$xchecked = 'checked'}
      {else}
        {$xchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Coronal Polish Certified in AZ') != false}
        {$corchecked = 'checked'}
      {else}
        {$corchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'IV Sedation') != false}
        {$IVchecked = 'checked'}
      {else}
        {$IVchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'AR') != false}
      {$ARchecked = 'checked'}
      {else}
      {$ARchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Insurance Processing') != false}
        {$inchecked = 'checked'}
      {else}
        {$inchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Treatment Presentation') != false}
        {$trchecked = 'checked'}
      {else}
        {$trchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Anethesia Certified') != false}
        {$anchecked = 'checked'}
      {else}
        {$anchecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'Laser Certified') != false}
        {$lachecked = 'checked'}
      {else}
        {$lachecked = ''}
      {/if}

      {if strpos($applicant.skills_prof, 'EFDA') != false}
        {$efdachecked = 'checked'}
      {else}
        {$efdachecked = ''}
      {/if}

      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="skills_prof[]" value="Custom Temps" class="checkbox-custom" type="checkbox" id="skills_prof" {$cuchecked}></input> 
          <h4 style="line-height: 2;">Custom Temps</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Cerec System" class="checkbox-custom" type="checkbox" {$cechecked}></input> 
          <h4 style="line-height: 2;">Cerec System</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" {$suchecked}></input> 
          <h4 style="line-height: 2;">Surgical Implants</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" {$xchecked}></input> 
          <h4 style="line-height: 2;">X-ray Certified in AZ</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" {$corchecked}></input> 
          <h4 style="line-height: 2;">Coronal Polish Certified in AZ</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="IV Sedation" class="checkbox-custom" type="checkbox" {$IVchecked}></input> 
          <h4 style="line-height: 2;">IV Sedation Certified</h4>
        </div>
      </div>
      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input name="skills_prof[]" value="AR" class="checkbox-custom" type="checkbox" {$ARchecked}></input> 
          <h4 style="line-height: 2;">AR</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" {$inchecked}></input> 
          <h4 style="line-height: 2;">Insurance Processing</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" {$trchecked}></input> 
          <h4 style="line-height: 2;">Treatment Presentation</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Anethesia Certified" class="checkbox-custom" type="checkbox" {$anchecked}></input> 
          <h4 style="line-height: 2;">Anethesia Certified</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="Laser Certified" class="checkbox-custom" type="checkbox" {$lachecked}></input> 
          <h4 style="line-height: 2;">Laser Certified</h4>
        </div>
        <div class="tos">
          <input name="skills_prof[]" value="EFDA" class="checkbox-custom" type="checkbox" {$efdachecked}></input> 
          <h4 style="line-height: 2;">EFDA</h4>
        </div>
      </div>
      <div id="feedback-err_cb_skills_prof" class="negative-feedback displayNone">Please check at least one Box</div>
      <br />

      <label class="fullW" style="margin-top: 20px">Types of Jobs :<red style="color: red;padding: 10px;">*</red><p id="type_of_jobs"></p></label>


      <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;">
        <div class="tos">
          <input onclick="TempDays();" class="form-check-input" type="radio" name="typepos" value="Temporary" {if ($applicant.typepos == 'Temporary')}checked{/if}>
          <label class="form-check-label" for="typepos">
            Temporary
          </label>
        </div>
      </div>
      <div class="col-md-6 permRow" style="margin-top: 10px;margin-left: -15px;">
        <input class="form-check-input" type="radio" name="typepos" value="Permanent" onclick="PermaDays();" {if ($applicant.typepos == 'Permanent')}checked{/if}>
        <label class="form-check-label" for="typepos">
          Permanent
        </label>
      </div>
      <div id="feedback-err_cb_typepos" class="negative-feedback displayNone">Please check at least one Box</div>
      <br>

      <label class="fullW tempMargin" style="margin-top: 20px">Temp days you are consistently available each week:</label>
      <!-- <div class="input textarea clearfix profileEditTaggle edit-profile">{$applicant.tempday}</div> -->

      <!-- ADDED -->

      {if strpos($applicant.tempday, 'Monday') != false}

      {$mchecked = 'checked'}

      {else}

      {$mchecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Tuesday') != false}

      {$tchecked = 'checked'}

      {else}

      {$tchecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Wednesday') != false}

      {$wchecked = 'checked'}

      {else}

      {$wchecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Thursday') != false}

      {$thchecked = 'checked'}

      {else}

      {$thchecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Friday') != false}

      {$fchecked = 'checked'}

      {else}

      {$fchecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Saturday') != false}

      {$sachecked = 'checked'}

      {else}

      {$sachecked = ''}

      {/if}

      {if strpos($applicant.tempday, 'Sunday') != false}

      {$sschecked = 'checked'}

      {else}

      {$sschecked = ''}

      {/if}

      <!-- ENDPOINT -->

      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday0" value="Monday" class="checkbox-custom" type="checkbox" {$mchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Mon</h4></label>  
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday1" value="Tuesday" class="checkbox-custom" type="checkbox" {$tchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Tue</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday2" value="Wednesday" class="checkbox-custom" type="checkbox" {$wchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Wed</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday3" value="Thursday" class="checkbox-custom" type="checkbox" {$thchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Thu</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday4" value="Friday" class="checkbox-custom" type="checkbox" {$fchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Fri</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday5" value="Saturday" class="checkbox-custom" type="checkbox" {$sachecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Sat</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" name="tempday[]" id="tempday6" value="Sunday" class="checkbox-custom" type="checkbox" {$sschecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Sun</h4></label>

      <!-- COMMENTED DUE TO UPDATE REQUEST -->
      <div class="tos">
        <p style="margin-top: 10px;" id="tempdaymsg" style="color:red;"></p>              
      </div>
      <div class="row">
  <!--                        <div class="col-md-5">
                            <label class="fullW">From :</label> 
                            <input class="minput opaque" type="text" name="datefrom" id="datefrom" value="{$applicant.datefrom}"> 
                          </div>
                          <div class="col-md-5">
                            <label class="fullW">To : </label> 
                            <input class="minput opaque" type="text" name="dateuntil" id="dateuntil" value="{$applicant.dateuntil}">
                          </div> -->
                        </div>

                        <!-- ENDPOINT -->

                        <br />
                        <!-- datepicker available -->
                        <!-- WORKING -->

                        <label class="fullW">Flex Schedule - Specific Days Available</label>

                        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css">
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
                        <script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>

                        <style type="text/css">

                          .ui-datepicker {

                           width: 300px;
                           height: auto;
                           margin: 5px auto 0;
                           font: 12pt Arial, sans-serif;
                           -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
                           -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
                           box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);

                         }

                         .ui-datepicker-header {
                          background: #7527a0;
                          color: white;
                        }

                        .ui-datepicker-title {
                          text-align: center;
                        }

                        .ui-datepicker th {

                          color: #7527a0;

                        }

                        .ui-datepicker td span, .ui-datepicker td a {

                          text-align: center;

                        }

                        /*comment for now*/
                        .ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
                          background: #00009c !important;
                          color: white !important;
                        }
                        /*end*/

  /*.ui-datepicker .ui-state-active .ui-state-hover a{

      background: #00009c none;
    color: white;

    }*/

  /*.ui-datepicker .ui-datepicker-calendar .ui-state-hover td{
    background: #00009c none;
    color: white;
  }

  .ui-datepicker .ui-datepicker-calendar .ui-state-active a{
    background: #00009c;
    -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    color: white;
    text-shadow: 0px 1px 0px #4d7a85;
    filter: dropshadow(color=#4d7a85, offx=0, offy=1);
    border: 1px solid #55838f;
    position: relative;
    margin: -1px;
    }*/

  /*.ui-datepicker .ui-datepicker-prev {
    background-color: white;
  }
  .ui-datepicker .ui-datepicker-next {
    background-color: white;
    }*/

  /*.ui-datepicker .ui-datepicker-prev-hover {
    background-color: white;
  }
  .ui-datepicker .ui-datepicker-next-hover {
    background-color: white;
    }*/

  /*#ui-datepicker-div tr td:hover *,#ui-datepicker-div tr td> .ui-state-default.ui-state-hover,.ui-datepicker-current-day{
  background-color:#F4777C !important;
  color:#F4777C !important;
  text-align:center !important;
  }*/

  .ui-datepicker-today a.ui-state-highlight {

    background: #fdff00 !important;
    color: black !important;    

  }
  .ui-datepicker-today.ui-datepicker-current-day a.ui-state-highlight {

    background: #fdff00 !important;
    color: black !important;  

  }

  </style>
  <!-- <div class="col-md-6" style="margin-top: 10px;margin-left: -15px;"> -->

    <input class="minput opaque" type="text" name="avdate" id="avdate" value="{$applicant.avdate}" hidden>

    <form name="select-multiple">
      <div></div>
      <div class="col-md-6" id="multiple-date-select" style="width: 335px;height: 280px;margin-top: 10px;text-align: center;">
        <label class="fullW">Available On</label>
      </div>

      <!--  <label class="fullW" style="font-size: 13pt;margin-top: 27px;margin-left: 12px;"><u>Available Dates</u></label> -->

      <div class="col-6 col-md-4" style="margin-top: 52px;margin-left: 12px;display: inline-block; height: 20px;text-align: center;">
        <label class="fullW" style="font-size: 13pt"><u>Available Dates</u></label>
      </div>

      <div class="col-6 col-md-4" style="overflow-y: scroll;margin-top: 12px;margin-left: 12px;display: inline-block; height: 180px;text-align: center;">
        <ol id="list" style="margin-top: 10px;" align="center"></ol>
      </div>

    </form>

    <script type="text/javascript">

      var arr = [];
      var dvalue = document.getElementById("avdate").value;

      if (dvalue == ""){
        $('#multiple-date-select').multiDatesPicker({
          onSelect: function(datetext) {
            if (arr.includes(datetext)) {
          // console.log("if");
          // var table = document.getElementById('table-data');
          // var data = document.getElementById('newrow');
          // data.removeChild(td);
        } else {
          // console.log("else");
          // var table = document.getElementById('table-data');
          // var row = document.createElement('tr');
          // var col = document.createElement('td');
          // row.setAttribute('id', 'newrow');
          // col.innerHTML = datetext;
          // row.appendChild(col);
          // table.appendChild(row);
          var $date = this.value;
          test($date);
        }
      }
    });

      }else{

        var res = dvalue.split(", ");

        for (i = 0; i < res.length; i ++){
          var x = i+1;
          var y = document.createElement("li");
          y.style.padding = "5px"; 
          var t = document.createTextNode(x+". "+res[i]);
          y.appendChild(t);
          document.getElementById("list").appendChild(y);

        }

        $('#multiple-date-select').multiDatesPicker({
          addDates: res
        });

        $('#multiple-date-select').multiDatesPicker({
          onSelect: function(datetext) {
            if (arr.includes(datetext)) {
          // console.log("if");
          // var table = document.getElementById('table-data');
          // var data = document.getElementById('newrow');
          // data.removeChild(td);
        } else {
          // console.log("else");
          // var table = document.getElementById('table-data');
          // var row = document.createElement('tr');
          // var col = document.createElement('td');
          // row.setAttribute('id', 'newrow');
          // col.innerHTML = datetext;
          // row.appendChild(col);
          // table.appendChild(row);
          var $date = this.value;
          test($date);
        }
      }
    });

      }


      function test(date) {

        $("#avdate").val(date);

        document.getElementById("list").innerHTML = "";
        var res = date.split(",");

        for (i = 0; i < res.length; i ++){
          var x = i+1;
          var y = document.createElement("li");
          y.style.padding = "5px"; 
          var t = document.createTextNode(x+". "+res[i]);
          y.appendChild(t);
          document.getElementById("list").appendChild(y);
        }
      }
    </script>

    <label class="fullW"></label>
    <input class="minput opaque" type="text" name="navdate" id="navdate" value="{$applicant.navdate}" hidden>

    <form name="select-multiple">
      <div class="col-md-6" id="nmultiple-date-select" style="width: 335px;height: 280px;margin-top: 0px;text-align: center;">
        <label class="fullW">Not Available On</label>
      </div>

      <div class="col-6 col-md-4" style="margin-top: 42px;margin-left: 12px;display: inline-block; height: 20px;text-align: center;">
        <label class="fullW" style="font-size: 13pt"><u>Not Available Dates</u></label>
      </div>

      <div class="col-6 col-md-4" style="overflow-y: scroll;margin-top: 11px;margin-left: 12px;display: inline-block; height: 180px;text-align: center; margin-bottom: 40px;">
        <ol id="nlist" style="margin-top: 10px;" align="center"></ol>
      </div>

    </form>
    <!-- <table id="table-data" border="1"></table> -->


    <script type="text/javascript">

      var narr = [];
      var ndvalue = document.getElementById("navdate").value;

      if (ndvalue == ""){
          // console.log("empty");
          // console.log(ndvalue);

          $('#nmultiple-date-select').multiDatesPicker({
            onSelect: function(datetext) {
              if (narr.includes(datetext)) {
          // console.log("if");
          // var table = document.getElementById('table-data');
          // var data = document.getElementById('newrow');
          // data.removeChild(td);
        } else {
          // console.log("else");
          // var table = document.getElementById('table-data');
          // var row = document.createElement('tr');
          // var col = document.createElement('td');
          // row.setAttribute('id', 'newrow');
          // col.innerHTML = datetext;
          // row.appendChild(col);
          // table.appendChild(row);
          var $date = this.value;
          ntest($date);
        }
      }
    });

        }else{


          var res = ndvalue.split(", ");


          for (i = 0; i < res.length; i ++){
            var x = i+1;
            var y = document.createElement("li");
            y.style.padding = "5px";
            var t = document.createTextNode(x+". "+res[i]);
            y.appendChild(t);
            document.getElementById("nlist").appendChild(y);

          }

          $('#nmultiple-date-select').multiDatesPicker({
            addDates: res
          });

          $('#nmultiple-date-select').multiDatesPicker({
            onSelect: function(datetext) {
              if (narr.includes(datetext)) {
          // console.log("if");
          // var table = document.getElementById('table-data');
          // var data = document.getElementById('newrow');
          // data.removeChild(td);
        } else {
          // console.log("else");
          // var table = document.getElementById('table-data');
          // var row = document.createElement('tr');
          // var col = document.createElement('td');
          // row.setAttribute('id', 'newrow');
          // col.innerHTML = datetext;
          // row.appendChild(col);
          // table.appendChild(row);
          var $date = this.value;
          ntest($date);
        }
      }
    });
        }


        function ntest(date) {

          $("#navdate").val(date);

          document.getElementById("nlist").innerHTML = "";
          var res = date.split(",");
          for (i = 0; i < res.length; i ++){
            var x = i+1;
            var y = document.createElement("li");
            y.style.padding = "5px";
            var t = document.createTextNode(x+". "+res[i]);
            y.appendChild(t);
            document.getElementById("nlist").appendChild(y);
          }
        }
      </script>
      <br/>

      <label class="fullW" style="margin-top: 20px;">Permanent days you are looking to work:</label>

      {if strpos($applicant.permaday, 'Monday') != false}

      {$monchecked = 'checked'}

      {else}

      {$monchecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Tuesday') != false}

      {$tuechecked = 'checked'}

      {else}

      {$tuechecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Wednesday') != false}

      {$wedchecked = 'checked'}

      {else}

      {$wedchecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Thursday') != false}

      {$thuchecked = 'checked'}

      {else}

      {$thuchecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Friday') != false}

      {$frichecked = 'checked'}

      {else}

      {$frichecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Saturday') != false}

      {$satchecked = 'checked'}

      {else}

      {$satchecked = ''}

      {/if}

      {if strpos($applicant.permaday, 'Sunday') != false}

      {$sunchecked = 'checked'}

      {else}

      {$sunchecked = ''}

      {/if}

      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday0" name="permaday[]" value="Monday" class="checkbox-custom" type="checkbox" {$monchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Mon</h4></label> 
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday1" name="permaday[]" value="Tuesday" class="checkbox-custom" type="checkbox" {$tuechecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Tue</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday2" name="permaday[]" value="Wednesday" class="checkbox-custom" type="checkbox" {$wedchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Wed</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday3" name="permaday[]" value="Thursday" class="checkbox-custom" type="checkbox" {$thuchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Thu</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday4" name="permaday[]" value="Friday" class="checkbox-custom" type="checkbox" {$frichecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Fri</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday5" name="permaday[]" value="Saturday" class="checkbox-custom" type="checkbox" {$satchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Sat</h4></label>
      <label style="margin-right: 15px;margin-top: 10px;"><input style="border-color: gray; background-color: lightgray;" id="permaday6" name="permaday[]" value="Sunday" class="checkbox-custom" type="checkbox" {$sunchecked} disabled></input><h4 style="margin-left: 35px;margin-top: 5px;">Sun</h4></label>
      <div class="tos">
        <p style="margin-top: 10px;" id="permadaymsg" style="color:red;"></p>              
      </div>
      <div class="row">
        <div class="col-md-5" style="z-index: 10;">
          <label class="fullW" style="margin-top: 20px">Minimum Hourly Rate (Permanent Jobs Only): <red style="color: red;padding: 10px;">*</red><p id='resulthr'></p></label> 
          <input required class="minput opaque rcorner" type="number" name="wage_sel" id="wage_sel" step="1" min="1" value="{$applicant.wage_sel}" placeholder="$"> 
        </div>
        <div class="row col-md-12">
          <div id="feedback-err_wage_sel" class="negative-feedback displayNone">Field is empty</div>
        </div>

        <div class="col-md-5">

          <label class="fullW" style="margin-top: 20px">How far are you willing to drive:<red style="color: red;padding: 10px;">*</red></label>
          <div class="down">
            <select class="selectLabel" required id="miles_sel" name="miles_sel" class="dropdown" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
            -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
            padding: 10px;
            box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
            min-height: 47px;
            width: 75%;
            border-radius: 5px;
            padding: 0% 3% 0% 6%;
            border: none;
            float: left;
            margin-bottom: 7%;
            margin-top: 7%;
            font-size: 14px;
            padding: 10px;
            background-color: white;">

            {if $applicant.miles_sel == '5 miles'}

            <option value="5 miles" selected>5 miles</option>
            <option value="10 miles">10 miles</option>
            <option value="20 miles">20 miles</option>
            <option value="35 miles">35 miles</option>
            <option value="36+ miles">36+ miles</option>

            {/if}  

            {if $applicant.miles_sel == '10 miles'}

            <option value="5 miles">5 miles</option>
            <option value="10 miles" selected>10 miles</option>
            <option value="20 miles">20 miles</option>
            <option value="35 miles">35 miles</option>
            <option value="36+ miles">36+ miles</option>

            {/if} 

            {if $applicant.miles_sel == '20 miles'}

            <option value="5 miles">5 miles</option>
            <option value="10 miles">10 miles</option>
            <option value="20 miles" selected>20 miles</option>
            <option value="35 miles">35 miles</option>
            <option value="36+ miles">36+ miles</option>

            {/if} 

            {if $applicant.miles_sel == '35 miles'}

            <option value="5 miles">5 miles</option>
            <option value="10 miles">10 miles</option>
            <option value="20 miles">20 miles</option>
            <option value="35 miles" selected>35 miles</option>
            <option value="36+ miles">36+ miles</option>

            {/if}

            {if $applicant.miles_sel == '36+ miles'}                

            <option value="5 miles">5 miles</option>
            <option value="10 miles">10 miles</option>
            <option value="20 miles">20 miles</option>
            <option value="35 miles">35 miles</option>
            <option value="36+ miles" selected>36+ miles</option>                           
            {/if}

          </select>
        </div>
      </div>
    </div>
    <br />  
  </div> 
  </div>
  </div>
  <!-- COMMENT FOR NOW SAVE BTN -->

  <!--            <div class="col-md-12 col-sm-12 mt50">
                  <a id="esmId" class="green" onclick="return SimpleJobScript.showSMfields();" >{$translations.js.edit_social_media}</a>

                  <div class="row mb50">
                      <div class="col-md-6 col-sm-12">
                    <button type="submit" class="btn mbtn zeromlplLeft mt50" name="submit" id="submit" >{$translations.website_general.text_save}</button>
                      </div>
                  </div>

                  <div id="showSMfieldsBlockId" class="displayNone">

                    <div id="SMsubBlock">
                   {foreach $applicant.sm_links as $obj}
                    <div id="sm_fg_{$obj@iteration}" class="form-group mb30">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl0 ml0 mb20">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pl0 ml0">
                          <div class="form-group">
                             <input value="{$obj->linkToShow}" name="sm_url_{$obj@iteration}" id="sm_url_{$obj@iteration}" type="text" class="form-control grayInput minput"  /> 
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pl0 ml20-desk">
                          <div class="form-group">
                             <select id="sm_select_{$obj@iteration}" name="sm_select_{$obj@iteration}" class="form-control minput">
                             {foreach $SM_PROFILES as $ITEM}
                              <option {if $obj->smId == $ITEM.id}selected{/if} value="{$ITEM.id}">{$ITEM.name}</option>
                             {/foreach}
                             </select>
                          </div>
                        </div>

                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                            <a id="sm_close_{$obj@iteration}" class="green" href="#sm_close_{$obj@iteration}" onclick="return SimpleJobScript.removeProfileSMfield('{$obj@iteration}');"><i class="fa fa-close mt18" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                   {/foreach}
                    </div>

                    <div class="form-group mb30">
                      <a id="addSMLink" class="green" onclick="return SimpleJobScript.addProfileSMLink();" >{if $applicant.sm_links|@count lt 4}{$translations.js.add_social_media}{else}<span class="limr">{$translations.js.limit_reached}</span>{/if}</a>
                    </div>

                      </div>
                    </div>
                  -->
                  <!--  END OF COMMENT SAVE BTN -->

                </div>
                <div id="profile1" class="tab-pane fade active show">
                  <div class=""><!-- row pb-2 -->
                    <div class="col-md-12">
                      <div class="tos">

                       <div class="row">
                         <div class="col-md-12 col-sm-12">
                           <br /> <br />
                           <label><h3>Upload new Docs</h3> (Max 2.5 MB - PDF, DOC, DOCX)</label>
                           <div id="upload_message" class="row displayNone" style="
                              padding-left: 15px;
                              padding-right: 15px;
                              background-color: salmon;
                              margin-left: 0px;
                              margin-right: 0px;
                              margin-top: 0px;
                              padding-top: 5px;
                              padding-bottom: 5px;
                              text-align: center;
                          ">
                            
                           </div>                                            
                         </div>
                         <br /> <br />
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="applicantCv" style="cursor:pointer;">Resume</label><span style="color:red"> * Required</span>
                            <input 
                              accept=".pdf,.doc,.docx" 
                              type="file" 
                              name="applicantCv" 
                              id="applicantCv"                               
                              class="form-control inputfile minput"
                              onchange="showResume(this)" 
                            />
                            
                            <div>
                              <a type="application/octet-stream" href="{if $applicant.cv_path neq ''}{$BASE_URL}{$applicant.cv_path}{else} {/if}">
                                <span class="cv-hint" id="cv-hint" name="cv-hint">{if $applicant.cv_path neq ''}{$applicant.cv_path|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentCv" name="currentCv" value="{$applicant.cv_path}" />
                          </div>
                          <div class="col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-12" style="width: 600px; margin-top: 10px; margin-bottom: 20px;">
                              <button 
                                type="submit" 
                                class="btn mbtn zeromlplLeft mt50 saveDoc" 
                                name="doc_submit" 
                                id="doc_submit"                               
                              >
                              {$translations.website_general.text_save} UPLOADS
                              </button>
                              {* onclick="check_input_fields();" *}
                            </div>
                          </div>              
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="cover_letter" style="cursor:pointer;">Cover Letter</label>
                            <input accept=".doc,.docx, .pdf" name="cover_letter" id="cover_letter" onchange="showCoverLetter.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.cover_letter neq ''}{$BASE_URL}{$applicant.cover_letter}{else} {/if}">
                                <span class="cv-hint" id="cover_letter-hint" name="cover_letter-hint" download >{if $applicant.cover_letter neq ''}{$applicant.cover_letter|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentCL" name="currentCL" value="{$applicant.cover_letter}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="x_ray" style="cursor:pointer;">X-ray Certification</label>
                            <input accept=".doc,.docx, .pdf" name="x_ray" id="x_ray" onchange="showXray.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.x_ray neq ''}{$BASE_URL}{$applicant.x_ray}{else} {/if}">
                                <span class="cv-hint" id="x-ray-hint" name="x-ray-hint">{if $applicant.x_ray neq ''}{$applicant.x_ray|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentX_ray" name="currentX_ray" value="{$applicant.x_ray}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="coronal_polish" style="cursor:pointer;">Coronal Polish Certification</label>
                            <input accept=".doc,.docx, .pdf" name="coronal_polish" id="coronal_polish" onchange="showCoronalPolish.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.coronal_polish neq ''}{$BASE_URL}{$applicant.coronal_polish}{else} {/if}">
                                <span class="cv-hint" id="coronal-polish-hint" name="coronal-polish-hint">{if $applicant.coronal_polish neq ''}{$applicant.coronal_polish|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentCP" name="currentCP" value="{$applicant.coronal_polish}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="laser" style="cursor:pointer;">Laser Certification</label>
                            <input accept=".doc,.docx, .pdf" name="laser" id="laser" onchange="showLaser.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.laser neq ''}{$BASE_URL}{$applicant.laser}{else} {/if}">
                                <span class="cv-hint" id="laser-hint" name="laser-hint">{if $applicant.laser neq ''}{$applicant.laser|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentLaser" name="currentLaser" value="{$applicant.laser}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="anesthesia" style="cursor:pointer;">Anesthesia Certification</label>
                            <input accept=".doc,.docx, .pdf" name="anesthesia" id="anesthesia" onchange="showAnesthesia.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.anesthesia neq ''}{$BASE_URL}{$applicant.anesthesia}{else} {/if}">
                                <span class="cv-hint" id="anesthesia-hint" name="anesthesia-hint">{if $applicant.anesthesia neq ''}{$applicant.anesthesia|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentAnesthesia" name="currentAnesthesia" value="{$applicant.anesthesia}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="efta" style="cursor:pointer;">Efta Certification</label>
                            <input accept=".doc,.docx, .pdf" name="efta" id="efta" onchange="showEfta.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.efta neq ''}{$BASE_URL}{$applicant.efta}{else} {/if}">
                                <span class="cv-hint" id="efta-hint" name="efta-hint">{if $applicant.efta neq ''}{$applicant.efta|substr:12:1000}{else}  {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentEfta" name="currentEfta" value="{$applicant.efta}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="hygiene_license" style="cursor:pointer;">Hygiene License</label>
                            <input accept=".doc,.docx, .pdf" name="hygiene_license" id="hygiene_license" onchange="showHygiene.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.hygiene_license neq ''}{$BASE_URL}{$applicant.hygiene_license}{else} {/if}">
                                <span class="cv-hint" id="hygiene-license-hint" name="hygiene-license-hint">{if $applicant.hygiene_license neq ''}{$applicant.hygiene_license|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentHL" name="currentHL" value="{$applicant.hygiene_license}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="dentist_license" style="cursor:pointer;">Dentist License</label>
                            <input accept=".doc,.docx, .pdf" name="dentist_license" id="dentist_license" onchange="showDentist.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.dentist_license neq ''}{$BASE_URL}{$applicant.dentist_license}{else} {/if}">
                                <span class="cv-hint" id="dentist-license-hint" name="dentist-license-hint">{if $applicant.dentist_license neq ''}{$applicant.dentist_license|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentDL" name="currentDL" value="{$applicant.dentist_license}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="w9" style="cursor:pointer;">W9</label>
                            <input accept=".doc,.docx, .pdf" name="w9" id="w9" onchange="showW9.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.w9 neq ''}{$BASE_URL}{$applicant.w9}{else} {/if}">
                                <span class="cv-hint" id="w9-hint" name="w9-hint">{if $applicant.w9 neq ''}{$applicant.w9|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentW9" name="currentW9" value="{$applicant.w9}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="w4" style="cursor:pointer;">W4</label>
                            <input accept=".doc,.docx, .pdf" name="w4" id="w4" onchange="showW4.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.w4 neq ''}{$BASE_URL}{$applicant.w4}{else} {/if}">
                                <span class="cv-hint" id="w4-hint" name="w4-hint">{if $applicant.w4 neq ''}{$applicant.w4|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentW4" name="currentW4" value="{$applicant.w4}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="a4" style="cursor:pointer;">A4</label>
                            <input accept=".doc,.docx, .pdf" name="a4" id="a4" onchange="showA4.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.a4 neq ''}{$BASE_URL}{$applicant.a4}{else} {/if}">
                                <span class="cv-hint" id="a4-hint" name="a4-hint">{if $applicant.a4 neq ''}{$applicant.a4|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentA4" name="currentA4" value="{$applicant.a4}" />
                          </div>
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="direct_deposit" style="cursor:pointer;">Direct Deposit</label>
                            <input accept=".doc,.docx, .pdf" name="direct_deposit" id="direct_deposit" onchange="showDirectDepost.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.direct_deposit neq ''}{$BASE_URL}{$applicant.direct_deposit}{else} {/if}">
                                <span class="cv-hint" id="direct-deposit-hint" name="direct-deposit-hint">{if $applicant.direct_deposit neq ''}{$applicant.direct_deposit|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentDD" name="currentDD" value="{$applicant.direct_deposit}" />
                          </div>  
                        </div>
                        <div class="col-md-6 row_padding docUpload">
                          <div class="back_color">
                            <label id="cvLabel" for="i9" style="cursor:pointer;">I9</label>
                            <input accept=".doc,.docx, .pdf" name="i9" id="i9" onchange="showI9.call(this)" class="inputfile" type="file" />
                            <div>
                              <a type="application/octet-stream" download href="{if $applicant.i9 neq ''}{$BASE_URL}{$applicant.i9}{else} {/if}">
                                <span class="cv-hint" id="i9-hint" name="i9-hint">{if $applicant.i9 neq ''}{$applicant.i9|substr:12:1000}{else} {/if}</span>
                              </a>
                            </div>
                            <input type="hidden" id="currentI9" name="currentI9" value="{$applicant.i9}" />
                          </div>  
                        </div>                                  

                        <br><br>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

          </form>
          {literal}
  <!--  <script type="text/javascript">
    
    $('#wage_sel').on('focusout', function(){
        var $wage_sel = this.value;
        validatehr($wage_sel);
    });

    function validatehr(wage_sel) {
      var $result = $("#resulthr");
      $result.text("");
        var hrReg = /^[1-9][0-9]*$/;
        if (!hrReg.test(wage_sel)) {
          /*$result.text("Must be a number.");
          $result.css({"color": "red", "font-size": "80%"});*/

          var hr  =  document.getElementById('wage_sel');
          /*hr.setCustomValidity("");*/
        } else {
          $result.text("");
          $result.css({"color": "green", "font-size": "80%"});

          var hr  =  document.getElementById('wage_sel');
         /* hr.setCustomValidity("");*/
        }
    }
  </script> -->

  <script type="text/javascript">
    {/literal}
    {foreach $applicant.sm_links as $obj}
    {literal}
    var elIndex = {/literal}{$obj@iteration}{literal} - 1;
    if ($('#sm_close_' + elIndex).length) {
      $('#sm_close_' + elIndex).addClass('displayNone');
    }
    {/literal}
    {/foreach}
    {literal}
    $('#applicantCv').change(function() {
     var fname = $('input[type=file]').val().split('\\').pop();
     if( fname )
      $('#applicantCvLabel').html(fname);
    else
      $('#applicantCvLabel').html($('#applicantCvLabel').html());
  });
    
  </script>


  <script type="text/javascript">
    $("input#subscription_flag").click(function(e){
      e.preventDefault();
      window.location = "http://jobboard.f9portal.net/subscribe?email={/literal}{$applicant.email}{literal}";
    });
  </script>
  {/literal}

  {foreach $applicant.sm_links as $obj}
  {literal}
  <script type="text/javascript">
    var elIndex = {/literal}{$obj@iteration}{literal} - 1;
    if ($('#sm_close_' + elIndex).length) {
      $('#sm_close_' + elIndex).addClass('displayNone');
    }
  </script>
  {/literal}
  {/foreach}

  {literal}
  <script type="text/javascript">
   $('#applicantCv').change(function() {
     var fname = $('input[type=file]').val().split('\\').pop();
     if( fname )
      $('#applicantCvLabel').html(fname);
    else
      $('#applicantCvLabel').html($('#applicantCvLabel').html());
  });
  </script>
  {/literal}

  {literal}
  <script type="text/javascript">
    function showImage(){
     $('#image').change(function () {
       var filePath=$('#f9_photo').val(); 
     });
     if(this.files && this.files[0]){
      var obj = new FileReader();
      obj.onload = function(data){
        var image = document.getElementById("image");
        image.src = data.target.result;
      }
      obj.readAsDataURL(this.files[0]);
      $("#myAlert").show();
      <!--$("#myAlert").first().hide().fadeIn(200).delay(2000).fadeOut(1000, function () { $(this).hide(); });-->
    }else{
      $("#image").attr("src","../uploads/images/Generic-Profile.jpg");
      $("#myAlert").first().hide();
    }
    /*$("#image").attr("src","../uploads/images/Generic-Profile.jpg");*/
  }

  $(document).ready(function(){
    var img = $('#image').attr('src');
    if(img == "/"){
      $("#image").attr("src","../uploads/images/Generic-Profile.jpg");
    }else{
      return img + "";
    }
  });


  $(document).ready(function(){
    $(".close").click(function(){
      $("#myAlert").alert("close");
    });
  });

  $(document).ready(function(){
    $("#Inactivate").click(function(){
      $("#Inactivate_modal").modal('show');
      document.body.style.overflow='scroll';
      $("#Suspend_modal").modal('hide');
      document.getElementById("suspend_check").checked = false;
      document.getElementById("date_from_to").style.display = "none";
    });
  });
  </script>
  {/literal}

  {literal}
  <script type="text/javascript">
    $(document).ready(function(){
      $("#Suspend").click(function(){
        $("#Suspend_modal").modal('show');
        document.body.style.overflow='scroll';
        $("#Inactivate_modal").modal('hide');
        document.getElementById("deactivate_check").checked = false;
      });
    });

    function myFunction() {
      var checkBox = document.getElementById("suspend_check");
      var text = document.getElementById("date_from_to");
      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
    }
  </script>
  {/literal}

  {literal}
  <script type="text/javascript">
    $(document).ready(function(){
      var img = $('#image').attr('src');
      var pieces = img.split(/[\s/]+/);
      var f9_photo = pieces[pieces.length-1];
      $('#f9_photo').text(f9_photo);
    });
  </script>
  {/literal}


  {literal}
  <script type="text/javascript">
    
    function showResume(){
      var resume = $('#applicantCv').val();
      console.log('resume submit');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);

      var xhr = new XMLHttpRequest();

      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);      
      $('#cv-hint').text(resume.substr(12, $('#cv-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#cv-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }


    function showCoverLetter(){
      var resume = $('#cover_letter').val();
      console.log('cover letter');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);

      var xhr = new XMLHttpRequest();

      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#cover_letter-hint').text(resume.substr(12, $('#cover_letter-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#cover_letter-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showXray(){
      var resume = $('#x_ray').val();
      console.log('xray cert');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#x-ray-hint').text(resume.substr(12, $('#x-ray-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#x-ray-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showCoronalPolish(){
      var resume = $('#coronal_polish').val();
      console.log('coronal polish');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#coronal-polish-hint').text(resume.substr(12, $('#coronal-polish-hint').text().length));   
      $("#upload_message").text(resume.substr(12, $('#coronal-polish-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }
    
    function showLaser(){
      var resume = $('#laser').val();
      console.log('laser cert');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#laser-hint').text(resume.substr(12, $('#laser-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#laser-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }
    
    function showAnesthesia(){
      var resume = $('#anesthesia').val();
      console.log('anesthesia cert');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#anesthesia-hint').text(resume.substr(12, $('#anesthesia-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#anesthesia-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showEfta(){
      var resume = $('#efta').val();
      console.log('efta cert');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#efta-hint').text(resume.substr(12, $('#efta-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#efta-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showHygiene(){
      var resume = $('#hygiene_license').val();
      console.log('hygine cert');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#hygiene-license-hint').text(resume.substr(12, $('#hygiene-license-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#hygiene-license-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showDentist(){
      var resume = $('#dentist_license').val();
      console.log('dentist');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#dentist-license-hint').text(resume.substr(12, $('#dentist-license-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#dentist-license-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showW9(){
      var resume = $('#w9').val();
      console.log('W9');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#w9-hint').text(resume.substr(12, $('#w9-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#w9-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showW4(){
      var resume = $('#w4').val();
      console.log('W4');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#w4-hint').text(resume.substr(12, $('#w4-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#w4-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showA4(){
      var resume = $('#a4').val();
      console.log('A4');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#a4-hint').text(resume.substr(12, $('#a4-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#a4-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showDirectDepost(){
      var resume = $('#direct_deposit').val();
      console.log('direct deposit');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#direct-deposit-hint').text(resume.substr(12, $('#direct-deposit-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#direct-deposit-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function showI9(){
      var resume = $('#i9').val();
      console.log('I9');
      var form = document.getElementById('form_upload');
      var formData = new FormData(form);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/profile/profile-edited', true);
      xhr.send(formData);
      $('#i9-hint').text(resume.substr(12, $('#i9-hint').text().length));
      $("#upload_message").text(resume.substr(12, $('#i9-hint').text().length)).fadeIn(1000).fadeOut(10000);
    }

    function TempDays(){
      if(document.forms['form_upload'].elements['typepos'].value == 'Temporary'){
        for (i = 0; i < 7; i++) { 
          $('#tempday' + i).attr('style',false);
          $('#tempday' + i).attr('disabled',false);
          $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#000000');
        }
        for (i = 0; i < 7; i++) {
          $('#permaday'+i).attr('style','border-color: gray; background-color: lightgray;');
          $('#permaday' + i).attr('disabled',true);
          $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
          //ADDED TO UNCHECK CHECK ITEMS
          $('#permaday' + i).attr('checked',false);
          //END
        }        
      }else{
        for (i = 0; i < 7; i++) { 
          $('#tempday'+i).attr('style','border-color: gray; background-color: lightgray;');
          $('#tempday' + i).attr('disabled',true);
          $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
            //ADDED TO UNCHECK CHECK ITEMS
          $('#tempday' + i).attr('checked',false);
            //END
        }           
      }
    }

    function PermaDays(){
      if(document.forms['form_upload'].elements['typepos'].value == 'Permanent'){
        for (i = 0; i < 7; i++) { 
          $('#permaday' + i).attr('style',false);
          $('#permaday' + i).attr('disabled',false);
          $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#000000');
        }
        for (i = 0; i < 7; i++) { 
          $('#tempday'+i).attr('style','border-color: gray; background-color: lightgray;');
          $('#tempday' + i).attr('disabled',true);
          $("#temp_day_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
            //ADDED TO UNCHECK CHECK ITEMS
          $('#tempday' + i).attr('checked',false);
            //END
        }         
      }else{
        for (i = 0; i < 7; i++) { 
          $('#permaday'+i).attr('style','border-color: gray; background-color: lightgray;');
          $('#permaday' + i).attr('disabled',true);
          $("#permaday_name" + i).attr('style', 'margin-left: 6px;margin-top: 6px;color:#CCCCCC');
          //ADDED TO UNCHECK CHECK ITEMS
          $('#permaday' + i).attr('checked',false);
          //END
        }
      }
    }
    $(document).ready(function() {    
      TempDays();
      PermaDays();
    });
      </script>   
      {/literal}

      {literal}
      <script type="text/javascript">

        function validate_checkboxes(event){
          if($('input[name="pro_soft[]"]:checked').length == 0) {      
            var $proresult = $("#proresult");
            $proresult.text("Must select at least one Software that you are Proficient in.");
            $proresult.css({"color": "red", "font-size": "80%"});

            var x = document.getElementById("errors");
            x.style.display = "block"; //show

            var pro_soft  =  document.getElementById('pro_soft');
            pro_soft.setCustomValidity("");
            event.preventDefault();
            return false;
          }else{
            var $proresult = $("#proresult");
            $proresult.text("");
            $proresult.css({"color": "red", "font-size": "80%"});

            var pro_soft  =  document.getElementById('pro_soft');
            pro_soft.setCustomValidity("");
          }



          if($('input[name="workarea[]"]:checked').length == 0) {
            var $arearesult = $("#arearesult");
            $arearesult.text("Must select at least one Areas of Dentistry you have worked in.");
            $arearesult.css({"color": "red", "font-size": "75%"});
            //$("#error-message-text").append('<li>Must select at least one Areas of Dentistry you have worked in.</li>');
            //MSG
            var x = document.getElementById("errors");
            x.style.display = "block"; //show

            var workarea  =  document.getElementById('workarea');
            workarea.setCustomValidity("");
            event.preventDefault();
          }else{
            var $arearesult = $("#arearesult");
            $arearesult.text("");
            $arearesult.css({"color": "red", "font-size": "75%"});

            var workarea  =  document.getElementById('workarea');
            workarea.setCustomValidity("");
          }

          if($('input[name="skills_prof[]"]:checked').length == 0) {
            var $skillsresult = $("#skillsresult");
            $skillsresult.text("Must select at least one Skills you are Proficient in.");
            $skillsresult.css({"color": "red", "font-size": "80%"});
            // $("#error-message-text").append('<li>Must select at least one Skills you are Proficient in.</li>');
            //MSG
            var x = document.getElementById("errors");
            x.style.display = "block"; //show

            var skills_prof  =  document.getElementById('skills_prof');
            skills_prof.setCustomValidity("");
          }else{
            var $skillsresult = $("#skillsresult");
            $skillsresult.text("");
            $skillsresult.css({"color": "red", "font-size": "80%"});

            var skills_prof  =  document.getElementById('skills_prof');
            skills_prof.setCustomValidity("");
          }

          if(document.forms['form_upload'].elements['typepos'].value != 'Temporary' || document.forms['form_upload'].elements['typepos'].value != 'Permanent'){

            var $typepos = $("#type_of_jobs");
            $typepos.text("");
            $typepos.css({"color": "red", "font-size": "80%"});
            // $("#error-message-text").append('<li>Must select at least one Type of Job.</li>');
            //MSG
            var x = document.getElementById("errors");
            x.style.display = "block"; //show

            var temreq  =  document.getElementById('temreq');
            temreq.setCustomValidity("");
          }else{
            var $typepos = $("#type_of_jobs");
            $typepos.text("");
            $typepos.css({"color": "red", "font-size": "80%"});

            var temreq  =  document.getElementById('temreq');
            temreq.setCustomValidity("");
          }
        }


    function check_input_fields() {
      var input_fields = {
       'apply_email': 'Email',
       'birthdate': 'Date of Birth',
       'f9_address_1': 'Street Address',
       'f9_city': 'City',
       'f9_state': 'State',
       'f9_zip' : 'ZIP Code Cannot be empty',
       'notifyby': 'Receive Notification'
     };
     var err_display = 0;
     $("#error-message-text").empty();
     $.each(input_fields, function (index, value) {
       console.log(index);

       if ($.trim($('#' + index).val()).length == 0) {
        $("#error_message_" + index).show();
        err_display = 1;
      } else {
        $("#error_message_" + index).hide();
      }
      console.log(index + ": " + index);
    });
     if (err_display == 1) {
       var x = document.getElementById("errors");
       x.style.display = "block";
       window.scrollTo({
        top: 80,
        behavior: 'smooth'
      });
       return false;
     } else {
       var x = document.getElementById("errors");
       x.style.display = "none";
     }
     return true;
   }
  </script>   
  {/literal}
  {literal}
  <script type="text/javascript">
    $(document).ready(function(){

      var imputElements = document.getElementsByTagName('input');

      for(var i = 0; i < imputElements.length; i++)
      {
        imputElements[i].addEventListener( "invalid",
          function( event ) {
            event.preventDefault();
          });
      }

      var selectElements = document.getElementsByTagName('select');

      for(var i = 0; i < selectElements.length; i++)
      {
        selectElements[i].addEventListener( "invalid",
          function( event ) {
            event.preventDefault();
          });
      }

    });
  </script>
  {/literal}
  {literal}
  <script type="text/javascript">
    $(document).ready(function(){
      $("#birthdate").blur(function(){
        var birthdateVal = $('#birthdate').val();
        var birthdatePattern = /^([0-9]{2})+\/([0-9]{2})+\/([0-9]{4})+/; 
        var birthdateIsValid = birthdatePattern.test(birthdateVal);
        if (birthdateVal.length !== 10 || birthdateIsValid == false) {
          $('#feedback-err_birthdate').removeClass('displayNone');
          console.log(birthdateIsValid);
          $('#submit').click(function (evt) {
            evt.preventDefault();
            var x = document.getElementById("errors");
            x.style.display = "block"; 
            window.scrollTo({
              top: 80,
              behavior: 'smooth'
            });

          });
        }else{
          $('#feedback-err_birthdate').addClass('displayNone');
          console.log(birthdateIsValid);
          $('#submit').unbind('click');
        }
      });

    });

    /*document.getElementById('wage_sel').addEventListener('blur', function (e) {
      valueNum = Number(e.target.value);
      var newCurrency = valueNum.toFixed(2);
      e.target.value = newCurrency;

      if (newCurrency == 0){
        var errText = $('#feedback-err_wage_sel').text();
        var newErrText = errText.replace(errText, 'Fill in a valid dollar amount');
        document.getElementById('feedback-err_wage_sel').innerHTML = newErrText;
        $('#feedback-err_wage_sel').removeClass('displayNone');
        $('#submit').click(function (evt) {
          evt.preventDefault();
          var x = document.getElementById("errors");
          x.style.display = "block"; 
          window.scrollTo({
            top: 80,
            behavior: 'smooth'
          });

        });
      } else {
        $('#feedback-err_wage_sel').addClass('displayNone');
        $('#submit').unbind('click');
      }
    });*/

      document.getElementById('wage_sel').addEventListener('blur', function (e) {
      var valueNum = e.target.value;
      //newCurrency = valueNum.toFixed(2);
      //e.target.value = newCurrency;
      var wageFormat = /^([0-9]+)\.([0-9]+)/;
      var wageFormatDecimal = wageFormat.test(valueNum);
      console.log(typeof(valueNum));

      if (valueNum == 0){
        var errText = $('#feedback-err_wage_sel').text();
        var newErrText = errText.replace('Field is empty', 'Fill in a valid dollar amount');
        document.getElementById('feedback-err_wage_sel').innerHTML = newErrText;
        $('#feedback-err_wage_sel').removeClass('displayNone');
        $('#submit').click(function (evt) {
          evt.preventDefault();
          var x = document.getElementById("errors");
          x.style.display = "block"; 
          window.scrollTo({
            top: 80,
            behavior: 'smooth'
          });

        });

      } else {
        $('#feedback-err_wage_sel').addClass('displayNone');
        $('#submit').unbind('click');
        if (wageFormatDecimal == true) {
         /* var newCurrency = valueNum.toFixed(2);*/
          var newCurrency = valueNum.slice(0, (valueNum.indexOf(".")) + 3);
          e.target.value = newCurrency;
          }
        }
      });

    function validate_input_fields(event) {
      var input_fields = {
        'cb_notifyby[]' : 'Receive Notification',
        'cb_pro_soft[]' : 'Software that you are Proficient in',
        'cb_workarea[]' : 'Areas of Dentistry you have worked in',
        'cb_skills_prof[]' : 'Skills you are PROFICIENT in'
      };
      var err_display = 0;
      $("#error-message-text").empty();
      $.each(input_fields, function( index, value ) {
        if(index.substring(0,2)=='cb'){   
          console.log(index);                                                                     
          if(!$('input[name="' + index.substring(3,index.length) + '"]').is(':checked')) {
              //$("#error-message-text").append('<li style="font-weight:400">' + value + '</li>');
              if(index.substr((index.length-2),2)=='[]') {
                err_name = index.substr(0,index.length-2);
                console.log(err_name);
                $('#' + 'feedback-err_' + err_name).removeClass('displayNone');
              }else{
                $('#' + 'feedback-err_' + index).removeClass('displayNone');
              }         
              err_display = 1;
              /*event.preventDefault();*/
            }else{
              if(index.substr((index.length-2),2)=='[]') {
                err_name = index.substr(0,index.length-2);
                console.log(err_name);
                $('#' + 'feedback-err_' + err_name).addClass('displayNone');
              }else{
                $('#' + 'feedback-err_' + index).addClass('displayNone');
              }
            }
          }else{
            if(($('#' + index).val()==0 || $('#' + index).val()==null)){
              //$("#error-message-text").append('<li style="font-weight:400">' + value + '</li>');
              $('#' + 'feedback-err_' + index).removeClass('displayNone');
              err_display = 1;
              /*event.preventDefault();*/
            }else{
              $('#' + 'feedback-err_' + index).addClass('displayNone');
            }
          }
        });

      if (err_display == 1){
        var x = document.getElementById("errors");
        x.style.display = "block"; 
          //window.scrollTo(0,80);
          event.preventDefault();
          window.scrollTo({
            top: 80,
            behavior: 'smooth'
          });
        }else{
          var x = document.getElementById("errors");
          x.style.display = "none"; 
        }
      }
    </script>
    {/literal}
    {literal}
    <script type="text/javascript">
      $('#submit').click(function (evt) {

        if($('input[name="tempday[]"]:checked').length == 0 && document.forms['form_upload'].elements['typepos'].value == 'Temporary') {
          var $tempday = $("#tempdaymsg");
          $tempday.text("Select Temporary Job Type above if checking temp days you are available.");
          $tempday.css({"color": "red", "font-size": "80%", "opacity": "0.75", "font-size": "0.89em", "width": "auto"});

            //MSG
            var x = document.getElementById("errors");
            x.style.display = "block"; //show
            window.scrollTo({
              top: 80,
              behavior: 'smooth'
            });
            evt.preventDefault();

          }else{
            var tempday = $("#tempdaymsg");
            tempday.text("");
            tempday.css({"color": "red", "font-size": "80%"});
          }

          if($('input[name="permaday[]"]:checked').length < 1 && document.forms['form_upload'].elements['typepos'].value == 'Permanent') {
            var $permaday = $("#permadaymsg");
            $permaday.text("Select Permanent Job Type above if checking temp days you are available");
            $permaday.css({"color": "red", "font-size": "80%", "opacity": "0.75", "font-size": "0.89em", "width": "auto"});

            var x = document.getElementById("errors");
            x.style.display = "block"; //show
            window.scrollTo({
              top: 80,
              behavior: 'smooth'
            });
            evt.preventDefault();

        }else{
          var permaday = $("#permadaymsg");
          permaday.text("");
          permaday.css({"color": "red", "font-size": "80%"});
            //MSG
            var x = document.getElementById("errors");
            x.style.display = "block"; //show
          }

        });

       document.getElementById('f9_zip').addEventListener('input', function (e) {
        var x = e.target.value.replace(/[^0-9]/g, '')
            e.target.value = x;
                });
      </script>
      {/literal}

      <!-- END -->
