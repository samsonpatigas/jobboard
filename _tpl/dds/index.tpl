<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<title>Desert Dental Staffing | Hot Jobs</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

		<meta name="description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}" />
		<meta name="keywords" content="{if $seo_keys}{$seo_keys}{else}{$meta_keywords}{/if}" />

		<!-- 
	 Chrome and Opera Icons - to add support create your icon with different resolutions. Default is 192x192
		 <link rel="icon" sizes="192x192" href="{$BASE_URL}fav.png" >
		<link rel="icon" sizes="144x144" href="{$BASE_URL}fav-144.png" >
		<link rel="icon" sizes="96x96" href="{$BASE_URL}fav-96.png" >
		<link rel="icon" sizes="48x48" href="{$BASE_URL}fav-48.png" >
	-->
	<link rel="icon" href="{$BASE_URL}fav.png" >
	<!-- 
	 Apple iOS icons
		<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	-->
	<link rel="apple-touch-icon" href="{$BASE_URL}fav-ios.png">
	<!-- iOS startup image -->
	<link rel="apple-touch-startup-image"  href="{$BASE_URL}fav-startup.png">
	<!-- Apple additional meta tags -->
	<meta name="apple-mobile-web-app-title" content="Desert Dental Staffing | Hot Jobs">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<!-- Android web app capable -->
	<meta name="mobile-web-app-capable" content="yes">
	<!-- IE & Windows phone pin icons. Almost same size like ios so it is reused -->
	<meta name="msapplication-square150x150logo" content="fav-ios.png">

	<!-- facebook meta tags for sharing -->
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Desert Dental Staffing | Hot Jobs" />
	<meta property="og:description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}" />
	<meta property="og:url" content="{$MAIN_URL}" />
	<meta property="og:site_name" content="{$SITE_NAME}" />
	<meta property="og:image" content="{$MAIN_URL}share-image.png" />

	<!-- twitter metatags -->
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}"/>
	<meta name="twitter:title" content="Desert Dental Staffing | Hot Jobs"/>
	<!-- add your twitter site 
	<meta name="twitter:site" content="@brand"/>
	--> 
	<meta name="twitter:domain" content="{$SITE_NAME}"/>
	<meta name="twitter:image" content="{$MAIN_URL}share-image.png" />
	
	<!-- RSS -->
	<link rel="alternate" type="application/rss+xml" title="{$SITE_NAME} RSS Feed" href="{$BASE_URL}rss">
 
	<link rel="canonical" href="http://{$BASE_URL}home" />

	<!-- IF ALL STYLES ARE REMOVED EXCEPT BOOTSTRAP, UNCOMMENT THIS STYLE, TO KEEP THE BASIC, WORKING WEB. READY TO BE CUSTOMIZED 
	<link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/css/saveStructure.css" type="text/css" /> -->
	<!-- ######################################################################################################################## -->
	
	<link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/style.css">
	<link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/reset.css">
	<link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/font-awesome.min.css">

	<link href="{$BASE_URL}_tpl/{$THEME}/1.5/css/dev.css" rel="stylesheet">

	<noscript>{$translations.website_general.noscript_msg}</noscript>

<style type="text/css">
{$custom_css}

div.header { background-image: url('uploads/images/orange.png') !important; }
</style>

{literal}
<script type="text/javascript">
		window.cookieconsent_options = {"message":"{/literal}{$translations.cookie.message}{literal}","dismiss":"{/literal}{$translations.cookie.accept_message}{literal}","learnMore":"{/literal}{$translations.cookie.more_info}{literal}","link":"/privacy-policy","theme":"light-bottom"};
</script>
{/literal}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>

</head>

<body>


<div id="wrapper">

<!-- //////////////// NAVBAR-UR ///////////// -->

<header role="banner" class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
				</div>
				<div class="side-collapse in">
					<nav role="navigation" class="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="{$BASE_URL}{$URL_JOBS}" >Jobs</a></li>
							<li><a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_POST}" >Post a job</a></li>
							{if $PROFILES_PLUGIN == 'true'}
							<li><a href="{$BASE_URL}{$URL_REGISTER_APPLICANTS}" >UPLOAD CV +</a></li>
							{/if}
							<li><a id="mob-menu-sign-in" data-toggle="dropdown" href="#">{$translations.website_general.sign_in} &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
			                  <ul class="dropdown-menu" role="menu" aria-labelledby="mob-menu-sign-in">
			                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{$BASE_URL}{$URL_LOGIN_RECRUITERS}"><!-- {$translations.website_general.top_menu_recruiters} -->client</a></li>

			                     {if $PROFILES_PLUGIN == 'true' and $jobs_candidates_on_flag == '1'}
			                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{$BASE_URL}{$URL_LOGIN_APPLICANTS}"><!--{$translations.website_general.top_menu_applicants}-->job seeker</a></li>
			                    {/if}

			                  </ul>
			                </li>
							<li><a href="{$BASE_URL}{$ABOUT_URL}" >About</a></li>
							<li><a href="{$BASE_URL}{$URL_SIGN_UP}">Signup</a></li>
						</ul>
					</nav>
				</div>
			</div>
</header> <!-- main-menu-->

<div class="header">
	<div class="navbar-ur-lp">
		<div class="container-fluid container-fluid_bg">
			<div class="row">
				<div class="col-md-6 col-xs-12 c-header-left">
					<a href="{$BASE_URL}"><img class="site-logo" src="{$BASE_URL}{$SITE_LOGO_PATH}" style="width: 243px;margin: 19px 8% 0% 5px;" alt="Desert Dental Staffing"></a>
				</div>
				<div class="col-md-6 col-xs-12 c-header-right">
					<ul>
						<li><a href="{$BASE_URL}{$URL_JOBS}" >Jobs board</a></li>
						<!--<li><a href="{$BASE_URL}{$URL_DASHBOARD}/{$URL_DASHBOARD_POST}" >Post a job</a></li>-->
						{if $PROFILES_PLUGIN == 'true'}
						{if !$SESSION_APPLICANT}
							{* <li><a href="{$BASE_URL}{$URL_REGISTER_APPLICANTS}" >UPLOAD CV +</a></li> *}
						{/if}
						{/if}
						{* <li><a href="{$BASE_URL}{$ABOUT_URL}" >About</a></li> *}
						{if !$SESSION_APPLICANT}
							<li>
                				<a id="menu2" data-toggle="dropdown" id="sign-in-btn">{$translations.website_general.sign_in} &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                  					<ul class="dropdown-menu" role="menu" aria-labelledby="menu2" style="width: auto;
																text-align: center;
																left: 65%;
																margin-top: 2px;">
                   					 <li class="r-m" role="presentation"><a role="menuitem" tabindex="-1" href="{$BASE_URL}{$URL_LOGIN_RECRUITERS}"><!-- {$translations.website_general.top_menu_recruiters} -->client</a></li>

                   					 {if $PROFILES_PLUGIN == 'true' and $jobs_candidates_on_flag == '1'}
                   					 <li class="r-m" role="presentation"><a role="menuitem" tabindex="-1" href="{$BASE_URL}{$URL_LOGIN_APPLICANTS}"><!--{$translations.website_general.top_menu_applicants}-->job seeker</a></li>
                    				{/if}

                  					</ul>
                			</li>

                			<!-- COMMENTED -->

							<!-- <li><a href="{$BASE_URL}{$URL_SIGN_UP}"  class="sign-up-btn">Registration</a></li> -->

							<!-- ENDPOINT -->

						{/if}
						{if $SESSION_APPLICANT}
							<li><a href="{$BASE_URL}{$URL_LOGIN_APPLICANTS}">My Account</a></li>
						{/if}
					</ul>
				</div>
			</div>
		</div>
	</div>
  <div class="header-content">
    <div class="container">
      <div class="row">
        <!-- <h1>{$customizer_data.header_title}</h1>
        <h2>{$customizer_data.header_subtitle}</h2> -->
        <div class="row header-filter">
          <div class="col-md-12 col-xs-12">

            <form method="post" action="{$BASE_URL}{$URL_LANDING_SEARCHED}" role="form">

              <div class="hf">

              			<!-- COMMENTED ORIG. -->

<!-- 							<div class="col-md-4">
								<div class="form-group position">
									<label>{$customizer_data.header_what_title}</label> -->
									<!-- <input placeholder="{$customizer_data.header_what_placeholder}" id="landing_title" name="landing_title" type="text" class="form-control"> -->
									<!-- <select id="landing_title" name="landing_title">
										  <option value="Hygienist_in_Arizona">Hygienist in Arizona</option>
								          <option value="Dental_Assistant_in_Arizona">Dental Assistant in Arizona</option>
										  <option value="EFTA_Certified_Assistant_in_Arizona">EFTA Certified Assistant in Arizona</option>
										  <option value="Front_Office">Front Office</option>
										  <option value="Crostrained_(Front_Office/Dental_Assistant)_in_Arizona">Crostrained (Front Office/Dental Assistant) in Arizona</option>
										  <option value="Dentist_in_Arizona">Dentist in Arizona</option>
									</select>
								</div>
							</div>

								{if $REMOTE_PORTAL == 'deactivated'}
							 <div class="col-md-4">
								<div class="form-group location">
									<label>{$customizer_data.header_where_title}</label>
									 <form>
										 <select id="landing_location" name="landing_location">
												{foreach from=$cities key=id item=value}
												<option value="{$id}">{$value}</option>
												{/foreach}
										 </select>   
									 </form>

								</div>
						</div>
								{/if}
								<div class="col-md-4">
									<button type="submit" class="btn btn-subc">{$customizer_data.header_search_btn_title}</button>
							</div> -->

						<!-- ENDPOINT -->

<!-- 						<style type="text/css">
							
							.col-centered{
    float: none;
    margin: 0 auto;
}

						</style> -->


						<div class="col-md-6">

						<a href="{$BASE_URL}{$URL_SIGN_UP}" class="btn btn-subc">Registration</a>

					</div>

						<div class="col-md-6">

						<a href="{$BASE_URL}{$URL_JOBS}" class="btn btn-subc">Jobs Board</a>

					</div>



							</div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div> <!-- header-->

<!-- ///////////// MAIN-CONTENT ///////////// -->

{if $more_jobs}
      <div id="candidates">
            <h2>{$translations.alljobs.all_jobs}</h2>
      </div>

      {include file="snippets/homepage-jobs.tpl"}
      <div class="col-md-12 col-xs-12">
        <a href="{$BASE_URL}{$URL_JOBS}" ><button type="button" class="btn btn-subc btn-home-jobs">{$translations.companies.view_jobs}</button></a>
      </div>
{/if}

{if $customizer_data.listings_on_flag == '1'}
{include file="snippets/listing-sitemap.tpl"}
{/if}

<script src="{$BASE_URL}_tpl/{$THEME}/1.5/js/jquery.min.js"></script>
<script src="{$BASE_URL}_tpl/{$THEME}/1.5/js/bootstrap.min.js"></script>


<script src="{$BASE_URL}js/landing/landing.js"></script>

{literal}
<script type="text/javascript">
	$('.counter').each(function() {

  var $this = $(this),
      countTo = $this.attr('data-count');

  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {
    duration: 10000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
    }

  });  
});
</script>
{/literal}

{include file="1.5/layout/sjs-footer.tpl"}
</body>
</html>