<!-- <div class="row">
<div class="col-md-12 col-xs-12">
<div class="top-search">
	 <form id="search_form" class="search-form" role="search" method="post" action="{$BASE_URL}search/" autocomplete="on" onsubmit="return false;" >
		<input type="text" class="form-control" name="keywords" id="keywords" placeholder="{$translations.searchbar.input_text}">		
		<select id="search_op" name="search_op" class="all_job_search">
			<option value="Dental_Assistant">Dental Assistant</option>
			<option value="FO">FO</option>
			<option value="Doctor_of_Dental_Surgery">Doctor of Dental Surgery</option>
			<option value="Registered_Dental_Hydrant">Registered Dental Hydrant</option>
		</select>
		<input type="button" id="search" name="search" class="btn" value="Search">
	</form>
</div>
</div>
</div> -->
	<div class="header-content">
		<div class="container">
			<div class="row">
				<div class="row header-filter alljob">
						<form method="post" action="{$BASE_URL}{$URL_LANDING_SEARCHED}" role="form">
							<div class="hf alljobhf" style="margin-top: -222px;">
								<div class="col-md-4">
									<div class="form-group position">
										<label style="float: left;">What</label>
									<select id="landing_title" name="landing_title">
										  <option value="Hygienist_in_Arizona">Hygienist in Arizona</option>
								          <option value="Dental_Assistant_in_Arizona">Dental Assistant in Arizona</option>
										  <option value="EFTA_Certified_Assistant_in_Arizona">EFTA Certified Assistant in Arizona</option>
										  <option value="Front_Office">Front Office</option>
										  <option value="Crostrained_(Front_Office/Dental_Assistant)_in_Arizona">Crostrained (Front Office/Dental Assistant) in Arizona</option>
										  <option value="Dentist_in_Arizona">Dentist in Arizona</option>
									</select>
								</div>
							</div>
						<div class="col-md-4">
					<div class="form-group location">
						<label style="float: left;">Where</label>
							<form>
										 <select id="landing_location" name="landing_location">
												<option value="74">East Valley</option>
												<option value="89">General Phoenix</option>
												<option value="86">North Phoenix</option>
												<option value="88">North Arizona</option>
												<option value="85">Scottsdale</option>	
												<option value="87">Tucson</option>
												<option value="84">West Valley</option>
										 </select>   
									 </form>
								</div>
							</div>
						<div class="col-md-4">
					<button type="submit" class="btn btn-subc">Search</button>
				</div>
			</div>
		</form>
	</div>
		</div>
			</div>
	</div>