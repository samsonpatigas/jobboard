<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:27:41
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/1.5/layout/dashboard-snippets.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fd8d8e6ea3_38614330',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f98a47941de628bccb601ff783b20ae9091c27b7' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/1.5/layout/dashboard-snippets.tpl',
      1 => 1539093756,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31fd8d8e6ea3_38614330 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--[if !IE]><?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/jquery.history.js" type="text/javascript"><?php echo '</script'; ?>
><![endif]-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/jquery.form.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/functions.js" type="text/javascript"><?php echo '</script'; ?>
>



	<?php if ($_smarty_tpl->tpl_vars['LOAD_TAGL']->value == 'true') {?>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/assets/js/jquery-ui.js" type="text/javascript"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/assets/js/rainbow-custom.min.js" type="text/javascript"><?php echo '</script'; ?>
>
		<!-- <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/src/taggle-ie8.js" type="text/javascript"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/src/taggle-ie9.js" type="text/javascript"><?php echo '</script'; ?>
> -->
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/src/taggle.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['TAGL_INIT_CVDB']->value == 'true') {?>
		<?php echo '<script'; ?>
 type="text/javascript">
		
			var searched_tags = [];
			
			   <?php if ($_smarty_tpl->tpl_vars['searched_skills']->value) {?>
				   	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['searched_skills']->value, 'tag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
?>
				   	
				   	searched_tags.push('<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
');
				    
				   	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			   <?php }?>
			

		   new Taggle($('.cvDbTaggle.textarea')[0], {
		   		tags: searched_tags,
			    duplicateTagClass: 'bounce'
			});
	    
		<?php echo '</script'; ?>
>
	<?php }?>

	<?php echo '<script'; ?>
 type="text/javascript">
	
 		$(document).ready(function()
		{

			var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
			SimpleJobScript.simplejobscript_url = "<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
";
			SimpleJobScript.I18n = <?php echo $_smarty_tpl->tpl_vars['translationsJson']->value;?>
;
			SimpleJobScript.InitEditor(theme);

			var sideslider = $('[data-toggle=collapse-side]');
            var sel = sideslider.attr('data-target');
            var sel2 = sideslider.attr('data-target-2');
            sideslider.click(function(event){
                $(sel).toggleClass('in');
                $(sel2).toggleClass('out');
            });
            
			
			<?php if ($_smarty_tpl->tpl_vars['LOAD_TAGL']->value == 'true') {?>
			
				SimpleJobScript.translateTaggle();
			
			<?php }?>
			
				
			var flag = parseInt("<?php echo $_smarty_tpl->tpl_vars['APP_PAGE']->value;?>
");
			if (flag == 1){
				SimpleJobScript.initJobApplicationsSelect(theme);
			}

			$('#<?php echo $_smarty_tpl->tpl_vars['JS_ID']->value;?>
').addClass('active');

		});
	
	<?php echo '</script'; ?>
>

	<?php if (@constant('GOOGLE_ANALYTICS_CODE')) {?>
	<?php echo '<script'; ?>
 type="text/javascript">
	
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php echo @constant('GOOGLE_ANALYTICS_CODE');?>
']);
		_gaq.push(['_trackPageview']);
		(function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	
	<?php echo '</script'; ?>
>
	<?php }
}
}
