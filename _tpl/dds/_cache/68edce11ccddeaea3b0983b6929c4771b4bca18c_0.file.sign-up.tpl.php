<?php
/* Smarty version 3.1.30, created on 2019-07-18 18:18:02
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/sign-up.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30a9ca758d02_02573195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68edce11ccddeaea3b0983b6929c4771b4bca18c' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/sign-up.tpl',
      1 => 1556198465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5d30a9ca758d02_02573195 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style type="text/css">

@media (min-width: 992px) and (max-width: 1200px) {
	.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
		min-height: 0!important;
	}
}


</style>

<div class="main-content signup-page">
	<div class="adjusted-signup-page" style="min-height: 550px; ">
		<div class="container">
			<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['sign_up']['headline'];?>
</h2>
			<div class="row">
				<div class="col-md-2 .hide-sm"></div>

				<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true' && $_smarty_tpl->tpl_vars['jobs_candidates_on_flag']->value == '1') {?>
				<div class="col-lg-4 col-md-6 col-xs-12 candidate">
					<img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/default/1.5/images/candidate.png">
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_BUFFER_APPLICANTS']->value;?>
" class="su-emp"><!--<?php echo $_smarty_tpl->tpl_vars['translations']->value['sign_up']['candidates_registration'];?>
-->JOB SEEKER REGISTRATION</a>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
" class="si-emp"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['sign_in'];?>
</a>
				</div>
				<?php }?>


				<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true' && $_smarty_tpl->tpl_vars['jobs_candidates_on_flag']->value == '1') {?>
				<div class="col-lg-4 col-md-6 col-xs-12  employer">
		
				
					<img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/default/1.5/images/employer.png">
					<!-- ADDED UPDATE ORIG -->

<!-- 						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_BUFFER_DOCTORS']->value;?>
" class="su-emp">CLIENT REGISTRATION
						</a> -->

						<!-- END -->

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_RECRUITERS']->value;?>
" class="su-emp">CLIENT REGISTRATION
						</a>

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_RECRUITERS']->value;?>
" class="si-emp">
							<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['sign_in'];?>

						</a>
				</div>
				<?php }?>

				<div class="col-md-2 .hide-sm"></div>

			</div>
		</div>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
