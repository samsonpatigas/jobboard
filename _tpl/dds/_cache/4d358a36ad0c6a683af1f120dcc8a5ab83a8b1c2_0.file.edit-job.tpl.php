<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:27:41
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/edit-job.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fd8d875ad4_40650438',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4d358a36ad0c6a683af1f120dcc8a5ab83a8b1c2' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/edit-job.tpl',
      1 => 1563557100,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31fd8d875ad4_40650438 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row board ml0 pl0">

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['editjob_headline'];?>
</h2>
    <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['editjob_about_tag'];?>
</p>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_JOBS']->value;?>
"><button type="button" class="btn btn-green tabletmt3p deskFr" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</button></a>
  </div>
</div>

<br /><br />

<div class="job-process">
  <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB_EDITED']->value;?>
" role="form">
    <input type="hidden" id="job_id" name="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" />

    <div class="col-sm-12 col-xs-12" >

      <div class="detail-font" >
        <div>
          Job ID: <strong class="process-heading"> <?php echo $_smarty_tpl->tpl_vars['job']->value['id']+10000;?>
 </strong>
        </div>
        <div>
          Created: <strong class="process-heading"><?php echo $_smarty_tpl->tpl_vars['job']->value['created_on'];?>
</strong>
        </div>
        <div>
          Status: 
          <?php if ($_smarty_tpl->tpl_vars['job']->value['is_active'] == 1) {?>
            <strong class="process-heading">Active</strong>
          <?php } else { ?>
            <strong class="process-heading">Inactive</strong>
          <?php }?>
        </div>        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_type_label'];?>
</h3>
          </div>
          <div class="col-md-5 col-xs-12">                
            <select id="type_select" name="type_select">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['types']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <option <?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['job']->value['type_name']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_category_label'];?>
</h3>
          </div>
          <div class="col-md-5 col-xs-12">                
            <select id="cat_select" name="cat_select">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cats']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['category_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </select>
          </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['remote_portal']->value == 'deactivated') {?>
          <div class="row checkboxes">
            <div class="col-md-3 col-xs-12">
              <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_location_label'];?>
</h3>
            </div>
            <div class="col-md-5 col-xs-12">                
              <select id="location_select" name="location_select">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                  <option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['city_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

              </select>
            </div>
          </div>
        <?php }?>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Date posted:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input type="date" id="f9_date_posted" name="f9_date_posted" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_date_posted'];?>
" class="job-title">
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Job title:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="title" name="title">
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['title'] == 'Dental Assistant') {?> selected="selected"<?php }?> value="Dental Assistant">Dental Assistant</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['title'] == 'Hygienist') {?> selected="selected"<?php }?> value="Hygienist">Hygienist</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['title'] == 'Front Office') {?> selected="selected"<?php }?> value="Front Office">Front Office</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['title'] == 'Dentist') {?> selected="selected"<?php }?> value="Dentist">Dentist</option>
            </select>      
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Positions:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_position" name="f9_position" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_position'];?>
">
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Hygienist') {?> selected <?php }?> value="Hygienist">Hygienist</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Dental Assistant') {?> selected <?php }?> value="Dental Assistant">Dental Assistant</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Front Office') {?> selected <?php }?> value="Front Office">Front Office</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Cross-trained (Front Office/Dental Assistant)') {?> selected <?php }?> value="Cross-trained (Front Office/Dental Assistant)">Cross-trained (Front Office/Dental Assistant)</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position'] == 'Dentist') {?> selected <?php }?> value="Dentist">Dentist</option>
            </select>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Practice Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_practice_type" name="f9_practice_type" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_practice_type'];?>
">
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'General') {?> selected="selected"<?php }?> value="General">General</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Ortho') {?> selected="selected"<?php }?> value="Ortho">Ortho</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Perio') {?> selected="selected"<?php }?> value="Perio">Perio</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_practice_type'] == 'Oral Surgery') {?> selected="selected"<?php }?> value="Oral Surgery">Oral Surgery</option>
            </select>            
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">City:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_city" id="f9_city" maxlength="400" type="text" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_city'];?>
" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">State:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_state" id="f9_state" maxlength="400" type="text" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_state'];?>
" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Zip code:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_zip" id="f9_zip" maxlength="400" type="text" " value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_zip'];?>
" class="job-title"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_label'];?>
</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input name="salary" class="job-title" id="salary" type="text" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
"  />
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Years of experience:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_yrs_of_experience" name="f9_yrs_of_experience">
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == 'New Graduate') {?>selected="selected"<?php }?> value="New Graduate">New Graduate</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '6 Months – 2 Years') {?>selected="selected"<?php }?> value="6 Months – 2 Years">6 Months – 2 Years</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '3 – 5 Years') {?>selected="selected"<?php }?> value="3 – 5 Years">3 – 5 Years</option>
              <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_yrs_of_experience'] == '6 Years Plus') {?>selected="selected"<?php }?> value="6 Years Plus">6 Years Plus</option>
            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Language:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <input required name="f9_language" id="f9_language" maxlength="400" type="text" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_language'];?>
" class="job-title"  />
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Gender:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_gender" name="f9_gender" value="$job.f9_gender">
               <option value="Male" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_gender'] == "Male") {?> selected <?php }?>>Male</option>
               <option value="Female" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_gender'] == "Female") {?> selected <?php }?>>Female</option>
            </select>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Specialties:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_specialties" name="f9_specialties" value="$job.f9_specialties">
               <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == "Dental Assistant") {?> selected <?php }?> value="Dental Assistant">Dental Assistant (DA)</option>
               <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == "FO") {?> selected <?php }?> value="FO">(FO)</option>
               <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == "Doctor of Dental Surgery") {?> selected <?php }?> value="Doctor of Dental Surgery">Doctor of Dental Surgery (DDS)</option>
               <option <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_specialties'] == "Registered Dental Hydrant") {?> selected <?php }?> value="Registered Dental Hydrant">Registered Dental Hydrant (RDH)</option>
            </select>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Position Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Full Time" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position_type'] == 'Permanent Full Time') {
echo 'checked="checked"';
}?> >Permanent Full Time</label>
            <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Part Time" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_position_type'] == 'Permanent Part Time') {
echo 'checked="checked"';
}?>>Permanent Part Time</label>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Service Type:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="padding-bottom: 10px;"><input <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_service_type'] == 'Desert Dental Staffing Job Board Posting') {?> checked="checked" <?php }?> type="radio" name="f9_service_type" id="f9_service_type" value="Desert Dental Staffing Job Board Posting">Desert Dental Staffing Job Board Posting</label>
            <label style="padding-bottom: 10px;"><input <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_service_type'] == 'Concierge Service') {?> checked="checked" <?php }?> type="radio" name="f9_service_type" id="f9_service_type" value="Concierge Service">Concierge Service</label>
          </div>
        </div>
        
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Post period:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <select id="f9_post_peroid" name="f9_post_peroid" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'];?>
">
              <option value="30" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == "30") {?> selected <?php }?>>(30 days)</option>
              <option value="60" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == "60") {?> selected <?php }?>>(60 days)</option>
              <option value="90" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'] == "90") {?> selected <?php }?>>(90 days)</option>
            </select>
          </div>
        </div>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Spanish Bilingual:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="Yes" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_bilingual'] == "Yes") {?> checked <?php }?>>Yes</label>
            <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="No" <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_bilingual'] == "No") {?> checked <?php }?>>No</label>
          </div>
        </div>

        <?php $_smarty_tpl->_assignInScope('askills', explode(",",$_smarty_tpl->tpl_vars['job']->value['f9_skills']));
?>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Skills:</h3>
          </div>
          <div class="col-md-8 col-xs-12">
            <p>TIP:  Select only the ABSOLUTE requirements needed for the position to maximize the number of resumes received</p>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Custom Temps',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>              
              <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Custom Temps</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Cerec System',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Cerec System</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Surgical Implants',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Surgical Implants</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('X-ray Certified in AZ',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">X-ray Certified in AZ</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Coronal Polish Certified in AZ',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Coronal Polish Certified in AZ</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('IV Sedation',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">IV Sedation</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Insurance Processing',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Insurance Processing</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Treatment Presentation',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Treatment Presentation</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Anesthesia Certified',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Anesthesia Certified</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('AR',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">AR</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('Laser Certified',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">Laser Certified</h4>
            </label>
            <?php $_smarty_tpl->_assignInScope('key', array_search('EFDA (Certified Assistant in Arizona)',$_smarty_tpl->tpl_vars['askills']->value));
?>
            <label>
              <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>/>
              <h4 style="line-height: 2;">EFDA (Certified Assistant in Arizona)</h4>                  
            </label>
          </div>      
        </div>

        
        <?php $_smarty_tpl->_assignInScope('aofficesoftware', explode(",",$_smarty_tpl->tpl_vars['job']->value['f9_office_software']));
?>
        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Software Experience:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
              <?php $_smarty_tpl->_assignInScope('key', array_search('Dentrix',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
              <label>
                <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>>
                <h4 style="padding-top: 5px;">Dentrix</h4>  
              </label>
              <?php $_smarty_tpl->_assignInScope('key', array_search('EagleSoft',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
              <label>
                <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>>
                <h4 style="padding-top: 5px;">EagleSoft</h4>
              </label>
              <?php $_smarty_tpl->_assignInScope('key', array_search('Open Dental',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
              <label>
                <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>> 
                <h4 style="padding-top: 5px;">Open Dental</h4>
              </label>
              <?php $_smarty_tpl->_assignInScope('key', array_search('SoftDent',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
              <label>
                <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>> 
                <h4 style="padding-top: 5px;">SoftDent</h4>
              </label>
              <?php $_smarty_tpl->_assignInScope('key', array_search('Dentimax',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
              <label>
                <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?> checked <?php }?>> 
                <h4 style="padding-top: 5px;">Dentimax</h4>
              </label>
              <label>
                <input name="f9_office_software[]" value="Others" class="checkbox-custom" type="checkbox"> 
                <h4 style="padding-top: 5px;">Others</h4>
              </label>
          </div>
        </div>

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Expiry Date:</h3>
          </div>
          <div class="col-md-5 col-xs-12">
            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
              <span style="<?php if ($_smarty_tpl->tpl_vars['job']->value['expires'] < time()) {?>color:red<?php }?>">
                <?php echo date("m/d/Y",$_smarty_tpl->tpl_vars['job']->value['expires']);?>

              </span>
            </div>
          </div>
        </div>
              

        <div class="row checkboxes">
          <div class="col-md-3 col-xs-12">
            <h3 class="process-heading">Renew:</h3>
          </div>
        
          <div class="col-md-5 col-xs-12">
            <select value="<?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['job']->value['expires']);?>
" id="expires" name="expires" <?php if ($_smarty_tpl->tpl_vars['job']->value['expires'] > time()) {?>disabled="disabled"<?php }?>>
              <option value="<?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['job']->value['expires']);?>
" selected >Select Number of Days</option>
              <option value="<?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['job']->value['expires']+(30*24*60*60));?>
">(30 days)</option>
              <option value="<?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['job']->value['expires']+(60*24*60*60));?>
">(60 days)</option>
              <option value="<?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['job']->value['expires']+(90*24*60*60));?>
">(90 days)</option>
            </select>
          </div> 
        </div>

        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Short Description</h3>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="description" name="description"  value="<?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
" class="process-textarea" rows="12" cols="1"><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
          </div>
        </div>
        <br />

        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Admin Notes</h3>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="f9_admin_notes" name="f9_admin_notes" class="process-textarea" rows="12" cols="1"><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_admin_notes'];?>
</textarea>
          </div>
        </div>
        <br />
        <div class="row checkboxes">
          <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="process-heading mb15">Position Notes</h3>
            <p>Enter additional information about the position.  Text entered here will be displayed in the Job Board posting.</p>
          </div>
          <div class="col-md-12 col-xs-12">
            <textarea id="f9_position_notes" name="f9_position_notes" class="process-textarea" rows="12" cols="1"><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_position_notes'];?>
</textarea>
          </div>
        </div>
        <br/>
        <div class="acceptance">
          <label>
            <input class="checkbox-custom" type="checkbox" onchange="SimpleJobScript.applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?>checked<?php }?> />
            <h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_label'];?>
<span class="apply-desc-span" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_desc'];?>
</span></h4>
          </label>
        </div>
      
        <div id="apply-desc-block" class="row checkboxes <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?> displayNone<?php }?>">
          <div class="col-md-12 col-xs-12 mtm20" >
            <h3 class="process-heading green"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['howto_apply_label'];?>
</h3>    
            <input value="http://<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
" id="howtoTA" class="process-textarea" id="howtoapply" name="howtoapply">
          </div>
        </div>
      </div>

      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt25 mb50">
        <button type="submit" onclick="return SimpleJobScript.validateDesc();" class="btn mbtn fl" name="submit" id="submit" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['text_save'];?>
</button>
      </div>
    </div>
  </form>
</div>


  <?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready( function() {
      $("input[name='f9_service_type']").change(function(){
          selected_value = $("#f9_service_type:checked").val();
          if(selected_value == 'Concierge Service'){
            $('#f9_post_peroid').append($('<option>', {
              value: 3650,
              text: 'No Expiry'
            }));
            $('option[value="3650"]').prop("selected", true);
          }else{
            $("#f9_post_peroid option[value='3650']").remove();
          }
      });


    });
  <?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 type="text/javascript">
    <?php if ($_smarty_tpl->tpl_vars['job']->value['expires'] > time()) {?>
      $('#expires').find(":selected").text('Job has not expired');
    <?php }?>
  <?php echo '</script'; ?>
><?php }
}
