<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:28:01
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/renew-modal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fda1475ef7_63493795',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6bb1e9280ffa6e7cbb390e11cf004536c597c8a' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/renew-modal.tpl',
      1 => 1539626276,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31fda1475ef7_63493795 (Smarty_Internal_Template $_smarty_tpl) {
$__section_index_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_index']) ? $_smarty_tpl->tpl_vars['__smarty_section_index'] : false;
$__section_index_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['dash_jobs']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_index_1_total = $__section_index_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_index'] = new Smarty_Variable(array());
if ($__section_index_1_total != 0) {
for ($__section_index_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index'] = 0; $__section_index_1_iteration <= $__section_index_1_total; $__section_index_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index']++){
?>
  <div class="modal fade" id="renewModal_<?php echo $_smarty_tpl->tpl_vars['dash_jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_index']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index'] : null)]['id'];?>
" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%; margin-left: 0px;">
        <!--<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body">
          <p>Do you want to renew this listing?</p>
          <div class="job-process">
            <div class="detail-font" >
              <div class="row checkboxes" style="margin-top: 10px;">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Post period</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_post_peroid" name="f9_post_peroid" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['f9_post_peroid'];?>
">
                    <option value="30 days">(30 days)</option>
                    <option value="60 days">(60 days)</option>
                    <option value="90 days">(90 days)</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
    
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">YES I DO</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
        </div>
      </div>
      
    </div>
  </div>
<?php
}
}
if ($__section_index_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_index'] = $__section_index_1_saved;
}
}
}
