<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:44:26
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/edit-company.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30da2a424776_37533464',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82129e1815c8022f5ab741deb2981068d1eec4e9' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/edit-company.tpl',
      1 => 1563297178,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30da2a424776_37533464 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
  .rcorner{
    margin-bottom:20px;
    padding: 10px;
    min-height: 40px;
    border-color: gainsboro;
    border-width: 1px;
    border-style: solid;
    border-radius: 5px;
  }

  #editCompanyLabel:hover {
    background-color: #7527a0;
      color: #ffffff;
      border: solid 1px #ffffff;
      cursor: pointer;
  }

</style>

<div class="row board">
  <h2>Edit My Practice</h2>
  <p style="display: none"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['edit_company_note'];?>
</p>
</div>
<br /><br />

<div class="dash-form mlm20">
  <form id="register-form" name="register-form" method="post" action="/dashboard-company" role="form" enctype="multipart/form-data">
    <input type="hidden" id="employer_id" name="employer_id" value="<?php echo $_smarty_tpl->tpl_vars['ID']->value;?>
" />
    <input type="hidden" id="oldlogo-path" name="oldlogo-path" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['logo_path'];?>
" />
    <input type="hidden" id="oldPP" name="oldPP" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['profile_picture'];?>
" />

    <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 mlReset">

      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_name'];?>
</label>
      <input required name="company_name" id="company_name" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['name'];?>
" maxlength="300" type="text" class="form-control minput rcorner"  />

      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_type'];?>
</label>
      <?php $_smarty_tpl->_assignInScope('awords', explode(",",$_smarty_tpl->tpl_vars['company']->value['f9_practice_type']));
?> 

      <div style="clear:both">
        <div>
          <?php $_smarty_tpl->_assignInScope('key', array_search('General',$_smarty_tpl->tpl_vars['awords']->value));
?>
          <label style="width:200px;">
            <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
              " name="f9_practice_type[]" id="f9_practice_type" value="General" 
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>
              /> General
          </label>
        </div>
        <div>
          <?php $_smarty_tpl->_assignInScope('key', array_search('Perio',$_smarty_tpl->tpl_vars['awords']->value));
?>
          <label style="width: 200px;">
            <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
              " name="f9_practice_type[]" value="Perio" 
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>
              /> Perio
          </label>
        </div>
        <div>
          <?php $_smarty_tpl->_assignInScope('key', array_search('Endo',$_smarty_tpl->tpl_vars['awords']->value));
?>
          <label style="width: 200px;">
            <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
              " name="f9_practice_type[]" value="Endo" 
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>
              /> Endo
          </label>
        </div>
        <div>
          <?php $_smarty_tpl->_assignInScope('key', array_search('Ortho',$_smarty_tpl->tpl_vars['awords']->value));
?>
          <label style="width: 200px;">
            <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
              " name="f9_practice_type[]" value="Ortho" 
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>
              /> Ortho
          </label>
        </div>
        <div>
          <?php $_smarty_tpl->_assignInScope('key', array_search('Oral / Max Surgery',$_smarty_tpl->tpl_vars['awords']->value));
?>
          <label style="width: 200px;">
            <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
              " name="f9_practice_type[]" value="Oral / Max Surgery" 
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>
              /> Oral / Max Surgery
          </label>
        </div>
      </div>
      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_phone_number'];?>
</label>
      <input name="f9_practice_phone_number" id="f9_practice_phone_number" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_practice_phone_number'];?>
" type="text" maxlength="1000" class="input-phone form-control minput rcorner" />

      <!-- <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_cellphone_number'];?>
</label>
      <input name="f9_cellphone_number" id="f9_cellphone_number" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_cellphone_number'];?>
" type="text" maxlength="1000" class="form-control minput" />
 -->
      <!-- <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_cellphone_provider'];?>
</label>
      <input name="f9_cellphone_provider" id="f9_cellphone_provider" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_cellphone_provider'];?>
" type="text" maxlength="1000" class="form-control minput" />
 -->
      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_email_address'];?>
</label>
      <input name="f9_practice_email_address" id="f9_practice_email_address" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_practice_email_address'];?>
" type="email" maxlength="1000" class="form-control minput rcorner" />

      <label class="fw" for="">Job application email address</label>
      <input name="f9_job_app_email_address" id="f9_job_app_email_address" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_job_app_email_address'];?>
" type="email" maxlength="1000" class="form-control minput rcorner" />

      <label class="fw" for="">Street Address</label>
      <input name="f9_address_1" id="f9_address_1" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_address_1'];?>
" type="text" maxlength="1000" class="form-control minput rcorner" />

      <label class="fw" for="">Unit Number</label>
      <input name="f9_address_2" id="f9_address_2" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_address_2'];?>
" type="text" maxlength="1000" class="form-control minput rcorner" />

      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_city'];?>
</label>
      <input name="f9_city" id="f9_city" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_city'];?>
" type="text" maxlength="1000" class="form-control minput rcorner" />
        <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_state'];?>
</label>
        <select id="f9_state" name="f9_state" class="dropdown" style="
        -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
          -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
          padding: 10px;
          box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
          min-height: 47px;
          width: 60%;
          border-radius: 5px;
          padding: 0% 3% 0% 6%;
          border: none;
          float: left;
          margin-bottom: 7%;
          font-size: 14px;
          line-height: 3;
          background-color: white;
          ">
          <?php $_smarty_tpl->_assignInScope('states', array('Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming'));
?>

            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['states']->value, 'x');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['x']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['x']->value == $_smarty_tpl->tpl_vars['company']->value['f9_state']) {?>
              <?php $_smarty_tpl->_assignInScope('selected', 'selected');
?>
            <?php } else { ?>
              <?php $_smarty_tpl->_assignInScope('selected', '');
?>
            <?php }?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</option>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </select>

      <!-- ENDPOINT -->

      <label class="fw" for=""><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_zip'];?>
</label>
      <input name="f9_zip" id="f9_zip" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['f9_zip'];?>
" type="text" maxlength="5" class="form-control minput rcorner" />
      <div id="feedback-err_f9_zip" class="negative-feedback col-md-6 displayNone" style="margin-top: -5px;">Zip Code is Empty or Invalid</div>
      <div style="display: none;">
            <label class="fw" for="company_hq"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_hq_label'];?>
</label>
            <input  name="company_hq" id="company_hq" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['hq'];?>
" maxlength="400" type="text" class="form-control minput"  />

            <label class="fw" for="company_url"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_url_label'];?>
</label>
            <input  name="company_url" id="company_url" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['url'];?>
" maxlength="1000" type="text" class="form-control minput"  />

            <label class="fw" for="company_street"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_street_label'];?>
</label>
            <input  name="company_street" id="company_street" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['street'];?>
" maxlength="300" type="text" class="form-control minput"  />

            <label class="fw" for="company_citypostcode"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_citypostcode_label'];?>
</label>
            <input  name="company_citypostcode" id="company_citypostcode" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['city_postcode'];?>
" maxlength="300" type="text" class="form-control minput"  />
      </div>
    </div>

    <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 pl5p">
      <label>Office Software</label>
      <?php $_smarty_tpl->_assignInScope('aofficesoftware', explode(",",$_smarty_tpl->tpl_vars['company']->value['f9_office_software']));
?>
      <?php $_smarty_tpl->_assignInScope('arr_soft', explode(",",$_smarty_tpl->tpl_vars['company']->value['f9_office_software']));
?>    
      <?php $_smarty_tpl->_assignInScope('arr2_soft', array("Dentrix","EagleSoft","Open Dental","SoftDent","DentiMax"));
?>
      <?php $_smarty_tpl->_assignInScope('result', array_diff($_smarty_tpl->tpl_vars['arr_soft']->value,$_smarty_tpl->tpl_vars['arr2_soft']->value));
?>
      
      <div>
        <?php $_smarty_tpl->_assignInScope('key', array_search('Dentrix',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
        <label style="width:200px;">
          <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
            "
              <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
                checked
              <?php }?>             
            name="f9_office_software[]" value="Dentrix" /> Dentrix
        </label>
      </div>
      <div>
        <?php $_smarty_tpl->_assignInScope('key', array_search('EagleSoft',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
        <label style="width:200px;">
          <input type="checkbox" class="checkbox-custom" style="
            background-color: #FFFFFF;
            border: 2px solid#7527a0;
            padding: 0;
            border-radius: 0px;
            display: inline-block;
            position: relative;
            width: 25px!important;
            height: 25px;
            color: #7527a0!important;
            float: left;
            "
            <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
              checked
            <?php }?>             
            name="f9_office_software[]" value="EagleSoft" /> EagleSoft
        </label>
      </div>
      <div>
        <?php $_smarty_tpl->_assignInScope('key', array_search('Open Dental',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
        <label style="width:200px">
          <input type="checkbox" class="checkbox-custom" style="
            background-color: #FFFFFF;
            border: 2px solid#7527a0;
            padding: 0;
            border-radius: 0px;
            display: inline-block;
            position: relative;
            width: 25px!important;
            height: 25px;
            color: #7527a0!important;
            float: left;
            "
            <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
              checked
            <?php }?>             
            name="f9_office_software[]" value="Open Dental" /> Open Dental
        </label>
      </div>
      <div>
        <?php $_smarty_tpl->_assignInScope('key', array_search('SoftDent',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
        <label style="width:200px;">
          <input type="checkbox" class="checkbox-custom" style="
              background-color: #FFFFFF;
              border: 2px solid#7527a0;
              padding: 0;
              border-radius: 0px;
              display: inline-block;
              position: relative;
              width: 25px!important;
              height: 25px;
              color: #7527a0!important;
              float: left;
            " 
            <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
              checked
            <?php }?>
            name="f9_office_software[]" value="SoftDent" /> SoftDent
        </label>
      </div>
      <div>
        <?php $_smarty_tpl->_assignInScope('key', array_search('DentiMax',$_smarty_tpl->tpl_vars['aofficesoftware']->value));
?>
        <label style="width: 200px;">
          <input type="checkbox" class="checkbox-custom" style="
            background-color: #FFFFFF;
            border: 2px solid#7527a0;
            padding: 0;
            border-radius: 0px;
            display: inline-block;
            position: relative;
            width: 25px!important;
            height: 25px;
            color: #7527a0!important;
            float: left;
          "
          <?php if ($_smarty_tpl->tpl_vars['key']->value !== false) {?>
            checked
          <?php }?>          
          name="f9_office_software[]" value="DentiMax" /> DentiMax
        </label>
      </div>
      <div>
        <label style="width: 200px;">
          <input type="text" name="f9_office_software[]" placeholder="Others" value="<?php echo implode(",",$_smarty_tpl->tpl_vars['result']->value);?>
"
            style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
        </label>
      </div>
      <label>
        What are your major cross-streets
        <p>(Highly Recommended)</p>
      </label>
      <textarea name="f9_cross_st" id="f9_cross_st" rows="4" cols="50" style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" maxlength="500" placeholder="What are your major cross-streets?"><?php echo $_smarty_tpl->tpl_vars['company']->value['f9_cross_st'];?>
</textarea>
      <br/><br/>
      <label>Notes</label>
      <textarea id="f9_notes" name="f9_notes" style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;"><?php echo $_smarty_tpl->tpl_vars['company']->value['f9_note'];?>
</textarea>
      
      <div id="uploadPreview"><img src="/<?php echo $_smarty_tpl->tpl_vars['company']->value['logo_path'];?>
" style="width: 276px;height: 276px;" /></div>
      <div id="logo-err" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_msg'];?>
</div>
      <div id="logo-err2" class="negative-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_samesize'];?>
</div>
      <div id="logo-ok" class="positive-feedback displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_ok'];?>
</div>
      <label class="fw" id="editCompanyLabel" for="company_logo" style="width:276px!important; padding: 20px!important">Logo (Optional)</label>
      <input accept=".jpg,.png,.gif" type="file" name="company_logo" id="company_logo" class="form-control inputfile minput" style="padding: 10px;" onchange="uploadLogo(this)"/>
      <div class="textarea-feedback mb25" >Allowed extensions (jpg, png, gif)</div>

      <br /><br />
      <div class="dash-cb" style="display: none;">
        <label>
          <input <?php if ($_smarty_tpl->tpl_vars['company']->value['public_page'] == '1') {?>checked<?php }?> name="profile_switch" id="profile_switch" type="checkbox" class="checkbox-custom" onchange="SimpleJobScript.companyPublicProfileSwitched(this.checked);">
          <h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['profile_picture_label'];?>
</h4>
        </label>
      </div>

      <div style="display: none;">
             <div id="public-profile-block" class="mb50 form-group <?php if ($_smarty_tpl->tpl_vars['company']->value['public_page'] == '0') {?>displayNone<?php }?>" >
              <label id="ppLabel" for="pp_file"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['pp_label'];?>
</label>
              <input type="file" name="pp_file" id="pp_file" class="form-control inputfile minput" />
              <div class="textarea-feedback fw mb20" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['pp_hint'];?>
</div>
              <div id="uploadPreviewPP"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['company']->value['profile_picture'];?>
" /></div>
              <div id="pp-err" class="negative-feedback mt10 displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['pp_err'];?>
</div>
             </div>
      </div>

    </div>

    <div class="col-lg-11 col-sm-11 col-md-11 col-xs-11 mlReset" >
    <div style="display: none;">
          <label class="fw" for="company_desc"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_desc'];?>
</label>
      <textarea class="form-control" name="company_desc" id="company_desc" rows="10"><?php echo $_smarty_tpl->tpl_vars['company']->value['description'];?>
</textarea>
      <br /><br />
    </div>
    </div>

    <div class="col-lg-6 col-sm-8 col-md-6 col-xs-12 mb50" >
      <button type="submit" class="btn mbtn fl" name="submit" id="submit4" ><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
    </div>
    
  </form>
</div>

<!-- Modal -->
<div class="modal fade" id="contactsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://unpkg.com/axios/dist/axios.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave-phone.us.js"><?php echo '</script'; ?>
>

<!-- <?php echo '<script'; ?>
>
new Vue({
  delimiters: ['%%', '%%'],
  el: '#app',
  data () {
    return {
      info: null
    }
  },
  mounted () {
    axios
      .get('http://jobboard.f9portal.net/get_contacts?emp=<?php echo $_smarty_tpl->tpl_vars['ID']->value;?>
')
      .then(response => (this.info = response))
  }
})

<?php echo '</script'; ?>
> -->

<?php echo '<script'; ?>
 type="text/javascript">
  $(document).ready(function() {
	    setTimeout(function(){
	      //SimpleJobScript.initRegisterFormStep2Validation();
/*	       $('#company_logo').change(function() {
	       var fname = $('input#company_logo').val().split('\\').pop();
	       if( fname )
	        $('#editCompanyLabel').html(fname);
	       else
	        $('#editCompanyLabel').html($('#editCompanyLabel').html());
	           });*/

	       SimpleJobScript.initPP();

	       $('#pp_file').change(function() {
	       var profileFname = $('input#pp_file').val().split('\\').pop();
	       if( profileFname )
	        $('#ppLabel').html(profileFname);
	       else
	        $('#ppLabel').html($('#ppLabel').html());
	           });

	    }, 800);

      var cleave = new Cleave('.input-phone', {
        phone: true,
        phoneRegionCode: 'US'
      });

    	$('#f9_zip').blur(function(){
			if ($('#f9_zip').val().length !== 5) {
				$('#submit4').attr("disabled", true);
				$('#feedback-err_f9_zip').removeClass('displayNone');
			}else{
				$('#submit4').attr("disabled", false);
				$('#feedback-err_f9_zip').addClass('displayNone');
			}
		});
    
  });

  

  document.getElementById('f9_zip').addEventListener('input', function (e) {
    var x = e.target.value.replace(/[^0-9]/g, '')
    e.target.value = x;
  });
<?php echo '</script'; ?>
>

<!-- ADDED FOR FIXES -->

<?php echo '<script'; ?>
 type="text/javascript">
  
  

      function uploadLogo(){
      var logo = $('company_logo').val();
      console.log('logo upload');
      var form = document.getElementById('register-form');
      var formData = new FormData(form);

      var xhr = new XMLHttpRequest();

      xhr.open('POST', '/dashboard-company', true);
      xhr.send(formData);

      $("#uploadPreview").load(" #uploadPreview > *");

    }

<?php echo '</script'; ?>
>

<!-- END -->
<?php }
}
