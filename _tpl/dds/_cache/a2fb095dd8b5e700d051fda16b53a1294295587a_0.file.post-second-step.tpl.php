<?php
/* Smarty version 3.1.30, created on 2019-07-18 19:29:55
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post-second-step.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30baa32b2952_19945728',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2fb095dd8b5e700d051fda16b53a1294295587a' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post-second-step.tpl',
      1 => 1563204403,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30baa32b2952_19945728 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="row " id="step-2">

	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mlpl0">

		<div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['company'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['category_name'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['job_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['location_asci'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_date_posted'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <?php if ($_smarty_tpl->tpl_vars['job_data']->value['f9_post_peroid'] == '3650') {?>
                        <p>No Expiry</p>
                      <?php } else { ?>
                        <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_post_peroid'];?>
</p>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Service Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_service_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_gender'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_language'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Position</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_specialties'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_city'];?>
  <?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_state'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_zip'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['description'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_admin_notes'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Practice Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_practice_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Position Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Skills</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_skills'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Office Software</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_office_software'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Pay Range (Minimum)</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_pay_min'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Pay Range (Maximum)</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_pay_max'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Bilingual</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_bilingual'];?>
</p>
                    </div>
                  </div>
                </div>                
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Position Notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position_notes'];?>
</p>
                    </div>
                  </div>
                </div>                
              </div>
            </div>            
          </div>
		<?php if ($_smarty_tpl->tpl_vars['PAY']->value == 'true') {?>

				<div class="row mb20">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['adtype'];?>
</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">		
					<?php if ($_smarty_tpl->tpl_vars['SPOTLIGHT']->value == 'true' && $_smarty_tpl->tpl_vars['PREMIUM_LISTING_PRICE']->value > 0) {?>                						
            <?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['premium'];?>
 &nbsp;-&nbsp;&nbsp; <?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['premium_desc'];?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['standard'];?>

					<?php }?>
					</div>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['SPOTLIGHT']->value == 'true' && $_smarty_tpl->tpl_vars['PREMIUM_LISTING_PRICE']->value > 0) {?>
					<div class="row mb20">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['premium_ad_fee'];?>
</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						                                                                       	<?php echo $_smarty_tpl->tpl_vars['FORMATED_CURRENCY']->value;?>

						</div>
					</div>
				<?php }?>
				
					<div class="row mb20">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['job_post_fee'];?>
</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">	
						<?php if ($_smarty_tpl->tpl_vars['JOB_POSTED_PRICE']->value > 0) {?>                                             							
						                                                                       	<?php echo $_smarty_tpl->tpl_vars['FORMATED_CURRENCY_JOBPOSTED']->value;?>

						<?php } elseif ($_smarty_tpl->tpl_vars['JOB_POSTED_PRICE']->value == 0) {?>
							<?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['free'];?>

						<?php }?>
						</div>
					</div>

				<br />
				<hr />

					<div class="row mb12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['fees'];?>
</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  <?php echo $_smarty_tpl->tpl_vars['PRICE']->value;?>

						</div>
					</div>

        <?php if ($_smarty_tpl->tpl_vars['VAT']->value) {?>
					<div class="row mb12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['vat'];?>
</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  <?php echo $_smarty_tpl->tpl_vars['PRICE_VAT']->value;?>

						</div>
					</div>

					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['final_price'];?>
</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  <?php echo $_smarty_tpl->tpl_vars['PRICE_VAT_TOTAL']->value;?>

						</div>
					</div>
      <?php }?>

		<?php } else { ?>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['adtype'];?>
</h3>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
				  <?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['standard'];?>

				</div>
			</div>
			<br />
			<hr />

			<?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '3') {?>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step1']['account_plan'];?>
</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
					 <?php echo $_smarty_tpl->tpl_vars['jobs_left_msg']->value;?>

					</div>
				</div>
			<?php } else { ?>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['fees'];?>
</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
					 <?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['free'];?>

					</div>
				</div>
			<?php }?>
		<?php }?>

	</div>

<!--	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mlpl0">
    		<button type="button" data-toggle="modal" data-target="#previewModal" id="preview-btn-mobile" class="btn mbtn fl" style="margin-left: 40px;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['preview_btn_label'];?>
</button>
    	</div> -->

</div>
<br />

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB_POSTED']->value;?>
/" role="form">
	<input type="hidden" id="step" name="step" value="2" />
	<input type="hidden" id="employer_id" name="employer_id" value="<?php echo $_smarty_tpl->tpl_vars['job_data']->value['employer_id'];?>
" />
	<input type="hidden" id="job_id" name="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job_data']->value['id'];?>
" />

	<div class="button-holder">
		<?php if ($_smarty_tpl->tpl_vars['PAY']->value == 'true') {?>
			<input type="hidden" id="price" name="price" />
			<?php if ($_smarty_tpl->tpl_vars['SPOTLIGHT']->value == 'true' && $_smarty_tpl->tpl_vars['PREMIUM_LISTING_PRICE']->value > 0) {?>
				<input type="hidden" id="price_premium" name="price_premium" />
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['JOB_POSTED_PRICE']->value > 0) {?>
				<input type="hidden" id="price_ad" name="price_ad" />
			<?php }?>

			<input type="hidden" name="item_name" value="<?php echo $_smarty_tpl->tpl_vars['translations']->value['paypal']['checkout_item_name'];?>
" /> 
			<input type="hidden" name="item_number" value="<?php echo $_smarty_tpl->tpl_vars['translations']->value['paypal']['checkout_item_number'];?>
" /> 
		    <input type="hidden" name="item_desc" value="<?php echo $_smarty_tpl->tpl_vars['translations']->value['paypal']['checkout_item_desc'];?>
" /> 
			<input type="hidden" name="item_price" />
			<input type="hidden" name="item_qty" value="1" />

		<button type="submit" class="btn btn-green" id="paypal_submit" name="paypal_submit" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['paypal_btn_label'];?>
</button>

		<?php } else { ?>

			<button type="submit" class="btn btn-green" id="pblish" name="pblish" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['label_publish'];?>
</button>

		<?php }?>

			<button type="submit" class="btn btn-gray sec-btn" id="goback" name="goback" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['go_back_edit_text'];?>
</button>

	</div>

</form>
<?php }
}
