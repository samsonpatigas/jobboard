<?php
/* Smarty version 3.1.30, created on 2019-07-19 13:23:57
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31b65d01d8f9_97500531',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c1810550e6320cf15ec8aada6229731c3df76d0d' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/index.tpl',
      1 => 1559662830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/homepage-jobs.tpl' => 1,
    'file:snippets/listing-sitemap.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5d31b65d01d8f9_97500531 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<title>Desert Dental Staffing | Hot Jobs</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

		<meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
		<meta name="keywords" content="<?php if ($_smarty_tpl->tpl_vars['seo_keys']->value) {
echo $_smarty_tpl->tpl_vars['seo_keys']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_keywords']->value;
}?>" />

		<!-- 
	 Chrome and Opera Icons - to add support create your icon with different resolutions. Default is 192x192
		 <link rel="icon" sizes="192x192" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav.png" >
		<link rel="icon" sizes="144x144" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-144.png" >
		<link rel="icon" sizes="96x96" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-96.png" >
		<link rel="icon" sizes="48x48" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-48.png" >
	-->
	<link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav.png" >
	<!-- 
	 Apple iOS icons
		<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	-->
	<link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-ios.png">
	<!-- iOS startup image -->
	<link rel="apple-touch-startup-image"  href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-startup.png">
	<!-- Apple additional meta tags -->
	<meta name="apple-mobile-web-app-title" content="Desert Dental Staffing | Hot Jobs">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<!-- Android web app capable -->
	<meta name="mobile-web-app-capable" content="yes">
	<!-- IE & Windows phone pin icons. Almost same size like ios so it is reused -->
	<meta name="msapplication-square150x150logo" content="fav-ios.png">

	<!-- facebook meta tags for sharing -->
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Desert Dental Staffing | Hot Jobs" />
	<meta property="og:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
	<meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
" />
	<meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
" />
	<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />

	<!-- twitter metatags -->
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>"/>
	<meta name="twitter:title" content="Desert Dental Staffing | Hot Jobs"/>
	<!-- add your twitter site 
	<meta name="twitter:site" content="@brand"/>
	--> 
	<meta name="twitter:domain" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
"/>
	<meta name="twitter:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />
	
	<!-- RSS -->
	<link rel="alternate" type="application/rss+xml" title="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
 RSS Feed" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
rss">
 
	<link rel="canonical" href="http://<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
home" />

	<!-- IF ALL STYLES ARE REMOVED EXCEPT BOOTSTRAP, UNCOMMENT THIS STYLE, TO KEEP THE BASIC, WORKING WEB. READY TO BE CUSTOMIZED 
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/css/saveStructure.css" type="text/css" /> -->
	<!-- ######################################################################################################################## -->
	
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/style.css">
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/reset.css">
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/font-awesome.min.css">

	<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/dev.css" rel="stylesheet">

	<noscript><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['noscript_msg'];?>
</noscript>

<style type="text/css">
<?php echo $_smarty_tpl->tpl_vars['custom_css']->value;?>


div.header { background-image: url('uploads/images/orange.png') !important; }
</style>


<?php echo '<script'; ?>
 type="text/javascript">
		window.cookieconsent_options = {"message":"<?php echo $_smarty_tpl->tpl_vars['translations']->value['cookie']['message'];?>
","dismiss":"<?php echo $_smarty_tpl->tpl_vars['translations']->value['cookie']['accept_message'];?>
","learnMore":"<?php echo $_smarty_tpl->tpl_vars['translations']->value['cookie']['more_info'];?>
","link":"/privacy-policy","theme":"light-bottom"};
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"><?php echo '</script'; ?>
>

</head>

<body>


<div id="wrapper">

<!-- //////////////// NAVBAR-UR ///////////// -->

<header role="banner" class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
				</div>
				<div class="side-collapse in">
					<nav role="navigation" class="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
" >Jobs</a></li>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_POST']->value;?>
" >Post a job</a></li>
							<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_APPLICANTS']->value;?>
" >UPLOAD CV +</a></li>
							<?php }?>
							<li><a id="mob-menu-sign-in" data-toggle="dropdown" href="#"><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['sign_in'];?>
 &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
			                  <ul class="dropdown-menu" role="menu" aria-labelledby="mob-menu-sign-in">
			                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_RECRUITERS']->value;?>
"><!-- <?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['top_menu_recruiters'];?>
 -->client</a></li>

			                     <?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true' && $_smarty_tpl->tpl_vars['jobs_candidates_on_flag']->value == '1') {?>
			                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
"><!--<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['top_menu_applicants'];?>
-->job seeker</a></li>
			                    <?php }?>

			                  </ul>
			                </li>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['ABOUT_URL']->value;?>
" >About</a></li>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_SIGN_UP']->value;?>
">Signup</a></li>
						</ul>
					</nav>
				</div>
			</div>
</header> <!-- main-menu-->

<div class="header">
	<div class="navbar-ur-lp">
		<div class="container-fluid container-fluid_bg">
			<div class="row">
				<div class="col-md-6 col-xs-12 c-header-left">
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><img class="site-logo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SITE_LOGO_PATH']->value;?>
" style="width: 243px;margin: 19px 8% 0% 5px;" alt="Desert Dental Staffing"></a>
				</div>
				<div class="col-md-6 col-xs-12 c-header-right">
					<ul>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
" >Jobs board</a></li>
						<!--<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_POST']->value;?>
" >Post a job</a></li>-->
						<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true') {?>
						<?php if (!$_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
							
						<?php }?>
						<?php }?>
						
						<?php if (!$_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
							<li>
                				<a id="menu2" data-toggle="dropdown" id="sign-in-btn"><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['sign_in'];?>
 &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                  					<ul class="dropdown-menu" role="menu" aria-labelledby="menu2" style="width: auto;
																text-align: center;
																left: 65%;
																margin-top: 2px;">
                   					 <li class="r-m" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_RECRUITERS']->value;?>
"><!-- <?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['top_menu_recruiters'];?>
 -->client</a></li>

                   					 <?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true' && $_smarty_tpl->tpl_vars['jobs_candidates_on_flag']->value == '1') {?>
                   					 <li class="r-m" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
"><!--<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['top_menu_applicants'];?>
-->job seeker</a></li>
                    				<?php }?>

                  					</ul>
                			</li>

                			<!-- COMMENTED -->

							<!-- <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_SIGN_UP']->value;?>
"  class="sign-up-btn">Registration</a></li> -->

							<!-- ENDPOINT -->

						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
">My Account</a></li>
						<?php }?>
					</ul>
				</div>
			</div>
		</div>
	</div>
  <div class="header-content">
    <div class="container">
      <div class="row">
        <!-- <h1><?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_title'];?>
</h1>
        <h2><?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_subtitle'];?>
</h2> -->
        <div class="row header-filter">
          <div class="col-md-12 col-xs-12">

            <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LANDING_SEARCHED']->value;?>
" role="form">

              <div class="hf">

              			<!-- COMMENTED ORIG. -->

<!-- 							<div class="col-md-4">
								<div class="form-group position">
									<label><?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_what_title'];?>
</label> -->
									<!-- <input placeholder="<?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_what_placeholder'];?>
" id="landing_title" name="landing_title" type="text" class="form-control"> -->
									<!-- <select id="landing_title" name="landing_title">
										  <option value="Hygienist_in_Arizona">Hygienist in Arizona</option>
								          <option value="Dental_Assistant_in_Arizona">Dental Assistant in Arizona</option>
										  <option value="EFTA_Certified_Assistant_in_Arizona">EFTA Certified Assistant in Arizona</option>
										  <option value="Front_Office">Front Office</option>
										  <option value="Crostrained_(Front_Office/Dental_Assistant)_in_Arizona">Crostrained (Front Office/Dental Assistant) in Arizona</option>
										  <option value="Dentist_in_Arizona">Dentist in Arizona</option>
									</select>
								</div>
							</div>

								<?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'deactivated') {?>
							 <div class="col-md-4">
								<div class="form-group location">
									<label><?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_where_title'];?>
</label>
									 <form>
										 <select id="landing_location" name="landing_location">
												<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
												<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										 </select>   
									 </form>

								</div>
						</div>
								<?php }?>
								<div class="col-md-4">
									<button type="submit" class="btn btn-subc"><?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['header_search_btn_title'];?>
</button>
							</div> -->

						<!-- ENDPOINT -->

<!-- 						<style type="text/css">
							
							.col-centered{
    float: none;
    margin: 0 auto;
}

						</style> -->


						<div class="col-md-6">

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_SIGN_UP']->value;?>
" class="btn btn-subc">Registration</a>

					</div>

						<div class="col-md-6">

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
" class="btn btn-subc">Jobs Board</a>

					</div>



							</div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div> <!-- header-->

<!-- ///////////// MAIN-CONTENT ///////////// -->

<?php if ($_smarty_tpl->tpl_vars['more_jobs']->value) {?>
      <div id="candidates">
            <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['all_jobs'];?>
</h2>
      </div>

      <?php $_smarty_tpl->_subTemplateRender("file:snippets/homepage-jobs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="col-md-12 col-xs-12">
        <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
" ><button type="button" class="btn btn-subc btn-home-jobs"><?php echo $_smarty_tpl->tpl_vars['translations']->value['companies']['view_jobs'];?>
</button></a>
      </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['customizer_data']->value['listings_on_flag'] == '1') {
$_smarty_tpl->_subTemplateRender("file:snippets/listing-sitemap.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/landing/landing.js"><?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 type="text/javascript">
	$('.counter').each(function() {

  var $this = $(this),
      countTo = $this.attr('data-count');

  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {
    duration: 10000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
    }

  });  
});
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</body>
</html><?php }
}
