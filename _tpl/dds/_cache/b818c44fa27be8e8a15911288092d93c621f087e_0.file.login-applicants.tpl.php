<?php
/* Smarty version 3.1.30, created on 2019-07-18 16:35:11
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/login-applicants.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d3091af6bf8b2_14557823',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b818c44fa27be8e8a15911288092d93c621f087e' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/login-applicants.tpl',
      1 => 1555333978,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/login-header.tpl' => 1,
    'file:1.5/layout/login-footer.tpl' => 1,
  ),
),false)) {
function content_5d3091af6bf8b2_14557823 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/login-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="main-content login-page">
	<div class="adjusted-login-page">
		<div class="container" style="margin-top: 75px;">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SITE_LOGO_PATH']->value;?>
"> -->
					<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['headline_login'];?>
 Job Seekers</h2>

					<?php if ($_smarty_tpl->tpl_vars['login_failed']->value) {?>
					 <div id="incorrect-login-err" class="negative-feedback-form centered"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['incorrect'];?>
</div>
					 <div class="clear-both"></div>
					 <br />
					<?php }?>

					<form id="login-form" name="login-form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
login-applicant/" role="form">

						<div id="login-email-fg">
							<h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['email'];?>
</h4>
							<input required type="email" <?php if ($_smarty_tpl->tpl_vars['relogin_email']->value) {?>value="<?php echo $_smarty_tpl->tpl_vars['relogin_email']->value;?>
"<?php }?> name="signin_email" id="signin_email" maxlength="500">
							<div id="err-login-email" class="negative-feedback-form displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_email'];?>
</div>
						</div>
						<div class="clear-both"></div>

						<div id="login-pass-fg">
							<h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['password'];?>
</h4>
							<input required name="signin_pass" id="signin_pass" maxlength="300" type="password">
							<div id="err-login-pass" class="negative-feedback-form displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_pass_short'];?>
</div>
						</div>

					</form> 
					
					<div id="forgotpassblockid">
						<a href="#" class="forgot-pass" onclick="SimpleJobScript.hideLoginFields();" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['forgot_password'];?>
</a>
					</div>

					<div id="submit-block" >
						<div class="back-to-home">
							<button type="submit" class="btn b2h" onclick="return SimpleJobScript.loginFormValidation();" name="submit" id="submit"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['sign_in'];?>
</button>
						</div>
					</div>

					<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true') {?>
						<a id="sign-up-link" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_APPLICANTS']->value;?>
" class="make-account"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['noacc_applicant'];?>
</a>
					<?php }?>


					<div id="forgotten-zone" class="displayNone">
						<br /><br /><br /><br />
						<form role="form" action="" method="post" >
							
								<h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['forgotten_enter_email'];?>
</h4>
								<input required name="forget_email" id="forget_email" maxlength="300" type="email">

								<div id="passrecovery-feedback-err" class="negative-feedback-form displayNone centered"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['recovery_err'];?>
</div>
								<div id="passrecovery-feedback-err2" class="negative-feedback-form displayNone centered"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['recovery_err2'];?>
</div>
								<div id="passrecovery-feedback-ok" class="pos-feedback-form centered displayNone centered"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['newpass_email_sent'];?>
</div>
				
						 		<div class="clear-both"></div>
								<br />

								<button type="button" class="btn mbtn" onclick="return SimpleJobScript.passwordRecoveryApplicants();"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['forgotten_submit'];?>
</button>

						 		<div class="clear-both"></div>
								<br />

								<i id="fspinner" class=" fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner displayNone"></i>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
var input = document.getElementById("signin_email");

input.addEventListener("keyup", function(event) {
  // Cancel the default action, if needed
  event.preventDefault();
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Trigger the button element with a click
    document.getElementById("submit").click();
  }
}); 
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/login-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
