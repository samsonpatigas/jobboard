<?php
/* Smarty version 3.1.30, created on 2019-07-18 19:29:55
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/preview-modal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30baa3312b26_19350718',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1e8b7e2dbe1c8250c4c930782241c37a5e9dc8db' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/preview-modal.tpl',
      1 => 1539355250,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30baa3312b26_19350718 (Smarty_Internal_Template $_smarty_tpl) {
?>

 <div id="previewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['job_data']->value['title'];?>
</h4>
      </div>

	    <div class="modal-body job-details">
          <div class="detail-font" >

        <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['company'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['category_name'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['job_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['location_asci'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_date_posted'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_post_peroid'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_gender'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_language'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Positions</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_specialties'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_city'];?>
  <?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_state'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_zip'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['description'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_admin_notes'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

            <?php if (!$_smarty_tpl->tpl_vars['job_data']->value['apply_online'] == 1) {?>
              <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['company'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['category_name'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['job_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['location_asci'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_date_posted'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_post_peroid'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_gender'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_language'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Positions</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_specialties'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_city'];?>
  <?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_state'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_zip'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['description'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;font-size: inherit!important">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_admin_notes'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <?php }?>

      </div>

      <div class="modal-footer preview-footer">
        <div class="row">
          <div class="col-md-6">
            <button type="button" class="btn mbtn fl" style="width: 170px!important;height: 40px;line-height: 0;" data-dismiss="modal"><?php echo $_smarty_tpl->tpl_vars['translations']->value['js']['close'];?>
</button>
          </div>
           <div class="col-md-6">
            <?php if ($_smarty_tpl->tpl_vars['job_data']->value['salary']) {?><p class="price-apply">Salary <c style="color: #7527a0;font-size: inherit!important"><?php echo $_smarty_tpl->tpl_vars['job_data']->value['salary'];?>
</c></p><?php }?>
          </div>
        </div>
      </div>
    </div>

  </div>
</div><?php }
}
