<?php
/* Smarty version 3.1.30, created on 2019-07-16 15:00:27
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/preview-job.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2dd87b6bd626_56960847',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '44ba4ff73e4ff281b4f7a8d5de8f4eea4a18d7d8' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/preview-job.tpl',
      1 => 1563214349,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d2dd87b6bd626_56960847 (Smarty_Internal_Template $_smarty_tpl) {
?>
<link rel="stylesheet" href="/_tpl/dds/1.5/css/reset.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" integrity="sha256-vK3UTo/8wHbaUn+dTQD0X6dzidqc5l7gczvH+Bnowwk=" crossorigin="anonymous" />
<div class="row board ml0 pl0">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <h2>Job Preview</h2>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_JOBS']->value;?>
"><button type="button" class="btn btn-green tabletmt3p deskFr" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</button></a>
  </div>
</div><br/>
<div class="columns">
  <div class="column">
    <h3><?php echo strip_tags($_smarty_tpl->tpl_vars['job']->value['title']);?>
 in <?php echo strip_tags($_smarty_tpl->tpl_vars['job']->value['location_asci']);?>
</h3><a class="button is-primary" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_EDIT_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
">Edit this Job Post</a>
  </div>
</div>
<div class="columns">
  <div class="column">
      <table class="table">
        <tr>
          <td class="has-text-weight-bold">Company:</td>
          <td><?php echo strip_tags($_smarty_tpl->tpl_vars['job']->value['company']);?>
</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Job Type:</td>
          <td><?php echo strip_tags($_smarty_tpl->tpl_vars['job']->value['job_type']);?>
</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Category:</td>
          <td><?php echo strip_tags($_smarty_tpl->tpl_vars['job']->value['category_name']);?>
</td>
        </tr>
        <tr>
          <td class="has-text-weight-bold">Salary</td>
          <td>$<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</td>
        </tr>
      </table>
  </div>
  <div class="column">
    <table class="table">
      <tr>
        <td class="has-text-weight-bold">Date Posted:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_date_posted'];?>
</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">Position:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_position'];?>
</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">City:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_city'];?>
</td>
      </tr>
      <tr>
        <td class="has-text-weight-bold">Zip Code:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['job']->value['f9_zip'];?>
</td>
      </tr>
    </table>
  </div>
</div>



<?php }
}
