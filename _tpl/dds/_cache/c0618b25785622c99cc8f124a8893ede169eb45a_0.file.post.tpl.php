<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:32:48
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30d7707e6e30_56212874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c0618b25785622c99cc8f124a8893ede169eb45a' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post.tpl',
      1 => 1559745334,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:dashboard/views/post-first-step.tpl' => 1,
    'file:dashboard/views/post-second-step.tpl' => 1,
  ),
),false)) {
function content_5d30d7707e6e30_56212874 (Smarty_Internal_Template $_smarty_tpl) {
?>
 
<?php if (!$_smarty_tpl->tpl_vars['lock_post']->value) {?>
    <div class="row board">
      <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_job_headline'];?>
</h2>
        <?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?> 
            <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step1']['infodesc_step1'];
if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '3') {?></p><br /><div class="red"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</div><?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['step']->value == 2) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step2']['infodesc_step2'];?>
</p>
        <?php } elseif ($_smarty_tpl->tpl_vars['step']->value == 3) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['infodesc_step3'];?>
</p>
        <?php }?>
    </div>

    <div class="main-content job-process steps-wizard-mov">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn <?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?>btn-primary<?php } else { ?>btn-default<?php }?> btn-tab btn-circle" disabled="disabled">1</a>
                        <p style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['step_1'];?>
</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn <?php if ($_smarty_tpl->tpl_vars['step']->value == 2) {?>btn-primary<?php } else { ?>btn-default<?php }?> btn-tab btn-circle" disabled="disabled">2</a>
                        <p style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['step_2'];?>
</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn <?php if ($_smarty_tpl->tpl_vars['step']->value == 3) {?>btn-primary<?php } else { ?>btn-default<?php }?> btn-tab btn-circle" disabled="disabled">3</a>
                        <p style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['step_3'];?>
</p>
                    </div>
                </div>
            </div>
    </div>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['step']->value == 1 || $_smarty_tpl->tpl_vars['step']->value == 2) {?>
    <div class="main-content job-process">
        <?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?> 
            <?php $_smarty_tpl->_subTemplateRender("file:dashboard/views/post-first-step.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['step']->value == 2) {?>
            <?php $_smarty_tpl->_subTemplateRender("file:dashboard/views/post-second-step.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php }?>
    </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['step']->value == 3) {?> 
    <div class="col-md-12 col-xs-12" >
        <div class="row board mt0">
          <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['job_published_msg'];?>
</h2>
          <br /><br />
        </div>
    </div>

    <div class="col-md-6 col-xs-12 mlpl0">
        <div class="action-req success-msg dash-succ-box">
            <div class="row caution succ-box">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-1 col-xs-12 mtm8">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-11 col-xs-12">
                            <p><span><?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '2') {
echo $_smarty_tpl->tpl_vars['paypal_result_message']->value;
}?> <?php echo $_smarty_tpl->tpl_vars['published_msg']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '2' && $_smarty_tpl->tpl_vars['paypal_result_message']->value) {
echo $_smarty_tpl->tpl_vars['new_invoice_msg']->value;?>
 <?php }?><span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
}
}
