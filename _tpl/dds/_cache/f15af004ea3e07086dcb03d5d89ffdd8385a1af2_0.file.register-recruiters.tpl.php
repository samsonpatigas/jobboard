<?php
/* Smarty version 3.1.30, created on 2019-07-18 18:47:03
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/register-recruiters.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30b097bf0d23_16686259',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f15af004ea3e07086dcb03d5d89ffdd8385a1af2' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/register-recruiters.tpl',
      1 => 1554040983,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/login-header.tpl' => 1,
    'file:auth/register-recruiters-step2-form-content.tpl' => 1,
    'file:1.5/layout/login-footer.tpl' => 1,
  ),
),false)) {
function content_5d30b097bf0d23_16686259 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/login-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if ($_smarty_tpl->tpl_vars['second_step']->value) {?>
<div class="main-content login-page register-employer-page-step-2">
	<div class="adjusted-login-page">
		<div class="container" style="margin-top: 75px;">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SITE_LOGO_PATH']->value;?>
"> -->
					<h2>Client Registration</h2>
					
					<?php $_smarty_tpl->_subTemplateRender("file:auth/register-recruiters-step2-form-content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

				</div>
			</div>
		</div>
	</div>
</div>
	
	
<?php }?>


<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/login-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
