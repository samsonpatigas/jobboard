<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:28:01
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/deactivate-modal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fda146f451_57249284',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '70ffc14adab7a1a66427bdc1a380b398459f5998' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/modals/deactivate-modal.tpl',
      1 => 1540481612,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31fda146f451_57249284 (Smarty_Internal_Template $_smarty_tpl) {
$__section_index_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_index']) ? $_smarty_tpl->tpl_vars['__smarty_section_index'] : false;
$__section_index_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['dash_jobs']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_index_0_total = $__section_index_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_index'] = new Smarty_Variable(array());
if ($__section_index_0_total != 0) {
for ($__section_index_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index'] = 0; $__section_index_0_iteration <= $__section_index_0_total; $__section_index_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index']++){
?>
  <div class="modal fade" id="myModal_<?php echo $_smarty_tpl->tpl_vars['dash_jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_index']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_index']->value['index'] : null)]['id'];?>
" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%; margin-left: 0px;">
        <!--<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body">
          <p>Do you want to renew this listing?</p>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mlpl0">

    <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['company'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['category_name'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['job_type'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['location_asci'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_date_posted'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_post_peroid'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_gender'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_language'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Position</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_position'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Specialties</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_specialties'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">City / State</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_city'];?>
  <?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_state'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Zip code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_zip'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['description'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Admin notes</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p><?php echo $_smarty_tpl->tpl_vars['job_data']->value['f9_admin_notes'];?>
</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <form action="" method="post">
            <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_DEACTIVATE_MODAL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><button id="deactivate" name="deactivate" class="btn btn-default" type="submit">YES I DO</button></a> 
            <button name="cancel" class="btn btn-default" data-dismiss="modal">NO</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
<?php
}
}
if ($__section_index_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_index'] = $__section_index_0_saved;
}
}
}
