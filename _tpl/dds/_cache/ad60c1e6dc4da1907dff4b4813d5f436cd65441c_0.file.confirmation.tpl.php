<?php
/* Smarty version 3.1.30, created on 2019-07-18 19:10:29
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/confirmation.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30b615b4a5e9_22103331',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad60c1e6dc4da1907dff4b4813d5f436cd65441c' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/confirmation.tpl',
      1 => 1554362034,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5d30b615b4a5e9_22103331 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['acc_confirmation'];?>
</h2>

				<?php if ($_smarty_tpl->tpl_vars['confirm_result']->value) {?>
					<i class="fa fa-check" aria-hidden="true"></i>
					<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['hash_confirmation_successful'];?>
</h3>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
">
						<button type="button" class="btn"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['sign_in'];?>
</button>
					</a>

				<?php } else { ?>
					<i class="fa fa-times" aria-hidden="true"></i>
					<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['hash_confirmation_failed'];?>
</h3>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_RECRUITERS']->value;?>
">
					 <button type="button" class="btn" style="width: 250px;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['new_acc'];?>
</button>
					</a>
				<?php }?>

			</div>
		</div>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
