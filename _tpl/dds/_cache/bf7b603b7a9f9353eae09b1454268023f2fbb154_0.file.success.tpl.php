<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:28:01
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/success.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fda1455ca2_10677772',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bf7b603b7a9f9353eae09b1454268023f2fbb154' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/success.tpl',
      1 => 1539093757,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31fda1455ca2_10677772 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="col-md-12 col-xs-12" >
	<div class="row board">
	  <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['success_tpl_headline_generic'];?>
</h2>
	  <br /><br />
	</div>
</div>

<div class="col-md-6 col-xs-12 mlpl0">
	<div class="action-req success-msg dash-succ-box">

			<div class="row caution succ-box">
				<div class="col-md-12 col-xs-12">
					<div class="row">
						<div class="col-md-1 col-xs-12 mtm8">
							<i class="fa fa-check" aria-hidden="true"></i>
						</div>
						<div class="col-md-11 col-xs-12">
							<p><span><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
<span></p>
						</div>
					</div>
				</div>
			</div>

	</div>
</div><?php }
}
