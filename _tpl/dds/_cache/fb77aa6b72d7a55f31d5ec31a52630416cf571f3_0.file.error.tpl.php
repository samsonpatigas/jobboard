<?php
/* Smarty version 3.1.30, created on 2019-07-19 09:24:44
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/err/error.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d317e4c909e63_93912065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fb77aa6b72d7a55f31d5ec31a52630416cf571f3' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/err/error.tpl',
      1 => 1539093757,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5d317e4c909e63_93912065 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h2>404</h2>
				<i class="fa fa-safari" aria-hidden="true"></i>
				<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_msg'];?>
</h3>
				<p class="404reason">
				<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_1'];?>
<br>
				<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_2'];?>
<br>
				<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_3'];?>
<br>
				<?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_4'];?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_5'];?>
</a> <?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['404_txt_part_6'];?>

				</p>
			</div>
		</div>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
