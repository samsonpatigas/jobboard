<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:28:01
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31fda13e1822_77163019',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '74fa50434d8e92826859296fa6e84f1354f7860b' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/dashboard.tpl',
      1 => 1561118518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-dashboard-header.tpl' => 1,
    'file:dashboard/modals/applicant-modal.tpl' => 1,
    'file:dashboard/modals/preview-modal.tpl' => 1,
    'file:dashboard/modals/jobinfo-modal.tpl' => 1,
    'file:dashboard/modals/applications-modal.tpl' => 1,
    'file:dashboard/modals/match-applicant-modal.tpl' => 1,
    'file:dashboard/modals/deactivate-modal.tpl' => 1,
    'file:dashboard/modals/renew-modal.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5d31fda13e1822_77163019 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-dashboard-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="candidate-v2 mobileMT10">
  <div class="row">

      <div class="col-md-2 col-sm-12">
        <ul>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_SEARCHABLE']->value;?>
"><li id="search-li">
              <i class="fa fa-search" aria-hidden="true"></i>
                  SEARCH CANDIDATES</li></a>

            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_EDIT_COMPANY']->value;?>
"><li id="company-li"><i class="fa fa-vcard-o" aria-hidden="true"></i>
                  MY PRACTICE</li></a>

            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_SETTINGS']->value;?>
"><li id="settings-li"><i class="fa fa-cogs" aria-hidden="true"></i>
                <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['left_menu_settings'];?>
</li></a>

            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_EMAIL_LOGS">
              <li>
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                CONTACTS
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_BANNED_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                BANNED JOB SEEKER
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_PREVIOUS_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                PREVIOUS JOB SEEKER
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_LOGOUT']->value;?>
"><li><i class="fa fa-sign-out" aria-hidden="true"></i>
                LOGOUT</li></a>

            <!--<?php if ($_smarty_tpl->tpl_vars['PROFILES_PLUGIN']->value == 'true') {?>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_CVDATABASE']->value;?>
"><li id="cvdb-li"><i class="fa fa-file-text" aria-hidden="true"></i>
                    <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['top_menu_browse_applicants'];?>
</li></a>
            <?php }?>-->

            <?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '3') {?>
              <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_ACCOUNT']->value;?>
"><li id="account-li"><i class="fa fa-cubes" aria-hidden="true"></i>
                <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['left_menu_myaccount'];?>
</li></a>
            <?php }?>

         </ul>

          <?php if (@constant('BANNER_MANAGER') == 'true') {?>
            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['banners_backoffice_rectangle']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

          <?php }?>

          <?php if (@constant('ADSENSE') == 'true') {?>
            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['adsense_backoffice_rectangle']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

          <?php }?>

        </div>

        <div class="col-md-10 col-sm-12">
         <div class="employer-dashboard">
          <div class="container">
            <?php $_smarty_tpl->_subTemplateRender("dashboard/views/".((string)$_smarty_tpl->tpl_vars['view']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

           </div>
          </div>
        </div>
  </div>

</div>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popups']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/applicant-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popup_preview']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/preview-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popup_jobs']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/jobinfo-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['applications_modal_init']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/applications-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['applications_modal_init']->value == '2') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/match-applicant-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>


<div class="dash-modal">
  <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/deactivate-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>

<div class="dash-modal">
  <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/renew-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
