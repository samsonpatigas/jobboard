<?php
/* Smarty version 3.1.30, created on 2019-07-19 18:08:03
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/jobs.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d31f8f35ed095_97931341',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a661c1f543c8d66519993ad753c2eaceab192b1' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/jobs.tpl',
      1 => 1563481848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d31f8f35ed095_97931341 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
.modal-body {
padding: 5% 3% !important;
}
.modal-dialog {
  margin-top: 10% !important;
}
</style>

<div class="row board">
  <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_headline'];?>
</h2>
  <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_about_tag'];?>
</p>
</div>
<br /><br />

<div class="panel-group">
    <div class="panel panel-default" >
      <div id="arrow" class="">
        <h4 class="panel-title arrow-down" data-toggle="collapse" href="#collapse1" onclick="this.classList.toggle('active')">ACTIVE JOBS</h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
        <div class="row board mt0">
          <div class="table-responsive job-table">
            
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_title'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_status'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_views'];?>
</th>
                    <th style="width: 20%;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_applications'];?>
</th>
                    <!--<th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_statistics'];?>
</th>-->
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['info'];?>
</th>
                    <!-- <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_edit'];?>
</th>-->
                    <th>Deactivate</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody> 
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dash_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>
                  <tr>
                    <?php if ($_smarty_tpl->tpl_vars['job']->value['is_active'] == '1') {?>
                      <td><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_active'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['apps_array']->value[$_smarty_tpl->tpl_vars['job']->value['id']];?>
</td>
                      <!--<td class="opaque"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_STATISTICS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
-<?php echo $_smarty_tpl->tpl_vars['apps_array']->value[$_smarty_tpl->tpl_vars['job']->value['id']];?>
"><i class="fa fa-bar-chart-o fa-lg" aria-hidden="true"></i></a></td>-->
                      <td><a data-toggle="modal" data-target="#modal_<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="#"><i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>
                      <!--<td class="opaque">
                      <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_EDIT_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-wrench fa-lg" aria-hidden="true"></i></a> 
                      </td>-->
                      <!--  <td class="opaque"><a data-toggle="modal" data-target="#myModal_<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-ban fa-lg" aria-hidden="true" ></i></a></td> -->
                      <td class="opaque"><a id="deactivateConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_DEACTIVATE_MODAL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-ban fa-lg" aria-hidden="true" ></i></a></td>
                      <td class="opaque"><a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_PREVIEW/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
">Preview</a></td>
                    <?php }?>
                  </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </tbody>
            </table>
          </div>
        </div>
      </div>
      </div>
  </div>
   
  <!--INACTIVE JOBS-->
    <div class="panel panel-default">
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapse2" onclick="this.classList.toggle('active1')">INACTIVE JOBS</h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">

    <div class="row board mt0">
          <div class="table-responsive job-table">
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_title'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_status'];?>
</th>
                    <th>Expiry Date</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_views'];?>
</th>
                    <th style="width: 20%;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_applications'];?>
</th>
                    <!--<th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_statistics'];?>
</th>-->
                    <th>Activate</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_edit'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['table_jobs_delete'];?>
</th>
                  </tr>
                </thead>
                <tbody>

              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dash_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>
                  <tr>
              <?php if ($_smarty_tpl->tpl_vars['job']->value['is_active'] == '0') {?>
                <td><s><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</s></td>
                <td class="red"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_inactive'];?>
</td>
                <td><?php echo date("m-d-Y",$_smarty_tpl->tpl_vars['job']->value['expires']);?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['apps_array']->value[$_smarty_tpl->tpl_vars['job']->value['id']];?>
</td>
                <!--<td class="opaque"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_STATISTICS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
-<?php echo $_smarty_tpl->tpl_vars['apps_array']->value[$_smarty_tpl->tpl_vars['job']->value['id']];?>
"><i class="fa fa-bar-chart-o fa-lg" aria-hidden="true"></i></a></td>-->
                <td class="opaque"><a id="renewConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_RENEW_MODAL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></a></td>
                <td class="opaque"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_EDIT_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-wrench fa-lg" aria-hidden="true"></i></a></td>
                <td class="opaque"><a id="deleteConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_DELETE_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
                  <?php }?>
                  </tr>
                 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>


    <!--EXpired-->
    <div class="panel panel-default">
      <div class="">
        <h4 class="panel-title arrow-down2" data-toggle="collapse" href="#collapse3" onclick="this.classList.toggle('active2')">EXPIRED</h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">

    <div class="row board mt0">
          <div class="table-responsive job-table">
            <table class="table table-striped">
                <thead height="20">
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_title'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_status'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_views'];?>
</th>
                    <th style="width: 20%;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_applications'];?>
</th>
                    <!--<th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_statistics'];?>
</th>-->
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['info'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['jobs_edit'];?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['table_jobs_delete'];?>
</th>
                  </tr>
                </thead>
                <tbody>
              
                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>

  </div>


  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dash_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
echo '<script'; ?>
 type="text/javascript">
  $("#deleteConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
").confirm({
        text: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['delete_job_message'];?>
",
        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['yes_i_do'];?>
",
        cancelButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['text_no'];?>
",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
<?php echo '</script'; ?>
>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>



<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dash_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
echo '<script'; ?>
 type="text/javascript">
  $("#deactivateConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
").confirm({
        text: "Do you want to deactivate this job?",
        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['yes_i_do'];?>
",
        cancelButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['text_no'];?>
",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
<?php echo '</script'; ?>
>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>



<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dash_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
echo '<script'; ?>
 type="text/javascript">
  $("#renewConfirm-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
").confirm({
        text: "Do you want to renew this job?", 

        confirm: function(button) {
            window.location.replace(button[0].pathname);

        },
        cancel: function(button) {
        },
        confirmButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['yes_i_do'];?>
",
        cancelButton: "<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['text_no'];?>
",
        confirmButtonClass: "btn btn-default",
        cancelButtonClass: "btn btn-default"
  });
  
<?php echo '</script'; ?>
>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


<!-- 
<?php echo '<script'; ?>
 type="text/javascript">
  $( document ).ready(function() {
    document.getElementById("arrow").style.backgroundImage = "url('/uploads/logos/uparrow.png')";
     $('#arrow').click(function(){
        document.getElementById("arrow").style.backgroundImage = "url('/uploads/logos/arrow.png')";
     });
  });
<?php echo '</script'; ?>
>
 -->
<?php }
}
