<?php
/* Smarty version 3.1.30, created on 2019-07-18 21:32:48
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post-first-step.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30d770878f99_72742674',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '55b20eb7cd3ed5afdda82368cd0b8434cd3ad120' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/dashboard/views/post-first-step.tpl',
      1 => 1562872948,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30d770878f99_72742674 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edit-company">
  <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB_POSTED']->value;?>
/" role="form">
    <input type="hidden" id="step" name="step" value="1" />
    <input type="hidden" id="employer_id" name="employer_id" value="<?php echo $_smarty_tpl->tpl_vars['employer_id']->value;?>
" />
    <input type="hidden" id="jobs_left" name="jobs_left" value="<?php echo $_smarty_tpl->tpl_vars['jobs_left']->value;?>
" />
    <input type="hidden" id="job_period" name="job_period" value="<?php echo $_smarty_tpl->tpl_vars['job_period']->value;?>
" />

<?php if (!$_smarty_tpl->tpl_vars['lock_post']->value) {?>


          <div class="row " id="step-1">
           <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0">
  
            <div class="row checkboxes">
              <div class="col-md-3 col-xs-12">
                <h3 class="process-heading">Type <red style="color: red">*</red></h3>
              </div>

              <div class="col-md-5 col-xs-12">
                <select id="type_select" name="type_select">
                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['types']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['value']->value;
$_prefixVariable1=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['type_name'] == $_prefixVariable1) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </select>
              </div>
            </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Category <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="cat_select" name="cat_select">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cats']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                      <option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['id']->value;
$_prefixVariable2=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['category_id'] == $_prefixVariable2) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </select>
                </div>
              </div>

              <?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'deactivated') {?>
               <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Location <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="location_select" name="location_select">
                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
ob_start();
echo $_smarty_tpl->tpl_vars['id']->value;
$_prefixVariable3=ob_get_clean();
if ($_smarty_tpl->tpl_vars['draft_data']->value['city_id'] == $_prefixVariable3) {?>selected<?php }
}?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </select>
                </div>
              </div>
              <?php }?>

              <?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '2') {?>
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['premium_text'];?>
 <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="premium_select" name="premium_select">
                    <option value="0"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['standard_label'];?>
</option>
                    <option value="1"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['premium_label'];?>
 <?php echo $_smarty_tpl->tpl_vars['FORMATED_CURRENCY']->value;?>
</option>
                  </select>
                </div>
              </div>
              <?php }?>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Date posted <!-- <red style="color: red">*</red> --></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input type="date" id="f9_date_posted" name="f9_date_posted" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_date_posted'];
}?>" class="job-title" readonly>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Title <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <!-- <input required name="title" id="title" maxlength="400" type="text" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['title'];
}?>" class="job-title"  /> -->
                  <select id="title" name="title">
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Hygienist">Hygienist</option>
                      <option value="Front Office">Front Office</option>
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Positions<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_position" name="f9_position" <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
if ($_smarty_tpl->tpl_vars['draft_data']->value['f9_specialties']) {?>selected<?php }
}?>>
                      <option value="Hygienist">Hygienist</option>
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Cross-trained (Front Office/Dental Assistant)">Cross-trained (Front Office/Dental Assistant)</option>
                      <option value="Front Office">Front Office</option>
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Practice Type:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_practice_type" name="f9_practice_type">
                    <option value="General">General</option>
                    <option value="Ortho">Ortho</option>
                    <option value="Perio">Perio</option>
                    <option value="Endo">Endo</option>
                    <option value="Oral Surgery">Oral Surgery</option>
                  </select>            
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Position Type:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Full Time">Permanent Full Time</label>
                  <label style="text-align:left;"><input type="radio" name="f9_position_type" id="f9_position_type" value="Permanent Part Time">Permanent Part Time</label>
                </div>
              </div>
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Service Type:</h3>
                </div>
                <div class="col-md-8 col-xs-12">
                  <div class="radio">
                    <label style="text-align:left;"><input type="radio" name="f9_service_type" id="f9_service_type" value="Desert Dental Staffing Job Board Posting">Desert Dental Staffing Job Board Posting</label>
                  </div> 
                  <div class="radio">  
                    <label style="text-align:left;"><input type="radio" name="f9_service_type" id="f9_service_type" value="Concierge Service">Concierge Service</label>
                  </div>
                </div>
              </div>              
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Post period <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_post_peroid" name="f9_post_peroid" <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
if ($_smarty_tpl->tpl_vars['draft_data']->value['f9_post_peroid']) {?>selected<?php }
}?>>
                    <option value="30">(30 days)</option>
                    <option value="60">(60 days)</option>
                    <option value="90">(90 days)</option>
                  </select>
                </div>
              </div>
              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Spanish Bilingual:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="Yes">Yes</label>
                  <label style="text-align:left;"><input type="radio" name="f9_bilingual" id="f9_bilingual" value="No">No</label>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Skills:</h3>
                </div>
                <div class="col-md-8 col-xs-12">
                  <p>TIP:  Select only the ABSOLUTE requirements needed for the position to maximize the number of resumes received</p>
                  <label>       
                    <input name="f9_skills[]" id="f9_skills[]" value="Custom Temps" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Custom Temps</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Cerec System" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Cerec System</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Surgical Implants" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Surgical Implants</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="X-ray Certified in AZ" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">X-ray Certified in AZ</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Coronal Polish Certified in AZ" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Coronal Polish Certified in AZ</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="IV Sedation" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">IV Sedation</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Insurance Processing" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Insurance Processing</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Treatment Presentation" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Treatment Presentation</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Anesthesia Certified" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Anesthesia Certified</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="AR" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">AR</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="Laser Certified" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">Laser Certified</h4>
                  </label>
                  <label>
                    <input name="f9_skills[]" id="f9_skills[]" value="EFDA (Certified Assistant in Arizona)" class="checkbox-custom" type="checkbox" />
                    <h4 style="line-height: 2;">EFDA (Certified Assistant in Arizona)</h4>                  
                  </label>
                </div>      
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Software Experience:</h3>
                </div>
                <div class="col-md-5 col-xs-12">
                    <label>
                      <input name="f9_office_software[]" value="Dentrix" type="checkbox" class="checkbox-custom" id="f9_office_software">
                      <h4 style="padding-top: 5px;">Dentrix</h4>  
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="EagleSoft" class="checkbox-custom" type="checkbox">
                      <h4 style="padding-top: 5px;">EagleSoft</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Open Dental" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Open Dental</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="SoftDent" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">SoftDent</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Dentimax" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Dentimax</h4>
                    </label>
                    <label>
                      <input name="f9_office_software[]" value="Others" class="checkbox-custom" type="checkbox"> 
                      <h4 style="padding-top: 5px;">Others</h4>
                    </label>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">City <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_city" id="f9_city" maxlength="400" type="text" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_city'];
}?>" class="job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Hourly Range (Min)<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_pay_min" id="f9_pay_min" maxlength="400" type="text" value="" class="input-f9_pay_min job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Hourly Range (Max)<red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_pay_max" id="f9_pay_max" maxlength="400" type="text" value="" class="input-f9_pay_max job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">State <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">        
                  <select required id="f9_state" name="f9_state" class="dropdown" style="
                  -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    padding: 10px;
                    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                    min-height: 47px;
                    width: 75%;
                    border-radius: 5px;
                    padding: 0% 3% 0% 6%;
                    border: none;
                    float: left;
                    margin-bottom: 2%;
                    font-size: 14px;
                    padding: 10px;
                    background-color:white;
                    border-color: gainsboro;
                    border-width: 1px;
                    border-style: solid;
                    ">
                    <?php $_smarty_tpl->_assignInScope('states', array('Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming'));
?>
                      <option value="" disabled selected>Select your option</option>
                      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['states']->value, 'x');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['x']->value) {
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</option>
                      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Zip code <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_zip" id="f9_zip" maxlength="5" type="text" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_zip'];
}?>" class="job-title"  />
                </div>
              </div>


              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_label'];?>
 <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input name="salary" class="input-salary job-title" id="salary" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['salary'];
}?>"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Years of experience <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_yrs_of_experience" name="f9_yrs_of_experience">
                    <option value="New Graduate">New Graduate</option>
                    <option value="6 Months – 2 Years">6 Months – 2 Years</option>
                    <option value="3 – 5 Years">3 – 5 Years</option>
                    <option value="6 Years Plus">6 Years Plus</option>
                  </select>
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Language <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <input required name="f9_language" id="f9_language" maxlength="400" type="text" value="<?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_language'];
}?>" class="job-title"  />
                </div>
              </div>

              <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Gender <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_gender" name="f9_gender" <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
if ($_smarty_tpl->tpl_vars['draft_data']->value['f9_gender']) {?>selected<?php }
}?>>
                     <option value="Male">Male</option>
                       <option value="Female">Female</option>
                  </select>
                    </div>
                </div>

                <div class="row checkboxes">
                <div class="col-md-3 col-xs-12">
                  <h3 class="process-heading">Specialties <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-5 col-xs-12">
                  <select id="f9_specialties" name="f9_specialties" <?php if ($_smarty_tpl->tpl_vars['DRAFT']->value) {
if ($_smarty_tpl->tpl_vars['draft_data']->value['f9_specialties']) {?>selected<?php }
}?>>
                      <option value="Dental Assistant">Dental Assistant</option>
                      <option value="Hygienist">Hygienist</option>
                      <!-- <option value="EFTA Certified Assistant">EFTA Certified Assistant</option> -->
                      <option value="Front Office">Front Office</option>
                      <!-- <option value="Crostrained (Front Office/Dental Assistant)">Crostrained (Front Office/Dental Assistant)</option> -->
                      <option value="Dentist">Dentist</option>
                  </select>
                </div>
              </div>



              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Short Description <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="description" name="description" class="process-textarea" rows="12" cols="1" spellcheck="true"><?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['description'];
}?></textarea>
                </div>
              </div>
              <br />

              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Admin Notes <red style="color: red">*</red></h3>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="f9_admin_notes" name="f9_admin_notes" class="process-textarea" rows="12" cols="1" spellcheck="true"><?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_admin_notes'];
}?></textarea>
                </div>
              </div>
              <br />
              <div class="row checkboxes">
                <div class="col-lg-8 col-md-12 col-md-12 col-sm-12 col-xs-12">
                  <h3 class="process-heading mb15">Position Notes <red style="color: red">*</red></h3>
                  <p>Enter additional information about the position.  Text entered here will be displayed in the Job Board posting.</p>
                </div>
                <div class="col-md-12 col-xs-12">
                  <textarea id="f9_admin_notes" name="f9_position_notes" class="process-textarea" rows="12" cols="1" spellcheck="true"><?php if ($_smarty_tpl->tpl_vars['DRAFT']->value == 'true') {
echo $_smarty_tpl->tpl_vars['draft_data']->value['f9_position_notes'];
}?></textarea>
                </div>
              </div>
              </div>
        
              <div class="acceptance mb50">
                <label>
                  <input class="checkbox-custom" type="checkbox" onchange="SimpleJobScript.applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" checked />

                  <h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_label'];?>
<span class="apply-desc-span" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_desc'];?>
</span></h4>

                </label>
              </div>
              <br />
              
               <div id="apply-desc-block" class="form-group mb20 displayNone" >
                <label class="green tal mb20"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['howto_apply_label'];?>
</label>
                <input id="howtoTA" class="form-control minput" id="howtoapply" name="howtoapply" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
"></input>
               </div>
        
          </div>

          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mlpl0 mt25">
            <button type="submit" onclick="return SimpleJobScript.validateDesc();" class="btn mbtn fl" name="submit" id="submit" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_submit'];?>
</button>
          </div>
        </div>


<?php } else { ?>

  <div class="col-md-10 col-sm-12 locked mt25">
    <div class="modal1">
      <div class="modal fade in" id="myModal" role="dialog" style="display: block;">
        <div class="modal-dialog">
      
          <div class="modal-content">
            <div class="modal-body">
              <h4 class="modal-title md-hide mb25">1/2</h4>
              <p class="tac fs16"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</p>
              <img class="anim-pic" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/default/1.5/images/lock-image.png">
              <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_ACCOUNT_ORDER']->value;?>
">
                <button type="button" class="btn btn-default btn-lock"><?php echo $_smarty_tpl->tpl_vars['translations']->value['post_step1']['renew'];?>
</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  

<?php }?>

  </form>
</div>
<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave-phone.us.js"><?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready( function() {
        var now = new Date();
        var month = (now.getMonth() + 1);
        var day = now.getDate();
        if (month < 10) 
            month = "0" + month;
        if (day < 10) 
            day = "0" + day;
        var today = now.getFullYear() + '-' + month + '-' + day;
        $('#f9_date_posted').val(today);
        document.getElementById('f9_date_posted').setAttribute("min",today);

        $("input[name='f9_service_type']").change(function(){
            selected_value = $("#f9_service_type:checked").val();
            if(selected_value == 'Concierge Service'){
              $('#f9_post_peroid').append($('<option>', {
                value: 3650,
                text: 'No Expiry'
              }));
              $('option[value="3650"]').prop("selected", true);
            }else{
              $("#f9_post_peroid option[value='3650']").remove();
            }
        });
    });

    var cleave = new Cleave('.input-f9_pay_min', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    var cleave = new Cleave('.input-f9_pay_max', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    var cleave = new Cleave('.input-salary', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });

    document.getElementById('f9_zip').addEventListener('input', function (e) {
      var x = e.target.value.replace(/[^0-9]/g, '')
          e.target.value = x;
            });

  <?php echo '</script'; ?>
>
<?php }
}
