<?php
/* Smarty version 3.1.30, created on 2019-07-18 18:47:03
  from "/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/register-recruiters-step2-form-content.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d30b097c61673_52649741',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '92f6af7953f4053da39d2cad36629b28397b9a0f' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/_tpl/dds/auth/register-recruiters-step2-form-content.tpl',
      1 => 1563471685,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d30b097c61673_52649741 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"><?php echo '</script'; ?>
>

<style type="text/css">
  .error-message {
    background-color: #fce4e4;
    border: 1px solid #fcc2c3;
    float: left;
    width: 100%;
    padding: 20px 30px;

  }

  .error-text {
    color: #cc0033;
    font-family: Helvetica, Arial, sans-serif;
    font-size: 13px;
    font-weight: 400;
    line-height: 20px;
    text-shadow: 1px 1px rgba(250, 250, 250, .3);
  }

  .error-message-text li {
    font-weight: 400;
  }

  .error-info {

    float: center;

  }

  #submit:hover, #add_contact:hover{
    background-color: #ffffff!important;
    color: #7527a0!important;
    border: 1px solid #7527a0;
  }

  #logoLabel:hover{
    background-color: #7527a0!important;
      border: solid 1px #ffffff!important;
      color: #ffffff;
  }

  .checkbox-custom:checked::after {
    content: '\2714';
    font-size: 21px;
    font-weight: 600;
    position: absolute;
    top: -5px;
    left: 2px;
    color: #337ab7;
  }

  @media screen and (max-width: 768px) {
    .table-responsive-sm {
      margin-left: -30px
    }

    .col-xs-12{
      padding-right: 0px;
        padding-left: 0px;
    }

    .main-content .container p {
        margin-top: 0;
    }

    #c_email_type\5b \5d {
      width: 115%!important;
    }
  }

  #f9_zip {
    font: 600 15px Arial;
  }

</style>

<form id="register-form" name="register-form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_RECRUITERS']->value;?>
" role="form"
  enctype="multipart/form-data">
  <input type="hidden" name="step" id="step" value="2">
  <input type="hidden" name="employerid" id="employerid" value="<?php echo $_smarty_tpl->tpl_vars['employer_id']->value;?>
">
  <input type="hidden" name="employer_hash" id="employer_hash" value="<?php echo $_smarty_tpl->tpl_vars['employer_hash']->value;?>
">
  <input type="hidden" name="employer_email" id="employer_email" value="<?php echo $_smarty_tpl->tpl_vars['employer_email']->value;?>
">
  <input type="hidden" name="updatingEntry" id="updatingEntry" value="<?php echo $_smarty_tpl->tpl_vars['updatingEntry']->value;?>
">

  <div class="row">
    <div class="error-message" id="errors" style="width=100%">
      <span class="error-info" style="float:left;">
        <i class="fa fa-warning" style="color:red;"></i> Incorrect or Missing Information. Please scroll down to
        correct your entries<br />
        <ul id="error-message-text" style="text-align:left; list-style-type: circle; font-weight:400;"></ul>
      </span>
    </div>
  </div>

  <?php echo '<script'; ?>
 type="text/javascript">
    var x = document.getElementById("errors");
    x.style.display = "none"; //hide
    var errorText = document.getElementById("error-message-text");
    errorText.style.display = "none"; //hide additional text
  <?php echo '</script'; ?>
>

  <div class="col-md-6 col-xs-12">
    <h3 style="margin-left:0px;">First Name</h3>
    <input name="f9_fname" id="f9_fname" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_fname_error_message" class="negative-feedback-form displayNone">First Name is empty</div>
  </div>

  <div class="col-md-6 col-xs-12">
    <h3 style="margin-left:0px;">Last Name</h3>
    <input name="f9_lname" id="f9_lname" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_lname_error_message" class="negative-feedback-form displayNone">Last Name is Empty</div>
  </div>

  <div class="col-md-12 col-xs-12" id="reg-name-fg">
    <h4 style="margin-left:0px;">Practice name<font color="red">*</font>
    </h4>
    <input type="text" name="register_name" id="register_name" maxlength="500"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;"
      >
    <div id="register_name_error_message" class="negative-feedback-form displayNone">Practice Name is Empty</div>
  </div>

  <div class="col-md-6 col-xs-12" id="reg-pass1-fg">
    <h4 style="margin-left:0px;">Password <font color="red">*</font>
    </h4>

    <input type="password" onfocus="SimpleJobScript.checkIfEmailExists();" name="register_pass1"
      id="register_pass1" maxlength="500"
      style="margin-bottom:0px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
    <br />
    <p id='resultpass' style="color: black;font-size: 80%;">Minimum 8-16 characters, 1 uppercase letter, 1 lowercase
      letter, and 1 number.</p>

    <p id='valpass' style="color: red; opacity: 0.75; font-size: 80%;margin-top: 10px;float: left;margin-left: 10px;"></p>

    
    <?php echo '<script'; ?>
 type="text/javascript">
      $('#register_pass1').on('change keydown paste input', function () {
        var $pass1 = this.value;
        validatePass($pass1);
      });

      function validatePass(pass) {

        var $result = $("#valpass");
        $result.text("");
        // Validate with special characters

        // var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/;

        // Validate without special characters

        var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;
        if (!passReg.test(pass)) {

          $result.text("This password is Invalid.");
          $result.css({
            "color": "red",
            "font-size": "90%",
            "font-weight": "bold"
          });
          var p = document.getElementById('register_pass1');
          //p.setCustomValidity("Minimum 8-16 characters, 1 uppercase letter, 1 lowercase letter, and 1 number.");
          p.setCustomValidity("");
        }else{
          $result.text("");
          $result.css({
            "color": "green",
            "font-size": "80%"
          });

          var p = document.getElementById('register_pass1');
          p.setCustomValidity("");
        }
      }
    <?php echo '</script'; ?>
>
    
  </div>
  <div class="col-md-6 col-xs-12" id="reg-pass2-fg">
    <h4 style="margin-left:0px;">Repeat password <font color="red">*</font>
    </h4>
    <input type="password" name="register_pass2" id="register_pass2" maxlength="500"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;">
    <div id="err-reg-pass2" class="negative-feedback-form displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_passes'];?>
</div>
  </div>
  <div class="col-md-12 col-xs-12" style="display: none">
    <h3 style="margin-left:0px;">Business Name <font color="red">*</font>
    </h3>
    <input name="f9_bizname" id="f9_bizname" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_bizname_error_message" class="negative-feedback-form displayNone">Business Name is Empty</div>
  </div>
  <div class="col-md-12 col-xs-12">
    <h3 style="margin-left:0px; margin-top:0px;">Street Address <font color="red">*</font>
    </h3>
    <input name="f9_address_1" id="f9_address_1" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_address_1_error_message" class="negative-feedback-form displayNone">Street Address is Empty</div>
  </div>
  <div class="col-md-12 col-xs-12">
    <h3 style="margin-left:0px;">Unit #</h3>
    <input name="f9_address_2" id="f9_address_2" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_address_2_error_message" class="negative-feedback-form displayNone">Unit # is Empty</div>
  </div>
  <div class="col-md-6 col-xs-12">
    <h3 style=" float: left; margin-top:0px; margin-left:0px"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_city'];?>
 <font
        color="red">*
      </font>
    </h3>
    <input  name="f9_city" id="f9_city" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_city_error_message" class="negative-feedback-form displayNone">City is Empty</div>
  </div>
  <div class="col-md-3 col-xs-12">
    <h3 style=" float: left;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_state'];?>
 <font color="red">*</font>
    </h3>
    <label class="wrap">
      <select required name="f9_state" id="f9_state" class="form-control minput" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 37px;
              width: 100%;
              border-radius: 5px;
              border: none;
              float: left;
              margin-bottom: 0%;
              font-size: 14px;">
        <?php $_smarty_tpl->_assignInScope('states', array('Alabama','Alaska','American
        Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of
        Columbia','Federated
        States of
        Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall
        Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New
        Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana
        Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South
        Carolina','South
        Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West
        Virginia','Wisconsin','Wyoming'));
?>
        <option value="" disabled selected>Select your option</option>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['states']->value, 'x');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['x']->value) {
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</option>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

      </select>
    </label>
    <div id="f9_state_error_message" class="negative-feedback-form displayNone" style="padding-top:20px;">State is
      Empty
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <h3 style=" float: left;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_zip'];?>
<font color="red">*</font></h3>
    <input  name="f9_zip" id="f9_zip" type="text" maxlength="5"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_zip_error_message" class="negative-feedback-form displayNone">Zip is Empty or Invalid</div>
  </div>
  <div class="col-md-12 col-xs-12">
    <h3 style="margin-left:0px;">What are your major cross-streets<br><small>(Highly
        Recommended)</small></h3>
    <textarea name="f9_cross_st" id="f9_cross_st" rows="4" cols="50"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;"
      maxlength="500" placeholder="What are your major cross-streets?"></textarea>
    <div id="f9_cross_st_error_message" class="negative-feedback-form displayNone">Major Cross Street is Empty</div>
  </div>
  <div class="col-md-6 col-xs-12">
    <h3 style="margin-left:0px;margin-top: 0px;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_phone_number'];?>
 <font color="red">*</font>
    </h3>
    <input class="input-f9_practice_phone_number" name="f9_practice_phone_number" id="f9_practice_phone_number" type="text" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="f9_practice_phone_number_error_message" class="negative-feedback-form displayNone">Phone
      number is invalid or field is
      empty</div>
  </div>
  <div class="col-md-6 col-xs-12">
    <h3 style="float: left;margin-top: 0px; margin-left:0px;"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_email_address'];?>

      <font color="red">*</font>
    </h3>
    <input name="f9_practice_email_address" id="f9_practice_email_address" type="email" maxlength="1000"
      style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
    <div id="email_f9_practice_email_address_exist" class="negative-feedback-form displayNone">Practice Email
      Address
      is Taken</div>
    <div id="f9_practice_email_address_error_message" class="negative-feedback-form displayNone">Email format is invalid or field is empty</div>
  </div>
  <div class="col-md-12 col-xs-12">
    <div id="vue-app">
      
    </div>
    <div class="table-responsive-sm">
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Contact Name</th>
          <th scope="col">Phone Number</th>
          <th scope="col">Phone Type</th>
          <th scope="col">Email Address</th>
          <th scope="col">Email Type</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="             
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 42px;
              width: 100%;
              border-radius: 5px;
              border: none;
              float: left;
              margin-bottom: 0px;
              font-size: 14px;
              padding: 10px;">
            <option value="Main">Main</option>
            <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td>
            <input name="c_email_address[]" type="email" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 37px;
              width: 100%;
              border-radius: 5px;
              border: none;
              float: left;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Office_Mgr">Office Mgr</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td>
            <input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="             
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="             
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="             
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="             
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style=" 
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]"  style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 42px;
                width: 100%;
                border-radius: 5px;
                border: none;
                float: left;
                margin-bottom: 0px;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td>
            <input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]" 
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
        <tr style="display:none;">
          <td><input name="c_contact_name[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td><input name="c_phone_number[]"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_phone_type[]" style="
              -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                padding: 10px;
                box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
                widows: 307px;
                min-height: 47px;
                width: 100%;
                border-radius: 10px;
                padding: 0% 3% 0% 6%;
                border: none;
                float: left;
                margin-bottom: 7%;
                font-size: 14px;
                padding: 10px;">
              <option value="Main">Main</option>
              <option value="Mobile">Mobile</option>
            </select>
          </td>
          <td><input name="c_email_address[]" type="email"
              style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
          </td>
          <td>
            <select class="form-control minput" name="c_email_type[]" style="
            -webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              padding: 10px;
              box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
              widows: 307px;
              min-height: 47px;
              width: 100%;
              border-radius: 10px;
              padding: 0% 3% 0% 6%;
              border: none;
              float: left;
              margin-bottom: 7%;
              font-size: 14px;
              padding: 10px;">
              <option value="Main">Main</option>
              <option value="Billing">Billing</option>
              <option value="Private">Private</option>
            </select>
          </td>
        </tr>
      </tbody>
    </table>
    </div>
    <button name="add_contact" style="
          min-height: 30px;
          min-width: 180px;
          background-color: #7527a0;
          color: white;
          border-radius: 10px;
          text-transform: uppercase;
          border: solid;
          border-width: 1px;" id="add_contact" onclick="JavaScript:$('tr:hidden:first').show(); return false; ">Add New
      Contact</button>
  </div>
  <div class="col-md-6 col-xs-12">
    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_name'];?>
 <font color="red">*</font>
      </h3>
      <input name="company_name" id="company_name" type="text" maxlength="300"
        value="<?php echo $_POST['register_name'];?>
" />
    </div>
    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_name'];?>
 <font color="red">*</font>
      </h3>
      <input name="f9_practice_name" id="f9_practice_name" type="text" maxlength="1000" />
    </div>

    <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_practice_type'];?>
 <font color="red">*</font>
    </h3>
    
    <style>
      label {
        display: block;
        padding-left: 15px;
        text-indent: 10px;
        text-align: left;
      }

      input[type="checkbox"] {
        width: 13px;
        height: 13px;
        padding: 0;
        margin: 0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
        *overflow: hidden;
      }
    </style>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" id="f9_practice_type" value="General" /> General
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" value="Perio" /> Perio
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" value="Endo" /> Endo
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" value="Pedo" /> Pedo
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" value="Ortho" /> Ortho
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_practice_type[]" value="Oral / Max Surgery" /> Oral / Max Surgery
      </label>
    </div>
    <p id="practice_type_message" color="red" style="margin-bottom:0px; padding-left: 10px;"></p>
    <div>
      <h3>Special Needs / Notes</h3>
      <textarea name="f9_notes" id="f9_notes" rows="4" cols="50"
        style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;"
        maxlength="500" placeholder="Add some notes."></textarea>
    </div>
    <div class="logo-upload-div">
      <div id="uploadPreview"></div>
      <div id="logo-err" class="negative-feedback displayNone mt25"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_msg'];?>

      </div>
      <div id="logo-err2" class="negative-feedback displayNone mt25">
        <?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_err_samesize'];?>

      </div>
      <div id="logo-ok" class="pos-feedback-registration displayNone mt25" style="display: none;">
        <?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['logo_ok'];?>
</div>

      <label id="logoLabel" for="company_logo" style="width: 276px!important;padding: 20px!important;">Logo
        (Optional)</label>
      <input type="file" name="company_logo" id="company_logo" class="form-control inputfile minput" />
      <div class="textarea-feedback mb25">Allowed extensions (jpg, png, gif)</div>
    </div>
    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_hq_label'];?>
 <font color="red">*</font>
      </h3>
      <input name="company_hq" id="company_hq" type="text" maxlength="400" />

      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_street_label'];?>
 <font color="red">*</font>
      </h3>
      <input name="company_street" id="company_street" type="text" maxlength="500" />

      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_citypostcode_label'];?>
 <font color="red">*</font>
      </h3>
      <input name="company_citypostcode" id="company_citypostcode" type="text" maxlength="500" />
    </div>
  </div>

  <div class="col-md-6 col-xs-12">
    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_url_label'];?>
 <font color="red">*</font>
      </h3>
      <input name="company_url" id="company_url" type="text" maxlength="1000" />
    </div>

    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['f9_job_app_email_address'];?>
 <font color="red">*</font>
      </h3>
      <input name="f9_job_app_email_address" id="f9_job_app_email_address" value="<?php echo $_smarty_tpl->tpl_vars['f9_job_app_email_address']->value;?>
"
        type="text" maxlength="1000" />
    </div>

    <div style="display: none;">
      <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['company_desc'];?>
</h3>
      <textarea class="noTinymceTA" name="company_desc" id="company_desc" maxlength="1000" rows="6"
        cols="42"><?php if ($_smarty_tpl->tpl_vars['current_form']->value) {
echo $_smarty_tpl->tpl_vars['current_form']->value['message'];
}?></textarea>
      <div class="textarea-feedback" id="textarea_feedback"></div>
    </div>


    <h3>Office Software</h3>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_office_software[]" value="Dentrix" /> Dentrix
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_office_software[]" value="EagleSoft" /> EagleSoft
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          " name="f9_office_software[]" value="Open Dental" /> Open Dental
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;" name="f9_office_software[]" value="SoftDent" /> SoftDent
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;" name="f9_office_software[]" value="DentiMax" /> DentiMax
      </label>
    </div>
    <div>
      <label style="padding-bottom: 20px;">
        <input type="text" name="f9_office_software[]" placeholder="Others"
          style="margin-bottom:20px;padding: 10px;min-height: 37px;border-color: gainsboro;border-width: 1px;border-style: solid;border-radius: 5px;" />
      </label>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="tos">
        <label>
          <input required type="checkbox" class="checkbox-custom" style="
          background-color: #FFFFFF;
          border: 2px solid#7527a0;
          padding: 0;
          border-radius: 0px;
          display: inline-block;
          position: relative;
          width: 25px;
          height: 25px;
          color: #7527a0!important;
          float: left;
          margin-top: -3px;" name="terms_condition[]"/>
          <h4><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['accept_part1'];?>
 <a target="_blank"
              href="<?php echo $_smarty_tpl->tpl_vars['BASEURL']->value;
echo TERMS_CONDITIONS_URL;?>
">
              <?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['accept_part2'];?>
</a></h4>
        </label>
      </div>
    </div>
  </div>
  <p id="terms_condition_message" color="red" style="margin-bottom:0px; padding-left: 10px;"></p>
  <div class="col-md-12 col-xs-12" style="padding-bottom: 140px;">
    <br /><br /><br /><br />
    <button type="submit"
      style="
          min-height: 45px;
          min-width: 230px;
          background-color: #7527a0;
          color: white;
          border-radius: 10px;
          text-transform: uppercase;
          border: solid;
          border-width: 1px;
          margin-top: 40px;
        ">Sign Up</button>
  </div>
</form>

<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/_tpl/dds/1.5/js/cleave/cleave-phone.us.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://unpkg.com/axios/dist/axios.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
  

  var cleave = new Cleave('.input-f9_practice_phone_number', {
      phone: true,
      phoneRegionCode: 'US'
  });

  var formCheck = function(event) {
    event.preventDefault();
    if (check_input_fields()){
      console.log('all is ok');
      form.submit();
    }   
  };

  var form = document.getElementById("register-form");

  form.addEventListener("submit", formCheck, true);
  
    function check_input_fields() {
      var input_fields = {
          'register_name': 'Practice Name',
          'f9_address_1': 'Street Address',
          'f9_city': 'City',
          'f9_state': 'State',
          'f9_zip': 'Zip',
          'f9_practice_phone_number': 'Practice Phone Number',
          'register_pass1': 'Password',
          'register_pass2': 'Password'
      };
      
      var err_display = 0;
      $("#error-message-text").empty();
      $.each(input_fields, function (index, value) {   
        if ($.trim($('#' + index).val()).length == 0) {
          console.log(index);
          $("#error-message-text").append('<li>' + value + '</li>');
          err_display = 1;
          $('#' + index + '_error_message').removeClass('displayNone');
          //window.scrollTo(0,0);
          window.scrollTo({
          top: 80,
          behavior: 'smooth'
          });        
        }else{
          console.log('passed: ' + index);
          $('#' + index + '_error_message').addClass('displayNone');
        }
        console.log(index + ": " + value);
      });

      var email = $('#f9_practice_email_address').val();
      var emailPattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+.([a-zA-Z])+([a-zA-Z])+/;
      var emailIsValid = emailPattern.test(email);

      if ($('#f9_practice_email_address').val().length != 0 || emailIsValid != false) {
        $.ajax({ url: '/email_verification_ajax_recruiters.php',
          data: "email=" + $('#f9_practice_email_address').val(),
          type: 'post'
        }).done(function(msg) {
          var json = JSON.parse(msg);
          console.log('json.result: ' + json.result);
          if (json.result == "1") {
            $('#email_f9_practice_email_address_exist').removeClass('displayNone');
            err_display = 0;
            var x = document.getElementById("errors");
            x.style.display = "block";
            window.scrollTo({
                top: 80,
                behavior: 'smooth'
              });
            $('#submit').click(function (evt) {
                evt.preventDefault();
              });
          }else{
            $('#email_f9_practice_email_address_exist').addClass('displayNone');  
          }
        });
      }
      
      if (err_display == 1) {
        console.log('error');
        var x = document.getElementById("errors");
        x.style.display = "block";
        return false
      }else{
        console.log('no error');      
        var x = document.getElementById("errors");
        x.style.display = "none";
        return true
      }



      /*
      if ($('#f9_practice_phone_number').val().length < 11) {
          $('#f9_practice_phone_number_error_message').removeClass('displayNone');
          var x = document.getElementById("errors");
          x.style.display = "block";
          window.scrollTo({
              top: 80,
              behavior: 'smooth'
          });
      } else {
          $('#f9_practice_phone_number_error_message').addClass('displayNone');
      }
      */
    }


    function checkEmail1() {
        console.log('checkEmail');
        var email = $('#f9_practice_email_address').val();
        var emailPattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+.([a-zA-Z])+([a-zA-Z])+/;
        var emailIsValid = emailPattern.test(email);

            $.ajax({ url: '/email_verification_ajax_recruiters.php',
                data: "email=" + $('#f9_practice_email_address').val(),
                type: 'post'
            }).done(function(msg) {
                var json = JSON.parse(msg);
                return json.result
            });        
        /*
        if ($('#f9_practice_email_address').val().length == 0 || emailIsValid == false) {
            $('#f9_practice_email_address_error_message').removeClass('displayNone');
            $('#email_f9_practice_email_address_exist').addClass('displayNone');
            //var x = document.getElementById("errors");
            //x.style.display = "block";
            console.log('email-invalid');
            return false
        } else {
            
            //$('#f9_practice_email_address_error_message').addClass('displayNone');
            //console.log('valid-email');
            $.ajax({ url: '/email_verification_ajax_recruiters.php',
                data: "email=" + $('#f9_practice_email_address').val(),
                type: 'post'
            }).done(function(msg) {
                var json = JSON.parse(msg);
                return json.result
            });
        }
        */
    }  

<?php echo '</script'; ?>
>

<?php }
}
