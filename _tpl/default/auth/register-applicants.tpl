{include file="1.5/layout/sjs-header.tpl"}

<div class="contact-us create-profile">
	<div class="container">
		<h2>{$translations.apply.create_profile_headline}</h2>

		<form role="form" method="post" action="{$BASE_URL}{$URL_REGISTER_APPLICANTS}" enctype="multipart/form-data" >
			<input type="hidden" id="external_links" name="external_links" value="0" />

			<div class="row">
				<div class="col-md-6 col-xs-12">
						<div style="display: none;">
						<h3>{$translations.apply.name}</h3>
						<input id="apply_name" name="apply_name" type="text" maxlength="500">
						</div>
						<h3>{$translations.apply.f9_first_name}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_first_name" name="f9_first_name" type="text" maxlength="500">

						<h3>{$translations.apply.f9_middle_name}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_middle_name" name="f9_middle_name" type="text" maxlength="500">
						
						<h3>{$translations.apply.f9_last_name}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_last_name" name="f9_last_name" type="text" maxlength="500">

						<h3>{$translations.apply.f9_gender}<red style="color: red;padding: 10px;">*</red></h3>
					<!--<input required id="f9_gender" name="f9_gender" type="text" maxlength="500">-->
					<div class="row" style="float: left;">
					    <div class="col-md-6">
					    	<div class="tos">
								<label><input name="f9_gender" value="Male" type="radio" class="checkbox-g"></input> 
								<h4>Male</h4></label>
							</div>
					    </div>
					    <div class="col-md-6" style="margin-left: -70px;">
							<div class="tos">
								<label><input name="f9_gender" value="Female" type="radio" class="checkbox-g"></input> 
								<h4>Female</h4></label>
							</div>
						</div>
				  </div>
						
						<h3>{$translations.apply.f9_address_1}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_address_1" name="f9_address_1" type="text" maxlength="500">
						
						<h3>{$translations.apply.f9_address_2}</h3>
						<input required id="f9_address_2" name="f9_address_2" type="text" maxlength="500">


						<h3>Cellphone number<red style="color: red;padding: 10px;">*</red></h3>
					<input type="tel" id="apply_phone" name="apply_phone">
					
					<h3>Cellphone carrier<red style="color: red;padding: 10px;">*</red></h3>
					<select id="phone_carrier" name="phone_carrier" onchange="changeFunc();" style="
						-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    padding: 10px;
					    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    min-height: 47px;
					    width: 75%;
					    border-radius: 25px;
					    padding: 0% 3% 0% 6%;
					    border: none;
					    float: left;
					    margin-bottom: 7%;
					    font-size: 14px;
					    padding: 10px;">
							<option value="Sprint">Sprint</option>
							<option value="Verizon">Verizon</option>
							<option value="T-Mobile">T-Mobile</option>
							<option value="MetroPCS">MetroPCS</option>
							<option value="AT&T">AT&T</option>
							<option value="Boost Mobile">Boost Mobile</option>
							<option value="Mint Mobile">Mint Mobile</option>
							<option value="Simple Mobile">Simple Mobile</option>
							<option value="FreedomPop">FreedomPop</option>
							<option value="Tello">Tello</option>
							<option value="UNREAL Mobile">UNREAL Mobile</option>
							<option value="Twigby">Twigby</option>
							<option value="US Mobile">US Mobile</option>
							<option value="Consumer Cellular">Consumer Cellular</option>
							<option value="Total Wireless">Total Wireless</option>
							<option value="FreeUp Mobile">FreeUp Mobile</option>
							<option value="Red Pocket">Red Pocket</option>
							<option value="TextNow">TextNow</option>
							<option value="Straight Talk">Straight Talk</option>
							<option value="XFINITY Mobile">XFINITY Mobile</option>
							<option value="Ting">Ting</option>
							<option value="H20 Wireless">H20 Wireless</option>
							<option value="TracFone">TracFone</option>
							<option value="Net10">Net10</option>
							<option value="Ultra Mobile">Ultra Mobile</option>
							<option value="Virgin Mobile">Virgin Mobile</option>
							<option value="Page Plus">Page Plus</option>
						</select>

						<div style="display: none;">
						<h3>{$translations.apply.portfolio}</h3>
						<input type="text" id="portfolio" name="portfolio" maxlength="500">
						

						<h3>{$translations.apply.location}</h3>
						<input type="text" id="apply_location" name="apply_location" >
						</div>

						<h3>{$translations.apply.f9_city}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_city" name="f9_city" type="text" maxlength="500">

						<h3>{$translations.apply.f9_zip}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_zip" name="f9_zip" type="text" maxlength="500">
						
						<h3>{$translations.apply.f9_state}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_state" name="f9_state" type="text" maxlength="500">
						
						<h3>{$translations.apply.f9_country}<red style="color: red;padding: 10px;">*</red></h3>
						<input required id="f9_country" name="f9_country" type="text" maxlength="500">

					<div style="display: none;">
					<h3>{$translations.apply.f9_isProfile}</h3>
					<input id="f9_isProfile" name="f9_isProfile" type="text" maxlength="500">
					</div>

					<h3>{$translations.apply.f9_language}<red style="color: red;padding: 10px;">*</red></h3>
					<input required id="f9_language" name="f9_language" type="text" maxlength="500">

					<h3>{$translations.apply.f9_yrs_experience}<red style="color: red;padding: 10px;">*</red></h3>
						<select id="f9_yrs_experience" name="f9_yrs_experience" style="
						-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    padding: 10px;
					    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    min-height: 47px;
					    width: 75%;
					    border-radius: 25px;
					    padding: 0% 3% 0% 6%;
					    border: none;
					    float: left;
					    margin-bottom: 7%;
					    font-size: 14px;
					    padding: 10px;">
					    <option value="0-2 yrs">(0-2 yrs)</option>
					 	<option value="3-5 yrs">(3-5 yrs)</option>
					  	<option value="6-10 yrs">(6-10 yrs)</option>
					  	<option value="11+ yrs">(11+ yrs)</option>
						</select>

				</div>

				<div class="col-md-6 col-xs-12">
					
					<h3>Photo<red style="color: red;padding: 10px;">*</red></h3>
					<img src="{$BASE_URL}{$applicant.f9_photo}" id="image" class="img" style="width:240px; height:240px;-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);-moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);">
					<label id="applicantCvLabel" for="f9_photo" class="fullD" style="width: 59% !important;float: left;height: 50px;line-height: 1;border-radius: 0px!Important;">{$translations.profile.form_upload}</label>
					
					<input accept=".gif,.JPG,.jpg,.jpeg,.png" onchange="showImage.call(this)" type="file" name="f9_photo" id="f9_photo" class="form-control inputfile minput" />
					
					<h3>Email<red style="color: red;padding: 10px;">*</red></h3>
					<input required type="email" id="apply_email" name="apply_email" maxlength="300">
					<div id="feedback-err-email_apply_email" class="negative-feedback displayNone">* Email format is invalid...</div>

					<h3>Repeat email<red style="color: red;padding: 10px;">*</red></h3>
					<input required type="email" id="re_email" name="re_email" maxlength="300">
					<div id="feedback-err-email_re_email" class="negative-feedback displayNone">* Email format is invalid...</div>
					<div id="feedback-err-email" class="negative-feedback displayNone">* Email format is invalid or mismatch</div>

					<h3>Password<red style="color: red;padding: 10px;">*</red></h3>
					<input required type="password" id="pass1" name="pass1"  maxlength="50" >
					<div id="feedback-err-pass1" class="negative-feedback displayNone">Minimum password of 6 characters...</div>
					
					<h3>Repeat-password<red style="color: red;padding: 10px;">*</red></h3>
					<input required type="password" id="pass2" name="pass2"  maxlength="50" >
					<div id="feedback-err" class="negative-feedback displayNone">Incorrect password...</div>

					<div style="display: none;">
					<h3>{$translations.apply.occupation_placeholder}</h3>
					<input placeholder="{$translations.apply.occupation_desc}" type="text" id="occupation" name="occupation" maxlength="500">
					</div>

				</div>

			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div style="display: none;">
					<h3>{$translations.js.skills_label}</h3>
					<div class="profile-taggl minput textarea clearfix skillsTaggle"></div>
					</div>
					<div class="row" style="float: left;">
					    <div class="col-md-6">
					    	<h3>Position Title<red style="color: red;padding: 10px;">*</red></h3>
							<select class="fullW form-control minput" style="-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);-moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);padding: 10px;box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);border: 1px solid #ddd;">
					          <option value="volvo">Dental Assistant (DA)</option>
					 		  <option value="saab">(FO)</option>
					  		  <option value="mercedes">Doctor of Dental Surgery (DDS)</option>
					  		  <option value="audi">Registered Dental Hydrant (RDH)</option>
							</select>
					    </div>
					    <div class="col-md-6">
						    <h3>Choose two Position:<red style="color: red;padding: 10px;">*</red></h3>
							<div class="tos">
								<label><input name="f9_position[]" onclick="return KeepCount()" value="DA" type="checkbox" class="checkbox-p"></input> 
								<h4>Dental Assistant (DA)</h4></label>
								
								<label><input name="f9_position[]" onclick="return KeepCount()" value="FO" type="checkbox" class="checkbox-p"></input> 
								<h4>(FO)</h4></label>
								
								<label><input name="f9_position[]" onclick="return KeepCount()" value="DDS" type="checkbox" class="checkbox-p"></input> 
								<h4>Doctor of Dental Surgery (DDS)</h4></label>
								
								<label><input name="f9_position[]" onclick="return KeepCount()" value="RDH" type="checkbox" class="checkbox-p"></input> 
								<h4>Registered Dental Hygienist (RDH)</h4></label>
							</div>
						</div>
				  </div>

					<h3>About you<red style="color: red;padding: 10px;">*</red></h3>
					<textarea required id="apply_msg" name="apply_msg" maxlength="500" rows="8" cols="50"></textarea>
					<div class="textarea-feedback tal" id="textarea_feedback"></div>

					<div class="clear-both"></div>

					<p>
						<label id="cvLabel" for="cv">{$translations.apply.cv_label}</label>
						<input accept=".doc,.docx, .pdf" name="cv" id="cv" class="inputfile" type="file" /><span class="cv-hint">{$cv_hint}</span><br />
						<span id="err" class="negative-feedback displayNone ml0 mt15">{$translations.apply.cv_err}</span>
				    </p>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="SMLinkDiv">
						<a id="addLink" class="green" onclick="return SimpleJobScript.addExternalLink();" href="#">{$translations.js.add_social_media}</a>
						<div id="addLinkBlock"></div>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-6 col-xs-12">						

						{if $ENABLE_RECAPTCHA}
						{$captcha_html}
						<div id="captcha_err" class="negative-feedback displayNone ml0" >{$translations.apply.captcha_empty_err}</div>
						{/if}

						<div class="tos">
							<label><input required type="checkbox" class="checkbox-custom"></input> 
							<h4>{$translations.registration.accept_part1} <a target="_blank" href="{$BASEURL}{TERMS_CONDITIONS_URL}"> {$translations.registration.accept_part2}</a></h4></label>
						</div>

				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12">

					<button type="submit" class="btn" id="save" onclick="return SimpleJobScript.createProfileValidation({$MAX_CV_SIZE});">{$translations.website_general.top_menu_register_label}</button>

				</div>
			</div>

			</div>
		</form>



</div>

{literal}
<script type="text/javascript">
	$(document).ready(function() {
		SimpleJobScript.I18n = {/literal}{$translationsJson}{literal};
		SimpleJobScript.initApplyValidation();

		$('#cv').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#cvLabel').html(fname);
			else
				$('#cvLabel').html($('#cvLabel').html());
        });
	});
</script>
{/literal}

{literal}
<script type="text/javascript">
		function showImage(){
			if(this.files && this.files[0]){
			var obj = new FileReader();
				obj.onload = function(data){
					var image = document.getElementById("image");
					image.src = data.target.result;
				}
				obj.readAsDataURL(this.files[0]);
			}else{
				$("#image").attr("src","uploads/images/Generic-Profile.jpg");
			}
		}
		$("#image").attr("src","uploads/images/Generic-Profile.jpg");
</script>		
{/literal}

{literal}
<script type="text/javascript">
	  $('.checkbox-p').on('change', function() {
	   if($('.checkbox-p:checked').length > 2) {
	       this.checked = false;
	   }
	});
  </script>
  {/literal}

  {literal}
	<script type="text/javascript">
	  $('.checkbox-g').on('change', function() {
	   if($('.checkbox-g:checked').length > 1) {
	       this.checked = false;
	   }
	});
	  </script>
  {/literal}

  {literal}
	<script type="text/javascript">
	  	$(document).ready(function(){
		  	$("#apply_email").keyup(function(){
		  	var apply_email = $('#apply_email').val();
		  	var re_email = $('#re_email').val();

		  	function validateEmail(email) {
			  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			  return re.test(email);
			}
			if (!validateEmail(apply_email)) {
				$('#feedback-err-email_apply_email').removeClass('displayNone');
			}
			else{
				$('#feedback-err-email_apply_email').addClass('displayNone');
				$('#feedback-err-email').addClass('displayNone');
				$('#feedback-err-email_re_email').addClass('displayNone');
			}
			});
		});
	  </script>
  {/literal}

  {literal}
	<script type="text/javascript">
	  	$(document).ready(function(){
		  	$("#re_email").keyup(function(){
		  	var apply_email = $('#apply_email').val();
			var re_email = $('#re_email').val();

			if(apply_email != re_email){
				$('#feedback-err-email').removeClass('displayNone');
			}
			else{
				$('#feedback-err-email').addClass('displayNone');
				$('#feedback-err-email_re_email').addClass('displayNone');
			}
			});
		});
	  </script>
  {/literal}

  {literal}
	<script type="text/javascript">
	  	$(document).ready(function(){
	  		$('#feedback-err-pass1').removeClass('displayNone');
				$("#pass1").keyup(function(){
					if($('#pass1').val().length >= 6){
						$('#feedback-err-pass1').addClass('displayNone');
					}
			    	else {
			    		$('#feedback-err-pass1').removeClass('displayNone');
			    		}
					});
				});
	  </script>
  {/literal}

   {literal}
	<script type="text/javascript">
	  	$(document).ready(function(){
			 $("#pass2").keyup(function(){
				  if ($('#pass1').val() != $('#pass2').val()) {
						$('#feedback-err').removeClass('displayNone');
			    	} 
			    	else {
			    		$('#feedback-err').addClass('displayNone');
			    			$('#feedback-err-pass1').addClass('displayNone');
			    		}
					});
				});
	  </script>
  {/literal}

{include file="1.5/layout/sjs-footer.tpl"}