
<form id="register-form" name="register-form" method="post" action="{$BASE_URL}{$URL_REGISTER_RECRUITERS}" role="form" enctype="multipart/form-data">
	<input type="hidden" name="step" id="step" value="2">
	<input type="hidden" name="employerid" id="employerid" value="{$employer_id}">
	<input type="hidden" name="employer_hash" id="employer_hash" value="{$employer_hash}">
	<input type="hidden" name="employer_email" id="employer_email" value="{$employer_email}">
	<input type="hidden" name="updatingEntry" id="updatingEntry" value="{$updatingEntry}">

	<div class="col-md-6 col-xs-12">

			<h3>{$translations.registration.company_name}</h3>
			<input required name="company_name" id="company_name" type="text" maxlength="300" />

			<h3>{$translations.registration.f9_practice_name}</h3>
			<input name="f9_practice_name" id="f9_practice_name" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_practice_type}</h3>
			<input name="f9_practice_type" id="f9_practice_type" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_address_1}</h3>
			<input name="f9_address_1" id="f9_address_1" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_address_2}</h3>
			<input name="f9_address_2" id="f9_address_2" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_city}</h3>
			<input name="f9_city" id="f9_city" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_state}</h3>
			<input name="f9_state" id="f9_state" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_zip}</h3>
			<input name="f9_zip" id="f9_zip" type="text" maxlength="1000" />

			<h3>{$translations.registration.company_hq_label}</h3>
			<input required name="company_hq" id="company_hq" type="text" maxlength="400" />

			<h3>{$translations.registration.company_street_label}</h3>
			<input name="company_street" id="company_street" type="text" maxlength="500" />

			<h3>{$translations.registration.company_citypostcode_label}</h3>
			<input name="company_citypostcode" id="company_citypostcode" type="text" maxlength="500" />

			<button type="submit" class="btn b2h btn-general" name="submit" id="submit">{$translations.registration.submit_final}</button>

	</div>

	<div class="col-md-6 col-xs-12">

			<h3>{$translations.registration.company_url_label}</h3>
			<input name="company_url" id="company_url" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_practice_phone_number}</h3>
			<input name="f9_practice_phone_number" id="f9_practice_phone_number" type="text" maxlength="1000" />

			<h3>{$translations.registration.company_url_label}</h3>
			<input name="f9_cellphone_number" id="f9_cellphone_number" type="text" maxlength="1000" />

			<h3>Cellphone Provider</h3>
					<select class="form-control minput" name="f9_cellphone_provider" id="f9_cellphone_provider" style="
						-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    -moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    padding: 10px;
					    box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);
					    widows: 307px;
					    min-height: 47px;
					    width: 100%;
					    border-radius: 25px;
					    padding: 0% 3% 0% 6%;
					    border: none;
					    float: left;
					    margin-bottom: 7%;
					    font-size: 14px;
					    padding: 10px;">
							<option value="Sprint">Sprint</option>
							<option value="Sprint">Verizon</option>
							<option value="Sprint">T-Mobile</option>
							<option value="Sprint">MetroPCS</option>
							<option value="Sprint">AT&T</option>
							<option value="Sprint">Boost Mobile</option>
							<option value="Sprint">Mint Mobile</option>
							<option value="Sprint">Simple Mobile</option>
							<option value="Sprint">FreedomPop</option>
							<option value="Sprint">Tello</option>
							<option value="Sprint">UNREAL Mobile</option>
							<option value="Sprint">Twigby</option>
							<option value="Sprint">US Mobile</option>
							<option value="Sprint">Consumer Cellular</option>
							<option value="Sprint">Total Wireless</option>
							<option value="Sprint">FreeUp Mobile</option>
							<option value="Sprint">Red Pocket</option>
							<option value="Sprint">TextNow</option>
							<option value="Sprint">Straight Talk</option>
							<option value="Sprint">XFINITY Mobile</option>
							<option value="Sprint">Ting</option>
							<option value="Sprint">H20 Wireless</option>
							<option value="Sprint">TracFone</option>
							<option value="Sprint">Net10</option>
							<option value="Sprint">Ultra Mobile</option>
							<option value="Sprint">Virgin Mobile</option>
							<option value="Sprint">Page Plus</option>
						</select>

			<h3>{$translations.registration.f9_practice_email_address}</h3>
			<input name="f9_practice_email_address" id="f9_practice_email_address" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_job_app_email_address}</h3>
			<input name="f9_job_app_email_address" id="f9_job_app_email_address" type="text" maxlength="1000" />

			<h3>{$translations.registration.f9_notification_preference}</h3>
			<input name="f9_notification_preference" id="f9_notification_preference" type="text" maxlength="1000" />

			<h3>{$translations.registration.company_desc}</h3>		
			<textarea class="noTinymceTA" required name="company_desc" id="company_desc" maxlength="1000" rows="6" cols="42">{if $current_form}{$current_form.message}{/if}</textarea>
			<div class="textarea-feedback" id="textarea_feedback"></div>

			<div class="logo-upload-div">
				<label id="logoLabel" for="company_logo">{$translations.registration.company_logo_label}</label>
			 	<input type="file" name="company_logo" id="company_logo" class="form-control inputfile minput" />
			 	<div class="textarea-feedback mb25" >{$translations.registration.company_logo_hint}</div>

			 	<div id="uploadPreview"></div>
			 	<div id="logo-err" class="negative-feedback displayNone mt25">{$translations.registration.logo_err_msg}</div>
			 	<div id="logo-err2" class="negative-feedback displayNone mt25">{$translations.registration.logo_err_samesize}</div>
			 	<div id="logo-ok" class="pos-feedback-registration displayNone mt25">{$translations.registration.logo_ok}</div>
			</div>

	</div>

	<div class="col-md-12 col-xs-12">
		<br /><br /><br /><br />
	</div>

</form>

<script type="text/javascript">
	$(document).ready(function() {
		SimpleJobScript.I18n = {$translationsJson};
	    SimpleJobScript.initRegisterFormStep2Validation();

	    $('#company_logo').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#logoLabel').html(fname);
			else
				$('#logoLabel').html($('#logoLabel').html());
       });
	});
</script>