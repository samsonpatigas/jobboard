
<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            {$translations.profile.edit_subheadline} {$translations.profile.edit_label}
        </p>
    </div>
</div>
	
<form role="form" action="/{$URL_PROFILE}/profile-edited" method="post" enctype="multipart/form-data" >
	<input type="hidden" id="external_links" name="external_links" value="{$applicant.sm_links|@count}" />
  
	<div class="row tal">
		<div class="col-md-6 col-sm-12">

		   <label class="fullW">Email: </label>
		   <input required class="minput opaque" value="{$applicant.email}" name="apply_email" id="apply_email" maxlength="50" type="text"  /> 
		   <br />

		   <label>Full name:</label>
		   <input required class="minput opaque" value="{$applicant.fullname}" name="fullname" id="fullname" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_first_name}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_first_name}" name="f9_first_name" id="f9_first_name" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_middle_name}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_middle_name}" name="f9_middle_name" id="f9_middle_name" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_last_name}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_last_name}" name="f9_last_name" id="f9_last_name" maxlength="50" type="text"  /> 
		   <br />

		    <label class="fullW">{$translations.profile.f9_gender}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_gender}" name="f9_gender" id="f9_gender" maxlength="50" type="text"  /> 
		   <!-- <div class="tos">
				<label class="fullW"><input id="f9_gender" name="f9_gender[]" checked="checked" value="Male" type="checkbox" class="checkbox-custom"></input> 
					<h4 style="line-height: 2;">{$applicant.f9_gender}</h4></label>
				</div> -->
		   <br />

		   <label class="fullW">{$translations.profile.f9_address_1}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_address_1}" name="f9_address_1" id="f9_address_1" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_address_2}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_address_2}" name="f9_address_2" id="f9_address_2" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">Cellphone number:</label>
		   <input class="minput opaque" value="{$applicant.phone}" name="phone" id="phone" maxlength="50" type="text"  />
		    <br />

		    <label class="fullW">Cellphone carrier:</label>
		   <input class="minput opaque" value="{$applicant.phone_carrier}" name="phone_carrier" id="phone_carrier" maxlength="50" type="text"  />
		    <br />
		   
		   <label class="fullW">{$translations.profile.f9_city}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_city}" name="f9_city" id="f9_city" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_state}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_state}" name="f9_state" id="f9_state" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_zip}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_zip}" name="f9_zip" id="f9_zip" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_country}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_country}" name="f9_country" id="f9_country" maxlength="50" type="text"  /> 
		   <br />

		   <label class="fullW">{$translations.profile.f9_language}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_language}" name="f9_language" id="f9_language" maxlength="50" type="text"  /> 
		   <br />

		    <label class="fullW">{$translations.apply.f9_yrs_experience}:</label>
		   <input required class="minput opaque" value="{$applicant.f9_yrs_experience}" name="f9_yrs_experience" id="f9_yrs_experience" maxlength="50" type="text"  /> 
		   <br />

		   <div style="display: none;">
		   <label>{$translations.profile.form_name}:</label>
		   <input class="minput opaque" value="{$applicant.fullname}" name="fullname" id="fullname" maxlength="50" type="text"  /> 
		   <br />


		   <label>{$translations.apply.occupation_label}:</label>
		   <input class="minput opaque" value="{$applicant.occupation}" name="occupation" id="occupation" maxlength="200" type="text"  /> 
		   <br />
			</div>

		    <div style="display: none;">
		   <label class="fullW">{$translations.dashboard_recruiter.post_location_label}</label>
		   <input class="minput opaque" value="{$applicant.location}" name="location" id="location" maxlength="400" type="text"  />
		    <br />


		   <label>{$translations.profile.form_weblink}:</label>
		   <input class="minput opaque" value="{$applicant.weblink}" name="weblink" id="weblink" maxlength="50" type="text"  />
		    <!-- <br /> -->
 
			<!-- <div class="tos">
					<label class="fullW"><input name="public_profile" id="public_profile" {if $applicant.public_profile == 1}checked{/if} type="checkbox" class="checkbox-custom"></input> 
					<h4 class="pt9">Suspend this Account</h4></label>
			</div>  -->
			<br />
			</div>

			<div class="tos">
					<label class="fullW"><input name="subscription_flag" id="subscription_flag" {if $subs_data == 1}checked{/if} type="checkbox" class="checkbox-custom"></input> 
					<h4 class="pt9">{$translations.profile.form_subscription}</h4></label>
			</div> 
		</div>

		<div class="col-md-6 col-sm-12 m-mobt8">
		
		   <label class="fullW">{$translations.profile.f9_photo}:</label>
		   
		   <div class="alert alert-danger alert-dismissible" id="myAlert" style="font-size: 14px;display:none; width: 250px;margin-bottom: 0px;border-radius:0px;border-top: solid 2px #fd5f60;color: #ff0003;padding: 8px;">
				<!--<a href="#" class="close">&times;</a>-->
				<span class="fa fa-exclamation-circle fa-fw"></span>
				Click save to update your image...
		  </div>
		
			<div class="container-hover">
			  <img src="{$BASE_URL}{$applicant.f9_photo}" id="image" name="image" class="img" style="width:240px; height:240px;-webkit-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);-moz-box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);box-shadow: 0px 5px 20px 1px rgba(0, 0, 0, 0.10);">
			  <label for="f9_photo">
			  <div class="middle">
				<div class="text">Upload Photo</div>
				</div>
			  </label>
			</div>
		   <br>
		   <input accept=".gif,.JPG,.jpg,.jpeg,.png" onchange="showImage.call(this)" type="file" name="f9_photo" id="f9_photo" class="form-control inputfile minput"/>
		   <br />

			<label>{$translations.profile.form_desc}</label>
			<textarea class="noTinymceTA minput opaque pTextArea" required id="msg" name="msg" maxlength="500" rows="8" cols="50">{$applicant.message}</textarea>
			 <br />

			 <div style="display: none;">
			<label class="fullW">{$translations.profile.skills}</label>
			<div class="input textarea clearfix profileEditTaggle minput edit-profile"></div>
			</div>

			 <div class="row">
				 <div class="col-md-6 col-sm-12">
				 	<label class="mob-mb5">{$translations.profile.form_cv} </label>
				 </div>

				 <div class="col-md-6 col-sm-12">
			 		{if $applicant.cv_path == '' or $applicant.cv_path == '-'}
			  		<i class="fa fa-close opaque-fa" aria-hidden="true"></i>
			  		{else}
			  		<a href="{$BASE_URL}{$applicant.cv_path}">
			  			<i class="{$img_path}" aria-hidden="true"></i>
			  		</a>
			  		{/if}
				 </div>
			 </div>
			 <br /><br />

			 <div class="row">
				 <div class="col-md-12 col-sm-12">
				 	<label>{$translations.profile.form_newcv} </label>
				 	
				 </div>
				 <br /> <br />

				 <div class="col-md-12 col-sm-12">
					<label id="applicantCvLabel" for="applicantCv">{$translations.profile.form_upload}</label>
				  	<input accept=".pdf,.doc,.docx" type="file" name="applicantCv" id="applicantCv" class="form-control inputfile minput" />
				  	<input type="hidden" id="currentCv" name="currentCv" value="{$applicant.cv_path}" />
				 </div>
				 <br><br>
				<button type="button" id="Inactivate" class="btn mbtn zeromlplLeft" style="margin-top: 60px;">Deactivate Account</button>			
				<button type="button" id="Suspend" class="btn mbtn zeromlplLeft" style="margin-top: 25px;">Suspend Account</button>	
				<div id="Inactivate_modal" class="modal fade">
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="modal-header">
			                		<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="background-color:#46CAC6 !important;padding-left: 5px;padding-right: 5px;">&times;</button> -->
			                    <h4 class="modal-title">Confirmation</h4>
			                </div>
			                <div class="modal-body">
			                	<label class="fullW"><input name="deactivate_check" id="deactivate_check" type="checkbox" class="checkbox-custom"><p>Are you sure you want to deactivate your Account?</p></input></label>
			                    
			                </div>
			                <div class="modal-footer">
			                    <button type="button" class="btn btn-default" data-dismiss="modal" style="padding: 4px;font-size: inherit;"><h4 class="modal-title">No</h4></button>
			                    <button type="button" class="btn btn-primary" style="padding: 4px;font-size: inherit;"><h4 class="modal-title">Yes</h4></button>
			                </div>
			            </div>
			        </div>
			    </div> 

			    <div id="Suspend_modal" class="modal fade">
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="modal-header">
			                		<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="background-color:#46CAC6 !important;padding-left: 5px;padding-right: 5px;">&times;</button> -->
			                    <h4 class="modal-title">Confirmation</h4>
			                </div>
			                <div class="modal-body">
			                	<div class="tos">
									<label class="fullW"><input name="suspend_check" id="suspend_check" onclick="myFunction()" type="checkbox" 	class="checkbox-custom"><p>Are you sure you want to Suspend your Account?</p></input></label>
									</div>
			                    <div id="date_from_to" style="display: none;">
				                    <label>From :</label>
					                    <input type="date" id="from" name="from" class="date_suspend">

					                    <label>To :</label>
				                    <input type="date" id="to" name="to" class="date_suspend">
				               
			                    </div>
			                </div>
			                <div class="modal-footer">
			                    <button type="button" class="btn btn-default" data-dismiss="modal" style="padding: 4px;font-size: inherit;"><h4 class="modal-title">Cancel</h4></button> 
			                    <button type="button" class="btn btn-primary" style="padding: 4px;font-size: inherit;"><h4 class="modal-title">Suspend</h4></button>
			                	</div> 
			            	</div>
			            </div>
			        </div>
			    </div> 
			 </div>
		 </div>

		  <div class="col-md-12 col-sm-12 mt50">
				<a id="esmId" class="green" onclick="return SimpleJobScript.showSMfields();" >{$translations.js.edit_social_media}</a>


				<div id="showSMfieldsBlockId" class="displayNone">

					<div id="SMsubBlock">
				 {foreach $applicant.sm_links as $obj}
				 	<div id="sm_fg_{$obj@iteration}" class="form-group mb30">
				 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl0 ml0 mb20">
					 		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pl0 ml0">
								<div class="form-group">
								   <input value="{$obj->linkToShow}" name="sm_url_{$obj@iteration}" id="sm_url_{$obj@iteration}" type="text" class="form-control grayInput minput"  /> 
								</div>
							</div>

							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pl0 ml20-desk">
								<div class="form-group">
								   <select id="sm_select_{$obj@iteration}" name="sm_select_{$obj@iteration}" class="form-control minput">
								   {foreach $SM_PROFILES as $ITEM}
									  <option {if $obj->smId == $ITEM.id}selected{/if} value="{$ITEM.id}">{$ITEM.name}</option>
								   {/foreach}
								   </select>
								</div>
							</div>

							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
									<a id="sm_close_{$obj@iteration}" class="green" href="#sm_close_{$obj@iteration}" onclick="return SimpleJobScript.removeProfileSMfield('{$obj@iteration}');"><i class="fa fa-close mt18" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				 {/foreach}
				 	</div>

				 	<div class="form-group mb30">
						<a id="addSMLink" class="green" onclick="return SimpleJobScript.addProfileSMLink();" >{if $applicant.sm_links|@count lt 4}{$translations.js.add_social_media}{else}<span class="limr">{$translations.js.limit_reached}</span>{/if}</a>
					</div>

    				</div>
    		  </div>

		<div class="row mb50">
		    <div class="col-md-6 col-sm-12">
			<button type="submit" class="btn mbtn zeromlplLeft mt50" name="submit" id="submit" >{$translations.website_general.text_save}</button>
		    </div>
		</div>

</form>

<script type="text/javascript">
{literal}
{/literal}
   {foreach $applicant.sm_links as $obj}
 {literal}
 	var elIndex = {/literal}{$obj@iteration}{literal} - 1;
	if ($('#sm_close_' + elIndex).length) {
		$('#sm_close_' + elIndex).addClass('displayNone');
	}
 {/literal}
   {/foreach}
 {literal}

	 $('#applicantCv').change(function() {
		 var fname = $('input[type=file]').val().split('\\').pop();
		 if( fname )
			$('#applicantCvLabel').html(fname);
		 else
			$('#applicantCvLabel').html($('#applicantCvLabel').html());
		 });
{/literal}
</script>

{literal}
<script type="text/javascript">
	$("input#subscription_flag").click(function(e){
		e.preventDefault();
		window.location = "http://jobboard.f9portal.net/subscribe?email={$applicant.email}";
});
</script>
{/literal}

{foreach $applicant.sm_links as $obj}
   {literal}
	   <script type="text/javascript">
		 	var elIndex = {/literal}{$obj@iteration}{literal} - 1;
			if ($('#sm_close_' + elIndex).length) {
				$('#sm_close_' + elIndex).addClass('displayNone');
			}
		</script>
	{/literal}
{/foreach}

 {literal}
<script type="text/javascript">
	 $('#applicantCv').change(function() {
		 var fname = $('input[type=file]').val().split('\\').pop();
		 if( fname )
			$('#applicantCvLabel').html(fname);
		 else
			$('#applicantCvLabel').html($('#applicantCvLabel').html());
		 });
</script>
{/literal}

	{literal}
	<script type="text/javascript">
		function showImage(){
			 $('#image').change(function () {
		         var filePath=$('#f9_photo').val(); 
		     });
			if(this.files && this.files[0]){
			var obj = new FileReader();
				obj.onload = function(data){
					var image = document.getElementById("image");
					image.src = data.target.result;
				}
				obj.readAsDataURL(this.files[0]);
				$("#myAlert").show();
				<!--$("#myAlert").first().hide().fadeIn(200).delay(2000).fadeOut(1000, function () { $(this).hide(); });-->
			}else{
				$("#image").attr("src","../uploads/images/Generic-Profile.jpg");
				$("#myAlert").first().hide();
			}
			/*$("#image").attr("src","../uploads/images/Generic-Profile.jpg");*/
		}
		
	</script>			
	{/literal}
	
	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$(".close").click(function(){
				$("#myAlert").alert("close");
			});
		});
	</script>
	{/literal}

	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$("#Inactivate").click(function(){
				$("#Inactivate_modal").modal('show');
      			document.body.style.overflow='scroll';
      			$("#Suspend_modal").modal('hide');
      			document.getElementById("suspend_check").checked = false;
      			document.getElementById("date_from_to").style.display = "none";
			});
		});
	</script>
	{/literal}

	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$("#Suspend").click(function(){
				$("#Suspend_modal").modal('show');
      			document.body.style.overflow='scroll';
      			$("#Inactivate_modal").modal('hide');
      			document.getElementById("deactivate_check").checked = false;
			});
		});
	</script>
	{/literal}

	{literal}
	<script type="text/javascript">
		function myFunction() {
		  var checkBox = document.getElementById("suspend_check");
		  var text = document.getElementById("date_from_to");
		  if (checkBox.checked == true){
		    text.style.display = "block";
		  } else {
		    text.style.display = "none";
		  }
		}
	</script>
	{/literal}

	{literal}
	<script type="text/javascript">
		$(document).ready( function() {
		    var now = new Date();
		    var month = (now.getMonth() + 1);               
		    var day = now.getDate();
		    if (month < 10) 
		        month = "0" + month;
		    if (day < 10) 
		        day = "0" + day;
		    var today = now.getFullYear() + '-' + month + '-' + day;
		    $('#from').val(today);
		    document.getElementById('to').setAttribute("min",today);
		    document.getElementById('from').setAttribute("min",today);
		});
	</script>
	{/literal}

	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			var img = $('#image').attr('src');
			var pieces = img.split(/[\s/]+/);
			var f9_photo = pieces[pieces.length-1];
			$('#f9_photo').text(f9_photo);
		});
	</script>
	{/literal}
