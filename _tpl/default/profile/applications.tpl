<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            {$translations.profile.apps_desc}
        </p>
    </div>
</div>

<!--  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse1">Collapsible panel</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">Panel Body</div>
        <div class="panel-footer">Panel Footer</div>
      </div>
    </div>
  </div>

  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse2">Collapsible panel</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Panel Body</div>
        <div class="panel-footer">Panel Footer</div>
      </div>
    </div>
  </div> -->


{$i=0}
{section name=application loop=$apps} 
{$i = $i + 1}

 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse{$i}">{$apps[application].job_title} @ {$apps[application].job_company}</a>
        </h4>
      </div>
      <div id="collapse{$i}" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="fa fa-calendar list-fa fa-lg" aria-hidden="true"></i>
            <p>Date Applied: {$apps[application].created_on}</p>
            <div>
              <p>Date Posted {$apps[application].date_posted}</p>
            </div>
            <div>
              <p>Title: {$apps[application].job_title}</p>
            </div>
            <div>
              <p>City: {$apps[application].city_name}</p>
            </div>
          </div>
        
          <div class="col-md-4 col-sm-4">
                    <div class="listing-type">
                        <p>Status: </p>
                        {if $apps[application].status == '0'}
                            <span class="profile-pending hideMobile">OPEN</span>
                        {else if $apps[application].status == '1'}
                            <span class="profile-rejected hideMobile">CLOSED</span>
                        {else}
                            <span class="profile-reviewed hideMobile">{$translations.profile.review_label}</span>
                        {/if}
                    </div>  
            </div>
        </div>
        <!-- <div class="panel-footer">Panel Footer</div> -->
      </div>
    </div>
  </div>
 {/section}
<br>

<div class="row">
<div class="col-md-12 col-sm-12 mb20">
  <h2>Available jobs</h2>   
  <p>Below are the jobs still available for application</p>
 </div>
</div>
<br>
<br>
{$j=0}
{section name=notapplied loop=$jobs} 
{$j = $j + 1}

 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapses{$j}">{$jobs[notapplied].job_title} @ {$jobs[notapplied].job_company}</a>
        </h4>
      </div>
      <div id="collapses{$j}" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="fa fa-calendar list-fa fa-lg" aria-hidden="true"></i>
            <p>Date Posted: {$jobs[notapplied].created_on}</p>
            <div>
              <p>Title: {$jobs[notapplied].job_title}</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
                    <div class="listing-type">
                        <p>Status: </p>
                        {if $jobs[notapplied].status == '0'}
                            <span class="profile-pending hideMobile">INACTIVE</span>
                        {else $jobs[notapplied].status == '1'}
                            <span class="profile-rejected hideMobile">ACTIVE</span>
                        {/if}
                    </div> 
                    <div>
                        <form action="{$BASE_URL}{$URL_JOB}/{$jobs[notapplied].url_title}/{$jobs[notapplied].job_id}">
                          <input type="submit" value="VIEW DETAILS" class="btn mbtn zeromlplLeft" style="margin-top: 10px;" />
                        </form>
                    </div> 
            </div>
        </div>
        <!-- <div class="panel-footer">Panel Footer</div> -->
      </div>
    </div>
  </div>
 {/section}
