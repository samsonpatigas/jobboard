<?php

require_once '_config/config.php';

if (isset($_POST['rejected'])) {
	$class = new JobApplication();
	$mailer = new Mailer();

	$APP_ID = $_POST['rejected'];

	// change application status to rejected
	$class->rejectApplication($APP_ID);

	// get candidate email and job id
	$data = $class->getCandidateDataByJobApplicationId($APP_ID);

	$job = new Job($data['job_id']);
	$job_data = $job->GetInfo();

	// notify him
	$mailer->rejectCandidateApplication($data['candidate_email'], $job_data);

	echo json_encode(array('result' => '1'));
	redirect_to('http://jobboard.f9portal.net/dashboard/URL_DASHBOARD_SEARCHABLE');
} else {
	echo json_encode(array('result' => '0'));
	redirect_to('http://jobboard.f9portal.net/dashboard/URL_DASHBOARD_SEARCHABLE');
}
exit;

?>