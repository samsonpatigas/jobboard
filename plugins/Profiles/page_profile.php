<?php
	
	$smarty->assign('LOAD_TAGL', true);

	// get SM profiles
	$sm_profiles = $job->getSMprofiles();
	$smarty->assign('SM_PROFILES', $sm_profiles);

	global $db;
	$cl = new Applicant();
	$applicant = $cl->getDataByIdForProfile($_SESSION['applicant']);
	$skills_arr = explode(",", $applicant['skills']);
	
	$smarty->assign('skills_arr', $skills_arr);
	$smarty->assign('profile', $applicant);


	$subs_data = $cl->getSubscriptionDetailsByEmail($applicant['email']);
	$smarty->assign('subs_data', $subs_data);

	if (!empty($id)) {
		switch($id){
			case URL_PROFILE_EDIT:
				$smarty->assign('TAGL_INIT_PROFILE_EDIT', true);
				$explode = explode(".",$applicant['cv_path']);
				if (strcmp(end($explode), "pdf") == 0) {
					$smarty->assign('img_path', "fa fa-file-pdf-o fa-lg pdf-el");
				} else {
					$smarty->assign('img_path', "fa fa-file-word-o fa-lg word-el");
				}

				$applicant['message'] = convertLineBreaks($applicant['message']); 

				$smarty->assign('applicant', $applicant);
				$smarty->assign('ACTIVE', $PROFILE_ROUTING[URL_PROFILE_EDIT]);
				$smarty->assign('title', $translations['profile']['edit_profile']);
			break;
			case 'profile-edited': //adding functionality to save logs of files that were uploaded
				$smarty->assign('TAGL_INIT_PROFILE_EDIT', true);
				escape($_POST);
				$flag = (isset($public_profile)) ? 1 : 0;

				$subs_flag = (isset($subscription_flag)) ? 1 : 0;

				$cl->updateSubscription($applicant['email'], $subs_flag);

				//new cv. upload file and save the path
				if (!empty($_FILES["applicantCv"]['tmp_name'])) {
						$f = pathinfo($_FILES['applicantCv']['name']);
						$f_rename = pathinfo(cleanString($_FILES['applicantCv']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'applicantCv', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['applicantCv']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['cv_path']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['cv_path'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['cv_path']);
								} catch (Exception $e) {}
							}

							$cv_path = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$cv_path = $currentCv;
						}

						if (!empty($_FILES['applicantCv']['error'])) {
							$cv_path = $currentCv;
						}

					} else {
					//cv path not updated
					$cv_path = $currentCv;
				}
				chmod($cv_path, 0777);

			//new Cover Letter upload file and save the path
				if (!empty($_FILES["cover_letter"]['tmp_name'])) {
						$f = pathinfo($_FILES['cover_letter']['name']);
						$f_rename = pathinfo(cleanString($_FILES['cover_letter']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'cover_letter', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['cover_letter']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['cover_letter']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['cover_letter'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['cover_letter']);
								} catch (Exception $e) {}
							}

							$cover_letter = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$cover_letter = $currentCL;
						}

						if (!empty($_FILES['cover_letter']['error'])) {
							$cover_letter = $currentCL;
						}

					} else {
					//cv path not updated
					$cover_letter = $currentCL;
				}
				chmod($cover_letter, 0777);

			//new X-ray upload file and save the path
				if (!empty($_FILES["x_ray"]['tmp_name'])) {
						$f = pathinfo($_FILES['x_ray']['name']);
						$f_rename = pathinfo(cleanString($_FILES['x_ray']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'x_ray', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['x_ray']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['x_ray']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['x_ray'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['x_ray']);
								} catch (Exception $e) {}
							}

							$x_ray = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$x_ray = $currentX_ray;
						}

						if (!empty($_FILES['x_ray']['error'])) {
							$x_ray = $currentX_ray;
						}

					} else {
					//cv path not updated
					$x_ray = $currentX_ray;
				}
				chmod($x_ray, 0777);

			//new Coronal Polish upload file and save the path
				if (!empty($_FILES["coronal_polish"]['tmp_name'])) {
						$f = pathinfo($_FILES['coronal_polish']['name']);
						$f_rename = pathinfo(cleanString($_FILES['coronal_polish']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'coronal_polish', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['coronal_polish']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['coronal_polish']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['coronal_polish'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['coronal_polish']);
								} catch (Exception $e) {}
							}

							$coronal_polish = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$coronal_polish = $currentCP;
						}

						if (!empty($_FILES['coronal_polish']['error'])) {
							$coronal_polish = $currentCP;
						}

					} else {
					//cv path not updated
					$coronal_polish = $currentCP;
				}
				chmod($coronal_polish, 0777);

			//new Laser upload file and save the path
				if (!empty($_FILES["laser"]['tmp_name'])) {
						$f = pathinfo($_FILES['laser']['name']);
						$f_rename = pathinfo(cleanString($_FILES['laser']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'laser', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['laser']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['laser']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['laser'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['laser']);
								} catch (Exception $e) {}
							}

							$laser = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$laser = $currentLaser;
						}

						if (!empty($_FILES['laser']['error'])) {
							$laser = $currentLaser;
						}

					} else {
					//cv path not updated
					$laser = $currentLaser;
				}
				chmod($laser, 0777);

			//new Anesthesia upload file and save the path
				if (!empty($_FILES["anesthesia"]['tmp_name'])) {
						$f = pathinfo($_FILES['anesthesia']['name']);
						$f_rename = pathinfo(cleanString($_FILES['anesthesia']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'anesthesia', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['anesthesia']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['anesthesia']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['anesthesia'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['anesthesia']);
								} catch (Exception $e) {}
							}

							$anesthesia = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$anesthesia = $currentAnesthesia;
						}

						if (!empty($_FILES['anesthesia']['error'])) {
							$anesthesia = $currentAnesthesia;
						}

					} else {
					//cv path not updated
					$anesthesia = $currentAnesthesia;
				}
				chmod($anesthesia, 0777);

			//new Efta upload file and save the path
				if (!empty($_FILES["efta"]['tmp_name'])) {
						$f = pathinfo($_FILES['efta']['name']);
						$f_rename = pathinfo(cleanString($_FILES['efta']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'efta', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['efta']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['efta']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['efta'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['efta']);
								} catch (Exception $e) {}
							}

							$efta = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$efta = $currentEfta;
						}

						if (!empty($_FILES['efta']['error'])) {
							$efta = $currentEfta;
						}

					} else {
					//cv path not updated
					$efta = $currentEfta;
				}
				chmod($efta, 0777);

			//new Hygiene License upload file and save the path
				if (!empty($_FILES["hygiene_license"]['tmp_name'])) {
						$f = pathinfo($_FILES['hygiene_license']['name']);
						$f_rename = pathinfo(cleanString($_FILES['hygiene_license']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log('hygiene_license', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['hygiene_license']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['hygiene_license']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['hygiene_license'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['hygiene_license']);
								} catch (Exception $e) {}
							}

							$hygiene_license = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$hygiene_license = $currentHL;
						}

						if (!empty($_FILES['hygiene_license']['error'])) {
							$hygiene_license = $currentHL;
						}

					} else {
					//cv path not updated
					$hygiene_license = $currentHL;
				}
				chmod($hygiene_license, 0777);

			//new Dentist License upload file and save the path
				if (!empty($_FILES["dentist_license"]['tmp_name'])) {
						$f = pathinfo($_FILES['dentist_license']['name']);
						$f_rename = pathinfo(cleanString($_FILES['dentist_license']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'dentist_license', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['dentist_license']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['dentist_license']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['dentist_license'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['dentist_license']);
								} catch (Exception $e) {}
							}

							$dentist_license = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$dentist_license = $currentDL;
						}

						if (!empty($_FILES['dentist_license']['error'])) {
							$dentist_license = $currentDL;
						}

					} else {
					//cv path not updated
					$dentist_license = $currentDL;
				}
				chmod($dentist_license, 0777);

			//new S9 upload file and save the path
				if (!empty($_FILES["w9"]['tmp_name'])) {
						$f = pathinfo($_FILES['w9']['name']);
						$f_rename = pathinfo(cleanString($_FILES['w9']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'w9', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['w9']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['w9']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['w9'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['w9']);
								} catch (Exception $e) {}
							}

							$w9 = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$w9 = $currentW9;
						}

						if (!empty($_FILES['w9']['error'])) {
							$w9 = $currentW9;
						}

					} else {
					//cv path not updated
					$w9 = $currentW9;
				}
				chmod($w9, 0777);

			//new W4 upload file and save the path
				if (!empty($_FILES["w4"]['tmp_name'])) {
						$f = pathinfo($_FILES['w4']['name']);
						$f_rename = pathinfo(cleanString($_FILES['w4']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'w4', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['w4']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['w4']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['w4'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['w4']);
								} catch (Exception $e) {}
							}

							$w4 = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$w4 = $currentW4;
						}

						if (!empty($_FILES['w4']['error'])) {
							$w4 = $currentW4;
						}

					} else {
					//cv path not updated
					$w4 = $currentW4;
				}
				chmod($w4, 0777);

			//new A4 upload file and save the path
				if (!empty($_FILES["a4"]['tmp_name'])) {
						$f = pathinfo($_FILES['a4']['name']);
						$f_rename = pathinfo(cleanString($_FILES['a4']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'a4', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['a4']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['a4']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['a4'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['a4']);
								} catch (Exception $e) {}
							}

							$a4 = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$a4 = $currentA4;
						}

						if (!empty($_FILES['a4']['error'])) {
							$a4 = $currentA4;
						}

					} else {
					//cv path not updated
					$a4 = $currentA4;
				}
				chmod($a4, 0777);

			//new Direct Deposit upload file and save the path
				if (!empty($_FILES["direct_deposit"]['tmp_name'])) {
						$f = pathinfo($_FILES['direct_deposit']['name']);
						$f_rename = pathinfo(cleanString($_FILES['direct_deposit']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'direct_deposit', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['direct_deposit']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['direct_deposit']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['direct_deposit'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['direct_deposit']);
								} catch (Exception $e) {}
							}

							$direct_deposit = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$direct_deposit = $currentDD;
						}

						if (!empty($_FILES['direct_deposit']['error'])) {
							$direct_deposit = $currentDD;
						}

					} else {
					//cv path not updated
					$direct_deposit = $currentDD;
				}
				chmod($direct_deposit, 0777);

			//new I9 upload file and save the path
				if (!empty($_FILES["i9"]['tmp_name'])) {
						$f = pathinfo($_FILES['i9']['name']);
						$f_rename = pathinfo(cleanString($_FILES['i9']['name']));

						$UID = uniqid();
						$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
						$filename = $basefilename . '.' . $f['extension'];
						add_file_log($applicant['id'], 'i9', $filename);
						$suffix = 0;
						while (file_exists(FILE_UPLOAD_DIR . $filename)) {
							$suffix++;
							$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
						}

						if (move_uploaded_file($_FILES['i9']['tmp_name'], FILE_UPLOAD_DIR . $filename))
						{
							// uploading new CV and has hold already? remove the old file
							if (strlen($applicant['i9']) > 1 && file_exists($DIR_CONST .  '/../' . $applicant['i9'])) {
								try {
									unlink($DIR_CONST .  '/../' . $applicant['i9']);
								} catch (Exception $e) {}
							}

							$i9 = FILE_UPLOAD_DIR . $filename;
						}
						else
						{
							$i9 = $currentI9;
						}

						if (!empty($_FILES['i9']['error'])) {
							$i9 = $currentI9;
						}

					} else {
					//cv path not updated
					$i9 = $currentI9;
				}
				chmod($i9, 0777);
			
			//Update Photo
				
			if (!empty($_FILES["f9_photo"]['tmp_name'])) {

			$f = pathinfo($_FILES['f9_photo']['name']);
			$f_rename = pathinfo(cleanString($_FILES['f9_photo']['name']));

			$UID = uniqid();
			$basefilename = $f_rename['filename'] . '_' . date('m-d-Y') . '_' . rand(1,999);
			$filename = $basefilename . '.' . $f['extension'];
			
			$suffix = 0;
			while (file_exists(FILE_UPLOAD_DIR . $filename)) {
				$suffix++;
				$filename = $basefilename . '_' . $suffix . '.' . $f['extension'];
			}

			if (move_uploaded_file($_FILES['f9_photo']['tmp_name'], FILE_UPLOAD_DIR . $filename))
			{
				$f9_photo = FILE_UPLOAD_DIR . $filename;
			}
			else
			{
				$f9_photo = '';
			}

			if (!empty($_FILES['f9_photo']['error'])) {
				$f9_photo = '';
			}

			}
			else {
					global $db;
					$sql = 'SELECT * FROM '.DB_PREFIX.' applicant WHERE id ="'.$applicant['id'].'"';
					$result = $db->query($sql);
					while ($row = $result->fetch_assoc()) {
						$default_img = $row['f9_photo'];
					}
					$f9_photo = $default_img;
				}

		chmod($f9_photo, 0777);

				$skills = tagglesToString($taggles);

				$sm_links = array();

				if (isset($sm_url_1) && $sm_url_1 != "") {
					$obj1 = constructSMlink($sm_url_1, $sm_select_1);
					$sm_links["first"] = $obj1;
				} else {
					$sm_links["first"] = "-";
				}

				if (isset($sm_url_2) && $sm_url_2 != "") {
					$obj2 = constructSMlink($sm_url_2, $sm_select_2);
					$sm_links["second"] = $obj2;
				} else {
					$sm_links["second"] = "-";
				}

				if (isset($sm_url_3) && $sm_url_3 != "") {
					$obj3 = constructSMlink($sm_url_3, $sm_select_3);
					$sm_links["third"] = $obj3;
				} else {
					$sm_links["third"] = "-";
				}

				if (isset($sm_url_4) && $sm_url_4 != "") {
					$obj4 = constructSMlink($sm_url_4, $sm_select_4);
					$sm_links["fourth"] = $obj4;
				} else {
					$sm_links["fourth"] = "-";
				}
				
// abganzon i edited this 
				$data = array(
				"fullname" => $fullname,
				"f9_first_name" => $f9_first_name,
				"f9_middle_name" => $f9_middle_name,
				"f9_last_name" => $f9_last_name,
				"f9_address_1" => $f9_address_1,
				"f9_address_2" => $f9_address_2,
				"f9_city" => $f9_city,
				"f9_state" => $f9_state,
				"f9_country" => $f9_country,
				"f9_zip" => $f9_zip,
				"f9_isProfile" => $f9_isProfile,
				"f9_language" => $f9_language,
				"f9_photo" => $f9_photo,
				"f9_yrs_experience" => $f9_yrs_experience,
				"f9_gender" => $f9_gender,
				"f9_category" => $f9_category,
				"f9_position" => $f9_position,
				"occupation" => $occupation,
				"phone" => $phone,
				"phone_carrier" => $phone_carrier,
				"birthdate" => $birthdate,
				"span_bilingual" => $span_bilingual,
				"pro_soft" => $pro_soft,
				"workarea" => $workarea,
				"skills_prof" => $skills_prof,
				"cover_letter" => $cover_letter,
				"x_ray" => $x_ray,
				"coronal_polish" => $coronal_polish,
				"laser" => $laser,
				"anesthesia" => $anesthesia,
				"efta" => $efta,
				"hygiene_license" => $hygiene_license,
				"dentist_license" => $dentist_license,
				"w9" => $w9,
				"w4" => $w4,
				"a4" => $a4,
				"direct_deposit" => $direct_deposit,
				"i9" => $i9,
				"tempday" => $tempday,
				"datefrom" => $datefrom,
				"dateuntil" => $dateuntil,
				"permaday" => $permaday,
				"wage_sel" => $wage_sel,
				"miles_sel" => $miles_sel,
				"location" => $location,
				"message" => $msg,
				"weblink" => $weblink,
				"skills" => $skills,
				"public_profile" => $flag,
				"cv_path" => $cv_path, 
				"email" => $email,
				"sm_links" => $sm_links,
				"avdate" => $avdate,
				"navdate" => $navdate,
				"typepos" => $typepos,
				"notifyby" => $notifyby);
				
				$cl->updateApplicant($data, $applicant['id']);
				$smarty->assign('msg', $translations['profile']['profile_edited']);
				$smarty->assign('title', $translations['profile']['op_success']);
				$smarty->assign('ACTIVE', 'success');

			break;
			case URL_PROFILE_APPLICATIONS:
				$applications = $cl->getJobApplicationsById($applicant['id']);
				$smarty->assign('apps', $applications);
			

				$notapplied = $cl->getJobsNotApplied($applicant['id']);
				$smarty->assign('jobs', $notapplied);


				$smarty->assign('ACTIVE', $PROFILE_ROUTING[URL_PROFILE_APPLICATIONS]); 
				$smarty->assign('title', $translations['profile']['recent_applications']);


			break;
			case URL_PROFILE_CHANGEPASSWORD:
				$smarty->assign('ACTIVE', $PROFILE_ROUTING[URL_PROFILE_CHANGEPASSWORD]); 
				$smarty->assign('title', $translations['profile']['passchange']);
			
			break;
			case URL_PROFILE_DELETE:
				$smarty->assign('ACTIVE', $PROFILE_ROUTING[URL_PROFILE_DELETE]); 
				$smarty->assign('title', $translations['profile']['delete_acc_label']);
			break;
			case 'logout':
				unset($_SESSION['applicant']); $_SESSION['applicant'] = null;
				unset($_SESSION['applicant_name']); $_SESSION['applicant_name'] = null;
				redirect_to(BASE_URL);
			break;
			case 'pass-edited':
				escape($_POST);
				$sql = 'UPDATE '.DB_PREFIX.'applicant SET password = "' . md5(trim($pass1)) . '" WHERE id =' . intval($applicant['id']);
				$db->query($sql);
				$smarty->assign('msg', $translations['profile']['pass_changed']);
				$smarty->assign('title', $translations['profile']['op_success']);
				$smarty->assign('ACTIVE', 'success');

			break;
			case 'deleteacc':
				//remove from subscribers
				$sql = 'SELECT auth FROM '.DB_PREFIX.'subscribers WHERE email = "' . $applicant['email'] . '"';
				$result = $db->QueryRow($sql);
				if (!empty($result)) {
					Subscriber::unsubscribe($result['auth']);
				}
				//delete all his applications
				$cl->removeJobApplicationsById($applicant['id']);
				//delete applicant himself
				$cl->deleteApplicant($applicant['id']);
				//send goodbye email - todo in next version
				//send home
				unset($_SESSION['applicant']); $_SESSION['applicant'] = null;
				unset($_SESSION['applicant_name']); $_SESSION['applicant_name'] = null;
				redirect_to(BASE_URL . 'deactivation-successful');
			break;

			default:
				$smarty->assign('ACTIVE', '404');
				$smarty->assign('title', 'Page does not exist');
			break;
		}

	} else { //default route
		$applications = $cl->getJobApplicationsById($applicant['id']);
		$smarty->assign('apps', $applications);

		$notapplied = $cl->getJobsNotApplied($applicant['id']);
		$smarty->assign('jobs', $notapplied);

		$smarty->assign('ACTIVE', $PROFILE_ROUTING[URL_PROFILE_APPLICATIONS]); 
		$smarty->assign('title', $translations['profile']['recent_applications']);
	}

	$smarty->assign('seo_title', SEO_APPLICANT_DASHBOARD_TITLE . ' - ' . $applicant['fullname']);
	$smarty->assign('seo_desc', SEO_APPLICANT_DASHBOARD_DESCRIPTION);
	$smarty->assign('seo_keys', SEO_APPLICANT_DASHBOARD_TITLE . ', ' . $applicant['fullname']);
	$smarty->assign('acc_idx', $acc_id);
	$template = 'profile/profile.tpl';


	function add_file_log($applicant_id, $doc_category = null, $doc_name = null){
		global $db;
		$date_updated = date("m/d/Y");
		$sql = 'INSERT INTO ' .DB_PREFIX. '.applicant_documents_log (applicant_id, doc_category, doc_name, date_updated) VALUES ("' . $applicant_id . '", "' . $doc_category . '", "' . $doc_name .'", "' . $date_updated . '")';
		$db->query($sql);
	}

?>
