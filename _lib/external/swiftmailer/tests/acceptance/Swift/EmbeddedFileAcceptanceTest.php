<?php

$DIR_CONST = '';
if (defined('__DIR__'))
	$DIR_CONST = __DIR__;
else
	$DIR_CONST = dirname(__FILE__);

require_once 'swift_required.php';
require_once $DIR_CONST.'/Mime/EmbeddedFileAcceptanceTest.php';

class Swift_EmbeddedFileAcceptanceTest extends Swift_Mime_EmbeddedFileAcceptanceTest
{
    protected function _createEmbeddedFile()
    {
        return Swift_EmbeddedFile::newInstance();
    }
}
