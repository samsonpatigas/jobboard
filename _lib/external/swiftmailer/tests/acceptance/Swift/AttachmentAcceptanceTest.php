<?php

$DIR_CONST = '';
if (defined('__DIR__'))
	$DIR_CONST = __DIR__;
else
	$DIR_CONST = dirname(__FILE__);

require_once 'swift_required.php';
require_once $DIR_CONST.'/Mime/AttachmentAcceptanceTest.php';

class Swift_AttachmentAcceptanceTest extends Swift_Mime_AttachmentAcceptanceTest
{
    protected function _createAttachment()
    {
        return Swift_Attachment::newInstance();
    }
}
