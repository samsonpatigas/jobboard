<?php

$DIR_CONST = '';
if (defined('__DIR__'))
	$DIR_CONST = __DIR__;
else
	$DIR_CONST = dirname(__FILE__);

$autoloader = require_once dirname($DIR_CONST).'/vendor/autoload.php';
$autoloader->add('Swift_', $DIR_CONST.'/unit');

set_include_path(get_include_path().PATH_SEPARATOR.dirname($DIR_CONST).'/lib');

\Mockery::getConfiguration()->allowMockingNonExistentMethods(false);

if (is_file($DIR_CONST.'/acceptance.conf.php')) {
    require_once $DIR_CONST.'/acceptance.conf.php';
}
if (is_file($DIR_CONST.'/smoke.conf.php')) {
    require_once $DIR_CONST.'/smoke.conf.php';
}
require_once $DIR_CONST.'/StreamCollector.php';
require_once $DIR_CONST.'/IdenticalBinaryConstraint.php';
require_once $DIR_CONST.'/SwiftMailerTestCase.php';
require_once $DIR_CONST.'/SwiftMailerSmokeTestCase.php';
