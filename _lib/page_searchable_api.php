<?php

  require_once '../_config/config.php';
  require_once '../_lib/class.Types.php';

    $sql = "SELECT DISTINCT
                ja.applicant_id AS 'id', 
                ja.applicant_id AS 'applicant_id', 
                a.fullname AS 'fullname', 
                a.f9_city AS 'city', 
                a.f9_gender AS 'gender', 
                ja.status AS 'status', 
                a.is_Banned AS 'banned', 
                j.title AS 'title',
                a.fullname AS 'name',
                a.phone AS 'phone',
                a.email AS 'email'
            FROM
                jobs AS j
                    LEFT JOIN
                job_applications AS ja ON j.id = ja.job_id
                    LEFT JOIN
                applicant AS a ON a.id = ja.applicant_id
            WHERE
                ja.applicant_id IS NOT NULL
                    AND j.employer_id = " . $_SESSION['user'];

    $result = $db->query($sql);
    $a = array();
    $data = [];
    $id = [];
    while ($row = $result->fetch_assoc()){
      if($row['status'] == 2){
        $up = '<i class="fa fa-check fa-lg" style="color:green;"></i>';
      }else{
        $up = '<i class="fa fa-thumbs-o-up fa-lg"></i>';
      }

      if($row['status'] == 1){
        $down = '<i class="fa fa-times fa-lg" style="color:red;"></i>';
      }else{
        $down = '<i class="fa fa-thumbs-o-down fa-lg"></i>';
      }

      if($row['banned'] == 1) {
        $banned_applicant = '<i class="fa fa-check fa-lg" style="color:green;"></i>';
      }else{
        $banned_applicant = '<i class="fa fa-thumbs-o-down fa-lg"></i>';
      }

      if($row['cv_path'] != null){
        $cv = explode(".", $row['cv_path']);
        if( $cv[1] == "pdf"){
          $icon = 'fa fa-file-pdf-o fa-lg pdf-el';
        }else{
          $icon = 'fa fa-file-word-o fa-lg word-el';
        }
      }else{
        $icon = '-';
      }

      if($row['status'] == '2'){
        $row['name'] = '<i style="color:green;">'.$row['name'].'</i>';
      }
      if($row['status'] == '1'){
        $row['name'] = '<i style="color:red;">'.$row['name'].'</i>';
      }
      else{
        $row['name'] = $row['name'];
      }

      if($row['bilingual'] != null){
        if($row['bilingual'] == 'Yes'){
          $row['bilingual'] = 'Bilingual';
        }else{
          $row['bilingual'] = 'Non-bilingual';
        }
      }else{
        $row['bilingual'] = '-';
      }

      $data[] = [
        $row['id'],
        $row['name'],
        $row['title'],
        $row['city'],
        $row['state'],
        $row['zip'],
        $row['phone'],
        $row['gender'],
        $row['bilingual'],
        $row['position'],
        $row['experience'],
        $row['prosoft'],
        $row['workarea'],
        $row['tempday'],
        $row['datefrom'],
        $row['dateuntil'],
        $row['permaday'],
        $row['skillsprof'],
        $row['wage'],
        $row['miles'],
        "<a href='http://jobboard.f9portal.net/".$row['cv_path']."' download> <i class='".$icon."' aria-hidden='true'></i></a>",
        "<a id='about' data-toggle='modal' data-target='#myModal' href=''><i class='fa fa-address-card-o fa-lg' aria-hidden='true'></i></a>",
        "<a id='review' href=''> ".$up." </a>",
        "<a id='reject' href=''> ".$down." </a>",
        "<a id='banned' href=''> ".$banned_applicant . " </a>",
        $row['email'],
        $row['phone'],
        $row['applicant_id']
      ];
    }
    $b['data'] = $data;
    // $b['id'] = $id;
    echo json_encode($b);
    // echo (json_encode($a));
?>