<?php
class Company {

  private $_name = null;
  private $f9_practice_name = null;
  private $f9_practice_type = null;
  private $f9_address_1 = null;
  private $f9_address_2 = null;
  private $f9_city = null;
  private $f9_state = null;
  private $f9_zip = null;
  private $f9_practice_phone_number = null;
  private $f9_cellphone_number = null;
  private $f9_cellphone_provider = null;
  private $f9_practice_email_address = null;
  private $f9_job_app_email_address = null;
  private $f9_notification_preference = null;
  private $_description = null;
  private $_hq = null;
  private $_url = null;
  private $_logopath = null;
  private $_employerid = 0;
  private $f9_fname = null;
  private $f9_lname = null;
  private $f9_bizname = null;
  private $f9_suite = null;
  private $f9_cross_st = null;

  function __construct() {}
  public function createCompany($params,$hash) {
    global $db;

    $sql = 'INSERT INTO '.DB_PREFIX.'company (
    f9_note,
    name,
    f9_practice_name,
    f9_address_1,
    f9_address_2,
    f9_city,
    f9_state,
    f9_zip,
    f9_practice_phone_number,
    f9_cellphone_number,
    f9_cellphone_provider,
    f9_practice_email_address,
    f9_job_app_email_address,
    f9_notification_preference,
    f9_practice_type,
    f9_office_software,
    description,
    hq,
    url,
    street,
    city_postcode,
    f9_fname,
    f9_lname,
    f9_bizname,
    f9_suite,
    f9_cross_st,
    logo_path,
    employer_id,
    public_page,
    is_Seen,
    profile_picture)
  VALUES (
    "' . $params['f9_note'] . '", 
    "' . $params['f9_practice_name'] . '",
    "' . $params['f9_practice_name'] . '",
    "' . $params['f9_address_1'] . '",
    "' . $params['f9_address_2'] . '",
    "' . $params['f9_city'] . '",
    "' . $params['f9_state'] . '",
    "' . $params['f9_zip'] . '",
    "' . $params['f9_practice_phone_number'] . '",
    "' . $params['f9_cellphone_number'] . '",
    "' . $params['f9_cellphone_provider'] . '",
    "' . $params['f9_practice_email_address'] . '",
    "' . $params['f9_job_app_email_address'] . '",
    "' . $params['f9_notification_preference'] . '",
    "' . $params['f9_practice_type'] . '",
    "' . $params['f9_office_software'] . '",
    "' . $params['company_desc'] . '",
    "' . $params['company_hq'] . '",
    "' . $params['company_url'] . '",
    "' . $params['company_street'] . '",
    "' . $params['company_citypostcode'] . '",
    "' . $params['f9_fname'] . '",
    "' . $params['f9_lname'] . '",
    "' . $params['f9_bizname'] . '",
    "' . $params['f9_suite'] . '",
    "' . $params['f9_cross_st'] . '",
    "' . $params['logo_path'] . '",
    "' . $params['employer_id'] . '",
     0,
     0,
     "' . DEFAULT_PROFILE_PATH . '")';
    $result = $db->query($sql);
    // $id = $db->getConnection()->insert_id;

    //ADDED 4/4/19

    // if($hash){
    //    $hashData = array('user_id' => $id, 'hash' => $hash);
    //    $class = new HashTables();
    //    $class->createEntryForConfirmation($hashData);

    //  }
  }


  public function updateEntry($params) {
    global $db;

    $sql = 'UPDATE ' .DB_PREFIX. 'company SET name = "' . $params['company_name'] . '",
    f9_practice_name = "' . $params['company_name'] . '",
    f9_address_1 = "' . $params['f9_address_1'] . '",
    f9_address_2 = "' . $params['f9_address_2'] . '",
    f9_city = "' . $params['f9_city'] . '",
    f9_state = "' . $params['f9_state'] . '",
    f9_zip = "' . $params['f9_zip'] . '",
    f9_practice_phone_number = "' . $params['f9_practice_phone_number'] . '",
    f9_cellphone_number = "' . $params['f9_cellphone_number'] . '",
    f9_cellphone_provider = "' . $params['f9_cellphone_provider'] . '",
    f9_practice_email_address = "' . $params['f9_practice_email_address'] . '",
    f9_job_app_email_address = "' . $params['f9_job_app_email_address'] . '",
    f9_notification_preference = "' . $params['f9_notification_preference'] . '",
    f9_practice_type = "' . $params['f9_practice_type'] . '",
    description = "' . $params['company_desc'] . '",
    hq = "' . $params['company_hq'] . '",
    url = "' . $params['company_url'] . '", 
    street = "' . $params['company_street'] . '",
    city_postcode = "' . $params['company_citypostcode'] . '",
    logo_path = "' . $params['logo_path'] . '",
    f9_note = "' . $params['f9_notes'] . '",
    f9_office_software = "' . $params['f9_office_software'] . '" 
    WHERE employer_id = ' . $params['employer_id'];
    $result = $db->query($sql);

    $sql_emp = 'UPDATE ' .DB_PREFIX. 'employer SET email = "'.$params['f9_practice_email_address'].'" WHERE id = ' .$params['employer_id'];
    $result = $db->query($sql_emp);
  }

  public function updateAdminEmployerData($eid, $email) {
    global $db;
    $sql = 'UPDATE employer SET email = "' . $email . '" WHERE id =' . $eid;
    $result = $db->query($sql);
  }


  public function getAdminCompanyData() {
    global $db;
    $sql = 'SELECT c.id as "company_id", c.name as "company_name", emp.id as "employer_id", emp.name as "employer_name", emp.email as "employer_email" FROM company c INNER JOIN 
    employer emp ON emp.id=c.employer_id WHERE emp.name="Admin"';
    $result = $db->query($sql);
    if ($result) {
      $row = $result->fetch_assoc();
      return $row;
    } else {
      return array();
    }
  }

  public function updateCompanyByEmployerId($params) {
    global $db;
    // var_dump($params);die();
    $sql = 'UPDATE ' . DB_PREFIX . 'company SET 
        name="' . $params['company_name'] . '",
        f9_practice_name = "' . $params['company_name'] . '",
        f9_address_1 = "' . $params['f9_address_1'] . '",
        f9_address_2 = "' . $params['f9_address_2'] . '",
        f9_city = "' . $params['f9_city'] . '",
        f9_state = "' . $params['f9_state'] . '",
        f9_zip = "' . $params['f9_zip'] . '",
        f9_practice_phone_number = "' . $params['f9_practice_phone_number'] . '",
        f9_cellphone_number = "' . $params['f9_cellphone_number'] . '",
        f9_cellphone_provider = "' . $params['f9_cellphone_provider'] . '",
        f9_practice_email_address = "' . $params['f9_practice_email_address'] . '",
        f9_job_app_email_address = "' . $params['f9_job_app_email_address'] . '",
        f9_notification_preference = "' . $params['f9_notification_preference'] . '",
        f9_practice_type = "' . $params['f9_practice_type'] . '",
        description="' . $params['company_desc'] . '",
        hq="' . $params['company_hq'] . '",
        url="' . $params['company_url'] . '",
        street="' . $params['company_street'] . '",
        city_postcode="' . $params['company_citypostcode'] . '",
        logo_path="' . $params['logo_path'] . '",
        public_page=' . $params['public_page'] . ',
        profile_picture="' . $params['profile_picture'] . '",
        f9_note="' . $params['f9_notes'] . '",
        f9_cross_st="' . $params['f9_cross_st'] . '",
        f9_office_software="' . $params['f9_office_software'] . '" 
        WHERE employer_id = ' . $params['employer_id'];
    $result = $db->query($sql);

    $sql_emp = 'UPDATE ' .DB_PREFIX. 'employer SET email = "'.$params['f9_practice_email_address'].'" WHERE id = ' .$params['employer_id'];
    $result = $db->query($sql_emp);

    return $result;
  }

  public function deleteCompanyDataByEmployerId($employer_id) {
    global $db;

    $DIR_CONST = '';
    if (defined('__DIR__'))
      $DIR_CONST = __DIR__;
    else
      $DIR_CONST = dirname(__FILE__);

    //first get logo
    $sql = 'SELECT logo_path FROM '.DB_PREFIX.'company WHERE employer_id = ' . $employer_id;
    $result = $db->query($sql);
    $row = $result->fetch_assoc();

    $sql = 'DELETE FROM '.DB_PREFIX.'company WHERE employer_id = ' . $employer_id;
    $db->query($sql);

    if (!empty($row)) {
      try {
         $path = $DIR_CONST . '/../' . $row['logo_path'];
         if (strpos($row['logo_path'], "company-default") === false) {
             unlink($path);
         }
      } catch (Exception $e) {}
      try {
         $path = __DIR__ . '/../' . $row['profile_picture'];
          if (strpos($row['profile_picture'], "profile-picture-default") === false) {
          unlink($path);
        }
      } catch (Exception $e) {}
    } 
  }

  public function unconfirmAndLogout($id) {
    global $db;
    $sql = 'UPDATE ' . DB_PREFIX . 'employer SET confirmed = 0 WHERE id = ' . $id;
    $result = $db->query($sql);
    redirect_to('logout');
  }

  public function getCompanyByEmployerId($id) {
    global $db;
    $sql = 'SELECT * FROM ' . DB_PREFIX . 'company WHERE employer_id = ' . $id;
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    return $row;
  }

  //ADDED

  public function getcontact($id) {
    global $db;
    $sql = 'SELECT * FROM ' . DB_PREFIX . 'client_contacts WHERE employer_id=' . $id ;
    $result = $db->query($sql);
    $contacts = array();

    while ($row = $result->fetch_assoc()){
      $contacts[] = $row;
    }
    return $contacts;
  }

  /*
    used in Client > My Practice 
  */
  public function delete_contact($id){
    global $db;
    $sql = 'DELETE FROM ' . DB_PREFIX .'client_contacts WHERE (id = ' . $id . ')';
    //var_dump($sql);
    $result = $db->query($sql);
    return true;
  }

  public function edit_contact($id){
    global $db;
    $sql = '';
  }

  public function getData() {
    $std = new stdClass;
    $std->name = $this->_name;
    $std->f9_practice_name = $this->f9_practice_name;
    $std->f9_address_1 = $this->f9_address_1;
    $std->f9_address_2 = $this->f9_address_2;
    $std->f9_city = $this->f9_city;
    $std->f9_state = $this->f9_state;
    $std->f9_zip = $this->f9_zip;
    $std->f9_practice_phone_number = $this->f9_practice_phone_number;
    $std->f9_cellphone_number = $this->f9_cellphone_number;
    $std->f9_cellphone_provider = $this->f9_cellphone_provider;
    $std->f9_practice_email_address = $this->f9_practice_email_address;
    $std->f9_job_app_email_address = $this->f9_job_app_email_address;
    $std->f9_notification_preference = $this->f9_notification_preference;
    $std->f9_practice_type = $this->f9_practice_type;
    $std->expires = $this->expires;
    $std->description = $this->_description;
    $std->f9_notes = $this->f9_note;
    $std->hq = $this->_hq;
    $std->url = $this->_url;
    $std->logopath = $this->_logopath;
    $std->employerid = $this->_employerid;
    $std->f9_office_software = $this->f9_office_software;
    return $std;
  }

  public function save_contact($params,$id) {
    global $db;
    $sql = "INSERT INTO " .DB_PREFIX. "client_contacts (
      contact_name, phone_number, phone_type, email_address, email_type, employer_id) 
      VALUES (
        '" . $params['contact_name'] . "','" . $params['phone_number'] . "','" .  $params['phone_type'] . "','" . $params['email_address'] . "','" . $params['email_type'] . "','" . $id . "')";
    $result = $db->query($sql);
  }


  public function get_email_log($client_id){
    global $db;
    $sql = "SELECT * FROM " .DB_PREFIX. "email_log WHERE client_id=" . $client_id;
    $result = $db->query($sql);
    return json_encode($result);
  }

  public function get_banned_job_seeker($client_id){ //called from index.php
    global $db;
    $seeker = array();
    $sql = "SELECT DISTINCT
          ja.applicant_id AS applicant_id, a.fullname AS fullname, a.email, a.phone
      FROM "
          .DB_PREFIX. "jobs AS j
              LEFT JOIN "
          .DB_PREFIX. "job_applications AS ja ON j.id = ja.job_id
              LEFT JOIN "
          .DB_PREFIX. "applicant AS a ON a.id = ja.applicant_id
      WHERE
          ja.applicant_id IS NOT NULL
              AND j.employer_id = " . $client_id .
              " AND a.is_Banned = 1";
    // echo $sql; die();        
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()){
      $seeker[] = $row;
    }    
    return $seeker;    
  }
}
?>
