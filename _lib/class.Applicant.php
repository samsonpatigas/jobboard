<?php
class Applicant {
  
  private $id = null;
  private $fullname = null;
  private $f9_first_name = null;
  private $f9_middle_name = null;
  private $f9_last_name = null;
  private $f9_address_1 = null;
  private $f9_address_2 = null;
  private $f9_city = null;
  private $f9_state = null;
  private $f9_country = null;
  private $f9_zip = null;
  private $f9_isProfile = null;
  private $f9_language = null;
  private $f9_photo = null;
  private $f9_yrs_experience = null;
  private $f9_gender = null;
  private $f9_category = null;
  private $f9_position = null;
  private $birthdate = null;
  private $span_bilingual = null;
  private $pro_soft = null;
  private $workarea = null; 
  private $skills_prof = null;
  private $cover_letter = null;
  private $x_ray = null;
  private $coronal_polish = null;
  private $laser = null;
  private $anesthesia = null;
  private $efta = null;
  private $hygiene_license = null;
  private $dentist_license = null;
  private $w9 = null;
  private $w4 = null;
  private $a4 = null;
  private $direct_deposit = null;
  private $i9 = null;
  private $tempday = null;
  private $datefrom = null;
  private $dateuntil = null;
  private $permaday = null;
  private $wage_sel = null;
  private $miles_sel = null;
  private $occupation = null;
  private $email = null;
  private $phone = null;
  private $phone_carrier = null;
  private $message = null;
  private $weblink = null;
  private $cv_path = null;
  private $last_activity = null;
  private $confirmHash = null;
  private $sm_links = array();

  function __construct() {}

  public function getId() {
    return $this->id;
  }

  public function getEmail() {
    return $this->email;
  }

  public function getConfirmHash(){ return $this->confirmHash; }


  public function getData() {
    $data = array(
    "id" => $this->id,
    "fullname" => $this->fullname, 
    "f9_first_name" => $this->f9_first_name,
    "f9_middle_name" => $this->f9_middle_name,
    "f9_last_name" => $this->f9_last_name,
    "f9_address_1" => $this->f9_address_1,
    "f9_address_2" => $this->f9_address_2,
    "f9_city" => $this->f9_city,
    "f9_state" => $this->f9_state,
    "f9_country" => $this->f9_country,
    "f9_zip" => $this->f9_zip,
    "f9_isProfile" => $this->f9_isProfile,
    "f9_language" => $this->f9_language,
    "f9_photo" => $this->f9_photo,
    "f9_yrs_experience" => $this->f9_yrs_experience,
    "f9_gender" => $this->f9_gender,
    "f9_category" => $this->f9_category,
    "phone_carrier" => $this->phone_carrier,
    "birthdate" => $this->birthdate,
    "span_bilingual" => $this->span_bilingual,
    "pro_soft" => $this->pro_soft,
    "workarea" => $this->workarea,
    "skills_prof" => $this->skills_prof,
    "cover_letter" => $this->cover_letter,
    "x_ray" => $this->x_ray,
    "coronal_polish" => $this->coronal_polish,
    "laser" => $this->laser,
    "anesthesia" => $this->anesthesia,
    "efta" => $this->efta,
    "hygiene_license" => $this->hygiene_license,
    "dentist_license" => $this->dentist_license,
    "w9" => $this->w9,
    "w4" => $this->w4,
    "a4" => $this->a4,
    "direct_deposit" => $this->direct_deposit,
    "i9" => $this->i9,
    "tempday" => $this->tempday,
    "datefrom" => $this->datefrom,
    "dateuntil" => $this->dateuntil,
    "permaday" => $this->permaday,
    "wage_sel" => $this->wage_sel,
    "miles_sel" => $this->miles_sel,
    "f9_position" => $this->f9_position,
    "occupation" => $this->occupation,
    "email" => $this->email,
    "phone" => $this->phone,
    "message" => $this->message,
    "weblink" => $this->weblink,
    "cv_path" => $this->cv_path, 
    "last_activity" => $last_activity,
    "sm_links" => $this->sm_links);
    return $data;
  }

  public function register($smarty) { 

    $DIR_CONST = '';
    if (defined('__DIR__'))
      $DIR_CONST = __DIR__;
    else
      $DIR_CONST = dirname(__FILE__);
    
    $tpl = $DIR_CONST . '/../plugins/Profiles/modal.tpl';
    if (!file_exists($tpl)) {
      $smarty->assign('modal_snippet','err/plugin-missing.tpl');
    } else {
      $smarty->assign('modal_snippet','../../../plugins/Profiles/modal.tpl');
    }
  }

  public function confirmUser($hash) {
    global $db;
 
    $sql = 'SELECT user_id
            FROM '.DB_PREFIX.'confirmed_applicants
            WHERE hash = "' . $hash . '"';
 
    $result = $db->query($sql); 
    if (($result->num_rows > 0)) {
    $assoc = $result->fetch_assoc();
    $user_id = $assoc['user_id'];
    //delete hash
    $sql = 'DELETE FROM '.DB_PREFIX.'confirmed_applicants WHERE user_id = ' . $user_id;
          $result = $db->query($sql);
    
           // set email for the welcome email
       $sql = 'SELECT email FROM '.DB_PREFIX.'applicant WHERE id = ' . $user_id;
 
           $result = $db->query($sql);
           $rowData = $result->fetch_assoc();
           $this->email = $rowData['email'];
 
        //set user active
        $sql = 'UPDATE '.DB_PREFIX.'applicant SET
              confirmed = 1 WHERE id = ' . $user_id;
 
          $result = $db->query($sql);
          return $result;
 
      } else {
        //activation failed
        return false;
      }
 
  }

  public function updateSubscription($email, $subs_status) {
    global $db;
    $sql = 'UPDATE subscriptions SET confirmed=' . $subs_status . ' WHERE email= "' . $email . '"';
    $db->query($sql);
  }

  public function getSubscriptionDetailsByEmail($email) {
    global $db;
    $sql = 'SELECT confirmed FROM subscriptions WHERE email="' . $email . '"';
    $result = $db->query($sql);
    if ($result){
      $row = $result->fetch_assoc();
      return intval($row['confirmed']);
    }
    else {
      return false;
    }
  }
 
  public function getIdByEmail($email) {
    global $db;
    $sql = 'SELECT id FROM applicant WHERE email="' . $email . '"';
    $result = $db->query($sql);
    if ($result) {
      $row = $result->fetch_assoc();
      return $row['id'];    
    } else 
      return 0;
  }

    //ADDED

  public function existing_email($email) {

    $newemail = str_replace('"', '', $email);

    global $db;
    $sql = "SELECT * FROM applicant WHERE email = '".$newemail."'";
    $result = $db->query($sql);

    if(mysqli_num_rows($result) > 0)
        {
          return 1;
        }
    else{
          return 0;
      }


  }

  //END

  public function isConfirmed($user_id) {
    global $db;
    $sql = 'SELECT confirmed FROM applicant WHERE id =' . intval($user_id);
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    return (intval($row['confirmed']) == 1) ? true : false;
  }

  public function removeJobApplicationsById($applicant_id) {
    global $db;
    $sql = 'DELETE FROM '.DB_PREFIX.'job_applications WHERE applicant_id = ' . $applicant_id;
    $result = $db->query($sql);
  }

  public function deleteApplicant($id) {
    global $db;
    $sql = 'DELETE FROM '.DB_PREFIX.'applicant WHERE id = ' . $id;
    $result = $db->query($sql);
  }

  public function getDataById($id) {
    global $db;
    $sql = 'SELECT * FROM '.DB_PREFIX.'applicant WHERE id = ' . $id;
    $result = $db->query($sql);
    $row = $result->fetch_assoc();

    $se = explode(",", $row['skills']);
    $skills = '';
    foreach ($se as $skill) {
      $skills .=  "#" . $skill . " ";
    }

    $sm_links = array();

    if (!empty($row['sm_link_1']) && $row['sm_link_1'] != "-") {
      $sm_links["first"] = deconstructSMlink($row['sm_link_1']);
    }

    if (!empty($row['sm_link_2']) && $row['sm_link_2'] != "-") {
      $sm_links["second"] = deconstructSMlink($row['sm_link_2']);
    }

    if (!empty($row['sm_link_3']) && $row['sm_link_3'] != "-") {
      $sm_links["third"] = deconstructSMlink($row['sm_link_3']);
    }

    if (!empty($row['sm_link_4']) && $row['sm_link_4'] != "-") {
      $sm_links["fourth"] = deconstructSMlink($row['sm_link_4']);
    }
    if (empty($row['pro_soft']) || strlen($row['pro_soft']) < 1) {
        $row['pro_soft'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['pro_soft']);
        $skills_formated = '';
        foreach ($se as $pro_soft) {
          $skills_formated .= "<span class=\"tag\">" . $pro_soft . "</span>";
        }
        $row['pro_soft'] = $skills_formated;  
      }
      if (empty($row['workarea']) || strlen($row['workarea']) < 1) {
        $row['workarea'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['workarea']);
        $skills_formated = '';
        foreach ($se as $workarea) {
          $skills_formated .= "<span class=\"tag\">" . $workarea . "</span>";
        }
        $row['workarea'] = $skills_formated;  
      }
      if (empty($row['skills_prof']) || strlen($row['skills_prof']) < 1) {
        $row['skills_prof'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['skills_prof']);
        $skills_formated = '';
        foreach ($se as $skills_prof) {
          $skills_formated .= "<span class=\"tag\">" . $skills_prof . "</span>";
        }
        $row['skills_prof'] = $skills_formated; 
      }
      if (empty($row['tempday']) || strlen($row['tempday']) < 1) {
        $row['tempday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['tempday']);
        $skills_formated = '';
        foreach ($se as $tempday) {
          $skills_formated .= "<span class=\"tag\">" . $tempday . "</span>";
        }
        $row['tempday'] = $skills_formated; 
      }
      if (empty($row['permaday']) || strlen($row['permaday']) < 1) {
        $row['permaday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['permaday']);
        $skills_formated = '';
        foreach ($se as $permaday) {
          $skills_formated .= "<span class=\"tag\">" . $permaday . "</span>";
        }
        $row['permaday'] = $skills_formated;  
      }
    

    $data = array(
    "id" => $row['id'],
    "fullname" => $row['fullname'],
    "f9_first_name" => $row['f9_first_name'],
    "f9_middle_name" => $row['f9_middle_name'],
    "f9_last_name" => $row['f9_last_name'],
    "f9_address_1" => $row['f9_address_1'],
    "f9_address_2" => $row['f9_address_2'],
    "f9_city" => $row['f9_city'],
    "f9_state" => $row['f9_state'],
    "f9_country" => $row['f9_country'],
    "f9_zip" => $row['f9_zip'],
    "f9_isProfile" => $row['f9_isProfile'],
    "f9_language" => $row['f9_language'],
    "f9_photo" => $row['f9_photo'],
    "f9_yrs_experience" => $row['f9_yrs_experience'],
    "f9_gender" => $row['f9_gender'],
    "f9_category" => $row['f9_category'],
    "f9_position" => $row['f9_position'],
    "phone_carrier" => $row['phone_carrier'],
    "birthdate" => $row['birthdate'], 
    "span_bilingual" => $row['span_bilingual'], 
    "pro_soft" => $row['pro_soft'],
    "workarea" => $row['workarea'],
    "skills_prof" => $row['skills_prof'], 
    "cover_letter" => $row['cover_letter'], 
    "x_ray" => $row['x_ray'],
    "coronal_polish" => $row['coronal_polish'], 
    "laser" => $row['laser'], 
    "anesthesia" => $row['anesthesia'], 
    "efta" => $row['efta'],
    "hygiene_license" => $row['hygiene_license'], 
    "dentist_license" => $row['dentist_license'],
    "w9" => $row['w9'], 
    "w4" => $row['w4'], 
    "a4" => $row['a4'], 
    "direct_deposit" => $row['direct_deposit'], 
    "i9" => $row['i9'], 
    "tempday" => $row['tempday'], 
    "datefrom" => $row['datefrom'], 
    "dateuntil" => $row['dateuntil'], 
    "permaday" => $row['permaday'],
    "wage_sel" => $row['wage_sel'],
    "miles_sel" => $row['miles_sel'],
    "occupation" => $row['occupation'],
    "email" => $row['email'],
    "phone" => $row['phone'],
    "location" => $row['location'],
    "message" => $row['message'],
    "weblink" => $row['weblink'],
    "skills" => $skills,
    "cv_path" => $row['cv_path'],
    "public_profile" => $row['public_profile'],
    "last_activity" => $row['last_activity'],
    "sm_links" => $sm_links);
    return $data;
  }

  public function getDataByIdForProfile($id) {
    global $db;
    $sql = 'SELECT * FROM '.DB_PREFIX.'applicant WHERE id = ' . $id;
    $result = $db->query($sql);
    $row = $result->fetch_assoc();

    /*$sql1 = 'SELECT name FROM '.DB_PREFIX.'categories WHERE id = ' . $row['f9_category'];
    $result1 = $db->query($sql1);
    $row1 = $result1->fetch_assoc();*/

    $sm_links = array();

    if (!empty($row['sm_link_1']) && $row['sm_link_1'] != "-") {
      $sm_links["first"] = deconstructSMlink($row['sm_link_1']);
    }

    if (!empty($row['sm_link_2']) && $row['sm_link_2'] != "-") {
      $sm_links["second"] = deconstructSMlink($row['sm_link_2']);
    }

    if (!empty($row['sm_link_3']) && $row['sm_link_3'] != "-") {
      $sm_links["third"] = deconstructSMlink($row['sm_link_3']);
    }

    if (!empty($row['sm_link_4']) && $row['sm_link_4'] != "-") {
      $sm_links["fourth"] = deconstructSMlink($row['sm_link_4']);
    }
    if (empty($row['pro_soft']) || strlen($row['pro_soft']) < 1) {
        $row['pro_soft'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['pro_soft']);
        $skills_formated = '';
        foreach ($se as $pro_soft) {
          $skills_formated .= "<span class=\"tag\">" . $pro_soft . "</span>";
        }
        $row['pro_soft'] = $skills_formated;  
      }
      if (empty($row['workarea']) || strlen($row['workarea']) < 1) {
        $row['workarea'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['workarea']);
        $skills_formated = '';
        foreach ($se as $workarea) {
          $skills_formated .= "<span class=\"tag\">" . $workarea . "</span>";
        }
        $row['workarea'] = $skills_formated;  
      }
      if (empty($row['skills_prof']) || strlen($row['skills_prof']) < 1) {
        $row['skills_prof'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['skills_prof']);
        $skills_formated = '';
        foreach ($se as $skills_prof) {
          $skills_formated .= "<span class=\"tag\">" . $skills_prof . "</span>";
        }
        $row['skills_prof'] = $skills_formated; 
      }
      if (empty($row['tempday']) || strlen($row['tempday']) < 1) {
        $row['tempday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['tempday']);
        $skills_formated = '';
        foreach ($se as $tempday) {
          $skills_formated .= "<span class=\"tag\">" . $tempday . "</span>";
        }
        $row['tempday'] = $skills_formated; 
      }
      if (empty($row['permaday']) || strlen($row['permaday']) < 1) {
        $row['permaday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['permaday']);
        $skills_formated = '';
        foreach ($se as $permaday) {
          $skills_formated .= "<span class=\"tag\">" . $permaday . "</span>";
        }
        $row['permaday'] = $skills_formated;  
      }
    

    $data = array(
    "id" => $row['id'],
    "fullname" => $row['fullname'],
    "f9_first_name" => $row['f9_first_name'],
    "f9_middle_name" => $row['f9_middle_name'],
    "f9_last_name" => $row['f9_last_name'],
    "f9_address_1" => $row['f9_address_1'],
    "f9_address_2" => $row['f9_address_2'],
    "f9_city" => $row['f9_city'],
    "f9_state" => $row['f9_state'],
    "f9_country" => $row['f9_country'],
    "f9_zip" => $row['f9_zip'],
    "f9_isProfile" => $row['f9_isProfile'],
    "f9_language" => $row['f9_language'],
    "f9_photo" => $row['f9_photo'],
    "f9_yrs_experience" => $row['f9_yrs_experience'],
    "f9_gender" => $row['f9_gender'],
    "f9_category" => $row['f9_category'],
    "f9_position" => $row['f9_position'],
    "phone_carrier" => $row['phone_carrier'],
    "birthdate" => $row['birthdate'], 
    "span_bilingual" => $row['span_bilingual'], 
    "pro_soft" => $row['pro_soft'],
    "workarea" => $row['workarea'],
    "skills_prof" => $row['skills_prof'], 
    "cover_letter" => $row['cover_letter'], 
    "x_ray" => $row['x_ray'],
    "coronal_polish" => $row['coronal_polish'], 
    "laser" => $row['laser'], 
    "anesthesia" => $row['anesthesia'], 
    "efta" => $row['efta'],
    "hygiene_license" => $row['hygiene_license'], 
    "dentist_license" => $row['dentist_license'],
    "w9" => $row['w9'], 
    "w4" => $row['w4'], 
    "a4" => $row['a4'], 
    "direct_deposit" => $row['direct_deposit'], 
    "i9" => $row['i9'], 
    "tempday" => $row['tempday'], 
    "datefrom" => $row['datefrom'], 
    "dateuntil" => $row['dateuntil'], 
    "permaday" => $row['permaday'],
    "wage_sel" => $row['wage_sel'],
    "miles_sel" => $row['miles_sel'],
    "occupation" => $row['occupation'],
    "email" => $row['email'],
    "phone" => $row['phone'],
    "location" => $row['location'], 
    "message" => $row['message'],
    "weblink" => $row['weblink'],
    "skills" => $row['skills'],
    "cv_path" => $row['cv_path'],
    "public_profile" => $row['public_profile'],
    "last_activity" => $row['last_activity'],
    "sm_links" => $sm_links,
    "avdate" => $row['avdate'],
    "navdate" => $row['navdate'],
    "typepos" => $row['typepos'],
    "notifyby" => $row['notifyby']);
    return $data;
  }

  public function updatePassword($id, $newpass) {
    global $db;
    $sql = 'UPDATE '.DB_PREFIX.'applicant SET password="'.$newpass.'" WHERE id="'.$id.'"';
    $result = $db->query($sql);
    if ($result) {
      return true;
    } else {
      return false;
    }
  }


  public function updateApplicant($p, $id) {
    global $db;

    if(empty($_POST['pro_soft'])){
      $sql = 'SELECT pro_soft FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $pro_soft = $row['pro_soft'];
    }else{
      $pro_soft = implode(",", $_POST['pro_soft']) . "," . $_POST['otherspec'];
    }
    if(empty($_POST['workarea'])){
      $sql = 'SELECT workarea FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $workarea = $row['workarea'];
    }else{
      $workarea = implode(",", $_POST['workarea']);
    }
    if(empty($_POST['skills_prof'])){
      $sql = 'SELECT skills_prof FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $skills_prof = $row['skills_prof'];
    }else{
      $skills_prof = implode(",", $_POST['skills_prof']);
    }
    if(empty($_POST['tempday'])){
      $sql = 'SELECT tempday FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $tempday = $row['tempday'];
    }else{
      $tempday = implode(",", $_POST['tempday']);
    }
    if(empty($_POST['permaday'])){
      $sql = 'SELECT permaday FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $permaday = $row['permaday'];
    }else{
      $permaday = implode(",", $_POST['permaday']);
    }

    // ADDED

    if(empty($_POST['f9_position'])){
      $sql = 'SELECT f9_position FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $positions = $row['f9_position'];
    }else{
      $positions = $_POST['f9_position'];
    }

    $full = $p['f9_first_name']." ".$p['f9_middle_name']." ".$p['f9_last_name'];

    if(empty($_POST['typepos'])){
      $sql = 'SELECT typepos FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $typepos = $row['typepos'];
    }else{
      $typepos = $_POST['typepos'];
    }

    $pstringrm = str_replace(' ', '', $p['phone']);

    $pstring = preg_replace('/[^A-Za-z0-9]/', '', $pstringrm); 

    if(empty($_POST['notifyby'])){
      $sql = 'SELECT notifyby FROM applicant WHERE id =' . $id;
      $result = $db->query($sql);
      $row = $result->fetch_assoc();
      $notifyby = $row['notifyby'];
    }else{
      $notifyby = implode(",", $_POST['notifyby']);
    }

    // END

    $sql = 'UPDATE '.DB_PREFIX.' applicant SET 
    fullname = "' . $full .'",
    f9_first_name = "' . $p['f9_first_name'] .'",
    f9_middle_name = "' . $p['f9_middle_name'] .'", 
    f9_last_name = "' . $p['f9_last_name'] .'",
    f9_address_1 = "' . $p['f9_address_1'] .'",
    f9_address_2 = "' . $p['f9_address_2'] .'", 
    f9_city = "' . $p['f9_city'] .'",
    f9_state = "' . $p['f9_state'] .'",
    f9_country = "' . $p['f9_country'] .'",
    f9_zip = "' . $p['f9_zip'] .'", 
    f9_isProfile = "' . $p['f9_isProfile'] .'",
    f9_language = "' . $p['f9_language'] .'",
    f9_photo = "' . $p['f9_photo'] .'", 
    f9_yrs_experience = "' . $p['f9_yrs_experience'] .'",
    email = "' . $_POST['apply_email'] .'",
    f9_gender = "' . $p['f9_gender'] .'",
    f9_category ="'.$p['f9_category'].'",
    f9_position ="'.$positions.'",
    phone_carrier = "' . $p['phone_carrier'] . '",
    birthdate = "' . $p['birthdate'] . '",
    span_bilingual = "' . $p['span_bilingual'] . '",
    pro_soft = "' . $pro_soft . '",
    workarea = "' . $workarea . '",
    skills_prof = "' . $skills_prof . '",
    cover_letter = "' . $p['cover_letter'] . '",
    x_ray = "' . $p['x_ray'] . '",
    coronal_polish = "' . $p['coronal_polish'] . '",
    laser = "' . $p['laser'] . '",
    anesthesia = "' . $p['anesthesia'] . '",
    efta = "' . $p['efta'] . '",
    hygiene_license = "' . $p['hygiene_license'] . '",
    dentist_license = "' . $p['dentist_license'] . '",
    w9 = "' . $p['w9'] . '",
    w4 = "' . $p['w4'] . '",
    a4 = "' . $p['a4'] . '",
    direct_deposit = "' . $p['direct_deposit'] . '",
    i9 = "' . $p['i9'] . '",
    tempday = "' . $tempday . '",
    datefrom = "' . $p['datefrom'] . '",
    dateuntil = "' . $p['dateuntil'] . '",
    permaday = "' . $permaday . '",
    wage_sel = "' . $p['wage_sel'] . '",
    miles_sel = "' . $p['miles_sel'] . '",
    occupation = "' . $p['occupation'] . '",
    phone = "' .  $pstring. '",
    message = "' .  $p['message'] . '",
    weblink = "' .  $p['weblink'] . '",
    cv_path = "' .  $p['cv_path'] . '",
    public_profile = ' .  $p['public_profile'] . ',
    location = "' . $p['location'] . '",
    skills = "' . $p['skills'] . '",
    avdate = "' . $p['avdate'] . '",
    navdate = "' . $p['navdate'] . '",
    typepos = "' . $typepos . '",
    notifyby = "' . $notifyby . '",
    sm_link_1 = "' . $p["sm_links"]["first"]->linkToSave . '",
    sm_link_2 = "' . $p["sm_links"]["second"]->linkToSave . '",
    sm_link_3 = "' . $p["sm_links"]["third"]->linkToSave . '",
    sm_link_4 = "' . $p["sm_links"]["fourth"]->linkToSave . '" WHERE id = ' . $id;

    $result = $db->query($sql);
  }

  public function getJobApplicationsById($id) {
    global $db;
    $sanitizer = new Sanitizer;
    $sql = 'SELECT distinct UNIX_TIMESTAMP(a.created_on) as "created_on", 
          a.status as "status",
          UNIX_TIMESTAMP(b.created_on) as "date_posted", 
          b.title as "job_title", 
          b.salary as "job_salary", 
          b.id as "job_id", 
          b.company as "job_company", 
          c.name as "city_name" 
          FROM '.DB_PREFIX.'job_applications a, 
          '.DB_PREFIX.'jobs b, 
          '.DB_PREFIX.'cities c 
          WHERE a.job_id = b.id 
          AND a.applicant_id = "' . $id . '" 
          AND b.city_id = c.id
          ORDER BY a.created_on DESC';
    $result = $db->query($sql);
    $final = array();
  
    while ($row = $result->fetch_assoc()) {
      $row['created_on'] = date(DATE_FORMAT, floatval(stripslashes($row['created_on'] )));
      $row['date_posted'] = date(DATE_FORMAT, floatval(stripslashes($row['date_posted'] )));
      // admin causing troubles with routing
      if (strpos(strtolower($row['job_title']), "admin") !== false) {
        $row['url_title'] = "";
      } else {
        $row['url_title'] = $sanitizer->sanitize_title_with_dashes($row['job_title'] . ' at ' . $row['job_company']);
      }
      
      $final[] = $row;
  
    }

    return $final;
    
  }

  public function getJobsNotApplied($id){
    global $db;
    $sanitizer = new Sanitizer;
    $sql = 'SELECT DISTINCT
            UNIX_TIMESTAMP(a.created_on) AS "date_posted",
            a.id AS "id",
            a.id AS "job_id",
            a.is_active AS "status",
            a.title AS "job_title",
            a.salary AS "job_salary",
            a.company AS "job_company"
          FROM
              '.DB_PREFIX.'jobs a
          WHERE
              a.id NOT IN (SELECT 
              job_id
          FROM
              '.DB_PREFIX.'job_applications
          WHERE
              applicant_id = "'.$id.'") AND a.review_status = 1';
            
    $result = $db->query($sql);
    $final = array();
    while ($row = $result->fetch_assoc()) {
      $row['created_on'] = date(DATE_FORMAT, floatval(stripslashes($row['date_posted'] )));
      
      // admin causing troubles with routing
      if (strpos(strtolower($row['job_title']), "admin") !== false) {
        $row['url_title'] = "";
      } else {
        $row['url_title'] = $sanitizer->sanitize_title_with_dashes($row['job_title'] . ' at ' . $row['job_company']);
      }

      $final[] = $row;

    }
    return $final;
  
  }

  public function createResetPasswordEntry($id, $hash) {
    $hashData = array('user_id' => $id, 'hash' => $hash);
    $class = new HashTables();
    $class->createEntryForPassrecoveryApplicants($hashData);
  }

  public function checkIfUserExistsAndIsConfirmed($email) {
    global $db;
    $sql = 'SELECT id, confirmed
               FROM '.DB_PREFIX.'applicant
               WHERE email = "' . $email . '"';
        $result = $db->query($sql);
        $assoc = $result->fetch_assoc();

        if ($result->num_rows > 0) {
          $confirmed = $assoc['confirmed'];

        } else {
          $confirmed = 2;
        }

        $this->id = $assoc['id'];

        return intval($confirmed);
  }

  public function checkIfUserExists($email) {
    global $db;
    $sql = 'SELECT id
               FROM '.DB_PREFIX.'applicant
               WHERE email = "' . $email . '"';
        $result = $db->query($sql);

        if ($result->num_rows > 0) {
          $assoc = $result->fetch_assoc();
          
        }

        $this->id = $assoc['id'];

        return ($result->num_rows > 0) ? true : false;
  }

  public function isSubscribed($email) {
    global $db;
    $sql = 'SELECT id
               FROM '.DB_PREFIX.'subscriptions
               WHERE email = "' . $email . '"';
        $result = $db->query($sql);

        return ($result->num_rows > 0) ? true : false;
  }

  public function updateApplicantActivity($public, $id) {
    global $db;
    if (intval($public)){
      $sql = 'UPDATE '.DB_PREFIX.'applicant SET last_activity=NOW() WHERE id ='. $id;
      $result = $db->query($sql); 
    }
  }

  public function createCandidateProfileFromAdmin($params) {
    global $db;

    // get SM objects
    if (!empty($params['sm_links']["first"]) && $params['sm_links']["first"] != "-")
      $sm_link_1 = $params['sm_links']["first"]->linkToSave;
    else
      $sm_link_1 = '-';

    if (!empty($params['sm_links']["second"]) && $params['sm_links']["second"] != "-")
      $sm_link_2 = $params['sm_links']["second"]->linkToSave;
    else
      $sm_link_2 = '-';

    if (!empty($params['sm_links']["third"]) && $params['sm_links']["third"] != "-")
      $sm_link_3 = $params['sm_links']["third"]->linkToSave;
    else
      $sm_link_3 = '-';

    if (!empty($params['sm_links']["fourth"]) && $params['sm_links']["fourth"] != "-")
      $sm_link_4 = $params['sm_links']["fourth"]->linkToSave;
    else
      $sm_link_4 = '-';

    //new applicant
    $sql = 'INSERT INTO '.DB_PREFIX.'applicant (
    id,
    fullname,
    f9_first_name,
    f9_middle_name,
    f9_last_name,
    f9_address_1,
    f9_address_2,
    f9_city,
    f9_state,
    f9_country,
    f9_zip, 
    f9_isProfile,
    f9_language,
    f9_photo,
    f9_yrs_experience,
    f9_gender,
    f9_category,
    f9_position,
    phone_carrier,
    birthdate,
    span_bilingual,
    pro_soft,
    workarea,
    skills_prof,
    cover_letter,
    x_ray,
    coronal_polish,
    laser,
    anesthesia,
    efta,
    hygiene_license,
    dentist_license,
    w9,
    w4,
    a4,
    direct_deposit,
    i9,
    tempday,
    datefrom,
    dateuntil,
    permaday,
    wage_sel,
    miles_sel,
    occupation,
    email,
    phone,
    message, 
    weblink, 
    cv_path, 
    public_profile,
    is_Seen,
    password, 
    last_activity,
    location, 
    skills, 
    confirmed,
    sm_link_1,
    sm_link_2, 
    sm_link_3,
    sm_link_4,
    notifyby)
         VALUES (NULL, 
  "' . $params['name'] . '",
      "' . $params['f9_first_name'] . '",
      "' . $params['f9_middle_name'] . '",
      "' . $params['f9_last_name'] . '",
      "' . $params['f9_address_1'] . '",
      "' . $params['f9_address_2'] . '",
      "' . $params['f9_city'] . '",
      "' . $params['f9_state'] . '",
      "' . $params['f9_country'] . '",
      "' . $params['f9_zip'] . '",
      "' . $params['f9_isProfile'] . '",
      "' . $params['f9_language'] . '",
      "' . $params['f9_photo'] . '",
      "' . $params['f9_yrs_experience'] . '",
      "' . $params['f9_gender'] . '",
      "' . $params['f9_category'] . '",
      "' . $params['f9_position'] . '",
      "' . $params['phone_carrier'] . '",
      "' . $params['birthdate'] . '",
      "' . $params['span_bilingual'] . '",
      "' . implode(",", $_POST['pro_soft']) . "," .$_POST['otherspec'] . '",
      "' . implode(",", $_POST['workarea']) . '",
      "' . implode(",", $_POST['skills_prof']) . '",
      "' . $params['cover_letter'] . '",
      "' . $params['x_ray'] . '",
      "' . $params['coronal_polish'] . '",
      "' . $params['laser'] . '",
      "' . $params['anesthesia'] . '",
      "' . $params['efta'] . '",
      "' . $params['hygiene_license'] . '",
      "' . $params['dentist_license'] . '",
      "' . $params['w9'] . '",
      "' . $params['w4'] . '",
      "' . $params['a4'] . '",
      "' . $params['direct_deposit'] . '",
      "' . $params['i9'] . '",
      "' . implode(",", $_POST['tempday']) . '",
      "' . $params['datefrom'] . '",
      "' . $params['dateuntil'] . '",
      "' . implode(",", $_POST['permaday']) . '",
      "' . $params['wage_sel'] . '",
      "' . $params['miles_sel'] . '",
  "' . $params['occupation'] . '",
  "' . $params['email'] . '",
  "' . $params['phone'] . '",
  "' . $params['message'] . '",
  "' . $params['website'] . '",
  "' . $params['cv_path'] . '",
  ' . $params['public_profile'] . ',
  ' . '0' . ',
  "' . $params['password'] . '", NOW(),
  "' . $params['location']  . '",
  "' . $params['skills']  . '", 
  ' . $params['confirmed'] . ',
  "' . $sm_link_1  . '",
  "' . $sm_link_2  . '",
  "' . $sm_link_3  . '",
  "' . $sm_link_4  . '",
  "' . implode(",", $_POST['notifyby']) . '")';

   $result = $db->query($sql);
   return $result;
  }

  public function createCandidateProfile($params) {
    global $db;

    // get SM objects
    if (!empty($params['sm_links']["first"]) && $params['sm_links']["first"] != "-")
      $sm_link_1 = $params['sm_links']["first"]->linkToSave;
    else
      $sm_link_1 = '-';

    if (!empty($params['sm_links']["second"]) && $params['sm_links']["second"] != "-")
      $sm_link_2 = $params['sm_links']["second"]->linkToSave;
    else
      $sm_link_2 = '-';

    if (!empty($params['sm_links']["third"]) && $params['sm_links']["third"] != "-")
      $sm_link_3 = $params['sm_links']["third"]->linkToSave;
    else
      $sm_link_3 = '-';

    if (!empty($params['sm_links']["fourth"]) && $params['sm_links']["fourth"] != "-")
      $sm_link_4 = $params['sm_links']["fourth"]->linkToSave;
    else
      $sm_link_4 = '-';

    //new applicant
    $sql = 'INSERT INTO '.DB_PREFIX.'applicant (
    id,
    fullname,
    f9_first_name,
    f9_middle_name,
    f9_last_name,
    f9_address_1,
    f9_address_2,
    f9_city,
    f9_state,
    f9_country,
    f9_zip, 
    f9_isProfile,
    f9_language,
    f9_photo,
    f9_yrs_experience,
    f9_gender,
    f9_category,
    f9_position,
    phone_carrier,
    birthdate,
    span_bilingual,
    pro_soft,
    workarea,
    skills_prof,
    cover_letter,
    x_ray,
    coronal_polish,
    laser,
    anesthesia,
    efta,
    hygiene_license,
    dentist_license,
    w9,
    w4,
    a4,
    direct_deposit,
    i9,
    tempday,
    datefrom,
    dateuntil,
    permaday,
    wage_sel,
    miles_sel,
    occupation,
    email,
    phone,
    message, 
    weblink, 
    cv_path, 
    public_profile,
    is_Seen,
    password, 
    last_activity,
    location, 
    skills, 
    confirmed,
    typepos,
    sm_link_1,
    sm_link_2, 
    sm_link_3,
    sm_link_4,
    notifyby)
         VALUES (NULL, 
  "' . $params['name'] . '",
      "' . $params['f9_first_name'] . '",
      "' . $params['f9_middle_name'] . '",
      "' . $params['f9_last_name'] . '",
      "' . $params['f9_address_1'] . '",
      "' . $params['f9_address_2'] . '",
      "' . $params['f9_city'] . '",
      "' . $params['f9_state'] . '",
      "' . $params['f9_country'] . '",
      "' . $params['f9_zip'] . '",
      "' . $params['f9_isProfile'] . '",
      "' . $params['f9_language'] . '",
      "' . $params['f9_photo'] . '",
      "' . $params['f9_yrs_experience'] . '",
      "' . $params['f9_gender'] . '",
      "' . $params['f9_category'] . '",
      "' . implode(",", $_POST['f9_position']) . '",
      "' . $params['phone_carrier'] . '",
      "' . $params['birthdate'] . '",
      "' . $params['span_bilingual'] . '",
      "' . implode(",", $_POST['pro_soft']) . "," .$_POST['otherspec'] . '",
      "' . implode(",", $_POST['workarea']) . '",
      "' . implode(",", $_POST['skills_prof']) . '",
      "' . $params['cover_letter'] . '",
      "' . $params['x_ray'] . '",
      "' . $params['coronal_polish'] . '",
      "' . $params['laser'] . '",
      "' . $params['anesthesia'] . '",
      "' . $params['efta'] . '",
      "' . $params['hygiene_license'] . '",
      "' . $params['dentist_license'] . '",
      "' . $params['w9'] . '",
      "' . $params['w4'] . '",
      "' . $params['a4'] . '",
      "' . $params['direct_deposit'] . '",
      "' . $params['i9'] . '",
      "' . implode(",", $_POST['tempday']) . '",
      "' . $params['datefrom'] . '",
      "' . $params['dateuntil'] . '",
      "' . implode(",", $_POST['permaday']) . '",
      "' . $params['wage_sel'] . '",
      "' . $params['miles_sel'] . '",
  "' . $params['occupation'] . '",
  "' . $params['email'] . '",
  "' . $params['phone'] . '",
  "' . $params['message'] . '",
  "' . $params['website'] . '",
  "' . $params['cv_path'] . '",
  ' . $params['public_profile'] . ',
  ' . '0' . ',
  "' . $params['password'] . '", NOW(),
  "' . $params['location']  . '",
  "' . $params['skills']  . '", 
  ' . $params['confirmed'] . ',
  "' . $_POST['typepos'] . '",
  "' . $sm_link_1  . '",
  "' . $sm_link_2  . '",
  "' . $sm_link_3  . '",
  "' . $sm_link_4  . '",
  "' . implode(",", $_POST['notifyby']) .'")';

     $result = $db->query($sql);
     $APPLICANT_ID = $db->getConnection()->insert_id;
     $this->id = $APPLICANT_ID;

     if (intval($params['confirmed']) == 0) {
       $hash = sha1($params['email'] . time());
       $sql = 'INSERT INTO confirmed_applicants (user_id, hash) VALUES (' . $APPLICANT_ID  .', "' . $hash . '")';
       $db->query($sql);
       $this->confirmHash = $hash;
     }

     // subscribe him to all jobs
     if (!$this->isSubscribed($params['email'])) {
      $subs = new Subscriber($params['email'], "0", false, false);
     }
       
    return $APPLICANT_ID;
  }

  public function updateExistingApplicant($params, $applicant_id, $job_id, $public) {
    global $db;

    // get SM objects
    if (!empty($params['sm_links']["first"]) && $params['sm_links']["first"] != "-")
      $sm_link_1 = $params['sm_links']["first"]->linkToSave;
    else
      $sm_link_1 = '-';

    if (!empty($params['sm_links']["second"]) && $params['sm_links']["second"] != "-")
      $sm_link_2 = $params['sm_links']["second"]->linkToSave;
    else
      $sm_link_2 = '-';

    if (!empty($params['sm_links']["third"]) && $params['sm_links']["third"] != "-")
      $sm_link_3 = $params['sm_links']["third"]->linkToSave;
    else
      $sm_link_3 = '-';

    if (!empty($params['sm_links']["fourth"]) && $params['sm_links']["fourth"] != "-")
      $sm_link_4 = $params['sm_links']["fourth"]->linkToSave;
    else
      $sm_link_4 = '-';

    $sql = 'UPDATE applicant SET 
    fullname = "' . $params['name'] . '",
    f9_first_name = "' . $params['f9_first_name'] . '",
    f9_middle_name = "' . $params['f9_middle_name'] . '",
    f9_last_name = "' . $params['f9_last_name'] . '",
    f9_address_1 = "' . $params['f9_address_1'] . '",
    f9_address_2 = "' . $params['f9_address_2'] . '",
    f9_city = "' . $params['f9_city'] . '",
    f9_state = "' . $params['f9_state'] . '",
    f9_country = "' . $params['f9_country'] . '",
    f9_zip = "' . $params['f9_zip'] . '",
    f9_isProfile = "' . $params['f9_isProfile'] . '",
    f9_language = "' . $params['f9_language'] . '",
    f9_photo = "' . $params['f9_photo'] . '",
    f9_yrs_experience = "' . $params['f9_yrs_experience'] . '",
    f9_gender = "' . $params['f9_gender'] . '",
    f9_category = "' . $params['f9_category'] . '",
    f9_position = "' . $_POST['f9_position'] . '",
    phone_carrier = "' . $params['phone_carrier'] . '",
    birthdate = "' . $params['birthdate'] . '",
    span_bilingual = "' . $params['span_bilingual'] . '",
    pro_soft = "' . implode(",", $_POST['pro_soft']) . "," . $_POST['otherspec'] . '",
    workarea = "' . implode(",", $_POST['workarea']) . '",
    skills_prof = "' . implode(",", $_POST['skills_prof']) . '",
    cover_letter = "' . $params['cover_letter'] . '",
    x_ray = "' . $params['x_ray'] . '",
    coronal_polish = "' . $params['coronal_polish'] . '",
    laser = "' . $params['laser'] . '",
    anesthesia = "' . $params['anesthesia'] . '",
    efta = "' . $params['efta'] . '",
    hygiene_license = "' . $params['hygiene_license'] . '",
    dentist_license = "' . $params['dentist_license'] . '",
    w9 = "' . $params['w9'] . '",
    w4 = "' . $params['w4'] . '",
    a4 = "' . $params['a4'] . '",
    direct_deposit = "' . $params['direct_deposit'] . '",
    i9 = "' . $params['i9'] . '",
    tempday = "' . implode(",", $_POST['tempday']) . '",
    datefrom = "' . $params['datefrom'] . '",
    dateuntil = "' . $params['dateuntil'] . '",
    permaday = "' . implode(",", $_POST['permaday']) . '",
    wage_sel = "' . $params['wage_sel'] . '",
    miles_sel = "' . $params['miles_sel'] . '",
    occupation = "' . $params['occupation'] . '",
    phone = "' . $params['phone'] . '",
    message = "' . $params['message']. '",
    weblink = "' . $params['website'] . '",
    cv_path = "' . $params['cv_path'] . '",
    public_profile = ' . $params['public_profile'] . ',
    password = "' . $params['password'] . '",
    last_activity = NOW(),
    location = "' . $params['location'] . '",
    skills = "'. $params['skills'] . '",
    sm_link_1 = "' . $sm_link_1 . '", 
    sm_link_2 = "' . $sm_link_2 . '",
    sm_link_3 = "' . $sm_link_3 . '",
    sm_link_4 = "' . $sm_link_4 . '" WHERE id =' . $applicant_id;

    $result = $db->query($sql);
    $this->id = $db->getConnection()->insert_id;

    if (intval($public) == 1){

      if (intval($params['confirmed']) == 0) {
        // confirmation & subscription
        $hash = sha1($params['email'] . time());
        $sql = 'INSERT INTO confirmed_applicants (user_id, hash) VALUES (' . $applicant_id  .', "' . $hash . '")';
        $db->query($sql);
        $this->confirmHash = $hash;
      }
      
      //susbcribe those who wish to have public profile
      //which category is applicant interested in ?
      if ($job_id !== false) {
        $s = 'SELECT category_id FROM '.DB_PREFIX.'jobs WHERE id = ' . $job_id;
        $r = $db->query($s);
        $row = $r->fetch_assoc();
      } else {
        $row = array();
        $row['category_id'] = 0;
      }

      //also subscribe him 
      if (!$this->isSubscribed($params['email'])) {
       $subs = new Subscriber($params['email'], $row['category_id'], false, false);
      }
    }

  }

  public function createEntry($params, $job_id, $public) {
    global $db;

    // get SM objects
    if (!empty($params['sm_links']["first"]) && $params['sm_links']["first"] != "-")
      $sm_link_1 = $params['sm_links']["first"]->linkToSave;
    else
      $sm_link_1 = '-';

    if (!empty($params['sm_links']["second"]) && $params['sm_links']["second"] != "-")
      $sm_link_2 = $params['sm_links']["second"]->linkToSave;
    else
      $sm_link_2 = '-';

    if (!empty($params['sm_links']["third"]) && $params['sm_links']["third"] != "-")
      $sm_link_3 = $params['sm_links']["third"]->linkToSave;
    else
      $sm_link_3 = '-';

    if (!empty($params['sm_links']["fourth"]) && $params['sm_links']["fourth"] != "-")
      $sm_link_4 = $params['sm_links']["fourth"]->linkToSave;
    else
      $sm_link_4 = '-';

    //new applicant
    $sql = 'INSERT INTO '.DB_PREFIX.'applicant (
    id,
    fullname,
    f9_first_name,
    f9_middle_name,
    f9_last_name,
    f9_address_1,
    f9_address_2,
    f9_city,
    f9_state,
    f9_country,
    f9_zip, 
    f9_isProfile,
    f9_language,
    f9_photo,
    f9_yrs_experience,
    f9_gender,
    f9_category,
    f9_position,
    phone_carrier,
    birthdate,
    span_bilingual,
    pro_soft,
    workarea,
    skills_prof,
    cover_letter,
    x_ray,
    coronal_polish,
    laser,
    anesthesia,
    efta,
    hygiene_license,
    dentist_license,
    w9,
    w4,
    a4,
    direct_deposit,
    i9,
    tempday,
    datefrom,
    dateuntil,
    permaday,
    wage_sel,
    miles_sel,
    occupation,
    email,
    phone,
    message, 
    weblink, 
    cv_path, 
    public_profile,
    is_Seen,
    password, 
    last_activity,
    location, 
    skills, 
    confirmed,
    sm_link_1,
    sm_link_2, 
    sm_link_3,
    sm_link_4)
         VALUES (NULL, 
          "' . $params['name'] . '",
      "' . $params['f9_first_name'] . '",
      "' . $params['f9_middle_name'] . '",
      "' . $params['f9_last_name'] . '",
      "' . $params['f9_address_1'] . '",
      "' . $params['f9_address_2'] . '",
      "' . $params['f9_city'] . '",
      "' . $params['f9_state'] . '",
      "' . $params['f9_country'] . '",
      "' . $params['f9_zip'] . '",
      "' . $params['f9_isProfile'] . '",
      "' . $params['f9_language'] . '",
      "' . $params['f9_photo'] . '",
      "' . $params['f9_yrs_experience'] . '",
      "' . $params['f9_gender'] . '",
      "' . $params['f9_category'] . '",
      "' . implode(",", $_POST['f9_position']) . '",
      "' . $params['phone_carrier'] . '",
      "' . $params['birthdate'] . '",
      "' . $params['span_bilingual'] . '",
      "' . implode(",", $_POST['pro_soft']) . "," .$_POST['otherspec'] . '",
      "' . implode(",", $_POST['workarea']) . '",
      "' . implode(",", $_POST['skills_prof']) . '",
      "' . $params['cover_letter'] . '",
      "' . $params['x_ray'] . '",
      "' . $params['coronal_polish'] . '",
      "' . $params['laser'] . '",
      "' . $params['anesthesia'] . '",
      "' . $params['efta'] . '",
      "' . $params['hygiene_license'] . '",
      "' . $params['dentist_license'] . '",
      "' . $params['w9'] . '",
      "' . $params['w4'] . '",
      "' . $params['a4'] . '",
      "' . $params['direct_deposit'] . '",
      "' . $params['i9'] . '",
      "' . implode(",", $_POST['tempday']) . '",
      "' . $params['datefrom'] . '",
      "' . $params['dateuntil'] . '",
      "' . implode(",", $_POST['permaday']) . '",
      "' . $params['wage_sel'] . '",
      "' . $params['miles_sel'] . '",
          "' . $params['occupation'] . '",
          "' . $params['email'] . '",
          "' . $params['phone'] . '",
          "' . $params['message'] . '",
          "' . $params['website'] . '",
          "' . $params['cv_path'] . '",
          ' . $params['public_profile'] . ',
          ' . '0' . ',
          "' . $params['password'] . '", NOW(),
          "' . $params['location']  . '",
          "' . $params['skills']  . '", 
          ' . $params['confirmed'] . ',
          "' . $sm_link_1  . '",
          "' . $sm_link_2  . '",
          "' . $sm_link_3  . '",
          "' . $sm_link_4  . '")';

     $result = $db->query($sql);
     $APPLICANT_ID = $db->getConnection()->insert_id;
     $this->id = $APPLICANT_ID;

    if (intval($public) == 1){

       $hash = sha1($params['email'] . time());
       $sql = 'INSERT INTO confirmed_applicants (user_id, hash) VALUES (' . $APPLICANT_ID  .', "' . $hash . '")';
       $db->query($sql);
       $this->confirmHash = $hash;

      //susbcribe those who wish to have public profile
      //which category is applicant interested in ?
      $s = 'SELECT category_id FROM '.DB_PREFIX.'jobs WHERE id = ' . $job_id;
      $r = $db->query($s);
      $row = $r->fetch_assoc();

      //also subscribe him if his account does not exist yet
      if (!$this->isSubscribed($params['email'])) {
        $subs = new Subscriber($params['email'], $row['category_id'], false, false);
      }
        
    }

    return $APPLICANT_ID;
  }

  public function getApplicationsCount($occupation, $location, $skills) {
    global $db;

    $cond = '';

    if (!empty($occupation) && strlen($occupation) > 2) {
      $cond .= ' AND occupation like "%' . $occupation . '%" ';
    }

    if (!empty($location) && strlen($location) > 2) {
      $cond .= ' AND location like "%' . $location . '%" ';
    }

    if (!empty($skills) && strlen($skills) > 2) {
      $cond .= ' AND skills LIKE "%' . $skills . '%" ';
    }

    $sql = 'SELECT COUNT(id) as "total" FROM applicant WHERE public_profile = 1' . $cond;

    $result = $db->query($sql);
      $row = $result->fetch_assoc();

      return $row['total'];
  }
   
  public function getCvDatabase($offset, $theme, $occupation, $location, $skills) {
    global $db;

    $cond = '';

    if (!empty($occupation) && strlen($occupation) > 2) {
      $cond .= ' AND occupation like "%' . $occupation . '%" ';
    }

    if (!empty($location) && strlen($location) > 2) {
      $cond .= ' AND location LIKE "%' . $location . '%" ';
    }

    if (!empty($skills) && strlen($skills) > 2) {

      $exp = explode(",", $skills);
      $cond .= ' AND (';
      $last = count($exp);
      $i = 0;

      foreach ($exp as $skl) {
        if (++$i == $last)
          $cond .= ' skills LIKE "%' . $skl . '%"';
        else
          $cond .= ' skills LIKE "%' . $skl . '%" OR';
      }
      $cond .= ') ';
      //$cond .= ' AND skills LIKE "%' . $skills . '%" ';
    }
    
    $sql = 'SELECT 
    id,
    fullname,
    f9_first_name,
    f9_middle_name,
    f9_last_name,
    f9_address_1,
    f9_address_2,
    f9_city,
    f9_state,
    f9_country,
    f9_zip, 
    f9_isProfile,
    f9_language,
    f9_photo,
    f9_yrs_experience,
    f9_gender,
    f9_category,
    f9_position,
    phone_carrier,
    birthdate,
    span_bilingual,
    pro_soft,
    workarea,
    skills_prof,
    cover_letter,
    x_ray,
    coronal_polish,
    laser,
    anesthesia,
    efta,
    hygiene_license,
    dentist_license,
    w9,
    w4,
    a4,
    direct_deposit,
    i9,
    tempday,
    datefrom,
    dateuntil,
    permaday,
    wage_sel,
    miles_sel,
    occupation,
    email,
    phone,
    message,
    weblink, 
    cv_path,
    UNIX_TIMESTAMP(last_activity) as "created_on",
    location,
    skills, 
    sm_link_1,
    sm_link_2,
    sm_link_3,
    sm_link_4  FROM '.DB_PREFIX.'applicant WHERE public_profile = 1 ' . $cond . ' ORDER BY UNIX_TIMESTAMP(last_activity) DESC limit ' .$offset . ', ' . APPLICATIONS_PER_PAGE;

    $result = $db->query($sql);
    $final = array();

    while($row = $result->fetch_assoc()) {
      //social media links

      $sm_links = array();

      if (!empty($row['sm_link_1']) && $row['sm_link_1'] != "-") {
        $sm_links["first"] = deconstructSMlink($row['sm_link_1']);
      }

      if (!empty($row['sm_link_2']) && $row['sm_link_2'] != "-") {
        $sm_links["second"] = deconstructSMlink($row['sm_link_2']);
      }

      if (!empty($row['sm_link_3']) && $row['sm_link_3'] != "-") {
        $sm_links["third"] = deconstructSMlink($row['sm_link_3']);
      }

      if (!empty($row['sm_link_4']) && $row['sm_link_4'] != "-") {
        $sm_links["fourth"] = deconstructSMlink($row['sm_link_4']);
      }

      $row['sm_links'] = $sm_links;

      //date change
      $row['created_on'] = date(DATE_FORMAT, floatval(stripslashes($row['created_on'] )));

      if ($row['phone'] == '')
        $row['phone'] = '-';

      if ($row['location'] == '')
        $row['location'] = '-';

      if ($row['weblink'] == '')
        $row['weblink'] = '-';

      if (strlen($row['weblink']) > 30) {
        $row['weblink_short'] = substr($row['weblink'], 0, 30) . "...";
      } else {
        $row['weblink_short'] = $row['weblink'];
      }

      if ($row['cv_path'] == ''){
        $row['cv_path'] = '-';
      } else {
        $explode = explode(".", $row['cv_path']);
        if (strcmp(end($explode), "pdf") == 0) {
          $row['fa_class'] = "fa fa-file-pdf-o fa-lg pdf-el";
        } else {
          $row['fa_class'] = "fa fa-file-word-o fa-lg word-el";
        }
      }

      if (empty($row['skills']) || strlen($row['skills']) < 1) {
        $row['skills_formated'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['skills']);
        $skills_formated = '';
        foreach ($se as $skill) {
          $skills_formated .= "<span class=\"tag\">" . $skill . "</span>";
        }
        $row['skills_formated'] = $skills_formated; 
      }
      if (empty($row['pro_soft']) || strlen($row['pro_soft']) < 1) {
        $row['pro_soft'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['pro_soft']);
        $skills_formated = '';
        foreach ($se as $pro_soft) {
          $skills_formated .= "<span class=\"tag\">" . $pro_soft . "</span>";
        }
        $row['pro_soft'] = $skills_formated;  
      }
      if (empty($row['workarea']) || strlen($row['workarea']) < 1) {
        $row['workarea'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['workarea']);
        $skills_formated = '';
        foreach ($se as $workarea) {
          $skills_formated .= "<span class=\"tag\">" . $workarea . "</span>";
        }
        $row['workarea'] = $skills_formated;  
      }
      if (empty($row['skills_prof']) || strlen($row['skills_prof']) < 1) {
        $row['skills_prof'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['skills_prof']);
        $skills_formated = '';
        foreach ($se as $skills_prof) {
          $skills_formated .= "<span class=\"tag\">" . $skills_prof . "</span>";
        }
        $row['skills_prof'] = $skills_formated; 
      }
      if (empty($row['tempday']) || strlen($row['tempday']) < 1) {
        $row['tempday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['tempday']);
        $skills_formated = '';
        foreach ($se as $tempday) {
          $skills_formated .= "<span class=\"tag\">" . $tempday . "</span>";
        }
        $row['tempday'] = $skills_formated; 
      }
      if (empty($row['permaday']) || strlen($row['permaday']) < 1) {
        $row['permaday'] = "<span class=\"tag\">-</span>";
      } else {
        $se = explode(",", $row['permaday']);
        $skills_formated = '';
        foreach ($se as $permaday) {
          $skills_formated .= "<span class=\"tag\">" . $permaday . "</span>";
        }
        $row['permaday'] = $skills_formated;  
      }
      $final[] = $row;
    }
    return $final;
  }

  public function getApplicantsByPosition($position) { // sam
    global $db;
    $applicant = array();
    $sql = "SELECT id, fullname FROM `fninpor1_jobboard-dev`.applicant WHERE f9_position =" . $position;

    $result = $db->query($sql);
    while($row = $result->fetch_assoc()) {
      $applicant[][$row['id']] = $row['fullname'];
    }
    return $applicant;

  }

  public function company_applicants($id){ // applicants for a company
    global $db;
    $applicants = array();
    $sql = 'SELECT DISTINCT ja.applicant_id as applicant_id, a.fullname as fullname FROM `jobs` as j LEFT JOIN job_applications as ja ON j.id = ja.job_id LEFT JOIN applicant as a ON a.id = ja.applicant_id WHERE ja.applicant_id IS NOT NULL AND j.employer_id=' . $id . ' AND a.is_Banned = 1';
    $sql = 'SELECT DISTINCT
                ja.applicant_id AS applicant_id, a.fullname AS fullname
            FROM
                jobs AS j
                    LEFT JOIN
                job_applications AS ja ON j.id = ja.job_id
                    LEFT JOIN
                applicant AS a ON a.id = ja.applicant_id
            WHERE
                ja.applicant_id IS NOT NULL
                    AND j.employer_id = ' . $id .
                    ' AND a.is_Banned = 1';
    $result = $db->query($sql);
    while($row = $result->fetch_assoc()) {
      // $applicants[][$row['id']] = $row['fullname'];
      $applicants[] = $row['fullname'];
    }
    return $applicants;
  }

  public function update_banned_applicant($id) {
    global $db;
    $applicant_sql = "SELECT applicant_id FROM job_applications WHERE id='" . $id . "'";
    $applicant_result = $db->query($applicant_sql);
    $banned_applicant = $applicant_result->fetch_assoc();

    $check_banned_sql = "SELECT is_Banned FROM applicant WHERE id = " . $id;
    $check_banned_result = $db->query($check_banned_sql);
    $check_banned = $check_banned_result->fetch_assoc();

    if($check_banned['is_Banned'] == 0) {
      $sql= "UPDATE applicant SET is_Banned = 1 WHERE (id = " . $id . ")";
    }else{
      $sql= "UPDATE applicant SET is_Banned = 0 WHERE (id = " . $id . ")";
    }
    $result = $db->query($sql);
    return true;
  }

  public function authenticate($email, $mdpass) {
    global $db;
    $sql = 'SELECT * FROM '.DB_PREFIX.'applicant WHERE email="'.$email.'" AND password="'.$mdpass.'"';

    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if (!empty($row))
    {
      
      $this->id = $row['id']; 
      $this->fullname = $row['fullname']; 
      $this->f9_first_name = $row['f9_first_name']; 
      $this->f9_middle_name = $row['f9_middle_name']; 
      $this->f9_last_name = $row['f9_last_name']; 
      $this->f9_address_1 = $row['f9_address_1']; 
      $this->f9_address_2 = $row['f9_address_2']; 
      $this->f9_city = $row['f9_city']; 
      $this->f9_state = $row['f9_state']; 
      $this->f9_country = $row['f9_country']; 
      $this->f9_zip = $row['f9_zip']; 
      $this->f9_isProfile = $row['f9_isProfile']; 
      $this->f9_language = $row['f9_language']; 
      $this->f9_photo = $row['f9_photo']; 
      $this->f9_yrs_experience = $row['f9_yrs_experience']; 
      $this->f9_gender = $row['f9_gender']; 
      $this->f9_category = $row['f9_category']; 
      $this->f9_position = $row['f9_position']; 
      $this->phone_carrier = $row['phone_carrier']; 
      $this->birthdate = $row['birthdate']; 
      $this->span_bilingual = $row['span_bilingual']; 
      $this->pro_soft = $row['pro_soft']; 
      $this->workarea = $row['workarea']; 
      $this->skills_prof = $row['skills_prof']; 
      $this->cover_letter = $row['cover_letter']; 
      $this->x_ray = $row['x_ray']; 
      $this->coronal_polish = $row['coronal_polish']; 
      $this->laser = $row['laser']; 
      $this->anesthesia = $row['anesthesia']; 
      $this->efta = $row['efta']; 
      $this->hygiene_license = $row['hygiene_license']; 
      $this->dentist_license = $row['dentist_license']; 
      $this->w9 = $row['w9']; 
      $this->w4 = $row['w4']; 
      $this->a4 = $row['a4']; 
      $this->direct_deposit = $row['direct_deposit']; 
      $this->i9 = $row['i9']; 
      $this->tempday = $row['tempday']; 
      $this->datefrom = $row['datefrom']; 
      $this->dateuntil = $row['dateuntil']; 
      $this->permaday = $row['permaday']; 
      $this->wage_sel = $row['wage_sel']; 
      $this->miles_sel = $row['miles_sel']; 
      $this->email = $row['email']; 
      $this->phone = $row['phone'];
      $this->message = $row['message']; 
      $this->weblink = $row['weblink']; 
      $this->cv_path = $row['cv_path'];

      $sm_links = array();

      if (!empty($row['sm_link_1']) && $row['sm_link_1'] != "-") {
        $sm_links["first"] = deconstructSMlink($row['sm_link_1']);
      }

      if (!empty($row['sm_link_2']) && $row['sm_link_2'] != "-") {
        $sm_links["second"] = deconstructSMlink($row['sm_link_2']);
      }

      if (!empty($row['sm_link_3']) && $row['sm_link_3'] != "-") {
        $sm_links["third"] = deconstructSMlink($row['sm_link_3']);
      }

      if (!empty($row['sm_link_4']) && $row['sm_link_4'] != "-") {
        $sm_links["fourth"] = deconstructSMlink($row['sm_link_4']);
      }

      $this->sm_links = $sm_links;
      return true;
    }
    return false;
  }


} ?>
