<?php


function escape($what, $dontStrip=array())
{
	global $db;

	foreach ($what as $variable => $value)
	{
		if (is_string($value))
		{
			if (in_array($variable, $dontStrip))
			{
				$GLOBALS[$variable] = $db->getConnection()->real_escape_string($value);
			}
			else
			{
				$GLOBALS[$variable] = $db->getConnection()->real_escape_string(strip_tags($value));
			}
		}
		else
		{
			$GLOBALS[$variable] = $value;
		}
	}
}
?>
